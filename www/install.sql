#
# Tabellenstruktur f�r Tabelle `phpbb_auction_account`
#

CREATE TABLE `phpbb_auction_account` (
  `pk_auction_account_id` mediumint(8) NOT NULL auto_increment,
  `fk_auction_account_creditor_id` mediumint(8) NOT NULL default '0',
  `fk_auction_account_debitor_id` mediumint(8) NOT NULL default '0',
  `auction_account_auction_amount` decimal(15,2) NOT NULL default '0.00',
  `auction_account_amount_date` int(11) NOT NULL default '0',
  `auction_account_notified` mediumint(8) NOT NULL default '0',
  `auction_account_amount_paid` decimal(15,2) NOT NULL default '0.00',
  `auction_account_amount_paid_by` mediumint(8) NOT NULL default '0',
  `fk_auction_offer_id` mediumint(8) NOT NULL default '0',
  `auction_account_action` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`pk_auction_account_id`)
) TYPE=MyISAM AUTO_INCREMENT=32 ;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_bid`
#

CREATE TABLE `phpbb_auction_bid` (
  `PK_auction_bid_id` mediumint(8) unsigned NOT NULL auto_increment,
  `FK_auction_bid_offer_id` mediumint(8) unsigned NOT NULL default '0',
  `FK_auction_bid_user_id` mediumint(8) unsigned NOT NULL default '0',
  `auction_bid_time` int(11) NOT NULL default '0',
  `auction_bid_price` decimal(15,2) NOT NULL default '0.00',
  PRIMARY KEY  (`PK_auction_bid_id`)
) TYPE=MyISAM AUTO_INCREMENT=89 ;

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_bid_increase`
#

CREATE TABLE `phpbb_auction_bid_increase` (
  `PK_bid_increase` mediumint(8) unsigned NOT NULL auto_increment,
  `bid_increase` decimal(15,2) NOT NULL default '0.00',
  PRIMARY KEY  (`PK_bid_increase`)
) TYPE=MyISAM AUTO_INCREMENT=60 ;

#
# Daten f�r Tabelle `phpbb_auction_bid_increase`
#

INSERT INTO `phpbb_auction_bid_increase` VALUES (49, '0.10');
INSERT INTO `phpbb_auction_bid_increase` VALUES (50, '0.20');
INSERT INTO `phpbb_auction_bid_increase` VALUES (51, '0.50');
INSERT INTO `phpbb_auction_bid_increase` VALUES (52, '1.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (53, '2.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (54, '5.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (55, '10.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (56, '20.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (57, '30.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (58, '50.00');
INSERT INTO `phpbb_auction_bid_increase` VALUES (59, '100.00');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_category`
#

CREATE TABLE `phpbb_auction_category` (
  `PK_auction_category_id` mediumint(8) unsigned NOT NULL auto_increment,
  `auction_category_title` varchar(100) NOT NULL default '',
  `auction_category_icon` varchar(255) default NULL,
  `auction_category_order` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`PK_auction_category_id`)
) TYPE=MyISAM AUTO_INCREMENT=7 ;

#
# Daten f�r Tabelle `phpbb_auction_category`
#

INSERT INTO `phpbb_auction_category` VALUES (1, 'Test', '', 10);


# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_config`
#

CREATE TABLE `phpbb_auction_config` (
  `config_name` varchar(255) NOT NULL default '',
  `config_value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`config_name`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_auction_config`
#

INSERT INTO `phpbb_auction_config` VALUES ('currency', 'JPY');
INSERT INTO `phpbb_auction_config` VALUES ('auction_config_close_to_end_number', '7');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_close_to_end', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_ticker', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_auction_rooms', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_statistics', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_myauctions', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_calendar', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_search', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_amount_max', '10000');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_amount_min', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_activate_paypal', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_bold', '2');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_on_top', '5');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_special', '10');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_allow_bold', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_allow_on_top', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_allow_special', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_basic', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_config_last_bids_number', '5');
INSERT INTO `phpbb_auction_config` VALUES ('auction_end_notify_email', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_end_notify_pm', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_last_bids', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_paypal_email', '-');
INSERT INTO `phpbb_auction_config` VALUES ('auction_news_forum_id', '4');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_news', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_specials', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_priceinformation', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_allow_self_bids', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_allow_coupons', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_moneybooker_email', '-');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_activate_moneybooker', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_disable', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_disable', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_allow_shipping', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_version', '1.2 m');
INSERT INTO `phpbb_auction_config` VALUES ('auction_allow_direct_sell', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_direct_sell', '10');
INSERT INTO `phpbb_auction_config` VALUES ('auction_show_timeline', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_allow_comment', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_allow_change_comment', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_email_notify', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_pm_notify', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_drop_down_auction_rooms', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_specials_limit', '2');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_activate_user_points', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_pseudo_cron', '0');
INSERT INTO `phpbb_auction_config` VALUES ('auction_pseudo_cron_frequence', 'm');
INSERT INTO `phpbb_auction_config` VALUES ('auction_pseudo_cron_last', '1103460607');
INSERT INTO `phpbb_auction_config` VALUES ('auction_room_pagination', '2');
INSERT INTO `phpbb_auction_config` VALUES ('auction_block_display_newest_offers', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_config_newest_offers_number', '10');
INSERT INTO `phpbb_auction_config` VALUES ('auction_paymentsystem_activate_debit', '1');
INSERT INTO `phpbb_auction_config` VALUES ('auction_offer_cost_final_percent', '5');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_coupon`
#

CREATE TABLE `phpbb_auction_coupon` (
  `PK_auction_coupon_id` varchar(200) NOT NULL default '0',
  `FK_auction_coupon_config_id` mediumint(8) NOT NULL default '0',
  `FK_auction_coupon_created_user_id` mediumint(8) NOT NULL default '0',
  `auction_coupon_date_created` int(11) NOT NULL default '0',
  `auction_coupon_date_used` int(11) NOT NULL default '0',
  `FK_auction_coupon_used_user_id` mediumint(8) NOT NULL default '0',
  PRIMARY KEY  (`PK_auction_coupon_id`)
) TYPE=MyISAM;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_coupon_config`
#

CREATE TABLE `phpbb_auction_coupon_config` (
  `PK_auction_coupon_config_id` mediumint(8) NOT NULL auto_increment,
  `auction_coupon_config_name` varchar(25) NOT NULL default '',
  `auction_coupon_config_amount` decimal(3,2) NOT NULL default '0.00',
  PRIMARY KEY  (`PK_auction_coupon_config_id`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

#
# Daten f�r Tabelle `phpbb_auction_coupon_config`
#

INSERT INTO `phpbb_auction_coupon_config` VALUES (1, '100% off', '1.00');
INSERT INTO `phpbb_auction_coupon_config` VALUES (2, '50% off', '0.50');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_image`
#

CREATE TABLE `phpbb_auction_image` (
  `pic_id` int(11) unsigned NOT NULL auto_increment,
  `pic_filename` varchar(255) NOT NULL default '',
  `pic_auction_id` int(11) NOT NULL default '0',
  `pic_user_id` mediumint(8) default NULL,
  `pic_time` int(11) NOT NULL default '0',
  `pic_cat` mediumint(8) NOT NULL default '0',
  `pic_room` mediumint(8) NOT NULL default '0',
  `pic_lock` tinyint(1) NOT NULL default '0',
  `pic_approval` tinyint(1) NOT NULL default '0',
  `pic_main` tinyint(1) NOT NULL default '1',
  `pic_width` mediumint(5) default NULL,
  `pic_height` mediumint(5) default NULL,
  `pic_main_width` mediumint(5) default NULL,
  `pic_main_height` mediumint(5) default NULL,
  `pic_thumb_width` mediumint(5) default NULL,
  `pic_thumb_height` mediumint(5) default NULL,
  `pic_comment` text,
  `pic_user_ip` varchar(8) NOT NULL default '',
  `pic_crop_id` tinyint(1) default NULL,
  `pic_gd_type` tinyint(1) NOT NULL default '0',
  `crop_id` tinyint(1) default NULL,
  PRIMARY KEY  (`pic_id`),
  KEY `pic_auction_id` (`pic_auction_id`),
  KEY `pic_cat` (`pic_cat`),
  KEY `pic_room` (`pic_room`),
  KEY `pic_time` (`pic_time`)
) TYPE=MyISAM AUTO_INCREMENT=14 ;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_image_config`
#

CREATE TABLE `phpbb_auction_image_config` (
  `config_name` varchar(255) NOT NULL default '0',
  `config_value` varchar(255) NOT NULL default '0',
  PRIMARY KEY  (`config_name`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_auction_image_config`
#

INSERT INTO `phpbb_auction_image_config` VALUES ('gd_version', '3');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_pictures_allow', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('allow_url_upload', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_picture_jpeg_allow', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_picture_png_allow', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('png_convert', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_picture_gif_allow', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('gif_convert', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_picture_size_allow', '100000');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_server_picture_size', '1024000');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_thumbnail_cache', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_pic_max_width', '800');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_pic_max_height', '600');
INSERT INTO `phpbb_auction_image_config` VALUES ('offer_auction_pic_quality', '91');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_main_size', '250');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_main_quality', '80');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_border', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_border_color', 'ABABAB');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_border_width', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_sharpen', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_bw', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_js_bw', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_mini_size', '40');
INSERT INTO `phpbb_auction_image_config` VALUES ('mini_pic_border', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('mini_pic_border_color', '000000');
INSERT INTO `phpbb_auction_image_config` VALUES ('mini_pic_border_width', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('mini_pic_sharpen', '2');
INSERT INTO `phpbb_auction_image_config` VALUES ('mini_pic_bw', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('allow_thumb_gallery', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('amount_of_thumbs', '6');
INSERT INTO `phpbb_auction_image_config` VALUES ('amount_of_thumb_per_line', '3');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_type', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_thumb_size', '80');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_thumb_quality', '80');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_border', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_border_color', '000000');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_border_width', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_sharpen', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_bw', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('thumb_pic_js_bw', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_hotlink_prevent', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_hotlink_allowed', '');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_pic_approval_admin', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_pic_approval_mod', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_use_water', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_for_all_water', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_pic_for_guest_water', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_water_img_qual', '88');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_watermarkpos', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('main_water_img_trans', '80');
INSERT INTO `phpbb_auction_image_config` VALUES ('big_pic_use_water', '1');
INSERT INTO `phpbb_auction_image_config` VALUES ('big_pic_for_guest_water', '0');
INSERT INTO `phpbb_auction_image_config` VALUES ('big_water_img_qual', '88');
INSERT INTO `phpbb_auction_image_config` VALUES ('big_watermarkpos', '4');
INSERT INTO `phpbb_auction_image_config` VALUES ('big_water_img_trans', '80');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_offer_mini_quality', '90');
INSERT INTO `phpbb_auction_image_config` VALUES ('edit_level', '2');
INSERT INTO `phpbb_auction_image_config` VALUES ('auction_gif_max_size', '180000');
INSERT INTO `phpbb_auction_image_config` VALUES ('first_run', '0');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_ipn_log`
#

CREATE TABLE `phpbb_auction_ipn_log` (
  `PK_auction_ipn_log_id` mediumint(8) NOT NULL auto_increment,
  `auction_ipn_log_date` int(11) NOT NULL default '0',
  `auction_ipn_log_status` varchar(200) NOT NULL default '0',
  `FK_auction_offer_id` mediumint(8) NOT NULL default '0',
  PRIMARY KEY  (`PK_auction_ipn_log_id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

#
# Daten f�r Tabelle `phpbb_auction_ipn_log`
#


# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_offer`
#

CREATE TABLE `phpbb_auction_offer` (
  `PK_auction_offer_id` mediumint(8) unsigned NOT NULL auto_increment,
  `FK_auction_offer_user_id` mediumint(8) unsigned NOT NULL default '0',
  `FK_auction_offer_room_id` mediumint(8) unsigned NOT NULL default '0',
  `auction_offer_title` varchar(100) NOT NULL default '',
  `auction_offer_text` text,
  `auction_offer_time_start` int(11) NOT NULL default '0',
  `auction_offer_time_stop` int(11) NOT NULL default '0',
  `auction_offer_state` int(1) NOT NULL default '0',
  `auction_offer_price_start` decimal(15,2) NOT NULL default '0.00',
  `auction_offer_views` int(11) NOT NULL default '0',
  `auction_offer_picture` varchar(200) NOT NULL default '',
  `auction_offer_special` tinyint(1) NOT NULL default '0',
  `auction_offer_bold` tinyint(1) NOT NULL default '0',
  `auction_offer_on_top` tinyint(1) NOT NULL default '0',
  `auction_offer_paid` tinyint(1) NOT NULL default '0',
  `auction_offer_sell_on_first` tinyint(1) NOT NULL default '0',
  `auction_offer_shipping_price` decimal(15,2) NOT NULL default '0.00',
  `auction_offer_shipping_location` mediumint(8) NOT NULL default '0',
  `auction_offer_last_bid_price` decimal(15,2) NOT NULL default '0.00',
  `FK_auction_offer_last_bid_user_id` mediumint(8) NOT NULL default '0',
  `auction_offer_direct_sell_price` decimal(15,2) NOT NULL default '0.00',
  `auction_offer_comment` varchar(255) NOT NULL default '',
  `auction_offer_comment_time` int(11) NOT NULL default '0',
  `auction_offer_notified_buyer` tinyint(1) NOT NULL default '0',
  `auction_offer_notified_seller` tinyint(1) NOT NULL default '0',
  `auction_offer_bid_increase` decimal(15,2) NOT NULL default '0.00',
  `auction_offer_accepted_payments` varchar(100) NOT NULL default '',
  `auction_offer_sellers_location` varchar(100) NOT NULL default '',
  `auction_offer_percentage_charged` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`PK_auction_offer_id`)
) TYPE=MyISAM AUTO_INCREMENT=68 ;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_rating`
#

CREATE TABLE `phpbb_auction_rating` (
  `PK_auction_rating_id` mediumint(8) NOT NULL default '0',
  `auction_rating_title` varchar(25) NOT NULL default '0',
  `auction_rating_order` mediumint(8) NOT NULL default '0',
  `auction_rating_icon` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`PK_auction_rating_id`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_auction_rating`
#

INSERT INTO `phpbb_auction_rating` VALUES (1, 'Super', 0, '1.gif');
INSERT INTO `phpbb_auction_rating` VALUES (2, 'Good', 0, '2.gif');
INSERT INTO `phpbb_auction_rating` VALUES (3, 'Fair', 0, '3.gif');
INSERT INTO `phpbb_auction_rating` VALUES (4, 'Poor', 0, '4.gif');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_role`
#

CREATE TABLE `phpbb_auction_role` (
  `PK_auction_role_id` mediumint(5) NOT NULL default '0',
  `auction_role_title` varchar(25) NOT NULL default '',
  `view_all` tinyint(1) NOT NULL default '0',
  `view_offer` tinyint(1) NOT NULL default '0',
  `view_bid_history` tinyint(1) NOT NULL default '0',
  `new` tinyint(1) NOT NULL default '0',
  `bid` tinyint(1) NOT NULL default '0',
  `direct_sell` tinyint(1) NOT NULL default '0',
  `image_upload` tinyint(1) NOT NULL default '0',
  `comment` tinyint(1) NOT NULL default '0',
  `move` tinyint(1) NOT NULL default '0',
  `delete_offer` tinyint(1) NOT NULL default '0',
  `delete_bid` tinyint(1) NOT NULL default '0',
  `special` tinyint(1) NOT NULL default '0'
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_auction_role`
#

INSERT INTO `phpbb_auction_role` VALUES (5, 'administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `phpbb_auction_role` VALUES (4, 'moderator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO `phpbb_auction_role` VALUES (1, 'guest', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `phpbb_auction_role` VALUES (2, 'registered', 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO `phpbb_auction_role` VALUES (3, 'auctioneer', 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1);

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_room`
#

CREATE TABLE `phpbb_auction_room` (
  `PK_auction_room_id` mediumint(8) unsigned NOT NULL auto_increment,
  `FK_auction_room_category_id` mediumint(8) unsigned NOT NULL default '0',
  `auction_room_order` mediumint(8) unsigned NOT NULL default '0',
  `auction_room_title` varchar(100) NOT NULL default '',
  `auction_room_description` text,
  `auction_room_state` mediumint(8) unsigned NOT NULL default '0',
  `auction_room_count_view` mediumint(8) unsigned NOT NULL default '0',
  `auction_room_icon` varchar(255) default NULL,
  PRIMARY KEY  (`PK_auction_room_id`)
) TYPE=MyISAM AUTO_INCREMENT=9 ;

#
# Daten f�r Tabelle `phpbb_auction_room`
#

INSERT INTO `phpbb_auction_room` VALUES (1, 1, 10, 'Test-Room', '', 0, 0, '');


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_room_prune`
#

CREATE TABLE `phpbb_auction_room_prune` (
  `PK_prune_id` mediumint(8) unsigned NOT NULL auto_increment,
  `FK_auction_room_id` smallint(5) unsigned NOT NULL default '0',
  `prune_days` smallint(5) unsigned NOT NULL default '0',
  `prune_frequency` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`PK_prune_id`),
  KEY `room_id` (`FK_auction_room_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

#
# Daten f�r Tabelle `phpbb_auction_room_prune`
#


# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_store`
#

CREATE TABLE `phpbb_auction_store` (
  `pk_auction_store_id` mediumint(8) NOT NULL auto_increment,
  `fk_user_id` mediumint(8) NOT NULL default '0',
  `store_name` varchar(255) NOT NULL default '',
  `store_description` text NOT NULL,
  `store_header` text NOT NULL,
  `show_block_drop_down` tinyint(1) NOT NULL default '1',
  `show_block_rooms` tinyint(1) NOT NULL default '1',
  `show_block_search` tinyint(1) NOT NULL default '1',
  `show_block_statistics` tinyint(1) NOT NULL default '1',
  `show_block_myauction` tinyint(1) NOT NULL default '1',
  `show_block_specials` tinyint(1) NOT NULL default '1',
  `show_block_priceinfo` tinyint(1) NOT NULL default '1',
  `show_block_calendar` tinyint(1) NOT NULL default '1',
  `show_block_closetoend` tinyint(1) NOT NULL default '1',
  `show_block_ticker` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`pk_auction_store_id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_user_rating`
#

CREATE TABLE `phpbb_auction_user_rating` (
  `FK_auction_offer_id` mediumint(8) unsigned NOT NULL default '0',
  `FK_auction_offer_seller_id` mediumint(8) unsigned NOT NULL default '0',
  `FK_auction_offer_buyer_id` mediumint(8) NOT NULL default '0',
  `auction_offer_buyer_rating_text` varchar(255) NOT NULL default '',
  `auction_offer_seller_rating_text` varchar(255) NOT NULL default '',
  `FK_auction_offer_buyer_rating_id` mediumint(8) NOT NULL default '0',
  `FK_auction_offer_seller_rating_id` mediumint(8) NOT NULL default '0',
  `auction_user_seller_rating_time` int(11) NOT NULL default '0',
  `auction_user_buyer_rating_time` int(11) NOT NULL default '0',
  PRIMARY KEY  (`FK_auction_offer_id`)
) TYPE=MyISAM;


#
# Tabellenstruktur f�r Tabelle `phpbb_auction_user_role`
#

CREATE TABLE `phpbb_auction_user_role` (
  `FK_user_id` mediumint(8) NOT NULL default '0',
  `FK_auction_role` mediumint(8) NOT NULL default '0',
  `auction_role_time_start` tinyint(11) NOT NULL default '0'
) TYPE=MyISAM;

#
# Tabellenstruktur f�r Tabelle `phpbb_auction_watchlist`
#

CREATE TABLE `phpbb_auction_watchlist` (
  `FK_auction_offer_id` mediumint(8) NOT NULL default '0',
  `FK_auction_user_id` mediumint(8) NOT NULL default '0',
  `auction_watchlist_time` int(11) NOT NULL default '0'
) TYPE=MyISAM;


#
# Tabellenstruktur f�r Tabelle `phpbb_config`
#

CREATE TABLE `phpbb_config` (
  `config_name` varchar(255) NOT NULL default '',
  `config_value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`config_name`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_config`
#

INSERT INTO `phpbb_config` VALUES ('config_id', '1');
INSERT INTO `phpbb_config` VALUES ('board_disable', '0');
INSERT INTO `phpbb_config` VALUES ('sitename', 'phpbbauction.com');
INSERT INTO `phpbb_config` VALUES ('site_desc', 'Auction-Test-Board');
INSERT INTO `phpbb_config` VALUES ('cookie_name', 'phpbb2mysql');
INSERT INTO `phpbb_config` VALUES ('cookie_path', '/');
INSERT INTO `phpbb_config` VALUES ('cookie_domain', '');
INSERT INTO `phpbb_config` VALUES ('cookie_secure', '0');
INSERT INTO `phpbb_config` VALUES ('session_length', '3600');
INSERT INTO `phpbb_config` VALUES ('allow_html', '1');
INSERT INTO `phpbb_config` VALUES ('allow_html_tags', 'b,i,u,pre,a');
INSERT INTO `phpbb_config` VALUES ('allow_bbcode', '1');
INSERT INTO `phpbb_config` VALUES ('allow_smilies', '1');
INSERT INTO `phpbb_config` VALUES ('allow_sig', '1');
INSERT INTO `phpbb_config` VALUES ('allow_namechange', '0');
INSERT INTO `phpbb_config` VALUES ('allow_theme_create', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_local', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_remote', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_upload', '0');
INSERT INTO `phpbb_config` VALUES ('enable_confirm', '0');
INSERT INTO `phpbb_config` VALUES ('override_user_style', '0');
INSERT INTO `phpbb_config` VALUES ('posts_per_page', '15');
INSERT INTO `phpbb_config` VALUES ('topics_per_page', '50');
INSERT INTO `phpbb_config` VALUES ('hot_threshold', '25');
INSERT INTO `phpbb_config` VALUES ('max_poll_options', '10');
INSERT INTO `phpbb_config` VALUES ('max_sig_chars', '255');
INSERT INTO `phpbb_config` VALUES ('max_inbox_privmsgs', '50');
INSERT INTO `phpbb_config` VALUES ('max_sentbox_privmsgs', '25');
INSERT INTO `phpbb_config` VALUES ('max_savebox_privmsgs', '50');
INSERT INTO `phpbb_config` VALUES ('board_email_sig', 'Thanks, The Management');
INSERT INTO `phpbb_config` VALUES ('board_email', 'test@test.de');
INSERT INTO `phpbb_config` VALUES ('smtp_delivery', '0');
INSERT INTO `phpbb_config` VALUES ('smtp_host', '');
INSERT INTO `phpbb_config` VALUES ('smtp_username', '');
INSERT INTO `phpbb_config` VALUES ('smtp_password', '');
INSERT INTO `phpbb_config` VALUES ('sendmail_fix', '0');
INSERT INTO `phpbb_config` VALUES ('require_activation', '0');
INSERT INTO `phpbb_config` VALUES ('flood_interval', '15');
INSERT INTO `phpbb_config` VALUES ('board_email_form', '0');
INSERT INTO `phpbb_config` VALUES ('avatar_filesize', '6144');
INSERT INTO `phpbb_config` VALUES ('avatar_max_width', '80');
INSERT INTO `phpbb_config` VALUES ('avatar_max_height', '80');
INSERT INTO `phpbb_config` VALUES ('avatar_path', 'images/avatars');
INSERT INTO `phpbb_config` VALUES ('avatar_gallery_path', 'images/avatars/gallery');
INSERT INTO `phpbb_config` VALUES ('smilies_path', 'images/smiles');
INSERT INTO `phpbb_config` VALUES ('default_style', '1');
INSERT INTO `phpbb_config` VALUES ('default_dateformat', 'D M d, Y g:i a');
INSERT INTO `phpbb_config` VALUES ('board_timezone', '0');
INSERT INTO `phpbb_config` VALUES ('prune_enable', '1');
INSERT INTO `phpbb_config` VALUES ('privmsg_disable', '0');
INSERT INTO `phpbb_config` VALUES ('gzip_compress', '1');
INSERT INTO `phpbb_config` VALUES ('coppa_fax', '');
INSERT INTO `phpbb_config` VALUES ('coppa_mail', '');
INSERT INTO `phpbb_config` VALUES ('record_online_users', '2');
INSERT INTO `phpbb_config` VALUES ('record_online_date', '1076611393');
INSERT INTO `phpbb_config` VALUES ('server_name', 'localhost');
INSERT INTO `phpbb_config` VALUES ('server_port', '80');
INSERT INTO `phpbb_config` VALUES ('script_path', '/phpBB2/');
INSERT INTO `phpbb_config` VALUES ('version', '.0.6');
INSERT INTO `phpbb_config` VALUES ('board_startdate', '1075129912');
INSERT INTO `phpbb_config` VALUES ('default_lang', 'english');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_confirm`
#

CREATE TABLE `phpbb_confirm` (
  `confirm_id` char(32) NOT NULL default '',
  `session_id` char(32) NOT NULL default '',
  `code` char(6) NOT NULL default '',
  PRIMARY KEY  (`session_id`,`confirm_id`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_confirm`
#


# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_privmsgs`
#

CREATE TABLE `phpbb_privmsgs` (
  `privmsgs_id` mediumint(8) unsigned NOT NULL auto_increment,
  `privmsgs_type` tinyint(4) NOT NULL default '0',
  `privmsgs_subject` varchar(255) NOT NULL default '0',
  `privmsgs_from_userid` mediumint(8) NOT NULL default '0',
  `privmsgs_to_userid` mediumint(8) NOT NULL default '0',
  `privmsgs_date` int(11) NOT NULL default '0',
  `privmsgs_ip` varchar(8) NOT NULL default '',
  `privmsgs_enable_bbcode` tinyint(1) NOT NULL default '1',
  `privmsgs_enable_html` tinyint(1) NOT NULL default '0',
  `privmsgs_enable_smilies` tinyint(1) NOT NULL default '1',
  `privmsgs_attach_sig` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`privmsgs_id`),
  KEY `privmsgs_from_userid` (`privmsgs_from_userid`),
  KEY `privmsgs_to_userid` (`privmsgs_to_userid`)
) TYPE=MyISAM AUTO_INCREMENT=591 ;

#
# Tabellenstruktur f�r Tabelle `phpbb_privmsgs_text`
#

CREATE TABLE `phpbb_privmsgs_text` (
  `privmsgs_text_id` mediumint(8) unsigned NOT NULL default '0',
  `privmsgs_bbcode_uid` varchar(10) NOT NULL default '0',
  `privmsgs_text` text,
  PRIMARY KEY  (`privmsgs_text_id`)
) TYPE=MyISAM;


#
# Tabellenstruktur f�r Tabelle `phpbb_sessions`
#

CREATE TABLE `phpbb_sessions` (
  `session_id` char(32) NOT NULL default '',
  `session_user_id` mediumint(8) NOT NULL default '0',
  `session_start` int(11) NOT NULL default '0',
  `session_time` int(11) NOT NULL default '0',
  `session_ip` char(8) NOT NULL default '0',
  `session_page` int(11) NOT NULL default '0',
  `session_logged_in` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `session_user_id` (`session_user_id`),
  KEY `session_id_ip_user_id` (`session_id`,`session_ip`,`session_user_id`)
) TYPE=MyISAM;

#
# Tabellenstruktur f�r Tabelle `phpbb_themes`
#

CREATE TABLE `phpbb_themes` (
  `themes_id` mediumint(8) unsigned NOT NULL auto_increment,
  `template_name` varchar(30) NOT NULL default '',
  `style_name` varchar(30) NOT NULL default '',
  `head_stylesheet` varchar(100) default NULL,
  `body_background` varchar(100) default NULL,
  `body_bgcolor` varchar(6) default NULL,
  `body_text` varchar(6) default NULL,
  `body_link` varchar(6) default NULL,
  `body_vlink` varchar(6) default NULL,
  `body_alink` varchar(6) default NULL,
  `body_hlink` varchar(6) default NULL,
  `tr_color1` varchar(6) default NULL,
  `tr_color2` varchar(6) default NULL,
  `tr_color3` varchar(6) default NULL,
  `tr_class1` varchar(25) default NULL,
  `tr_class2` varchar(25) default NULL,
  `tr_class3` varchar(25) default NULL,
  `th_color1` varchar(6) default NULL,
  `th_color2` varchar(6) default NULL,
  `th_color3` varchar(6) default NULL,
  `th_class1` varchar(25) default NULL,
  `th_class2` varchar(25) default NULL,
  `th_class3` varchar(25) default NULL,
  `td_color1` varchar(6) default NULL,
  `td_color2` varchar(6) default NULL,
  `td_color3` varchar(6) default NULL,
  `td_class1` varchar(25) default NULL,
  `td_class2` varchar(25) default NULL,
  `td_class3` varchar(25) default NULL,
  `fontface1` varchar(50) default NULL,
  `fontface2` varchar(50) default NULL,
  `fontface3` varchar(50) default NULL,
  `fontsize1` tinyint(4) default NULL,
  `fontsize2` tinyint(4) default NULL,
  `fontsize3` tinyint(4) default NULL,
  `fontcolor1` varchar(6) default NULL,
  `fontcolor2` varchar(6) default NULL,
  `fontcolor3` varchar(6) default NULL,
  `span_class1` varchar(25) default NULL,
  `span_class2` varchar(25) default NULL,
  `span_class3` varchar(25) default NULL,
  `img_size_poll` smallint(5) unsigned default NULL,
  `img_size_privmsg` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`themes_id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

#
# Daten f�r Tabelle `phpbb_themes`
#

INSERT INTO `phpbb_themes` VALUES (1, 'subSilver', 'subSilver', 'subSilver.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, \'Courier New\', sans-serif', 10, 11, 12, '444444', '006600', 'FFA34F', '', '', '', NULL, NULL);

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_themes_name`
#

CREATE TABLE `phpbb_themes_name` (
  `themes_id` smallint(5) unsigned NOT NULL default '0',
  `tr_color1_name` char(50) default NULL,
  `tr_color2_name` char(50) default NULL,
  `tr_color3_name` char(50) default NULL,
  `tr_class1_name` char(50) default NULL,
  `tr_class2_name` char(50) default NULL,
  `tr_class3_name` char(50) default NULL,
  `th_color1_name` char(50) default NULL,
  `th_color2_name` char(50) default NULL,
  `th_color3_name` char(50) default NULL,
  `th_class1_name` char(50) default NULL,
  `th_class2_name` char(50) default NULL,
  `th_class3_name` char(50) default NULL,
  `td_color1_name` char(50) default NULL,
  `td_color2_name` char(50) default NULL,
  `td_color3_name` char(50) default NULL,
  `td_class1_name` char(50) default NULL,
  `td_class2_name` char(50) default NULL,
  `td_class3_name` char(50) default NULL,
  `fontface1_name` char(50) default NULL,
  `fontface2_name` char(50) default NULL,
  `fontface3_name` char(50) default NULL,
  `fontsize1_name` char(50) default NULL,
  `fontsize2_name` char(50) default NULL,
  `fontsize3_name` char(50) default NULL,
  `fontcolor1_name` char(50) default NULL,
  `fontcolor2_name` char(50) default NULL,
  `fontcolor3_name` char(50) default NULL,
  `span_class1_name` char(50) default NULL,
  `span_class2_name` char(50) default NULL,
  `span_class3_name` char(50) default NULL,
  PRIMARY KEY  (`themes_id`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_themes_name`
#

INSERT INTO `phpbb_themes_name` VALUES (1, 'The lightest row colour', 'The medium row color', 'The darkest row colour', '', '', '', 'Border round the whole page', 'Outer table border', 'Inner table border', 'Silver gradient picture', 'Blue gradient picture', 'Fade-out gradient on index', 'Background for quote boxes', 'All white areas', '', 'Background for topic posts', '2nd background for topic posts', '', 'Main fonts', 'Additional topic title font', 'Form fonts', 'Smallest font size', 'Medium font size', 'Normal font size (post body etc)', 'Quote & copyright text', 'Code text colour', 'Main table header text colour', '', '', '');

# --------------------------------------------------------

#
# Tabellenstruktur f�r Tabelle `phpbb_users`
#

CREATE TABLE `phpbb_users` (
  `user_id` mediumint(8) NOT NULL default '0',
  `user_active` tinyint(1) default '1',
  `username` varchar(25) NOT NULL default '',
  `user_password` varchar(32) NOT NULL default '',
  `user_session_time` int(11) NOT NULL default '0',
  `user_session_page` smallint(5) NOT NULL default '0',
  `user_lastvisit` int(11) NOT NULL default '0',
  `user_regdate` int(11) NOT NULL default '0',
  `user_level` tinyint(4) default '0',
  `user_posts` mediumint(8) unsigned NOT NULL default '0',
  `user_timezone` decimal(5,2) NOT NULL default '0.00',
  `user_style` tinyint(4) default NULL,
  `user_lang` varchar(255) default NULL,
  `user_dateformat` varchar(14) NOT NULL default 'd M Y H:i',
  `user_new_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_unread_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_last_privmsg` int(11) NOT NULL default '0',
  `user_emailtime` int(11) default NULL,
  `user_viewemail` tinyint(1) default NULL,
  `user_attachsig` tinyint(1) default NULL,
  `user_allowhtml` tinyint(1) default '1',
  `user_allowbbcode` tinyint(1) default '1',
  `user_allowsmile` tinyint(1) default '1',
  `user_allowavatar` tinyint(1) NOT NULL default '1',
  `user_allow_pm` tinyint(1) NOT NULL default '1',
  `user_allow_viewonline` tinyint(1) NOT NULL default '1',
  `user_notify` tinyint(1) NOT NULL default '1',
  `user_notify_pm` tinyint(1) NOT NULL default '0',
  `user_popup_pm` tinyint(1) NOT NULL default '0',
  `user_rank` int(11) default '0',
  `user_avatar` varchar(100) default NULL,
  `user_avatar_type` tinyint(4) NOT NULL default '0',
  `user_email` varchar(255) default NULL,
  `user_icq` varchar(15) default NULL,
  `user_website` varchar(100) default NULL,
  `user_from` varchar(100) default NULL,
  `user_sig` text,
  `user_sig_bbcode_uid` varchar(10) default NULL,
  `user_aim` varchar(255) default NULL,
  `user_yim` varchar(255) default NULL,
  `user_msnm` varchar(255) default NULL,
  `user_occ` varchar(100) default NULL,
  `user_interests` varchar(255) default NULL,
  `user_actkey` varchar(32) default NULL,
  `user_newpasswd` varchar(32) default NULL,
  `user_points` mediumint(8) NOT NULL default '0',
  PRIMARY KEY  (`user_id`),
  KEY `user_session_time` (`user_session_time`)
) TYPE=MyISAM;

#
# Daten f�r Tabelle `phpbb_users`
#

INSERT INTO `phpbb_users` VALUES (-1, 0, 'Anonymous', '', 0, 0, 0, 1075129912, 0, 0, '0.00', NULL, '', '', 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, NULL, '', 0, '', '', '', '', '', NULL, '', '', '', '', '', '', '', 0);
INSERT INTO `phpbb_users` VALUES (2, 1, 'Admin', 'e10adc3949ba59abbe56e057f20f883e', 1109349548, 441, 1109348214, 1075129912, 1, 9, '0.00', 1, 'english', 'd M Y h:i a', 0, 977, 1103378212, NULL, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, '', 0, 'email', '-', '', '-', '', '', '-', '-', 'email', '-', '', '', '', 99732);

CREATE TABLE `phpbb_smilies` (
  `smilies_id` smallint(5) unsigned NOT NULL auto_increment,
  `code` varchar(50) default NULL,
  `smile_url` varchar(100) default NULL,
  `emoticon` varchar(75) default NULL,
  PRIMARY KEY  (`smilies_id`)
) TYPE=MyISAM AUTO_INCREMENT=43 ;

INSERT INTO `phpbb_smilies` VALUES (1, ':D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (2, ':-D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (3, ':grin:', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (4, ':)', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (5, ':-)', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (6, ':smile:', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (7, ':(', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (8, ':-(', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (9, ':sad:', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (10, ':o', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (11, ':-o', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (12, ':eek:', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (13, ':shock:', 'icon_eek.gif', 'Shocked');
INSERT INTO `phpbb_smilies` VALUES (14, ':?', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (15, ':-?', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (16, ':???:', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (17, '8)', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (18, '8-)', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (19, ':cool:', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (20, ':lol:', 'icon_lol.gif', 'Laughing');
INSERT INTO `phpbb_smilies` VALUES (21, ':x', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (22, ':-x', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (23, ':mad:', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (24, ':P', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (25, ':-P', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (26, ':razz:', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (27, ':oops:', 'icon_redface.gif', 'Embarassed');
INSERT INTO `phpbb_smilies` VALUES (28, ':cry:', 'icon_cry.gif', 'Crying or Very sad');
INSERT INTO `phpbb_smilies` VALUES (29, ':evil:', 'icon_evil.gif', 'Evil or Very Mad');
INSERT INTO `phpbb_smilies` VALUES (30, ':twisted:', 'icon_twisted.gif', 'Twisted Evil');
INSERT INTO `phpbb_smilies` VALUES (31, ':roll:', 'icon_rolleyes.gif', 'Rolling Eyes');
INSERT INTO `phpbb_smilies` VALUES (32, ':wink:', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (33, ';)', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (34, ';-)', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (35, ':!:', 'icon_exclaim.gif', 'Exclamation');
INSERT INTO `phpbb_smilies` VALUES (36, ':?:', 'icon_question.gif', 'Question');
INSERT INTO `phpbb_smilies` VALUES (37, ':idea:', 'icon_idea.gif', 'Idea');
INSERT INTO `phpbb_smilies` VALUES (38, ':arrow:', 'icon_arrow.gif', 'Arrow');
INSERT INTO `phpbb_smilies` VALUES (39, ':|', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (40, ':-|', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (41, ':neutral:', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (42, ':mrgreen:', 'icon_mrgreen.gif', 'Mr. Green');
    

CREATE TABLE `phpbb_disallow` (
  `disallow_id` mediumint(8) unsigned NOT NULL auto_increment,
  `disallow_username` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`disallow_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

CREATE TABLE `phpbb_words` (
  `word_id` mediumint(8) unsigned NOT NULL auto_increment,
  `word` char(100) NOT NULL default '',
  `replacement` char(100) NOT NULL default '',
  PRIMARY KEY  (`word_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;