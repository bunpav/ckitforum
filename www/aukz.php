<?php
//
// phpBB3 Аукцион v1.1 (c) 2012-2013 c61@yandex.ru
// (прототип phpBB2: Злодейские Аукционы v1.0 (c) 2009, http://zlodey.su/forum)
//
// todo:
// сделать разбивку по категориям
// сделать добавление лотов с пользовательской страницы
// снимать деньги за выставляемый лот, если в админке указана номинальная такса
// в админке сделать `cat` TINYINT(1) NOT NULL default '0' -> `cat` varchar(16) NOT NULL default '0'
// ввести в БД таблицу соответствия категорий

// c61:
//+сделать шаг цены в %
//+сделать блиц-цену, задаваемую пользователем
//+сделать "удалить навсегда" для админов
//+учёт часового пояса пользователя
//+id лота в таблице
//+шрифты
//+сортировка при просмотре списка лотов
//+бб-код лота

/*------------------------------------------------------------------------------
Установка
-------------------------------------------------------------------
1) Базы SQL
-------------------------------------------------------------------
***ВЫПОЛНИТЬ ЗАПРОС SQL********************************************
***(если нужно, заменить префикс "phpbb_" на свой)*****************
-------------------------------------------------------------------
CREATE TABLE `phpbb_aukz_main` (
  `id` int(6) NOT NULL auto_increment,
  `user` int(7) NOT NULL,
  `name1` varchar(32) NOT NULL,
  `name2` varchar(64) NOT NULL,
  `name3` varchar(256) NOT NULL default '',
  `name4` varchar(256) NOT NULL default '',
  `f1` varchar(256) NOT NULL default '',
  `time_auk` INT(11) NOT NULL default '0',
  `time_0` INT(11) NOT NULL,
  `price_0` double(15,2) NOT NULL default '0',
  `price_b` double(15,2) NOT NULL default '0',
  `price_5` double(15,2) NOT NULL default '0',
  `user_5` int(7) NOT NULL default '0',
  `time_5` INT(11) NOT NULL default '0',
  `closed` INT(11) NOT NULL default '0',
  `pause` INT(11) NOT NULL default '0',
  `history_st` text NOT NULL default '',
  `cat` TINYINT(1) NOT NULL default '0',
  `az1` TINYINT(1) NOT NULL default '0',
  `adm` TINYINT(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
);
CREATE TABLE `phpbb_aukz_config` (
  `config_name` varchar(32) NOT NULL default '',
  `config_value` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`config_name`)
);
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('aukz_ver', '1.0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('money_name', 'УЕ');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('pause', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('users_can', 'ADM');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('type_plata', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('plata_nominal', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('plata_procent', '3');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('max_srok', '72');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('price_1', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('price_1p', '5');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('type_price', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('price_2', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('antis', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('antist', '15');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('history_st', '1');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('adm_id', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('double_id', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('cats', '');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('history_depth', '100');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('forum_id', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('forumd_id', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('user_go', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('user_edit', '0');
INSERT INTO `phpbb_aukz_config` ( `config_name` , `config_value` ) VALUES ('user_delete', '0');

-------------------------------------------------------------------
В phpbb_config добавить `money_name` = 'УЕ',
В phpbb_users добавить `user_money` double (15,2) NOT NULL default '0.00',
В phpbb_users добавить `user_az_sort_field` int(6) NOT NULL default '0',
В phpbb_users добавить `user_az_sort_dir` TINYINT(1) NOT NULL default '0',

-------------------------------------------------------------------
2) BB-Code
-------------------------------------------------------------------
BB-code (требуется jQuery !!!):

[lot={NUMBER}][/lot]
<div class="inner aukz_lot{NUMBER}"></div><img src="images/blank.gif" alt="" width="1" height="1" border="0px" onload="jQuery.get('aukz.php', {'lot': '{NUMBER}'}, function(data) { $('div.aukz_lot{NUMBER}').replaceWith(data); });" />
Краткое описание лота Аукциона [lot=идентификатор лота][/lot]

[lotx={NUMBER1},{NUMBER2}][/lotx]
<div class="inner aukz_lotx{NUMBER1}_explain{NUMBER2}"></div><img src="images/blank.gif" alt="" width="1" height="1" border="0px" onload="jQuery.get('aukz.php', {'lot': '{NUMBER1}', 'explain': '{NUMBER2}'}, function(data) { $('div.aukz_lotx{NUMBER1}_explain{NUMBER2}').replaceWith(data); });" />
Описание лота Аукциона [lotx=идентификатор лота,режим отображения][/lotx]

--------------------------------------------------------------------------------*/

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

define('AUKZ_MAIN_TABLE', $table_prefix.'aukz_main');
define('AUKZ_CONFIG_TABLE', $table_prefix.'aukz_config');

//------------------------------------------------------------------------------
// Получение информации о лоте - для бб-кода
if ( (request_var('lot',0)!=0) )
{
	// Start session management
	$user->session_begin(false);
	$auth->acl($user->data);

	$user->setup('aukz');

	$points_name = $config['money_name'];

	// конфигурация аукциона:
	$sql="SELECT *
		FROM " . AUKZ_CONFIG_TABLE;
	if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."01". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
	while( $row = $db->sql_fetchrow($result) )
	{
		$aukz_conf[$row['config_name']] = $row['config_value'];
	}
	$db-> sql_freeresult($result);

	$conf_pause = $aukz_conf['pause']; 					// 1 - аукционы отключены
	$conf_money_name = $config['money_name'];			// название форумных средств ползователя
	if ( $conf_money_name != '' ) $points_name = $conf_money_name;

	if ($conf_pause)
	{
		echo '<table class="tablebg" width="100%" cellspacing="1">';
		echo '<tr style="background-color: white; padding: 4px">';
		echo '<td align="center">' .  $user->lang['AZ_A_OTKL'] . '</td>';
		echo '</tr>';
		echo '</table>';
		echo '<br />';
	}

	$lot_id = request_var('lot',0);			// лот
	$explain = request_var('explain',0);	// расширение

	$sql = "SELECT *
		FROM " . AUKZ_MAIN_TABLE . "
		WHERE id = $lot_id";
	if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."02". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
	$row = $db->sql_fetchrow($result);
	$db-> sql_freeresult($result);

	if ( $row )
	{
		$user_id = $row['user'];
		$time_last = $row['time_5'];
		$time_auk = $row['time_auk'];
		$name1 = $row['name1'];
		$name2 = $row['name2'];
		$name3 = $row['name3'];
		$name4 = $row['name4'];
		$time_0 = $row['time_0'];
		$time_5 = $row['time_5'];
		$price_0 = $row['price_0'];
		$price_5 = $row['price_5'];
		$price_b = $row['price_b'];
		$closed = $row['closed'];
		$pause = $row['pause'];
		$test_az1 = $row['az1'];
		$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';

		$br = ( $explain < 2 ) ? '<br />' : ' ';
		if ( $closed )
		{			$time_ost = '<b>' . (($name2 == '<deleted />') ? ('<font color="red">' . $user->lang['AZ_TORG_DELETED'] . '</font>') : ('<font color="green">' . $user->lang['AZ_TORG_FINISHED'] . '</font>')) . $br . aukz_date($user->lang['AZ_TIME_V'],$closed) . '</b>';
		}
		else if ( $pause )
		{
			$time_ost = '<b><font color="maroon">' . $user->lang['AZ_TORG_PAUSED'] . '</font>' . $br . aukz_date($user->lang['AZ_TIME_V'],$pause) . '</b>';
		}
		else
		{
			$time_now = date("U")-date("Z");					// текущее время в сек от начала века по гринвичу
			$time_close = max(($aukz_conf['antist']*60)+$time_last, $time_auk);
			$time_ost_temp = floor( ($time_close-$time_now)/60 ) +1; // осталось в минутах до завершения
			$time_ost_temp = ($time_ost_temp<10) ? '0' . $time_ost_temp : $time_ost_temp;
			if ( $time_ost_temp < 60 )
			{
				$time_ost = '0/00:' . $time_ost_temp;
			}
			else if ( $time_ost_temp < (60*24) )
			{
				$time_ost = '0/' . ((floor($time_ost_temp/60) < 10) ? '0' : '') . floor($time_ost_temp/60) . ':00';
			}
			else
			{
				$time_ost = floor($time_ost_temp/(60*24)) . '/' . ((floor(($time_ost_temp % (60*24))/60) < 10) ? '0' : '') . floor(($time_ost_temp % (60*24))/60) . ':00';
			}
		}

		echo '<table class="tablebg" width="100%" cellspacing="1">';

		if ( $explain < 2 )
		{
			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="center">' . $user->lang['AZ_NUM'] . '&nbsp;#' . $lot_id . '</td>';
			echo '<td align="center">' . $user->lang['AZ_DESC'] . '</td>';
			echo '<td align="center">' . $user->lang['AZ_CONDITIONS'] . '</td>';
			echo '<td align="center">' . $user->lang['AZ_STARTPRICE'] . ', ' . $points_name . '</td>';
			if ( $price_b != 0 ) echo '<td align="center">' . $user->lang['AZ_BLITZPRICE'] . ', ' . $points_name . '</td>';
			echo '<td align="center">' . $user->lang['AZ_LASTPRICE'] . ', ' . $points_name . '</td>';
			echo '<td align="center">' . $user->lang['AZ_2END'] . '</td>';
			echo '</tr>';

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="center">' . $name_test . $name1 . '</td>';
			echo '<td align="center">' . $name3 . '</td>';
			echo '<td align="center">' . $name4 . '</td>';
			echo '<td align="center">' . (($price_0 != 0) ? $price_0 : $user->lang['AZ_DAROM']) . '</td>';
			if ( $price_b != 0 ) echo '<td align="center">' . $price_b . '</td>';
			echo '<td align="center">' . (($price_5 != 0) ? $price_5 : $user->lang['AZ_NETSTAVOK']) . '</td>';
			echo '<td align="center">' . $time_ost . '</td>';
			echo '</tr>';
		}
		else
		{			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left" width="25%">' . $user->lang['AZ_NUM'] . '&nbsp;#' . $lot_id . '</td>';
			echo '<td align="left">' . $name_test . $name1 . '</td>';
			echo '</tr>';

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left">' . $user->lang['AZ_DESC'] . '</td>';
			echo '<td align="left">' . $name3 . '</td>';
			echo '</tr>';

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left">' . $user->lang['AZ_CONDITIONS'] . '</td>';
			echo '<td align="left">' . $name4 . '</td>';
			echo '</tr>';

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left">' . $user->lang['AZ_STARTPRICE'] . ', ' . $points_name . '</td>';
			echo '<td align="left">' . (($price_0 != 0) ? $price_0 : $user->lang['AZ_DAROM']) . '</td>';
			echo '</tr>';

			if ( $price_b != 0 )
			{				echo '<tr style="background-color: white; padding: 4px">';
				echo '<td align="left">' . $user->lang['AZ_BLITZPRICE'] . ', ' . $points_name . '</td>';
				echo '<td align="left">' . $price_b . '</td>';
				echo '</tr>';
			}

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left">' . $user->lang['AZ_LASTPRICE'] . ', ' . $points_name . '</td>';
			echo '<td align="left">' . (($price_5 != 0) ? $price_5 : $user->lang['AZ_NETSTAVOK']) . '</td>';
			echo '</tr>';

			echo '<tr style="background-color: white; padding: 4px">';
			echo '<td align="left">' . $user->lang['AZ_2END'] . '</td>';
			echo '<td align="left">' . $time_ost . '</td>';
			echo '</tr>';
		}
		echo '</table>';

		if ( ($explain == 1) OR ($explain == 3) )
		{
			echo '<br />';
			echo '<span class="gensmall">' . $user->lang['AZ_POST_EXPLAIN'] . '</span>';
		}
	}
	else
	{		echo '<table class="tablebg" width="100%" cellspacing="1">';
		echo '<tr style="background-color: white; padding: 4px">';
		echo '<td align="center"><b><font color="red">' .  $user->lang['AZ_DELETED_FOREVER'] . '</font></b></td>';
		echo '</tr>';
		echo '</table>';
	}

}
//------------------------------------------------------------------------------
else
{
	// Start session management
	$user->session_begin();
	$auth->acl($user->data);

	$user->setup('aukz');

	// Ботов и анонимов допускаем с ограничениями...
	$acl_limited = ( ($user->data['is_bot']) || ($user->data['user_id'] == ANONYMOUS) ) ? true : false;

	//------------------------------------------------------------------------------
	// Специальное управление - только для админов
	if( (request_var('u',0)!=0) AND $auth->acl_get('a_') )
	{
		$user_id = request_var('u',0);			// id полььзователя
		$user_add_money = request_var('a',0);	// добавляемая сумма (есди <0 - обнуление счёта)
		$user_view_money = request_var('v',0);	// просмотр суммы (если == 1)

		if ( $user_add_money != 0 )
		{
			$sql = "SELECT user_money FROM " . USERS_TABLE . " WHERE user_id = $user_id ";
			if ( ($result = $db->sql_query($sql)) )
			{
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
				$user_money = $row['user_money'];

				if ( $user_add_money > 0 )
				{
					$user_money = $user_money + $user_add_money;
					if ( $user_money > 9999999.99 ) $user_money = 9999999.99;
				}
				else
				{
					$user_money = 0;
				}

				$sql_user_money = 'user_money = ' . $user_money;
				$sql = "UPDATE " . USERS_TABLE . "
						SET $sql_user_money
						WHERE user_id = $user_id";
				$db->sql_query($sql);
			}
		}
		else if ( $user_view_money != 0 )
		{
			$sql = "SELECT user_money FROM " . USERS_TABLE . " WHERE user_id = $user_id ";
			if ( ($result = $db->sql_query($sql)) )
			{
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
				$user_money = $row['user_money'];

				echo $user_money;
			}
		}
	}
	//------------------------------------------------------------------------------
	// Модифицированный и переработанный c61 код от Злодея
	else
	{
		$user_id = $user->data['user_id'];
		$user_money = $user->data['user_money'];
		$points_name = $config['money_name'];
		$page_title = $user->lang['AZ_PAGE_TITLE'];
		$template_html = 'aukz.html';

		$start = request_var('start',0);
		$opn = request_var('opn',0);
		$name_access = $opn;

		// конфигурация аукциона:
		$sql="SELECT *
			FROM " . AUKZ_CONFIG_TABLE;
		if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."01". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
		while( $row = $db->sql_fetchrow($result) )
		{
			$aukz_conf[$row['config_name']] = $row['config_value'];
		}
		$db-> sql_freeresult($result);

		// $conf_version = $aukz_conf['aukz_ver']; // версия аукциона (пока не требуется)
		$conf_pause = $aukz_conf['pause']; // 1 - аукционы отключены
		$conf_otkl = '';
		if ($conf_pause)
		{
			$conf_otkl = $user->lang['AZ_A_OTKL'];
			if ( !($auth->acl_get('a_') OR ($user_id == $conf_adm_id)) )
			{
				$message = $user->lang['AZ_A_OTKL'] . $user->lang['AZ_A_OTKL_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			$conf_otkl .=  $user->lang['AZ_A_OTKL_ADMIN'];
		}
		$conf_money_name = $config['money_name'];			// название форумных средств ползователя
		if ( $conf_money_name != '' ) $points_name = $conf_money_name;
		$conf_users_can = $aukz_conf['users_can']; 			// список тех, кто может добавлять лоты, MOD, ADM, ALL
		$conf_type_plata = $aukz_conf['type_plata']; 		// тип комиссионных. 1 - номинальные деньги, при выставлении лота, 2 - проценты от окончательной стоимости лота
		$conf_plata_nominal = $aukz_conf['plata_nominal']; 	// размер комиссионных, номинал
		$conf_plata_procent = $aukz_conf['plata_procent']; 	// размер комиссионных, проценты
		$conf_price_1 = $aukz_conf['price_1']; 				// минимальных шаг аукциона (номинал)
		$conf_price_1p = $aukz_conf['price_1p']; 			// минимальных шаг аукциона (проценты)
		$conf_type_price = $aukz_conf['type_price']; 		// тип шага. 1 - номинальные деньги, 0 - проценты от текущей стоимости
		$conf_price_2 = $aukz_conf['price_2']; 				// бабки за просмотр лидеров
		$conf_max_srok = $aukz_conf['max_srok']; 			// максимальный срок выставления лотов
		$conf_antis = $aukz_conf['antis']; 					// режим антиснайпера, если 0, то выключен
		$conf_antist = $aukz_conf['antist']*60; 			// время режима "антиснайпер" (перевод в секунды)
		$conf_history_st = $aukz_conf['history_st']; 		// включена ли история вставок (1/0)
		$conf_cat = $aukz_conf['cat']; 						// список разрешенных категорий
		$conf_history_depth = $aukz_conf['history_depth'];	// глубина истории при выдаче списка лотов (в сутках)
		$conf_history_depth = 24*$conf_history_depth;			// переводим в часы
		$conf_history_depth = 3600*$conf_history_depth;			// переводим в секунды
		$conf_forum_id = $aukz_conf['forum_id']; 			// в каком форуме постить при размещении лота и закрытии...
		$conf_forumd_id = $aukz_conf['forumd_id']; 			// в каком форуме постить при размещении лота даром и закрытии...
		$conf_adm_id = $aukz_conf['adm_id']; 				// от чьего имени посылать ЛС
		$conf_double_id = $aukz_conf['double_id']; 			// дублирование ЛС
	//	$conf_adm_id = 2; 									// от чьего имени посылать ЛС
		$from_id = $conf_adm_id;							// от чьего имени посылать ЛС
		$double_id = $conf_double_id;						// дублирование ЛС
		$conf_user_go = $aukz_conf['user_go'];				// Пользователь может продолжить торги, приостановленные Администратором (if == 1)
		$conf_user_edit = $aukz_conf['user_edit'];			// Пользователь может редактировать лот, если торг приостановлен Администратором (if == 1)
		$conf_user_delete = $aukz_conf['user_delete'];		// Пользователь может удалить лот, если торг приостановлен Администратором (if == 1)

		$lot_id_darom = -1;									// флажок отдачи лота даром
		$lot_id_blitz = -1;									// флажок покупки лота по блиц-цене
		$time_now = date("U")-date("Z");					// текущее время в сек от начала века по гринвичу

		$use_adm_checkbox = false;							// убрать checkbox для спецопераций
		$use_simple_buttoons = false;						// использовать простые кнопки

		if ( !$use_simple_buttons )
		{			$use_adm_checkbox = false;						// без чекбоксов, с подтврждениями через js

			$style_id = $user->data['user_style'];
			$sql = 'SELECT style_name
					FROM ' . STYLES_TABLE . "
					WHERE style_id = $style_id";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."02". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);
			$style_name = $row['style_name'];

			$user_image_lang = (file_exists($phpbb_root_path . 'styles/' . $style_name . '/imageset/' . $user->data['user_lang'])) ? $user->data['user_lang'] : $config['default_lang'];
			$imageset_lang_path = "{$phpbb_root_path}styles/" . $style_name . '/imageset/' . $user_image_lang;
		}

	//------------------------------------------------------------------------------
	// Смотрим, делал ли кто ставки:
		if( (((request_var('do_stavka1',0)<>0) AND (request_var('do_stavka2','')<>'')) OR (request_var('do_stavkab','')<>'')) AND !$acl_limited )
		{
			$stavka_new = request_var('do_stavka1',0);
			if ( !preg_match("/^[\d]*[\.,]?[\d]*$/", $stavka_new) )		// проверка на правильность ввода ставки
			{
				$message = $user->lang['AZ_U_CRETINO'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			$stavka_new = str_replace(',','.',$stavka_new);

			$lot_id = request_var('hidden_lot_id',0);

			$sql = "SELECT *
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $lot_id";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."02". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);

			$time_last = $row['time_5'];
			$time_auk = $row['time_auk'];
			$closed = $row['closed'];
			$name2 = $row['name2'];
			$pause = $row['pause'];
			$test_az1 = $row['az1'];
			$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';
			$time_close = max($conf_antis+$time_last, $time_auk);
			if ( $time_close < $time_now )							// проверяем, не закончилось ли время аукциона
			{
				$message = $user->lang['AZ_POZDNYAK'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			if ( ($closed != 0) AND ($name2 == '<deleted />') )		// проверяем, был ли удален лот
			{
				$message = $user->lang['AZ_POZDNYAK_DELETED'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			if ( $pause != 0 )										// проверяем паузу
			{
				$message = $user->lang['AZ_POZDNYAK_PAUSE'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}

			// проверяем, превышает ли ставка текущую ставку плюс минимальный шаг
			$price_0 = $row['price_0'];
			$price_darom = ( $price_0 == 0.00 OR $price_0 == 0 ) ? true : false;
			$price_5 = $row['price_5'];

			$price_add = $conf_type_price ? $conf_price_1 : round($conf_price_1p*($price_5/100));
			if( $price_add <= 0 ) $price_add = 1;

			$price_min = ($price_0>$price_5) ? $price_0 : ( ($price_0==$price_5) ? $price_0+$price_add : $price_5+$price_add );
		/*
			0	5
			0	0 (без н/ц, никто не ставил)
			0	2 (без н/ц, ставили)
			5	0 (с н/ц, никто не ставил)
			5	6 (с н/ц, ставили)
		*/
			if  ( $price_darom )
			{				$lot_id_darom = $lot_id; // Признак закрытия торгов

				// заносим в базу данные по новому владельцу и историю
				if ($conf_history_st) // нужно ли обновлять историю
				{
					$hist_old = $row['history_st'];
					$hist_add0 = ($hist_old) ? '|' : '';
					$hist_new =  $hist_old . $hist_add0 . $user_id . ';0;' . $time_now;
				}
				else $hist_new = $row['history_st'];

				$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
					SET user_5 = $user_id,
					time_5 = $time_now,
					history_st = '$hist_new'
					WHERE id = $lot_id";
				if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."04". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			}
			else if (request_var('do_stavka2','')<>'')
			{
				if (round($stavka_new*100) < round($price_min*100))
				{
					$message = $user->lang['AZ_STA_NOW'] . $price_min . $points_name . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}

				// проверяем, достаточно ли у юзера денег
				if ($user_money < $stavka_new)
				{
					$message = $user->lang['AZ_NO_MONEY']. $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}

				// возвращаем предыдущему ставившему его деньги и отсылаем ЛС, что его перебили
				$user_last = $row['user_5'];
				if ($user_last AND $price_5)
				{
					$sql = "UPDATE " . USERS_TABLE . "
						SET user_money = user_money + $price_5
						WHERE user_id = $user_last
						LIMIT 1";
					if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."03". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
					if( $user_last == $user_id ) $user_money = $user_money + $price_5;
				}
				if ($user_last /*AND ($user_last != $user_id)*/)
				{
					$name_lota = $row['name1'];
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $lot_id . ' ]';
					$dest_user = $user_last;
					$message = $user->lang['AZ_YOU_ST'] . $name_test . $name_lota . $user->lang['AZ_WAS_PERE'] . aukz_date($user->lang['AZ_DATE_IN'], $time_close,$dest_user); // проверить насчет часовых поясов
					adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
				}
				// заносим в базу данные по новому лидеру и историю
				if ($conf_history_st) // нужно ли обновлять историю
				{
					$hist_old = $row['history_st'];
					$hist_add0 = ($hist_old) ? '|' : '';
					$hist_new =  $hist_old . $hist_add0 . $user_id . ';' . $stavka_new . ';' . $time_now;
				}
				else $hist_new = $row['history_st'];

				$sql = "UPDATE " . AUKZ_MAIN_TABLE . " AS az, " . USERS_TABLE . " AS us
					SET az.price_5 = $stavka_new,
					az.user_5 = $user_id,
					az.time_5 = $time_now,
					az.history_st = '$hist_new',
					us.user_money = user_money - $stavka_new
					WHERE az.id = $lot_id AND us.user_id = $user_id";
				if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."04". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);

				$user_money = $user_money - $stavka_new;

				$name_lota = $row['name1'];
				$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $lot_id . ' ]';
				$dest_user = $user_id;
				$message = $user->lang['AZ_YOU_ST'] . $name_test . $name_lota . $user->lang['AZ_WAS_DONE'] . $stavka_new . $points_name . $user->lang['AZ_WAS_DONE2'] . aukz_date($user->lang['AZ_DATE_IN'], $time_close,$dest_user); // проверить насчет часовых поясов
				adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);

				redirect(append_sid("aukz.$phpEx" . '?start=' . $start . '&opn=' . $opn));
			}
			else if (request_var('do_stavkab','')<>'')
			{
				$stavka_new = request_var('hidden_price_b',0);
				$stavka_new = str_replace(',','.',$stavka_new);

				if (round($stavka_new*100) < round($price_min*100))
				{
					$message = $user->lang['AZ_STA_NOW'] . $price_min . $points_name . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}

				// проверяем, достаточно ли у юзера денег
				if ($user_money < $stavka_new)
				{
					$message = $user->lang['AZ_NO_MONEY']. $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}

				// возвращаем предыдущему ставившему его деньги и отсылаем ЛС, что его перебили
				$user_last = $row['user_5'];
				if ($user_last AND $price_5)
				{
					$sql = "UPDATE " . USERS_TABLE . "
						SET user_money = user_money + $price_5
						WHERE user_id = $user_last
						LIMIT 1";
					if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."03". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
					if ( $user_last == $user_id ) $user_money = $user_money + $price_5;
				}
				if ($user_last /*AND ($user_last != $user_id)*/)
				{
					$name_lota = $row['name1'];
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $lot_id . ' ]';
					$dest_user = $user_last;
					$message = $user->lang['AZ_YOU_ST'] . $name_test . $name_lota . $user->lang['AZ_WAS_PERE'] . aukz_date($user->lang['AZ_DATE_IN'], $time_close,$dest_user); // проверить насчет часовых поясов
					adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
				}
				// заносим в базу данные по новому владкльцу и историю
				if ($conf_history_st) // нужно ли обновлять историю
				{
					$hist_old = $row['history_st'];
					$hist_add0 = ($hist_old) ? '|' : '';
					$hist_new =  $hist_old . $hist_add0 . $user_id . ';' . $stavka_new . ';' . $time_now;
				}
				else $hist_new = $row['history_st'];

				$sql = "UPDATE " . AUKZ_MAIN_TABLE . " AS az, " . USERS_TABLE . " AS us
					SET az.price_5 = $stavka_new,
					az.user_5 = $user_id,
					az.time_5 = $time_now,
					az.history_st = '$hist_new',
					us.user_money = user_money - $stavka_new
					WHERE az.id = $lot_id AND us.user_id = $user_id";
				if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."04". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
				$user_money = $user_money - $stavka_new;

				$lot_id_blitz = $lot_id; // Блиц-покупка
			}
		} // закончили делать ставку

	//------------------------------------------------------------------------------
	// Управление собственным счётом
		if( ((request_var('a_m1','')!='') OR (request_var('a_m2','')!='') OR (request_var('a_m3','')!='') OR (request_var('a_m4','')!='') OR (request_var('a_m0','')!='')) AND !$acl_limited )
		{			$sql = "SELECT user_money FROM " . USERS_TABLE . " WHERE user_id = $user_id ";
			if ( ($result = $db->sql_query($sql)) )
			{
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
				$user_money = $row['user_money'];

				$user_add_money = 0;
				if ( request_var('a_m0','') == '= 0' ) $user_add_money = -1;
				else if ( request_var('a_m1','') == '+ 100' ) $user_add_money = 100;
				else if ( request_var('a_m2','') == '+ 1 000' ) $user_add_money = 1000;
				else if ( request_var('a_m3','') == '+ 10 000' ) $user_add_money = 10000;
				else if ( request_var('a_m4','') == '+ 100 000' ) $user_add_money = 100000;

				if ( $user_add_money > 0 )
				{
					$user_money = $user_money + $user_add_money;
					if ( $user_money > 3333333.00 ) $user_money = 3333333.00;
					$sql_user_money = 'user_money = ' . $user_money;
					$sql = "UPDATE " . USERS_TABLE . "
							SET $sql_user_money
							WHERE user_id = $user_id";
					$db->sql_query($sql);
				}
				else if ( $user_add_money < 0 )
				{
					$user_money = 0;
					$sql_user_money = 'user_money = ' . $user_money;
					$sql = "UPDATE " . USERS_TABLE . "
							SET $sql_user_money
							WHERE user_id = $user_id";
					$db->sql_query($sql);
				}
				if ( $user_add_money != 0 )
				{
					$subject = '[ ' . $user->lang['AUKZ'] . ' ]';
					$dest_user = $user_id;
					$message = $user->lang['AZ_MONEY_CHANGED'] . $user_money . $points_name;
					adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
				}
			}
			redirect(append_sid("aukz.$phpEx" . '?start=' . $start . '&opn=' . $opn));
		} // закончили управление собственным счётом

	//------------------------------------------------------------------------------
	// Админ или хозяин лота удаляет лот или ставит на паузу или редактирует:
	// Постановка лота на паузу и снятие с нее
		if( ((request_var('a_p','')!='') AND (($use_adm_checkbox == false) OR (request_var('a_d2','')!=''))) AND !$acl_limited )
		{
			$id_lota = request_var('hidden_lot_id',0);

			$sql = "SELECT user, user_5, name1, pause, time_auk, az1, adm
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."104". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);

			$avtor_lota = $row['user'];
			$user_5 = $row['user_5'];
			$name_lota = $row['name1'];
			$pause = $row['pause'];
			$date_end = $row['time_auk'];
			$test_az1 = $row['az1'];
			$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';
			$adm = $conf_user_go ? 0 : $row['adm'];
			if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) ) $adm = 0;

			if ( $auth->acl_get('a_') OR ($avtor_lota == $user_id) OR ($user_id == $conf_adm_id) )
			{
				if ($pause)			// если $pause есть, то лот на паузе. снимем с нее (добавив время паузы ко времени окончания аукциона):
				{
					if ( $adm AND !(($user_id == $conf_adm_id) OR $auth->acl_get('a_')) )
					{
						$message = $user->lang['AZ_USER_CANT_GO'] . $user->lang['AZ_RETURN'];
						message_die(GENERAL_MESSAGE, $message);
					}

					$added_time = $time_now-$pause;
					$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
						SET time_auk = time_auk + $added_time,
						pause = 0,
						adm = 0
						WHERE id = $id_lota";
					if (!$db->sql_query($sql)){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."105". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}

					if ($user_5)
					{
						if ($user_5 != $avtor_lota)
						{
							$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
							$dest_user = $user_5;
							$message = $user->lang['AZ_LOTT'] . $name_test . $name_lota . $user->lang['AZ_GOADM'];
							adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
						}
					}
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
					$dest_user = $avtor_lota;
					$message = $user->lang['AZ_ULOTT'] . $name_test . $name_lota . $user->lang['AZ_GOADM'];
					adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
				}
				else			// если $pause нет, то лот НЕ на паузе. поставим на паузу:
				{
					$adm = (($user_id == $conf_adm_id) OR $auth->acl_get('a_')) ? 1 : 0;
					$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
						SET pause = $time_now,
						adm = $adm
						WHERE id = $id_lota";
					if (!$db->sql_query($sql)){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."106". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}

					if ($user_5)
					{
						if ($user_5 != $avtor_lota)
						{
							$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
							$dest_user = $user_5;
							$message = $user->lang['AZ_LOTT'] . $name_test . $name_lota . $user->lang['AZ_PAUSADM'];
							adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
						}
					}
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
					$dest_user = $avtor_lota;
					$message = $user->lang['AZ_ULOTT'] . $name_test . $name_lota . $user->lang['AZ_PAUSADM'];
					adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
				}
			}
		}

	//------------------------------------------------------------------------------
	// Удаление лота
		if( ((request_var('a_d1','')!='') AND (($use_adm_checkbox == false) OR (request_var('a_d2','')!=''))) AND !$acl_limited )
		{
			$id_lota = request_var('hidden_lot_id',0);

			$sql = "SELECT user, name1, user_5, price_5, az1, adm
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."107". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);

			$avtor_lota = $row['user'];
			$name_lota = $row['name1'];
			$price_5 = $row['price_5'];
			$user_5 = $row['user_5'];
			$test_az1 = $row['az1'];
			$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';
			$adm = $conf_user_delete ? 0 : $row['adm'];
			if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) ) $adm = 0;

			if ( $auth->acl_get('a_') OR ($avtor_lota == $user_id) OR ($user_id == $conf_adm_id) )
			{
				if ( $adm AND !(($user_id == $conf_adm_id) OR $auth->acl_get('a_')) )
				{
					$message = $user->lang['AZ_USER_CANT_DELETE'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
				//		удалить и не забыть вернуть последнюю ставку, а также отправить ЛС последнему и автору, если они != тому, кто удаляет
				$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
					SET closed = $time_now,
					name2 = '<deleted />'
					WHERE id = $id_lota";
				if(!$result = $db->sql_query($sql)) {message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."108". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
				if ($user_5)
				{
					$sql = "UPDATE " . USERS_TABLE . "
						SET user_money = user_money + $price_5
						WHERE user_id = $user_5";
					if (!$db->sql_query($sql)){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."109". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
					if ( $user_5 == $user_id ) $user_money = $user_money + $price_5;

					if ($user_5 != $avtor_lota)
					{
						$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
						$dest_user = $user_5;
						$message = $user->lang['AZ_LOT'] . $name_test . $name_lota . $user->lang['AZ_DELADM2'];
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
					}
				}
				$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
				$dest_user = $avtor_lota;
				$message = $user->lang['AZ_ULOT'] . $name_test . $name_lota . $user->lang['AZ_DELADM'];
				adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
		//		redirect(append_sid('aukz'.$phpEx . '?start=' . $start . '&opn=' . $opn));
			} // удалили
		}

	//------------------------------------------------------------------------------
	// Удаление лота навсегда
		if( ((request_var('a_d1f','')!='') AND (($use_adm_checkbox == false) OR (request_var('a_d2','')!=''))) AND !$acl_limited )
		{
			$id_lota = request_var('hidden_lot_id',0);

			$sql = "SELECT name1, closed, az1
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."107". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);

			$name_lota = $row['name1'];
			$closed = $row['closed'];
			$test_az1 = $row['az1'];
			$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';

			if ( $auth->acl_get('a_') AND $closed )
			{	//		удалить навсегда
				$sql = "DELETE FROM " . AUKZ_MAIN_TABLE . "
					WHERE id = $id_lota";
				if(!$result = $db->sql_query($sql)) {message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."108". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
				$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
				$dest_user = $user_id;
				$message = $user->lang['AZ_CLOT'] . $name_test . $name_lota . $user->lang['AZ_DELADMC'];
				adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
		//		redirect(append_sid('aukz'.$phpEx . '?start=' . $start . '&opn=' . $opn));
			} // удалили навсегда
		}

	//------------------------------------------------------------------------------
	// Запрос на редактирование лота
		if( ((request_var('a_e','')!='') AND (($use_adm_checkbox == false) OR (request_var('a_d2','')!=''))) AND !$acl_limited )
		{
			$id_lota = request_var('hidden_lot_id',0);

			$sql = "SELECT user, name1, name2, closed, user_5, price_5, adm
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."107". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$row = $db->sql_fetchrow($result);
			$db-> sql_freeresult($result);

			$avtor_lota = $row['user'];
			$adm = $conf_user_edit ? 0 : $row['adm'];
			if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) ) $adm = 0;

			if ( $auth->acl_get('a_') OR ($avtor_lota == $user_id) OR ($user_id == $conf_adm_id) )
			{
				if ( $adm AND !(($user_id == $conf_adm_id) OR $auth->acl_get('a_')) )
				{
					$message = $user->lang['AZ_USER_CANT_EDIT'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
				redirect(append_sid('aukz.' . $phpEx . '?edit=1&id=' . $id_lota . '&start=' . $start . '&opn=' . $opn));
			} // перенаправлены на редактирование
		}

	//------------------------------------------------------------------------------
	// Смотрим, не запросил ли кто имена хозяев, поставивших, историю...
		if( $opn )
		{			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}
			if ($auth->acl_get('a_') OR ($user_id == $conf_adm_id)) // админы и аукционист смотрят бесплатно
			{				$name_access = 2;
			}
			else
			{
				if ($conf_price_2 <= $user_money)
				{
					if ($conf_price_2 != 0)
					{						$sql = "UPDATE " . USERS_TABLE . "
								SET user_money = user_money - $conf_price_2
								WHERE user_id = $user_id";
						if (!$db->sql_query($sql)){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."05". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}

						$subject = '[ ' . $user->lang['AUKZ'] . ' ]';
						$dest_user = $user_id;
						$message = $user->lang['AZ_VIEW_NAMES'] . $conf_price_2 . $points_name;
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
						$user_money = $user_money - $conf_price_2;

						if ( $from_id )
						{							$sql = "UPDATE " . USERS_TABLE . "
									SET user_money = user_money + $conf_price_2
									WHERE user_id = $from_id";
							if (!$db->sql_query($sql)){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."05". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
						}

						$subject = '[ ' . $user->lang['AUKZ'] . ' ]';
						$dest_user = $from_id;
						$message = $user->lang['AZ_VIEW_NAMES_ADMIN'] . $conf_price_2 . $points_name;
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id);
					}
					$time_closed_history = $time_now - $conf_history_depth;
					$sql_count = "SELECT COUNT(id) AS num_lots
								FROM " . AUKZ_MAIN_TABLE . "
								WHERE (user = $user_id) AND (closed = 0 OR closed > $time_closed_history)";
					if(!$result = $db->sql_query($sql_count))
					{						$name_access = 1;
					}
					else
					{						$total_lots = (int) $db->sql_fetchfield('num_lots');
						$db->sql_freeresult($result);
						$name_access = ($total_lots > 0) ? 2 : 1;
					}
				}
				else
				{
					$message = $user->lang['AZ_2VIEW_NAMES'] . $conf_price_2 . $points_name . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
			}
			$opn = $name_access;
		}

	//------------------------------------------------------------------------------
	// Повтор создания или редактирования лота в случае ошибок
		$form_error = '';
	//------------------------------------------------------------------------------
	// Смотрим, не хочет ли кто выставить новый лот и, если хочет, вывод таблицы для нового лота
		if(request_var('new',0)==1)
		{
			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}

			// Узнаем, сколько денег пользователя в аукционе
			if (!$acl_limited)
			{
				$sql = "SELECT SUM(price_5) AS my_bablos
					FROM " . AUKZ_MAIN_TABLE . "
					WHERE user_5 = $user_id AND closed = 0";
				if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."11". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
				$row4 = $db->sql_fetchrow($result);
				$db-> sql_freeresult($result);
				$my_bablos = $row4['my_bablos'];
			}

			$est_comiss = ( ($conf_type_plata==1 AND $conf_plata_nominal) OR ($conf_type_plata==2 AND $conf_plata_procent) ) ? 1 : 0;
			$template->assign_vars(array(
				'MY_BABLOS' => ($my_bablos) ? ('(+' . '<b>' . $my_bablos . '</b>' . $points_name . $user->lang['AZ_VZALOGE']) : '',
		 		'NAL' => '<b>' . (($user_money AND !$acl_limited) ? $user_money : 0) . '</b>',
				'L_MYNAL' => $user->lang['AZ_MY_NAL'],
				'POINTS' => $points_name,
				'L_AUKZ' => $user->lang['AUKZ'],
				'L_AUKZ2' => $user->lang['AZ_AUKZ2'],
				'L_AUKZ_RELOAD' => '',
				'L_CO' => $user->lang['AZ_CO'],
				'L_OTKL' => '<h2>' . $conf_otkl . '</h2>',
				'START'	=> $start,
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_NUM' => $user->lang['AZ_NUM'],
			));

			$st_html_add = $use_simple_buttons
						  ? '<span title="' . $user->lang['AZ_ADD_U_TOOLTIP'] . '"><input type="submit" name="save_new" value="' . $user->lang['AZ_ADD_U'] . '" style="cursor:pointer" /></span>'
						  : '<input type="hidden" name="save_new" value="' . $user->lang['AZ_ADD_U'] . '" />'
						  . az_make_button($user->lang['AZ_ADD_U'],$user->lang['AZ_ADD_U_TOOLTIP'],'save_new','',$imageset_lang_path,'aukz_go.png','add');
						  ;
			$st_html = '<input type="hidden" name="hidden_lot_id" value="0" />' . $st_html_add;

			$template->assign_block_vars('new_lot', array(
				'L_FORMNAME' => $user->lang['AZ_FORM_NEW'],
				'L_NUM' => $user->lang['AZ_LOTNAME'],
				'L_TEST' => $user->lang['AZ_TEST'],
				'ACTION' => 'aukz.php?start=' . $start . '&opn=' . $opn,
				'L_PASS' => $user->lang['AZ_HIDDEN'],
				'L_INFO' => $user->lang['AZ_ADD_INFO'],
				'L_COND' => $user->lang['AZ_ADD_COND'],
				'L_PRICE0' => $user->lang['AZ_START_PRICE2'],
				'L_2END' => $user->lang['AZ_TIME_H_M'],
				'L_BLITZ' => $user->lang['AZ_BLITZ'],
				'COMISS1' => ($est_comiss) ? $user->lang['AZ_COMISS00'] : $user->lang['AZ_NO_COMISS'],
				'COMISS2' => (!$est_comiss) ? '' : ( ($conf_type_plata==1 AND $conf_plata_nominal) ? $user->lang['AZ_COMISS10'] .$conf_plata_nominal.$points_name : $user->lang['AZ_COMISS20'] .$conf_plata_procent. $user->lang['AZ_COMISS21'] ),
				'L_CAT' => $user->lang['AZ_CAT'],
				'L_FOTO' => $user->lang['AZ_FOTO'],
				'FILESIZE' => '32000',
				'L_DOB' => $user->lang['AZ_ADD_U'],
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_ADD_CONFIRM' => $user->lang['AZ_ADD_U_TOOLTIP'],
				'ST_HTML' => $st_html,
				'L_FORM_ERROR' => '',
				'NAME1' => '',
				'NAME2' => '',
				'NAME3' => '',
				'NAME4' => '',
				'PRICE' => '0',
				'BLITZ' => '0',
				'TEST_CHECKED' => '',
				'TIME_HOURS' => $conf_max_srok,
				'TIME_MINUTES' => '00',
			));
		}

	//------------------------------------------------------------------------------
	// Занесение в базу информации по новому лоту
		if( request_var('save_new','') != '' )
		{
			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}

			if (!$conf_users_can)
			{
				$message = $user->lang['AZ_NEW_OFF'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			// проверяем разрешения:
			$razresheno = 0;
			$users_can_array = explode(',',$conf_users_can);
			$minus_user = '-'.$user_id;
			if (in_array('$minus_user', $users_can_array))
			{
				$message = $user->lang['AZ_NOT4U'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			if (in_array('ALL', $users_can_array)) $razresheno=1;
			if (!$razresheno AND in_array($user_id, $users_can_array)) $razresheno=1;
			if (!$razresheno AND $auth->acl_get('a_') AND in_array('ADM', $users_can_array)) $razresheno=1;
			if (!$razresheno AND $auth->acl_get('m_') AND in_array('MOD', $users_can_array)) $razresheno=1;

			if (!$razresheno)
			{
				$message = $user->lang['AZ_NOT4U'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			else
			{
				$name_lota = utf8_normalize_nfc(request_var('nomer','',true));
				$pass_lota = utf8_normalize_nfc(request_var('pass','',true));
				$info_lota = utf8_normalize_nfc(request_var('info','',true));
				$cond_lota = utf8_normalize_nfc(request_var('cond','',true));
				$price_0 = request_var('price_0','0');
				$price_b = request_var('price_b','0');
				// проверка, чтобы в цене были только цифры и десятичная точка, а также наличия названия и пароля:
				if ( !preg_match("/^[\d]*[\.,]?[\d]*$/", $price_0) OR !preg_match("/^[\d]*[\.,]?[\d]*$/", $price_b) OR ($name_lota=='') OR ($pass_lota==''))
				{
					//$message = $user->lang['AZ_ERR_IN_FORM'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_TEXT'];
				}
				if ( strlen($name_lota) > 31 )
				{
					//$message = $user->lang['AZ_ERR_IN_FORM1'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM1_TEXT'];
				}
				if ( strlen($pass_lota) > 63 )
				{
					//$message = $user->lang['AZ_ERR_IN_FORM2'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM2_TEXT'];
				}
				if ( strlen($info_lota) > 255 )
				{
					//$message = $user->lang['AZ_ERR_IN_FORM3'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM3_TEXT'];
				}
				if ( strlen($cond_lota) > 255 )
				{
					//$message = $user->lang['AZ_ERR_IN_FORM4'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM4_TEXT'];
				}
				$price_0 = str_replace(',','.',$price_0);
				$price_b = str_replace(',','.',$price_b);
				$name_lota = str_replace("'",'"',$name_lota);
				$pass_lota = str_replace("'",'"',$pass_lota);
				$info_lota = str_replace("'",'"',$info_lota);
				$cond_lota = str_replace("'",'"',$cond_lota);
				if ( $price_b > 0 )
				{					if ( $price_0 == 0 )
					{						//$message = $user->lang['AZ_ERR_IN_FORM_BLITZ_FOR_DAROM'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_BLITZ_FOR_DAROM'];
					}
					if ( $price_b < $price_0 )
					{						//$message = $user->lang['AZ_ERR_IN_FORM_BLITZ_LESS_PRICE'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_BLITZ_FOR_DAROM'];
					}				}

				$endtime1 = request_var('endtime1',0);
				$endtime2 = request_var('endtime2',0);
				$category = request_var('category',0);
				$fotolot = request_var('fotolot',0);
				$test_az1 = (request_var('a_test','') != '') ? 1 : 0;
				$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';

				$end_lota = $time_now + $endtime1*3600 + $endtime2*60;
				if ( (($end_lota-$time_now)/3600 > $conf_max_srok) AND !$auth->acl_get('a_'))
				{
					//$message = $user->lang['AZ_ONLY_MAXSROK'] . $conf_max_srok . $user->lang['AZ_HS'] . $user->lang['AZ_RETURN_JS'];
					//message_die(GENERAL_MESSAGE, $message);
					$form_error .= '<br />' . $user->lang['AZ_ONLY_MAXSROK'] . $conf_max_srok . $user->lang['AZ_HS'];
				}
				if ( $form_error == '' )
				{					$sql = "INSERT INTO " . AUKZ_MAIN_TABLE . "
							(user, name1, name2, name3, name4, time_auk, time_0, price_0, price_b, az1)
							VALUES ($user_id, '$name_lota', '$pass_lota', '$info_lota', '$cond_lota', $end_lota, $time_now, $price_0, $price_b, $test_az1)";
					if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."102". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
					$lot_id = $db->sql_nextid();

					if($conf_type_plata AND	$conf_plata_nominal)			// вычтем комиссию, если она снимается при выставлении
					{	// ввести в админку предупреждение о двойной комиссии при смене типа
						$sql = "UPDATE " . USERS_TABLE . "
							SET user_money = user_money - $conf_plata_nominal
							WHERE user_id = $user_id";
						if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."07". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
						$user_money = $user_money - $conf_plata_nominal;
						$comiss = $conf_plata_nominal;
					}
					else
					{						$comiss = 0;
					}

					// отправим ЛС автору
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $lot_id . ' ]';
					$sql = "SELECT username FROM " . USERS_TABLE . " WHERE user_id = $user_id LIMIT 1";
					if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."08". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
					$row1 = $db->sql_fetchrow($result);
					$db-> sql_freeresult($result);
					$username_end = $row1['username'];
					$message_comiss = $comiss ? ($user->lang['AZ_COMISS_USER'] .  $comiss . $points_name) : '';
					$message = $user->lang['AZ_SETLOT'] . $name_test . $name_lota . '"' . $user->lang['AZ_ZA'] . $price_0 . $points_name . $message_comiss;
					adr_send_pm_az ($user_id, $subject, $message, $from_id, $double_id); // отправили сообщение хозяину лота

					// перечислили комиссию
					if ( $comiss )
					{
						if ( $from_id )
						{							$sql = "UPDATE " . USERS_TABLE . "
									SET user_money = user_money + $conf_plata_nominal
									WHERE user_id = $from_id";
							if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."07". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
						}
						$dest_user = $from_id;
						$message = $user->lang['AZ_COMISS3_ADMIN'] . $name_test . $name_lota . $user->lang['AZ_COMISS4_ADMIN'] . $comiss. $points_name;
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id); // отправили сообщение аукционисту
					}

					// создаём тему
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $lot_id . ' ] ' . $name_test . $name_lota;
					$message = '[lotx=' . $lot_id . ',1][/lotx]';
					$url = az_submit_post(($price_0 == 0) ? $conf_forumd_id : $conf_forum_id,$subject,$message);
					$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
						SET f1 = '$url'
						WHERE id = $lot_id";
					if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."102". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);

					redirect(( $url != '' ) ? $url : append_sid("aukz.$phpEx" . '?start=' . $start . '&opn=' . $opn));
				}
			}
		} // добавили новый лот с пользовательской страницы

	//------------------------------------------------------------------------------
	// Смотрим, не хочет ли кто редактировать лот и, если да, вывод таблицы для редактирования лота
		if(request_var('edit',0)==1)
		{
			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}

			$id_lota = request_var('id',0);

			$sql = "SELECT user, time_auk, user_5, time_5, name1, name2, name3, name4, price_0, price_b, price_5, closed, pause, az1, adm
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."107". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			if ( !($row = $db->sql_fetchrow($result)) )
			{
				$message = $user->lang['AZ_EDIT_NO_LOT'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			$db-> sql_freeresult($result);

			$user_lota = $row['user'];
			$pause = $row['pause'];
			$closed = $row['closed'];
			$name1 = $row['name1'];
			$name2 = $row['name2'];
			$name3 = $row['name3'];
			$name4 = $row['name4'];
			$price_0 = $row['price_0'];
			$price_b = $row['price_b'];
			$price_5 = $row['price_5'];
			$user_5 = $row['user_5'];
			$time_last = $row['time_5'];
			$time_auk = $row['time_auk'];
			$test_az1 = $row['az1'];
			$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';
			$adm = $conf_user_edit ? 0 : $row['adm'];
			if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) ) $adm = 0;

			if ( !($auth->acl_get('a_') OR ($user_id == $conf_adm_id)) )
			{				if ( $user_lota != $user_id )
				{
					$message = $user->lang['AZ_EDIT_NO_EDIT'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
				if ( $adm )
				{
					$message = $user->lang['AZ_USER_CANT_EDIT'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
			}
			$edit_closed = false;
			if ( $closed )
			{				if ( ($name2 == '<hidden />') AND ($user_lota == $user_id) )
				{					$edit_closed = true;				}
				else
				{					$message = ($name2 == '<deleted />') ? $user->lang['AZ_EDIT_NO_DELETED'] : $user->lang['AZ_EDIT_NO_ACTIVE'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
			}
			else if ( !$pause )
			{
				$message = $user->lang['AZ_EDIT_NO_PAUSE'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}

			// Узнаем, сколько денег пользователя в аукционе
			if (!$acl_limited)
			{
				$sql = "SELECT SUM(price_5) AS my_bablos
					FROM " . AUKZ_MAIN_TABLE . "
					WHERE user_5 = $user_id AND closed = 0";
				if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."11". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
				$row4 = $db->sql_fetchrow($result);
				$db-> sql_freeresult($result);
				$my_bablos = $row4['my_bablos'];
			}

			$template->assign_vars(array(
				'MY_BABLOS' => ($my_bablos) ? ('(+' . '<b>' . $my_bablos . '</b>' . $points_name . $user->lang['AZ_VZALOGE']) : '',
		 		'NAL' => '<b>' . (($user_money AND !$acl_limited) ? $user_money : 0) . '</b>',
				'L_MYNAL' => $user->lang['AZ_MY_NAL'],
				'POINTS' => $points_name,
				'L_AUKZ' => $user->lang['AUKZ'],
				'L_AUKZ2' => $user->lang['AZ_AUKZ2'],
				'L_AUKZ_RELOAD' => '',
				'L_CO' => $user->lang['AZ_CO'],
				'L_OTKL' => '<h2>' . $conf_otkl . '</h2>',
				'START'	=> $start,
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_NUM' => $user->lang['AZ_NUM'],
			));
			$price = $price_0;
			if ( $price_5 > $price ) $price = $price_5;
			if ( $price_b )
			{				if ( $price >= $price_b ) $price_b = 0;			}
			$time_close = max($conf_antist+$time_last, $time_auk);
			$time_ost_temp = floor( ($time_close-$time_now)/60 ) +1; // осталось в минутах до завершения
			if ( $time_ost_temp >= 60 )
			{				$time_ost_hours = floor(($time_ost_temp+59)/60);
				$time_ost_minutes = '00';
			}
			else
			{				$time_ost_hours = 1;
				$time_ost_minutes = '00';
			}

			$st_html_edit = $use_simple_buttons
						  ? '<span title="' . $user->lang['AZ_SAVE_U_TOOLTIP'] . '"><input type="submit" name="save_edit" value="' . $user->lang['AZ_SAVE_U'] . '" style="cursor:pointer" /></span>'
						  : '<input type="hidden" name="save_edit" value="' . $user->lang['AZ_SAVE_U'] . '" />'
						  . az_make_button($user->lang['AZ_SAVE_U'],$user->lang['AZ_SAVE_U_TOOLTIP'],'save_edit','',$imageset_lang_path,'aukz_go.png','save',$id_lota);
						  ;
			$st_html = '<input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $st_html_edit;

			$template->assign_block_vars('edit_lot', array(
				'L_FORMNAME' => $user->lang['AZ_FORM_EDIT'],
				'L_NUM' => $user->lang['AZ_LOTNAME'],
				'L_TEST' => $user->lang['AZ_TEST'],
				'ACTION' => 'aukz.php?start=' . $start . '&opn=' . $opn,
				'L_PASS' => $user->lang['AZ_HIDDEN'] . ($edit_closed ? $user->lang['AZ_HIDDEN_RESTORE'] : ''),
				'L_INFO' => $user->lang['AZ_ADD_INFO'],
				'L_COND' => $user->lang['AZ_ADD_COND'],
				'L_PRICE0' => $user->lang['AZ_START_PRICE2'],
				'L_BLITZ' => $user->lang['AZ_BLITZ'],
				'L_2END' => $user->lang['AZ_TIME_H_M'],
				'L_CAT' => $user->lang['AZ_CAT'],
				'L_FOTO' => $user->lang['AZ_FOTO'],
				'FILESIZE' => '32000',
				'L_DOB' => $user->lang['AZ_SAVE_U'],
				'ID' => $id_lota,
				'NAME1' => $name1,
				'NAME2' => $edit_closed ? '' : $name2,
				'NAME3' => $name3,
				'NAME4' => $name4,
				'PRICE' => $price,
				'BLITZ' => $price_b,
				'DIS' => $user_5 ? 'disabled' : '',
				'TIME_HOURS' => $time_ost_hours,
				'TIME_MINUTES' => $time_ost_minutes,
				'TEST_CHECKED' => $test_az1 ? ' checked="checked"' : '',
				'START'	=> $start,
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_SAVE_CONFIRM' => $user->lang['AZ_SAVE_U_TOOLTIP'],
				'ST_HTML' => $st_html,
				'L_FORM_ERROR' => '',
			));
		}

	//------------------------------------------------------------------------------
	// Занесение в базу информации по редактируемому лоту
		if( request_var('save_edit','') != '' )
		{
			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}

			$id_lota = request_var('hidden_lot_id',0);

			$sql = "SELECT user, user_5, name2, closed, pause, price_0, price_5, az1, adm
				FROM " . AUKZ_MAIN_TABLE . "
				WHERE id = $id_lota";
			if(!$result = $db->sql_query($sql))	message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."107". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			if ( !($row = $db->sql_fetchrow($result)) )
			{
				$message = $user->lang['AZ_EDIT_NO_LOT'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			$db-> sql_freeresult($result);

			$user_lota = $row['user'];
			$user5 = $row['user_5'];
			$adm = $conf_user_edit ? 0 : $row['adm'];
			if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) ) $adm = 0;

			if ( !($auth->acl_get('a_') OR ($user_id == $conf_adm_id)) )
			{
				if ( $user_lota != $user_id )
				{
					$message = $user->lang['AZ_EDIT_NO_EDIT'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
				if ( $adm )
				{
					$message = $user->lang['AZ_USER_CANT_EDIT'] . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
			}
			$pause = $row['pause'];
			$closed = $row['closed'];
			$name2 = $row['name2'];
			if ( $closed )
			{
				if ( ($name2 == '<hidden />') AND ($user_lota == $user_id) )
				{					$pause = $time_now;
				}
				else
				{					$message = ($name2 == '<deleted />') ? $user->lang['AZ_EDIT_NO_DELETED'] : $user->lang['AZ_EDIT_NO_ACTIVE']  . $user->lang['AZ_RETURN'];
					message_die(GENERAL_MESSAGE, $message);
				}
			}
			else if ( !$pause )
			{
				$message = $user->lang['AZ_EDIT_NO_PAUSE'] . $user->lang['AZ_RETURN'];
				message_die(GENERAL_MESSAGE, $message);
			}
			$price_0_prev = $row['price_0'];

			$name_lota = utf8_normalize_nfc(request_var('nomer','',true));
			$pass_lota = utf8_normalize_nfc(request_var('pass','',true));
			$info_lota = utf8_normalize_nfc(request_var('info','',true));
			$cond_lota = utf8_normalize_nfc(request_var('cond','',true));
			$price_0 = request_var('price_0','0');
			$price_b = request_var('price_b','0');
			// проверка, чтобы в цене были только цифры и десятичная точка, а также наличия названия и пароля:
			if ( !preg_match("/^[\d]*[\.,]?[\d]*$/", $price_0) OR !preg_match("/^[\d]*[\.,]?[\d]*$/", $price_b) OR ($name_lota=='') OR ($pass_lota==''))
			{
				//$message = $user->lang['AZ_ERR_IN_FORM'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_TEXT'];
			}
			if ( strlen($name_lota) > 31 )
			{
				//$message = $user->lang['AZ_ERR_IN_FORM1'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM1_TEXT'];
			}
			if ( strlen($pass_lota) > 63 )
			{
				//$message = $user->lang['AZ_ERR_IN_FORM2'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM2_TEXT'];
			}
			if ( strlen($info_lota) > 255 )
			{
				//$message = $user->lang['AZ_ERR_IN_FORM3'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM3_TEXT'];
			}
			if ( strlen($cond_lota) > 255 )
			{
				//$message = $user->lang['AZ_ERR_IN_FORM4'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM4_TEXT'];
			}
			$price_0 = str_replace(',','.',$price_0);
			$price_b = str_replace(',','.',$price_b);
			$name_lota = str_replace("'",'"',$name_lota);
			$pass_lota = str_replace("'",'"',$pass_lota);
			$info_lota = str_replace("'",'"',$info_lota);
			$cond_lota = str_replace("'",'"',$cond_lota);

			$disd = false;
			if ( ($conf_forum_id != 0) AND ($conf_forumd_id != 0) AND ($conf_forum_id != $conf_forumd_id) ) $disd = true;
			else if ( ($conf_forum_id == 0) AND ($conf_forumd_id != 0) ) $disd = true;
			else if ( ($conf_forum_id != 0) AND ($conf_forumd_id == 0) ) $disd = true;
			if ( $disd )
			{				if ( $price_0_prev == 0 )
				{					if ( $price_0 != 0 )
					{
						//$message = $user->lang['AZ_ERR_IN_FORM_PREV_DAROM'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_PREV_DAROM'];
					}
				}
				else
				{
					if ( $price_0 == 0 )
					{
						//$message = $user->lang['AZ_ERR_IN_FORM_NOW_DAROM'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_NOW_DAROM'];
					}
				}
			}

			if ( $price_b > 0 )
			{
				if ( $price_0 != '' )
				{
					if ( $price_0 == 0 )
					{
						//$message = $user->lang['AZ_ERR_IN_FORM_BLITZ_FOR_DAROM'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_BLITZ_FOR_DAROM'];
					}
					if ( $price_b < $price_0 )
					{
						//$message = $user->lang['AZ_ERR_IN_FORM_BLITZ_LESS_PRICE'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_BLITZ_LESS_PRICE'];
					}
				}
				else
				{
					$price_5 = $row['price_5'];
					if ( $price_b < $price_5 )
					{
						//$message = $user->lang['AZ_ERR_IN_FORM_BLITZ_LESS_PRICE'] . $user->lang['AZ_RETURN_JS'];
						//message_die(GENERAL_MESSAGE, $message);
						$form_error .= '<br />' . $user->lang['AZ_ERR_IN_FORM_BLITZ_LESS_PRICE'];
					}
				}
			}

			$endtime1 = request_var('endtime1',0);
			$endtime2 = request_var('endtime2',0);
			$category = request_var('category',0);
			$fotolot = request_var('fotolot',0);

			$end_lota = $time_now + $endtime1*3600 + $endtime2*60;
			if ( (($end_lota-$time_now)/3600 > $conf_max_srok) AND !$auth->acl_get('a_'))
			{
				//$message = $user->lang['AZ_ONLY_MAXSROK'] . $conf_max_srok . $user->lang['AZ_HS'] . $user->lang['AZ_RETURN_JS'];
				//message_die(GENERAL_MESSAGE, $message);
				$form_error .= '<br />' . $user->lang['AZ_ONLY_MAXSROK'] . $conf_max_srok . $user->lang['AZ_HS'];
			}
			$test_az1 = $row['az1'];

			if ( $form_error == '' )
			{
				if ( $price_0 != '' )
				{					$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
						SET name1 = '$name_lota',
							name2 = '$pass_lota',
							name3 = '$info_lota',
							name4 = '$cond_lota',
							closed = '0',
							pause = '$pause',
							time_auk = $end_lota,
							price_0 = $price_0,
							price_b = $price_b
						WHERE id = $id_lota";
				}
				else
				{
					$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
						SET name1 = '$name_lota',
							name2 = '$pass_lota',
							name3 = '$info_lota',
							name4 = '$cond_lota',
							closed = '0',
							pause = '$pause',
							time_auk = $end_lota,
							price_b = $price_b
						WHERE id = $id_lota";
				}
				if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."102". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);

				$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';

				// отправим ЛС автору
				$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
				$sql = "SELECT username FROM " . USERS_TABLE . " WHERE user_id = $user_id LIMIT 1";
				if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."08". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
				$row1 = $db->sql_fetchrow($result);
				$db-> sql_freeresult($result);
				$username_end = $row1['username'];
				$message = $user->lang['AZ_EDITLOT'] . $name_test . $name_lota . '"' . $user->lang['AZ_ZA'] . $price_0 . $points_name;
				adr_send_pm_az ($user_id, $subject, $message, $from_id, $double_id); // отправили сообщение хозяину лота
				redirect(append_sid("aukz.$phpEx" . '?start=' . $start . '&opn=' . $opn));
			}
		} // отредактировали лот с пользовательской страницы

	//------------------------------------------------------------------------------
	// Ошибка в форме - повторяем
		if ( ($form_error != '') AND ((request_var('save_new','') != '') OR (request_var('save_edit','') != '')) )
		{			if ($acl_limited) // гость запрашивает данную возможность
			{
				$message = $user->lang['AZ_HREN_VAM'];
				message_die(GENERAL_MESSAGE, $message);
			}

			// Узнаем, сколько денег пользователя в аукционе
			if (!$acl_limited)
			{
				$sql = "SELECT SUM(price_5) AS my_bablos
					FROM " . AUKZ_MAIN_TABLE . "
					WHERE user_5 = $user_id AND closed = 0";
				if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."11". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
				$row4 = $db->sql_fetchrow($result);
				$db-> sql_freeresult($result);
				$my_bablos = $row4['my_bablos'];
			}

			$est_comiss = ( ($conf_type_plata==1 AND $conf_plata_nominal) OR ($conf_type_plata==2 AND $conf_plata_procent) ) ? 1 : 0;
			$template->assign_vars(array(
				'MY_BABLOS' => ($my_bablos) ? ('(+' . '<b>' . $my_bablos . '</b>' . $points_name . $user->lang['AZ_VZALOGE']) : '',
		 		'NAL' => '<b>' . (($user_money AND !$acl_limited) ? $user_money : 0) . '</b>',
				'L_MYNAL' => $user->lang['AZ_MY_NAL'],
				'POINTS' => $points_name,
				'L_AUKZ' => $user->lang['AUKZ'],
				'L_AUKZ2' => $user->lang['AZ_AUKZ2'],
				'L_AUKZ_RELOAD' => '',
				'L_CO' => $user->lang['AZ_CO'],
				'L_OTKL' => '<h2>' . $conf_otkl . '</h2>',
				'START'	=> $start,
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_NUM' => $user->lang['AZ_NUM'],
			));

			$new = (request_var('save_new','') != '') ? true : false;
			if ( $new )
			{
				$st_html_add = $use_simple_buttons
							  ? '<span title="' . $user->lang['AZ_ADD_U_TOOLTIP'] . '"><input type="submit" name="save_new" value="' . $user->lang['AZ_ADD_U'] . '" style="cursor:pointer" /></span>'
							  : '<input type="hidden" name="save_new" value="' . $user->lang['AZ_ADD_U'] . '" />'
							  . az_make_button($user->lang['AZ_ADD_U'],$user->lang['AZ_ADD_U_TOOLTIP'],'save_new','',$imageset_lang_path,'aukz_go.png','add');
							  ;
				$st_html = '<input type="hidden" name="hidden_lot_id" value="0" />' . $st_html_add;
			}
			else
			{				$st_html_edit = $use_simple_buttons
							  ? '<span title="' . $user->lang['AZ_SAVE_U_TOOLTIP'] . '"><input type="submit" name="save_edit" value="' . $user->lang['AZ_SAVE_U'] . '" style="cursor:pointer" /></span>'
							  : '<input type="hidden" name="save_edit" value="' . $user->lang['AZ_SAVE_U'] . '" />'
							  . az_make_button($user->lang['AZ_SAVE_U'],$user->lang['AZ_SAVE_U_TOOLTIP'],'save_edit','',$imageset_lang_path,'aukz_go.png','save',$id_lota);
							  ;
				$st_html = '<input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $st_html_edit;
			}

			$template->assign_block_vars($new ? 'new_lot' : 'edit_lot', array(
				'L_FORMNAME' => $new ? $user->lang['AZ_FORM_NEW'] : $user->lang['AZ_FORM_EDIT'],
				'L_NUM' => $user->lang['AZ_LOTNAME'],
				'L_TEST' => $user->lang['AZ_TEST'],
				'ACTION' => 'aukz.php?start=' . $start . '&opn=' . $opn,
				'L_PASS' => $user->lang['AZ_HIDDEN'] . ($new ? '' :  ($edit_closed ? $user->lang['AZ_HIDDEN_RESTORE'] : '')),
				'L_INFO' => $user->lang['AZ_ADD_INFO'],
				'L_COND' => $user->lang['AZ_ADD_COND'],
				'L_PRICE0' => $user->lang['AZ_START_PRICE2'],
				'L_2END' => $user->lang['AZ_TIME_H_M'],
				'L_BLITZ' => $user->lang['AZ_BLITZ'],
				'COMISS1' => ($est_comiss) ? $user->lang['AZ_COMISS00'] : $user->lang['AZ_NO_COMISS'],
				'COMISS2' => (!$est_comiss) ? '' : ( ($conf_type_plata==1 AND $conf_plata_nominal) ? $user->lang['AZ_COMISS10'] .$conf_plata_nominal.$points_name : $user->lang['AZ_COMISS20'] .$conf_plata_procent. $user->lang['AZ_COMISS21'] ),
				'L_CAT' => $user->lang['AZ_CAT'],
				'L_FOTO' => $user->lang['AZ_FOTO'],
				'FILESIZE' => '32000',
				'L_DOB' => $new ? $user->lang['AZ_ADD_U'] : $user->lang['AZ_SAVE_U'],
				'ID' => $id_lota,
				'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
				'L_ADD_CONFIRM' => $new ? $user->lang['AZ_ADD_U_TOOLTIP'] : $user->lang['AZ_SAVE_U_TOOLTIP'],
				'ST_HTML' => $st_html,
				'L_FORM_ERROR' => '<span class="gen"><center><b><font color="red">' . $user->lang['AZ_ERR_IN_FORM_HEAD'] . '<br />' . $form_error . '</font></b></center><br /></span>',
				'START'	=> $start,
				'NAME1' => $name_lota,
				'NAME2' => $new ? $pass_lota : ($edit_closed ? '' : $pass_lota),
				'NAME3' => $info_lota,
				'NAME4' => $cond_lota,
				'PRICE' => $price_0,
				'BLITZ' => $price_b,
				'TIME_HOURS' => $endtime1,
				'TIME_MINUTES' => $endtime2,
				'DIS' => $user_5 ? 'disabled' : '',
				'TEST_CHECKED' => $test_az1 ? ' checked="checked"' : '',
			));

//			redirect(append_sid("aukz.$phpEx" . '?start=' . $start . '&opn=' . $opn));
		}

	//------------------------------------------------------------------------------
	// Посмотрим, не завершились ли какие аукционы и, если завершились, поставить отметку о завершении, отослать ЛС, и редирект сделать
		$sql = "SELECT *
			FROM " . AUKZ_MAIN_TABLE . "
			WHERE closed = 0 AND pause = 0";
		if(!$result = $db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."06". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
		$rows = $db->sql_fetchrowset($result);
		$db-> sql_freeresult($result);
		if (count($rows))
		{
			$conf_antist = ($conf_antis) ? $conf_antist : 0;
			for($i = 0; $i < count($rows); $i++)
			{
				$time_last = $rows[$i]['time_5'];
				$time_auk = $rows[$i]['time_auk'];
				$time_close = max($conf_antist+$time_last, $time_auk);
				$id_lota = $rows[$i]['id'];

				if (($time_close <= $time_now) OR ($lot_id_darom == $id_lota) OR ($lot_id_blitz == $id_lota)) // закроем прошедшие аукционы и переведем деньги выставившим лоты
				{
					$user_lota = $rows[$i]['user'];
					$name_lota = $rows[$i]['name1'];
					$price_now = $rows[$i]['price_5'];
					$user_now = $rows[$i]['user_5'];
					$comiss = ($conf_type_plata == 2) ? ($price_now*$conf_plata_procent/100) : 0;
					$test_az1 = $row['az1'];
					$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';

					$sql = "UPDATE " . AUKZ_MAIN_TABLE . " AS au, " . USERS_TABLE . " AS us
						SET au.closed = $time_now,
						au.name2 = '<hidden />',
						us.user_money = us.user_money + $price_now - $comiss
						WHERE au.id = $id_lota AND us.user_id = $user_lota";
					if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."07". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
					if ( $user_id == $user_lota ) $user_money = $user_money - $comiss;
					// отправим ЛС автору и победителю
					$subject = '[ ' . $user->lang['AUKZ'] . ': ' . $user->lang['AZ_NUM'] . ' #' . $id_lota . ' ]';
					if ($lot_id_darom == $id_lota) $user_now = $user_id;	// даром
					if ($user_now) // если есть хоть одна ставка
					{
						$sql = "SELECT username FROM " . USERS_TABLE . " WHERE user_id = $user_now LIMIT 1";
						if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."08". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
						$row1 = $db->sql_fetchrow($result);
						$db-> sql_freeresult($result);
						$username_end = $row1['username'];
						$dest_user = $user_lota;
						$comisstext = ($comiss) ? $user->lang['AZ_COMISSIA'] . $conf_plata_procent . $user->lang['AZ_COMISSIA_PROCENT'] . $comiss . $points_name : '';
						$message_bay = ($lot_id_darom == $id_lota) ? $user->lang['AZ_GET'] : $user->lang['AZ_BAY'];
						$message_add = ($config['allow_html']==1) ? $message_bay . '<a href = "profile.php?mode=viewprofile&u=' . $user_now . '">' . $username_end . '</a>' . $user->lang['AZ_ZA'] : $message_bay . $username_end . $user->lang['AZ_ZA'];
						$message = $user->lang['AZ_ULOT'] . $name_test . $name_lota . $message_add . $price_now . $points_name . $comisstext . $user->lang['AZ_ADDINFO_2PM'];
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id); // отправили сообщение старому хозяину лота

						$pass_lota = $rows[$i]['name2'];
						$sql = "SELECT username FROM " . USERS_TABLE . " WHERE user_id = $user_lota LIMIT 1";
						if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."09". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
						$row1 = $db->sql_fetchrow($result);
						$db-> sql_freeresult($result);
						$username_avtor = $row1['username'];
						$dest_user = $user_now;
						$message_ubay = ($lot_id_darom == $id_lota) ? $user->lang['AZ_UGET'] : $user->lang['AZ_UBAY'];
						$message_add = ($config['allow_html']==1) ? '<a href = "profile.php?mode=viewprofile&u=' . $user_lota . '">' . $username_avtor . '</a>' : $username_avtor;
						$message = $message_ubay . $name_test . $name_lota . $user->lang['AZ_ADDINFO'] . $pass_lota . $user->lang['AZ_GO2PREV_U'] . $message_add;
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id); // отправили сообщение новому хозяину лота

						if ( $comiss )
						{							$dest_user = $from_id;
							$message = $user->lang['AZ_COMISS1_ADMIN'] . $name_test . $name_lota . $user->lang['AZ_COMISS2_ADMIN'] . $conf_plata_procent . $user->lang['AZ_COMISSIA_PROCENT'] . $comiss . $points_name;
							adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id); // отправили сообщение аукционисту
							// перечислили комиссию
							if ( $from_id )
							{
								$sql = "UPDATE " . USERS_TABLE . "
										SET user_money = user_money + $comiss
										WHERE user_id = $from_id";
								if (!$db->sql_query($sql)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."07". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
							}
						}
					}
					else // отправим автору сообщение, что лот не сыграл
					{
						$dest_user = $user_lota;
						$message = $user->lang['AZ_AUK_ULOT'] . $name_test . $name_lota . $user->lang['AZ_CLOSED0'];
						adr_send_pm_az ($dest_user, $subject, $message, $from_id, $double_id); // отправили сообщение старому хозяину лота
					}
				}
			}
		} // закрыли прошедшие аукционы

	//------------------------------------------------------------------------------
		// **********************
		// КАТЕГОРИИ

		// КАТЕГОРИИ
		// **********************

	//------------------------------------------------------------------------------
	// Выводим активные аукционы, если не требуется переход на создание нового лота или редактирование
		if( (request_var('new',0) == 0) AND (request_var('edit',0) == 0) AND ($form_error == ''))
		{
			// поле сортировки
			$sql = "SELECT user_az_sort_field, user_az_sort_dir FROM " . USERS_TABLE . " WHERE user_id = $user_id ";
			if ( ($result = $db->sql_query($sql)) )
			{
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);

				$user_sort_field = $row['user_az_sort_field'];  // 0 - до окончания, 1 - текущая ставка, 2 - id лота
				$user_sort_dir = $row['user_az_sort_dir'];      // 0 - decs, 1 - asc
			}
			else
			{				$user_sort_field = $user->data['user_az_sort_field'];  // 0 - до окончания, 1 - текущая ставка, 2 - id лота
				$user_sort_dir = $user->data['user_az_sort_dir'];      // 0 - decs, 1 - asc
			}

			// изменение поля сортировки
			$new_sort_field = request_var('sort',-1);
			if ( $new_sort_field >= 0 )
			{				if ( $new_sort_field > 2 ) $new_sort_field = 0;
				if ( $new_sort_field == $user_sort_field )
				{					$user_sort_dir = 1 - $user_sort_dir;				}
				else
				{					$user_sort_field = $new_sort_field;
				}
				$sql_user_sort_field = 'user_az_sort_field = ' . $user_sort_field;
				$sql_user_sort_dir = 'user_az_sort_dir = ' . $user_sort_dir;
				$sql = "UPDATE " . USERS_TABLE . "
						SET $sql_user_sort_field, $sql_user_sort_dir
						WHERE user_id = $user_id";
				$db->sql_query($sql);
			}

			$arrow2 = '';
			$arrow1 = '';
			$arrow0 = '';
			$user_sort_dir_name = ($user_sort_dir == 0) ? 'DESC' : 'ASC';
			if ( $user_sort_field == 2 )
			{				$user_sort_field_name = 'id';
				$arrow2 = ($user_sort_dir_name == 'ASC') ? '&#9660;' : '&#9650;';
			}
			else if ( $user_sort_field == 1 )
			{				$user_sort_field_name = 'price_5+closed+closed';
				$arrow1 = ($user_sort_dir_name == 'ASC') ? '&#9660;' : '&#9650;';
			}
			else
			{				$user_sort_field_name = $time_now . '-time_auk-pause-closed-closed';
				$arrow0 = ($user_sort_dir_name == 'ASC') ? '&#9650;' : '&#9660;';
			}

			$sql_count = "SELECT COUNT(id) AS num_lots
					FROM " . AUKZ_MAIN_TABLE . "
					WHERE closed = 0";
			$sql_select = "SELECT *
					FROM " . AUKZ_MAIN_TABLE . "
					WHERE closed = 0";
			$sql_order = " ORDER BY $user_sort_field_name $user_sort_dir_name";
			if ( $name_access == 2 )	// включаем историю
			{				$time_closed_history = $time_now - $conf_history_depth;
				if ($auth->acl_get('a_') OR ($user_id == $conf_adm_id))
				{
					$sql_count .= " OR closed > $time_closed_history";
					$sql_select .= " OR closed > $time_closed_history";
				}
				else
				{
					$sql_count .= " OR ((closed > $time_closed_history) AND (user = $user_id) AND (name2 = '<hidden />'))";
					$sql_select .= " OR ((closed > $time_closed_history) AND (user = $user_id) AND (name2 = '<hidden />'))";
				}
			}

			if(!$result = $db->sql_query($sql_count)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."10". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$total_lots = (int) $db->sql_fetchfield('num_lots');
			$db->sql_freeresult($result);

			if(!$result = $db->sql_query($sql_select . $sql_order)) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."10". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
			$rows = $db->sql_fetchrowset($result);
			$db->sql_freeresult($result);

			$view_name = '';
			if (!$acl_limited)
			{				if ( !$name_access )
				{					if ( $conf_price_2>0 )
					{						$view_name = ($auth->acl_get('a_')) ? $user->lang['AZ_ADMIN_LIDERNAMES'] : ($user->lang['AZ_LIDERNAMES'] . $conf_price_2 . $points_name. ')');
					}
					else
					{						$view_name = ($auth->acl_get('a_')) ? $user->lang['AZ_ADMIN_LIDERNAMES'] : $user->lang['AZ_FREE_LIDERNAMES'];
					}
				}
				else
				{					$view_name = ($auth->acl_get('a_')) ? $user->lang['AZ_ADMIN_SKRYT'] : $user->lang['AZ_FREE_SKRYT'];
				}
			}

			$az_config  = $user->lang['AZ_HELP_CONFIG_TOP'];
			$az_config .= $user->lang['AZ_HELP_CONFIG0']  . $points_name . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG1']  . (($conf_type_plata==1) ? ($conf_plata_nominal . $points_name) : ($conf_plata_procent . $user->lang['AZ_PROC_END'])) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG2']  . $conf_max_srok . $user->lang['AZ_HS'] . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG3']  . (($conf_type_price==1) ? ($conf_price_1 . $points_name) : ($conf_price_1p . $user->lang['AZ_PROC_LAST'])) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG7']  . $conf_price_2 . $points_name . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG4']  . ($conf_antis ? ($user->lang['AZ_YES'] . ', ' . ($conf_antist/60) . $user->lang['AZ_MS']) : ($user->lang['AZ_NO'])) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG5']  . ($conf_history_st ? $user->lang['AZ_YES'] : $user->lang['AZ_NO']) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG6']  . ($conf_history_depth/3600/24) . $user->lang['AZ_DS'] . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG8']  . ($conf_history_st ? $user->lang['AZ_YES'] : $user->lang['AZ_NO']) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG9']  . ($conf_history_st ? $user->lang['AZ_YES'] : $user->lang['AZ_NO']) . ';</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG10'] . ($conf_history_st ? $user->lang['AZ_YES'] : $user->lang['AZ_NO']) . '.</li>';
			$az_config .= $user->lang['AZ_HELP_CONFIG_BOTTOM'];

			if (!count($rows))
			{
				$start = 0;
				$template->assign_vars(array(
					'MY_BABLOS' => ($my_bablos) ? ('(+' . '<b>' . $my_bablos . '</b>' . $points_name . $user->lang['AZ_VZALOGE']) : '',
			 		'NAL' => '<b>' . ((!$acl_limited) ? $user_money : 0) . '</b>',
					'POINTS' => $points_name,
					'L_AUKZ' => $user->lang['AUKZ'],
					'L_AUKZ2' => $user->lang['AZ_AUKZ2'],
					'L_AUKZ_RELOAD' => ' : ' . $user->lang['AZ_RELOAD'],
					'L_MYNAL' => $user->lang['AZ_MY_NAL'],
					'L_CO' => $user->lang['AZ_CO'],
					'L_OTKL' => '<h2>' . $conf_otkl . '</h2>',
					'L_AZ_HELP_CONFIG' => $az_config,
					'START'	=> $start,
					'OPN' => $opn,
					'OPNNEW' => $opn ? 0 : 1,
				));
				$template->assign_block_vars('active', array(
					'L_NUM' => $user->lang['AZ_NUM'],
					'VIEW_NAME' => $view_name,
					'L_VYST' => ($acl_limited) ? '' : $user->lang['AZ_PUT_UNUM'],
					'L_STAVKA' => $user->lang['AZ_NOW_ST'],
					'L_DO_ST' => $user->lang['AZ_DO_ST'],
					'L_2END' => $user->lang['AZ_2END'],
					'L_AZ_HELP_SHOW_HIDE' => (($user_money > 0) AND !$acl_limited) ? $user->lang['AZ_HELP_SHOW'] : $user->lang['AZ_HELP_HIDE'],
					'L_AZ_HELP_STYLE_DIV' => (($user_money > 0) AND !$acl_limited) ? ' style="display: none;"' : '',
					'L_AZ_HELP_MONEY' => $user->lang['AZ_HELP_MONEY_PREF']
									   . az_make_num_button('+ 100','','',$imageset_lang_path,100,'+','adm_m1') . $user->lang['AZ_HELP_MONEY_100']
									   . az_make_num_button('+ 1000','','',$imageset_lang_path,1000,'+','adm_m2') . $user->lang['AZ_HELP_MONEY_1000']
									   . az_make_num_button('+ 10 000','','',$imageset_lang_path,10000,'+','adm_m3') . $user->lang['AZ_HELP_MONEY_10000']
									   . az_make_num_button('+ 100 000','','',$imageset_lang_path,100000,'+','adm_m4') . $user->lang['AZ_HELP_MONEY_100000']
									   . az_make_num_button('= 0','','',$imageset_lang_path,0,'=','adm_m0') . $user->lang['AZ_HELP_MONEY_0']
									   . $user->lang['AZ_HELP_MONEY_SUFF'],
				));
				// выводим пустую таблицу, с сообщением "нет ни одного лота"
				$template->assign_block_vars('active.nolots', array(
					'L_NOLOTS' => $user->lang['AZ_NOLOTS_NOW']
				));
			}
			else
			{				$adm_u = ($auth->acl_get('a_') OR ($user_id == $conf_adm_id)) ? true : false;
				$end = $start + $config['posts_per_page'];
				if ( $end > count($rows) ) $end = count($rows);
				while ( ($start >= $end) AND ($start > 0) )
				{					$start -= $config['posts_per_page'];
					if ( $start < 0 ) $start = 0;
				}
				$end = $start + $config['posts_per_page'];
				if ( $end > count($rows) ) $end = count($rows);
				for($i = $start; $i < $end; $i++)
				{
					if ( ($rows[$i]['closed'] == 0) OR ($rows[$i]['name2'] == '<hidden />') )
					{						$user_lota = $rows[$i]['user'];
						if ($user_lota == $user_id) $adm_u = true;
					}
				}
				// страницы
				include_once($phpbb_root_path . 'includes/functions_display.' . $phpEx);
				// Узнаем, сколько денег пользователя в аукционе
				if (!$acl_limited)
				{
					$sql = "SELECT SUM(price_5) AS my_bablos
						FROM " . AUKZ_MAIN_TABLE . "
						WHERE user_5 = $user_id AND closed = 0";
					if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."11". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);
					$row4 = $db->sql_fetchrow($result);
					$db-> sql_freeresult($result);
					$my_bablos = $row4['my_bablos'];
				}
				$template->assign_vars(array(
					'MY_BABLOS' => ($my_bablos) ? ('(+' . '<b>' . $my_bablos . '</b>' . $points_name . $user->lang['AZ_VZALOGE']) : '',
			 		'NAL' => '<b>' . ((!$acl_limited) ? $user_money : 0) . '</b>',
					'POINTS' => $points_name,
					'L_AUKZ' => $user->lang['AUKZ'],
					'L_AUKZ2' => $user->lang['AZ_AUKZ2'],
					'L_AUKZ_RELOAD' => ' : ' . $user->lang['AZ_RELOAD'],
					'L_MYNAL' => $user->lang['AZ_MY_NAL'],
					'L_CO' => $user->lang['AZ_CO'],
					'L_OTKL' => '<h2>' . $conf_otkl . '</h2>',
					'L_AZ_HELP_CONFIG' => $az_config,
					'PAGINATION'	=> generate_pagination(append_sid("{$phpbb_root_path}aukz.$phpEx") . '?opn=' . $opn, count($rows), $config['posts_per_page'], $start),
					'PAGE_NUMBER'	=> on_page(count($rows), $config['posts_per_page'], $start),
					'TOTAL_LOTS'    => $user->lang['AZ_TOTAL_LOTS'] . ': ' . count($rows),
					'START'	=> $start,
					'OPN' => $opn,
					'OPNNEW' => $opn ? 0 : 1,
					'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
					'L_NUM' => $user->lang['AZ_NUM'],
				));
					$template->assign_block_vars('active', array(
					'L_NUM' => $user->lang['AZ_NUM_NOSPACE'],
					'L_NUM_NAME' => $user->lang['AZ_NUM_NAME'],
					'L_STAVKA' => $user->lang['AZ_NOW_ST'],
					'L_DO_ST' => $user->lang['AZ_DO_ST'],
					'L_2END' => $user->lang['AZ_2END'],
					'ADM_U' => $adm_u ? ('<th>' . $user->lang['AZ_ADMINISTRATE'] . '</th>') : '',
					'VIEW_NAME' => $view_name,
					'L_VYST' => ($acl_limited) ? '' : $user->lang['AZ_PUT_UNUM'],
					'ARROW2' => $arrow2,
					'ARROW1' => $arrow1,
					'ARROW0' => $arrow0,
					'L_DO_CONFIRM' => $user->lang['AZ_PRICE_BUTTON_PLUS_TOOLTIP'],
					'L_GET_CONFIRM' => $user->lang['AZ_GET_DAROM_TOOLTIP'],
					'L_BLITZ_CONFIRM' => $user->lang['AZ_BLITZ_BUTTON_PLUS_TOOLTIP'],
					'L_GO_CONFIRM' => $user->lang['AZ_GO_TOOLTIP'],
					'L_PAUSE_CONFIRM' => $user->lang['AZ_PAUSE_TOOLTIP'],
					'L_EDIT_CONFIRM' => $user->lang['AZ_EDIT_TOOLTIP'],
					'L_DELETE_CONFIRM' => $user->lang['AZ_DELETE_TOOLTIP'],
					'L_DELETE_FOREVER_CONFIRM' => $user->lang['AZ_DELETE_FOREVER_TOOLTIP'],
					'USE_JS_CONFIRM' => $use_adm_checkbox ? false : true,
					'L_AZ_HELP_SHOW_HIDE' => (($user_money > 0) AND !$acl_limited) ? $user->lang['AZ_HELP_SHOW'] : $user->lang['AZ_HELP_HIDE'],
					'L_AZ_HELP_STYLE_DIV' => (($user_money > 0) AND !$acl_limited) ? ' style="display: none;"' : '',
					'L_AZ_HELP_MONEY' => $user->lang['AZ_HELP_MONEY_PREF']
									   . az_make_num_button('+ 100','','',$imageset_lang_path,100,'+','adm_m1') . $user->lang['AZ_HELP_MONEY_100']
									   . az_make_num_button('+ 1000','','',$imageset_lang_path,1000,'+','adm_m2') . $user->lang['AZ_HELP_MONEY_1000']
									   . az_make_num_button('+ 10 000','','',$imageset_lang_path,10000,'+','adm_m3') . $user->lang['AZ_HELP_MONEY_10000']
									   . az_make_num_button('+ 100 000','','',$imageset_lang_path,100000,'+','adm_m4') . $user->lang['AZ_HELP_MONEY_100000']
									   . az_make_num_button('= 0','','',$imageset_lang_path,0,'=','adm_m0') . $user->lang['AZ_HELP_MONEY_0']
									   . $user->lang['AZ_HELP_MONEY_SUFF'],
				));

				$row = 'row1';
				for($i = $start; $i < $end; $i++)
				{
					$id_lota = $rows[$i]['id'];
					$user_lota = $rows[$i]['user'];
					$num_icq = $rows[$i]['name1'];
					$deleted = $rows[$i]['name2'];
					$icq_info = $rows[$i]['name3'];
					$icq_cond = $rows[$i]['name4'];
					$price_0 = $rows[$i]['price_0'];
					$price_b = $rows[$i]['price_b'];
					$price_now = $rows[$i]['price_5'];
					$user_now = $rows[$i]['user_5'];
					$closed = $rows[$i]['closed'];
					$pause = $rows[$i]['pause'];
					$url = $rows[$i]['f1'];
					$time_0 = $rows[$i]['time_0'];
					$time_last = $rows[$i]['time_5'];
					$time_auk = $rows[$i]['time_auk'];
					$time_close = max($conf_antist+$time_last, $time_auk);
					$test_az1 = $rows[$i]['az1'];
					$name_test = $test_az1 ? $user->lang['AZ_LOT_FOR_TEST'] : '';
					$adm_go = $conf_user_go ? 0 : $rows[$i]['adm'];
					$adm_edit = $conf_user_edit ? 0 : $rows[$i]['adm'];
					$adm_delete = $conf_user_delete ? 0 : $rows[$i]['adm'];
					if ( $auth->acl_get('a_') OR ($user_id == $conf_adm_id) )
					{						$adm_go = 0;
						$adm_edit = 0;
						$adm_delete = 0;
					}

					$time_ost_temp = floor( ($time_close-$time_now)/60 ) +1; // осталось в минутах до завершения
					$time_ost_temp = ($time_ost_temp<10) ? '0' . $time_ost_temp : $time_ost_temp;
					if ( $time_ost_temp < 60 )
					{
						$time_ost = '0/00:' . $time_ost_temp;
					}
					else if ( $time_ost_temp < (60*24) )
					{
						$time_ost = '0/' . ((floor($time_ost_temp/60) < 10) ? '0' : '') . floor($time_ost_temp/60) . ':00';
					}
					else
					{						$time_ost = floor($time_ost_temp/(60*24)) . '/' . ((floor(($time_ost_temp % (60*24))/60) < 10 ) ? '0': '' ) . floor(($time_ost_temp % (60*24))/60) . ':00';
					}
					if ($rows[$i]['pause']) $time_ost = '<font color="maroon">' . $user->lang['AZ_TORG_PAUSED'] . '</font><br />' . aukz_date($user->lang['AZ_TIME_V'],$pause);

					$price_add = $conf_type_price ? $conf_price_1 : round($conf_price_1p*($price_now/100));
					if( $price_add <= 0 ) $price_add = 1;

					$price_min = ($price_0>$price_now) ? $price_0 : ( ($price_0==$price_now) ? $price_0+$price_add : $price_now+$price_add );
					if ($price_0 == 0.00 OR $price_0 == 0)
					{						$price_min = 0;
						$price_darom = true;
					}
					else
					{						$price_darom = false;
					}
					$dis = ($acl_limited OR $price_min>$user_money OR $rows[$i]['pause']) ? 'disabled' : ''; // если не хватает денег, или гость, или пауза, то ввод неактивен
					$price_plus = ($price_now>0) ? ($price_now+$price_add) : $price_0;

					if ( $acl_limited ) $color = 'black';
					else if ( $user_now == $user_id ) $color = 'blue';
					else if ( $user_lota == $user_id ) $color = 'blue';
					else if ( $price_min>$user_money ) $color = 'red';
					else $color = 'green';

					$bl_html = '';
					$bl_hidden_html = '';
					if ($user_id == $user_now)
					{
						$st_html = $user->lang['AZ_MY_ST'];
					}
					else if ($user_id == $user_lota)
					{
						$st_html = $user->lang['AZ_MY_LOT'];
					}
					else
					{
						if ( $price_darom )
						{							$st_html_get_darom = $use_simple_buttons
											   ? '<input type="text" name="do_stavka1hide" size="10" maxlenth="10" value="1" style="visibility:hidden" />&nbsp;&nbsp;'
											   . '<span title="' . $user->lang['AZ_GET_DAROM_TOOLTIP'] . '"><input type="submit" name="do_stavka2" value="' . $user->lang['AZ_GET_DAROM'] . '" ' . (($dis == '') ? 'style="cursor:pointer" ' : '') . $dis . ' /></span>'
											   : '<input type="hidden" name="do_stavka2" value="' . $user->lang['AZ_GET_DAROM'] . '" />'
											   . az_make_button($user->lang['AZ_GET_DAROM'],$user->lang['AZ_GET_DAROM_TOOLTIP'],'post_active1_' . $id_lota,$dis,$imageset_lang_path,'aukz_get.png','get',$id_lota);
											   ;							$st_html = '<input type="hidden" name="do_stavka1" size="10" maxlenth="10" value="1" style="visibility:hidden" />' . $st_html_get_darom;
						}
						else
						{							$st_style = ' style="vertical-align: middle; font-weight: bold; color: ' . (($dis == '') ? 'black' : ' darkgray') . '"';
							$st_html_price_before = ( $dis == '' ) ? ('<span title="' . $user->lang['AZ_PRICE_PLUS_TOOLTIP'] . '">') : ('<span title="' . $user->lang['AZ_PRICE_PLUS_DISABLED_TOOLTIP'] . '">');
							$st_html_price_after = '</span>';
							if ( $use_simple_buttons )
							{								$st_html_button_before = ( $dis == '' ) ? ('<span title="' . $user->lang['AZ_PRICE_BUTTON_PLUS_TOOLTIP'] . '">') : ('<span title="' . $user->lang['AZ_PRICE_BUTTON_PLUS_DISABLED_TOOLTIP'] . '">');
								$st_html_button_after = '</span>';
								$st_html_button = $st_html_button_before . '<input type="submit" name="do_stavka2" value="' . $user->lang['AZ_DO'] . '" ' . (($dis == '') ? 'style="cursor:pointer" ' : '') . $dis . ' />' . $st_html_button_after;
							}
							else
							{								$st_html_button = '<input type="hidden" name="do_stavka2" value="' . $user->lang['AZ_DO'] . '" />'
												. az_make_button($user->lang['AZ_DO'],( $dis == '' ) ? $user->lang['AZ_PRICE_BUTTON_PLUS_TOOLTIP'] : $user->lang['AZ_PRICE_BUTTON_PLUS_DISABLED_TOOLTIP'],'post_active1_' . $id_lota,$dis,$imageset_lang_path,'aukz_stake.png','do',$id_lota)
												;							}
							$st_html_price = '<input type="text" name="do_stavka1" size="10" maxlenth="10"' . $st_style . ' value="' . $price_plus . '" ' . $dis . ' />';
							$st_html = $st_html_price_before . $st_html_price . $st_html_price_after . '&nbsp;&nbsp;' . $st_html_button;
							if ( $price_b > $price_plus )
							{								$disbl = ($user_money >= $price_b) ? $dis : 'disabled';
								$bl_style = ' style="font-weight: bold; color: ' . ($disbl ? 'darkgray' : 'black') . '; background-color: lightgray; vertical-align: middle"';
								$bl_html_price_before = ( $disbl == '' ) ? ('<span title="' . $user->lang['AZ_BLITZ_TOOLTIP'] . '">') : ('<span title="' . $user->lang['AZ_BLITZ_DISABLED_TOOLTIP'] . '">');
								$bl_html_price_after = '</span>';
								if ( $use_simple_buttons )
								{									$bl_html_button_before = ( $disbl == '' ) ? ('<span title="' . $user->lang['AZ_BLITZ_BUTTON_PLUS_TOOLTIP'] . '">') : ('<span title="' . $user->lang['AZ_BLITZ_BUTTON_PLUS_DISABLED_TOOLTIP'] . '">');
									$bl_html_button_after = '</span>';
									$bl_html_button = $bl_html_button_before . '<input type="submit" name="do_stavkab" value="' . $user->lang['AZ_DO_BLITZ'] . '" ' . (($disbl == '') ? 'style="cursor:pointer" ' : '') . $disbl . ' />' . $bl_html_button_after;
								}
								else
								{									$bl_html_button = '<input type="hidden" name="do_stavkab" value="' . $user->lang['AZ_DO_BLITZ'] . '" />'
													. az_make_button($user->lang['AZ_DO_BLITZ'],( $disbl == '' ) ? $user->lang['AZ_BLITZ_BUTTON_PLUS_TOOLTIP'] : $user->lang['AZ_BLITZ_BUTTON_PLUS_DISABLED_TOOLTIP'],'post_active2_' . $id_lota,$disbl,$imageset_lang_path,'aukz_blitz.png','blitz',$id_lota)
													;								}
								$bl_html = '<div style="padding-top:4px"><input type="hidden" name="hidden_price_b" value="' . $price_b .'" />' . $bl_html_price_before . '<input type="text" name="do_stavkab1" size="10" maxlenth="10"' . $bl_style . ' value="' . $price_b . '" disabled />' . $bl_html_price_after . '&nbsp;&nbsp;' . $bl_html_button . '</div>';
							}
						}
					}

					$username_now = '';
					$username_lota = '';
					$username_all = '';
					if ($name_access)
					{						if ($user_now)
						{
							$sql = "SELECT username, user_colour FROM " . USERS_TABLE . " WHERE user_id = $user_now LIMIT 1";
							if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."12". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
							$row1 = $db->sql_fetchrow($result);
							$db->sql_freeresult($result);
							$username_now = '<br />' . $user->lang['AZ_LASTPRICE'] . '&nbsp;' . get_username_string('full', $user_now, $row1['username'], $row1['user_colour']);
						}
						if ($user_lota)
						{
							$sql = "SELECT username, user_colour FROM " . USERS_TABLE . " WHERE user_id = $user_lota LIMIT 1";
							if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."12". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
							$row2 = $db->sql_fetchrow($result);
							$db->sql_freeresult($result);
							$username_lota = '<br />' . $user->lang['AZ_LOT_SET'] . '&nbsp;' . get_username_string('full', $user_lota, $row2['username'], $row2['user_colour']);
						}

						if ($auth->acl_get('a_') OR ($user_id == $conf_adm_id))
						{							$username_lota .= '<br />' . aukz_date($user->lang['AZ_TIME_V'],$time_0);

							$history = $rows[$i]['history_st'];
							if ($history != '')
							{								$hist3_array = explode('|', $history);
								$block_h='';
								for ($j=0; $j<count($hist3_array); $j++)
								{
									$hist1_array = explode (';',$hist3_array[$j]);
									$temp_id = $hist1_array[0];
									$sql = "SELECT username, user_colour FROM " . USERS_TABLE . " WHERE user_id = $temp_id LIMIT 1";
									if ( !($result = $db->sql_query($sql)) ){message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."111". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);}
									$row3 = $db->sql_fetchrow($result);
									$db->sql_freeresult($result);
									$temp_name = get_username_string('full', $temp_id, $row3['username'], $row3['user_colour']);
									$block_h .= '<tr><td>' . ($j+1) . '</td><td>' . $temp_name . '</td><td>' . aukz_date($user->lang['AZ_TIME_V'],$hist1_array[2]) . '</td><td><b>' . $hist1_array[1] . '</b></td></tr>';
								}
								$username_all = ',&nbsp;' . $user->lang['AZ_HIST'] . '<table width="100%" cellspacing="1px" cellpadding="1px"><tr><td colspan="4">' . '</td></tr>' . $block_h . '</table>';
							}
						}
					}

					$adm_pd = '';
					$adm_checkbox = $use_adm_checkbox ? '</td><td align="right"><input type="checkbox" name="a_d2" style="cursor:pointer;vertical-align:text-bottom" /></td>' : '';
					$adm_table_begin = $use_adm_checkbox ? '<table width="100%"><tr><td align="left">' : '';
					$adm_table_end = $use_adm_checkbox ? '</tr></table>' : '';
					if ( $closed == 0 )
					{						if ( $auth->acl_get('a_') OR ($user_lota == $user_id) OR ($user_id == $conf_adm_id))
						{							if ( $use_simple_buttons )
							{								$adm_pd_pause    = '<span title="' . $user->lang['AZ_PAUSE_TOOLTIP'] . '"><input type="submit" name="a_p" value="' . $user->lang['AZ_PAUSE'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_pause()"') . ' /></span>'
												 ;
								$adm_pd_go       = ($adm_go AND ($user_lota == $user_id))
												 ? ''
												 : '<span title="' . $user->lang['AZ_GO_TOOLTIP'] . '"><input type="submit" name="a_p" value="' . $user->lang['AZ_GO'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_go()"') . ' /></span>'
												 ;
								$adm_pd_pause_go = (!$pause) ? $adm_pd_pause : $adm_pd_go;
								$adm_pd_delete   = ($adm_delete AND ($user_lota == $user_id))
												 ? ''
												 : ('<span title="' . $user->lang['AZ_DELETE_TOOLTIP'] . '"><input type="submit" name="a_d1" value="' . $user->lang['AZ_DELETE'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_delete()"') . ' /></span>'
												   )
												 ;
								$adm_pd_edit     = ($adm_edit AND ($user_lota == $user_id))
												 ? ''
												 : ($pause
												  ? '<span title="' . $user->lang['AZ_EDIT_TOOLTIP'] . '"><input type="submit" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_edit()"') . ' /></span>'
												  : ''
												   )
												 ;
								$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><form' . ($use_adm_checkbox ? '' : ' onsubmit="return az_confirm_form(' . $id_lota . ');"') . ' action="aukz.php?start=' . $start  . '&opn=' . $opn . '" method="post" name="adm_u">' . $adm_table_begin . '<input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_pause_go . '&nbsp;' . $adm_pd_delete . '&nbsp;' . $adm_pd_edit . $adm_checkbox . $adm_table_end . '</form></span></td>';
							}
							else
							{								$adm_pd_pause    = '<input type="hidden" name="a_p" value="' . $user->lang['AZ_PAUSE'] . '" />'
												 . az_make_button($user->lang['AZ_PAUSE'],$user->lang['AZ_PAUSE_TOOLTIP'],'adm_u1_' . $id_lota,'',$imageset_lang_path,'aukz_pause.png','pause',$id_lota)
												 ;
								$adm_pd_go       = ($adm_go AND ($user_lota == $user_id))
												 ? ''
												 : '<input type="hidden" name="a_p" value="' . $user->lang['AZ_GO'] . '" /> '
												 . az_make_button($user->lang['AZ_GO'],$user->lang['AZ_GO_TOOLTIP'],'adm_u1_' . $id_lota,'',$imageset_lang_path,'aukz_go.png','go',$id_lota)
												 ;
								$adm_pd_pause_go = (!$pause) ? $adm_pd_pause : $adm_pd_go;
								$adm_pd_delete   = ($adm_delete AND ($user_lota == $user_id))
												 ? ''
												 : '<input type="hidden" name="a_d1" value="' . $user->lang['AZ_DELETE'] . '" />'
												 . az_make_button($user->lang['AZ_DELETE'],$user->lang['AZ_DELETE_TOOLTIP'],'adm_u2_' . $id_lota,'',$imageset_lang_path,'aukz_delete.png','delete',$id_lota)
												 ;
								$adm_pd_edit     = ($adm_edit AND ($user_lota == $user_id))
												 ? ''
												 : ($pause
												  ? '<input type="hidden" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" />'
												  . az_make_button($user->lang['AZ_EDIT'],$user->lang['AZ_EDIT_TOOLTIP'],'adm_u3_' . $id_lota,'',$imageset_lang_path,'aukz_edit.png','edit',$id_lota)
												  : ''
												   )
												 ;
								$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><table width="10%"><tr>'
										 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u1_' . $id_lota . '" name="adm_u1_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_pause_go . '</form></td>'
										 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u2_' . $id_lota . '" name="adm_u2_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_delete . '</form></td>'
										 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u3_' . $id_lota . '" name="adm_u3_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_edit . '</form></td>'
										 . '</tr></table></span></td>';
							}
						}
						else if ( $adm_u )
						{							$adm_pd = '<td class="' . $row . '"><span class="gen"></span></td>';
						}
					}
					else if ( $adm_u )
					{						if ( $auth->acl_get('a_') )
						{							$adm_pd_edit = '';
							if ( ($deleted == '<hidden />') AND ($user_lota == $user_id) )
							{
								if ( $use_simple_buttons )
								{
									$adm_pd_edit     = '<span title="' . $user->lang['AZ_EDIT_TOOLTIP'] . '"><input type="submit" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_edit()"') . ' /></span>'
													 ;
								}
								else
								{
									$adm_pd_edit     = '<input type="hidden" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" />'
												  . az_make_button($user->lang['AZ_EDIT'],$user->lang['AZ_EDIT_TOOLTIP'],'adm_u3_' . $id_lota,'',$imageset_lang_path,'aukz_edit.png','edit',$id_lota)
													 ;
								}
							}
							if ( $use_simple_buttons )
							{								$adm_pd_delete_forever = '<span title="' . $user->lang['AZ_DELETE_FOREVER_TOOLTIP'] . '"><input type="submit" name="a_d1f" value="' . $user->lang['AZ_DELETE_FOREVER'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_delete_forever()"') . ' /></span>';								$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><form' . ($use_adm_checkbox ? '' : ' onsubmit="return az_confirm_form(' . $id_lota . ');"') . ' action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" name="adm_u">' . $adm_table_begin . '<input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_delete_forever . $adm_pd_edit . $adm_pd_delete_forever . $adm_checkbox . $adm_table_end . '</form></span></td>';
							}
							else
							{								$adm_pd_delete_forever =   '<input type="hidden" name="a_d1f" value="' . $user->lang['AZ_DELETE_FOREVER'] . '" />'
													   . az_make_button($user->lang['AZ_DELETE_FOREVER'],$user->lang['AZ_DELETE_FOREVER_TOOLTIP'],'adm_u4_' . $id_lota,'',$imageset_lang_path,'aukz_delete_forever.png','delete_forever',$id_lota)
													   ;
								$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><table width="10%"><tr>'
										 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u4_' . $id_lota . '" name="adm_u4_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_delete_forever . '</form></td>'
										 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u3_' . $id_lota . '" name="adm_u3_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_edit . '</form></td>'
										 . '</tr></table></span></td>';
							}
						}
						else
						{
							if ( ($deleted == '<hidden />') AND ($user_lota == $user_id) )
							{
								if ( $use_simple_buttons )
								{
									$adm_pd_edit     = '<span title="' . $user->lang['AZ_EDIT_TOOLTIP'] . '"><input type="submit" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" style="cursor:pointer"' . ($use_adm_checkbox ? '' : ' onclick="az_confirm_edit()"') . ' /></span>'
													 ;
									$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><form' . ($use_adm_checkbox ? '' : ' onsubmit="return az_confirm_form(' . $id_lota . ');"') . ' action="aukz.php?start=' . $start  . '&opn=' . $opn . '" method="post" name="adm_u">' . $adm_table_begin . '<input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_edit . $adm_checkbox . $adm_table_end . '</form></span></td>';
								}
								else
								{
									$adm_pd_edit     = '<input type="hidden" name="a_e" value="' . $user->lang['AZ_EDIT'] . '" />'
												  . az_make_button($user->lang['AZ_EDIT'],$user->lang['AZ_EDIT_TOOLTIP'],'adm_u3_' . $id_lota,'',$imageset_lang_path,'aukz_edit.png','edit',$id_lota)
													 ;
									$adm_pd = '<td class="' . $row . '" align="center" valign="middle"><span class="gen"><table width="10%"><tr>'
											 . '<td align="center"><form action="aukz.php?start=' . $start . '&opn=' . $opn . '" method="post" id="adm_u3_' . $id_lota . '" name="adm_u3_' . $id_lota . '"><input type="hidden" name="hidden_lot_id" value="' . $id_lota . '" />' . $adm_pd_edit . '</form></td>'
											 . '</tr></table></span></td>';
								}
							}
							else
							{								$adm_pd = '<td class="' . $row . '"><span class="gen"></span></td>';
							}
						}
						$st_html = '';
						$bl_html = '';
						$time_ost = (($deleted == '<deleted />') ? ('<font color="red">' . $user->lang['AZ_TORG_DELETED'] . '</font>') : ('<font color="green">' . $user->lang['AZ_TORG_FINISHED'] . '</font>')) . '<br />' . aukz_date($user->lang['AZ_TIME_V'],$closed);
						$color = 'black';
					}
					if ( $use_simple_buttons )
					{						$id_url = ($url == '') ? ('#' . $id_lota) : ('<a href="' . $url . '&view=unread#unread" style="cursor:pointer" target="_blank" title="' . $user->lang['AZ_GO_TO_LOT_TOPIC'] . '">#' . $id_lota . '</a>');
					}
					else
					{						$burl = ($url == '') ? $url : ($url . '&view=unread#unread');
						$id_url = az_make_num_button ( $user->lang['AZ_GO_TO_LOT_TOPIC'], $burl, '', $imageset_lang_path, $id_lota );
					}
					$template->assign_block_vars('active.lots', array(
						'NN' => $i+1, // №пп
						'ID' => $id_lota,
						'ID_URL' => $id_url,
						'USER_0' => $user_lota,
						'USERNAME_0' => $username_lota,
						'USERNAME_ALL' => $username_all,
						'NAME1' => $name_test . $num_icq,
						'NAME3' => ($icq_info OR $icq_cond) ? ($icq_info . ($icq_cond ? (' (' . $icq_cond . ')') : '')) : $user->lang['AZ_NODESC'],
						'NAME4' => $icq_cond,
						'PRICE_5' => $price_darom ? $user->lang['AZ_DAROM'] : (($price_now>0) ? $price_now : $user->lang['AZ_NOST_ST'] . $price_0 . ')'),
						'USER_5' => $username_now,
						'COLOR' => $color,
						'DIS' => $dis,
						'ADM_PD' => $adm_pd,
						'TIME_OST' => $time_ost,
						'ST_HTML' => $st_html,
						'BL_HTML' => $bl_html,
						'S_ROW_COUNT' => $i,
					));
					$row = ($row == 'row1' ) ? 'row2' : 'row1';
				}
			}
		} // конец вывода активных аукционов

	//------------------------------------------------------------------------------
	// Refreshing the page every 60 seconds...
	//meta_refresh(60, append_sid("{$phpbb_root_path}aukz.$phpEx"));

	//------------------------------------------------------------------------------
	// Output the page
		page_header($page_title, false);

		$template->set_filenames(array(
			'body' => $template_html)
		);

		page_footer();
	}
}

//------------------------------------------------------------------------------
// отправка ЛС
//
// функция отправки личных сообщений, частично взятая из functions_thanks.php
//function send_thanks_pm($user_id, $to_id, $send_pm = true, $post_id = 0, $lang_act)
// функция отправки личных сообщений, взятая из ADR
//function adr_send_pm_az ( $dest_user , $subject , $message, $from_id )
function adr_send_pm_az ( $to_id , $subj , $mess, $from_id, $double_id = 0 )
{	global $db, $phpEx, $phpbb_root_path, $config, $user;

	include_once($phpbb_root_path . 'includes/functions_privmsgs.' . $phpEx);
	include_once($phpbb_root_path . 'includes/message_parser.' . $phpEx);

	if ( ($to_id!=0) AND ($subj!='') AND ($mess!='') )
	{		if ( ($from_id!=0) )
		{
			$message_parser = new parse_message();
			$message_parser->message = sprintf($mess);
			$message_parser->parse(true, true, true, false, false, true, true);

			$sql = "SELECT username, user_ip FROM " . USERS_TABLE . " WHERE user_id = $from_id ";
			if ( !($result = $db->sql_query($sql)) ) return;
			$row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);
			$from_username = $row['username'];
			$from_ip = $row['user_ip'];

			$pm_data = array(
				'from_user_id'			=> $from_id,
				'from_user_ip'			=> $from_ip,
				'from_username'			=> $from_username,
				'enable_sig'			=> false,
				'enable_bbcode'			=> true,
				'enable_smilies'		=> true,
				'enable_urls'			=> false,
				'icon_id'				=> 0,
				'bbcode_bitfield'		=> $message_parser->bbcode_bitfield,
				'bbcode_uid'			=> $message_parser->bbcode_uid,
				'message'				=> $message_parser->message,
				'address_list'			=> array('u' => array($to_id => 'to')),
			);

			submit_pm('post', $subj, $pm_data, false);

			if ( ($double_id!=0) AND ($double_id!=$from_id) AND ($double_id!=$to_id) )
			{				$sql = "SELECT username FROM " . USERS_TABLE . " WHERE user_id = $to_id ";
				if ( !($result = $db->sql_query($sql)) ) return;
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);
				$to_username = $row['username'];

				$message_double_parser = new parse_message();
				$message_double_parser->message = sprintf($user->lang['AZ_DOUBLE_MESSAGE'] . $to_username . $user->lang['AZ_DOUBLE_MESSAGE2'] . $mess);
				$message_double_parser->parse(true, true, true, false, false, true, true);

				$pm_double_data = array(
					'from_user_id'			=> $from_id,
					'from_user_ip'			=> $from_ip,
					'from_username'			=> $from_username,
					'enable_sig'			=> false,
					'enable_bbcode'			=> true,
					'enable_smilies'		=> true,
					'enable_urls'			=> false,
					'icon_id'				=> 0,
					'bbcode_bitfield'		=> $message_double_parser->bbcode_bitfield,
					'bbcode_uid'			=> $message_double_parser->bbcode_uid,
					'message'				=> $message_double_parser->message,
					'address_list'			=> array('u' => array($double_id => 'to')),
				);

				submit_pm('post', $subj, $pm_double_data, false);
			}
		}
		add_log('user',$to_id,$user->lang['AZ_LOG'],$subj . ' : ' . $mess);
	}
	return;
}

//------------------------------------------------------------------------------
// Дата и время
function aukz_date ( $format, $time, $user_id = 0 )
{
	global $db, $phpEx, $phpbb_root_path, $config, $user;

	$time_user = $time+(3600*(($user->data['user_timezone']+$user->data['user_dst'])));
	if ( ($user_id != 0) AND ($user_id != $user->data['user_id']) )
	{
		$sql = "SELECT user_timezone, user_dst FROM " . USERS_TABLE . " WHERE user_id = $user_id ";
		if ( ($result = $db->sql_query($sql)) )
		{
			$row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);

			$user_timezone = $row['user_timezone'];
			$user_dst = $row['user_dst'];

			$time_user = $time+(3600*(($user_timezone+$user_dst)));
		}
	}
	return date($format,$time_user);
}

//------------------------------------------------------------------------------
// Совместимая функция...
function message_die ( $message_type, $message)
{
	trigger_error($message);
}

//------------------------------------------------------------------------------
// Создание темы и первого поста от имени текущего пользователя. При этом:
// - подпись отключена
// - пост защищён от редактирования
// - первый пост закрепляется
// Возвращается URL на тему.
function az_submit_post ( $forum_id, $topic_title, $message )
{
	global $db, $phpEx, $phpbb_root_path, $config, $user;

	include_once($phpbb_root_path . 'includes/functions_posting.' . $phpEx);
	include_once($phpbb_root_path . 'includes/message_parser.' . $phpEx);

	if ( !$forum_id OR !$topic_title OR !$message ) return '';
/*	// $user_id исключен из параметров !!!!!

     // Keep this, as we now overwrite this with the user of our choice
    $realuserdata= $user-> data;

    $sql= "SELECT *
		FROM " . USERS_TABLE . "
		WHERE user_id = $user_id";  // Your account of choice
    $result= $db-> sql_query( $sql );
    if( $row= $db-> sql_fetchrow( $result ) ) {
        // Only overwrite Keys which actually exist, no other ones
        foreach( $row as $k1=> $v1 ) if( isset( $user-> data[$k1] ) ) $user-> data[$k1]= $v1;
    };
    $db-> sql_freeresult( $result );
    $auth-> acl( $user->data );  // Also take permissions of the new account
*/

    $sql= "SELECT forum_name FROM " . FORUMS_TABLE . " WHERE forum_id = $forum_id LIMIT 1";
    $result= $db-> sql_query( $sql );
    if( $row= $db-> sql_fetchrow( $result ) ) $forum_name = $row['forum_name'];
    $db-> sql_freeresult( $result );

    $uid = $bitfield = $options = '';
   	generate_text_for_storage($topic_title, $uid, $bitfield, $options, false, false, false);
   	generate_text_for_storage($message, $uid, $bitfield, $options, true, true, true);

    $newdata= array
    ( 'topic_title'=> $topic_title
    , 'poster_id'=> $user-> data['user_id']
    , 'forum_id'=> $forum_id  // Forum of choice
    , 'forum_name'=> $forum_name  // Forum of choice
    , 'force_approved_state'=> TRUE
    , 'icon_id'=> 0
    , 'enable_bbcode'=> TRUE
    , 'enable_smilies'=> TRUE
    , 'enable_urls'=> TRUE
    , 'enable_sig'=> FALSE
    , 'message'=> $message
    , 'message_md5'=> md5( $message )
    , 'bbcode_bitfield'=> $bitfield
    , 'bbcode_uid'=> $uid
    , 'post_edit_locked'=> FALSE
    , 'topic_type'=> POST_NORMAL
    , 'enable_indexing'=> TRUE
    , 'notify_set'=> FALSE
    , 'notify'=> FALSE
    , 'post_time'=> time()
    );

    $aUnused= array();
    $url= submit_post( 'post', $newdata['topic_title'], $user-> data['username'], $newdata['topic_type'], $aUnused, $newdata, TRUE, FALSE );

/*
    // Revert data from logged on user
    $user-> data= $realuserdata;
    $auth-> acl( $user-> data );
*/

	############################Закрепляем первый пост в теме#############################
	if ( $url != '' )
	{
		$topic_id = $newdata['topic_id'];
		$sql = "UPDATE " . TOPICS_TABLE . "
					SET topic_first_post_show = 1
					WHERE topic_id = $topic_id";
		$db->sql_query($sql);

   		$sql= "SELECT *
				FROM " . TOPICS_TABLE . "
				WHERE topic_id = $topic_id";
    	$result= $db-> sql_query( $sql );
    	$row= $db-> sql_fetchrow( $result );
		$db-> sql_freeresult( $result );
		$post_id = $row['topic_first_post_id'];
		if ( $post_id )
		{
			$sql = "UPDATE " . POSTS_TABLE . "
					SET post_edit_locked = 1
					WHERE post_id = $post_id";
			$db->sql_query($sql);
		}
	}
	######################################################################################

    return $url;
}
//------------------------------------------------------------------------------
// Создание html кнопки
// $title   - имя для картинки
// $tooltip - подсказка
// $form    - имя формы
// $dis     - '' или 'disabled'
// $path    - путь к картинке
// $file    - имя файла картинки
// $confirm - суффикс процедуры js подтверждения
// $lot_id  - идентификатор лота
function az_make_button ( $title, $tooltip, $form, $dis, $path, $file, $confirm = '', $lot_id = 0 )
{
	$style = ' vertical-align: middle;';
	if ( $dis != '' )
	{
		$browser_opacity = 0.5;
		$browser_ie_opacity = 50;
		$style .= ' vertical-align: middle; filter: alpha(opacity=' . $browser_ie_opacity . '); -moz-opacity: ' . $browser_opacity . '; -khtml-opacity: ' . $browser_opacity . '; opacity: ' . $browser_opacity . ';';
	}
	else
	{
		$style .= ' cursor:pointer;';
	}
	return '<span title="' . $tooltip . '">'
		 . '<img width="48px" height="22px" alt="' . $title . '" src="' . $path . '/' . $file . '"' . (($dis == '') ? (' style="' . $style . '" onclick="' . (($confirm != '') ? ('az_confirm_' . $confirm . '(); if ( az_confirm_form(' . $lot_id . ') ) ') : '') . 'document.getElementById(&quot;' . $form . '&quot;).submit()"') : ' style="' . $style. '"') . ' />'
		 . '</span>';
}
//------------------------------------------------------------------------------
// Создание сборной html кнопки с номером
// $tooltip - подсказка
// $url     - ссылка или ''
// $dis     - '' или 'disabled'
// $path    - путь к картинкам
// $num     - номер
// $prefix	- префикс (+ или =)
// $form    - имя формы
function az_make_num_button ( $tooltip, $url, $dis, $path, $num, $prefix = '', $form = '' )
{
	$style = ' vertical-align: middle;';
	$ap = '';
	$as = '';
	if ( $dis != '' )
	{
		$browser_opacity = 0.5;
		$browser_ie_opacity = 50;
		$style .= ' vertical-align: middle; filter: alpha(opacity=' . $browser_ie_opacity . '); -moz-opacity: ' . $browser_opacity . '; -khtml-opacity: ' . $browser_opacity . '; opacity: ' . $browser_opacity . ';';
	}
	else if ( ($url != '') OR ($form != '') )
	{
		$style .= ' cursor:pointer;';
		if ( $url != '' )
		{			$ap = '<a target="_blank" style="' . $style . '" href="' . $url . '" title="' . $tooltip . '">';
			$as = '</a>';
		}
	}
	else
	{		$style = '';
	}
	$title = $num;
	$submit = '';
	if ( $form != '' ) $submit = ' onclick="document.getElementById(&quot;' . $form . '&quot;).submit()"';
	$img_left  = '<img width="10px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_left.png"  style="' . $style . '"' . $submit . ' />';
	$img_right = '<img width="10px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_right.png" style="' . $style . '"' . $submit . ' />';
	$n = $num;
	if ( $n > 0 )
	{
		$img = '';
		$ns = 0;
		while ( $n >= 1 )
		{
			$i = $n % 10;
			$img = '<img width="' . (($i == 1) ? '8' : '11') . 'px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_' . $i . '.png" style="' . $style . '"' . $submit . ' />' . $img;
			$n = $n / 10;
			$ns = $ns + 1;
			if ( ($prefix != '') AND ($n >= 1) AND (($ns % 3) == 0) ) $img = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_space.png" style="' . $style . '"' . $submit . ' />' . $img;
		}
	}
	else if ( $n == 0 )
	{
		$img = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_0.png" style="' . $style . '"' . $submit . ' />';
	}
	else
	{
		$img = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_space.png" style="' . $style . '"' . $submit . ' />';
	}
	$img_prefix = '';
	$img_pspace = '';
	$img_sspace = '';
	$img_suffix = '';
	if ( $prefix == '+' ) $img_prefix = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_plus.png" style="' . $style . '"' . $submit . ' />';
	if ( $prefix == '=' ) $img_prefix = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_equal.png" style="' . $style . '"' . $submit . ' />';
	if ( $img_prefix != '' ) $img_pspace = '<img width="11px" height="22px" alt="' . $title . '" src="' . $path . '/aukz_space.png" style="' . $style . '"' . $submit . ' />';

	return '<span title="' . $tooltip . '">'
		 . $ap
		 . $img_left
		 . $img_prefix
		 . $img_pspace
		 . $img
		 . $img_sspace
		 . $img_suffix
		 . $img_right
		 . $as
		 . '</span>';
}
//------------------------------------------------------------------------------
?>