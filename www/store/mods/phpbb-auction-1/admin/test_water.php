<?php
/***************************************************************************
 *                            test_watermark.php
 *                            -------------------
 *   begin                : June 05, 2004
 *   copyright            : (C) 2003 mr.luc
 *   email                : llg@gmx.at
 *
 *   
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

define('IN_PHPBB', true);
//
// Let's set the root dir for phpBB
//
$phpbb_root_path = "./../";
require($phpbb_root_path . 'extension.inc');
include($phpbb_root_path . 'common.'.$phpEx);
// require('./pagestart.' . $phpEx);
include($phpbb_root_path . 'auction/functions_general.php');
include($phpbb_root_path . 'auction/auction_constants.php');

/* BEGIN include auction-pic-config information */
$auction_config_pic = init_auction_config_pic();
/* END include auction-pic-config information */

// function for watermark 

function mergePix($sourcefile,$insertfile, $pos=0, $transition=50, $unreg_quality) 
{ 

   //Get the resource id�s of the pictures 
   $insertfile_id = imageCreateFromPNG($insertfile); 
   $sourcefile_id = imageCreateFromJPEG($sourcefile); 
	
   //Get the sizes of both pix 
   $sourcefile_width=imageSX($sourcefile_id); 
   $sourcefile_height=imageSY($sourcefile_id); 
   $insertfile_width=imageSX($insertfile_id); 
   $insertfile_height=imageSY($insertfile_id); 

   //middle 
   if( $pos == 0 ) 
   { 
   $dest_x = ( $sourcefile_width / 2 ) - ( $insertfile_width / 2 ); 
   $dest_y = ( $sourcefile_height / 2 ) - ( $insertfile_height / 2 ); 
   } 

   //top left 
   if( $pos == 1 ) 
   { 
   $dest_x = 0; 
   $dest_y = 0; 
   } 

   //top right 
   if( $pos == 2 ) 
   { 
   $dest_x = $sourcefile_width - $insertfile_width; 
   $dest_y = 0; 
   } 

   //bottom right 
   if( $pos == 3 ) 
   { 
   $dest_x = $sourcefile_width - $insertfile_width; 
   $dest_y = $sourcefile_height - $insertfile_height; 
   } 

   //bottom left 
   if( $pos == 4 ) 
   { 
   $dest_x = 0; 
   $dest_y = $sourcefile_height - $insertfile_height; 
   } 

   //top middle 
   if( $pos == 5 ) 
   { 
   $dest_x = ( ( $sourcefile_width - $insertfile_width ) / 2 ); 
   $dest_y = 0; 
   } 

   //middle right 
   if( $pos == 6 ) 
   { 
   $dest_x = $sourcefile_width - $insertfile_width; 
   $dest_y = ( $sourcefile_height / 2 ) - ( $insertfile_height / 2 ); 
   } 

   //bottom middle 
   if( $pos == 7 ) 
   { 
   $dest_x = ( ( $sourcefile_width - $insertfile_width ) / 2 ); 
   $dest_y = $sourcefile_height - $insertfile_height; 
   } 

   //middle left 
   if( $pos == 8 ) 
   { 
   $dest_x = 0; 
   $dest_y = ( $sourcefile_height / 2 ) - ( $insertfile_height / 2 ); 
   } 

   //The main thing : merge the two pix 
   imageCopyMerge($sourcefile_id, $insertfile_id,$dest_x,$dest_y,0,0,$insertfile_width,$insertfile_height,$transition); 

   //Create a jpeg out of the modified picture 

    @Imagejpeg($sourcefile_id,'',$unreg_quality); 
   @ImageDestroy($sourcefile_id); 


} 

// get the variables
if( isset($HTTP_GET_VARS['file']) )
{
	$file = $HTTP_GET_VARS['file'];
}
else
{
	$file = 0;
}
$dummy_file = "";
if($file == 0)
{
	$main_watermark_file = "main_watermark.png";
	if(!file_exists($phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH . "wmk/" . $main_watermark_file))
	{
		die("Picture " . $main_watermark_file . " does not exist!<br /> Make sure your <br />'phpbb/auction/upload/wmk'<br />directory contains the file: " . $main_watermark_file . "!<br /><br /><b><u>The file must be writable !!!</u></b>");
	}

	$dummy_file = "wmk_test.jpg";

	if(!file_exists($phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH . "wmk/" . $dummy_file))
	{

		die("Picture " . $dummy_file . " does not exist!<br /> Make sure your <br />'phpbb/auction/upload/wmk'<br />directory contains the file: ". $dummy_file . "!");
	}
}
else
{
	$main_watermark_file = "big_watermark.png";
	if(!file_exists($phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH . "wmk/" . $main_watermark_file))
	{
		die("Picture " . $main_watermark_file . " does not exist!<br /> Make sure your <br />'phpbb/auction/upload/wmk'<br />directory contains the file: " . $main_watermark_file . "!<br /><br /><b><u>The file must be writable !!!</u></b>");
	}

	$dummy_file = "wmk_test_2.jpg";

	if(!file_exists($phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH . "wmk/" . $dummy_file))
	{

		die("Picture " . $dummy_file . " does not exist!<br /> Make sure your <br />'phpbb/auction/upload/wmk'<br />directory contains the file: ". $dummy_file . "!");
	}
}
/*
+----------------------------------------------------------
| Main work here...
+----------------------------------------------------------
*/




	// --------------------------------
	// Hmm, both caches are empty. Try to re-generate!
	// --------------------------------

	$pic_size = @getimagesize($phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH. "wmk/" . $dummy_file);
	$pic_width = $pic_size[0];
	$pic_height = $pic_size[1];

	$gd_errored = FALSE;

	
			
				/* 
				----------------------------------------
				SETTINGS:
				//$transition = Intensity of the transition (in percent) 
				//$position  = Position where $insertfile will be inserted in $sourcefile 
				// 0 = middle 
				// 1 = top left 
				// 2 = top right 
				// 3 = bottom right 
				// 4 = bottom left 
				// 5 = top middle 
				// 6 = middle right 
				// 7 = bottom middle 
				// 8 = middle left 
				---------------------------------------------
				*/
			
			if($file == 0)
			{
				$position  = $auction_config_pic['main_watermarkpos'];
				
				if((!isset($position))  OR ($postion > 8) OR ($postion < 0))
				{
					$position = 3;
				}
				$transition = $auction_config_pic['main_water_img_trans'];
				if((!isset($transition))  OR ($transition > 100) OR ($transition < 0))
				{
					$transition = 70;
				}
				$unreg_quality = $auction_config_pic['main_water_img_qual'];
								if((!isset($unreg_quality))  OR ($unreg_quality > 99) OR ($unreg_quality < 0))
				{
					$unreg_quality = 70;
				}
			}
			else
			{
				$position  = $auction_config_pic['big_watermarkpos'];
				
				if((!isset($position))  OR ($postion > 8) OR ($postion < 0))
				{
					$position = 3;
				}
				$transition = $auction_config_pic['big_water_img_trans'];
				if((!isset($transition))  OR ($transition > 100) OR ($transition < 0))
				{
					$transition = 70;
				}
				$unreg_quality = $auction_config_pic['big_water_img_qual'];
								if((!isset($unreg_quality))  OR ($unreg_quality > 99) OR ($unreg_quality < 0))
				{
					$unreg_quality = 70;
				}
				// settings end
			}
			//Get the resource id�s of the pictures 
			$sourcefile = $phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH. "wmk/" . $dummy_file;
			$insertfile = $phpbb_root_path . AUCTION_PICTURE_UPLOAD_PATH. "wmk/" . $main_watermark_file;
				
			mergePix($sourcefile, $insertfile, $position, $transition, $unreg_quality) ;


	exit;
?>