**************************************************************************
** PHPBB3                                                               **
** Module for 123 Flash Chat Server software                            **
** ==============================================                       **
**                                                                      **
** Copyright (c) by TopCMM                                              **
** Daniel Jiang (support@123flashchat.com)                              **
** http://www.topcmm.com                                                **
** http://www.123flashchat.com                                          **
** http://www.phpbb.com/	                                        **
**                                                                      **
** Version : 123FlashChat for PHPBB3 Module 3.0                         **
** Author : TOPCMM SOFTWARE CORP.                                       **
** Contributors : 123FlashChat.com                                      **
** Requires at least : 3.0.10                                           **
** Tested up to : 3.0.10                                                **
** Donate link : www.123flashchat.com                                   **
** Tags : 123 Flash Chat, Chat Software, Flash Chat Hosting,            **
**        Video Webcam Audio, Chat Server, Live Help, IM,               **
**                                                                      **
**************************************************************************


############################################################################################################
## Notes:                                                                                                 ##
##                                                                                                        ##
##     IMPORTANT!!!!                                                                                      ##
##     This module need the 123FlashChat Server Service running.                                          ##
##     And we provide 3 options for the chat server:                                                      ##
##     1. Chat server hosted by your own server.                                                          ##
##     2. Chat server hosted by 123flashchat.com(paid or 15 days free trial)                              ##
##     3. Chat server hosted by 123flashchat.com for free demo(by default)                                ##
##                                                                                                        ##
##     If you need to run the chat service on your own server, before installing this module, please      ##
##     download 123FlashChat Server software first from this page:                                        ##
##     http://www.123flashchat.com/download.html                                                          ##
##     You may get the latest free demo version of 123 flash chat server software at there.               ##
##                                                                                                        ##
##     Or you can purchase 123flashchat hosting service here:                                             ##
##     http://www.123flashchat.com/host.html                                                              ##
##     To apply a free trial host service:                                                                ##
##     http://www.123flashchat.com/host/apply.php                                                         ##
##                                                                                                        ##
##     And get professional support from: http://www.123flashchat.com/support.html                        ##
##     We provide email support, online live support, and phone support                                   ##
##     etc.                                                                                               ##
##                                                                                                        ##
############################################################################################################
## Before Adding This MOD To Your PHPBB, You Should Backup All The Files Related To This MOD              ##
############################################################################################################

Use 123flashchat you need to install AutoMod first. If you have installed ,you can go Step 1.
	
	You can visit http://www.phpbb.com/mods/automod/  to download AutoMod and install.


Step 1, Upload files in "Normal install mod" module to your phpbb:

           1) if you use the template prosilver, please upload the prosilver.zip  into the website with the automod.
		   
		   2) if you use the template sublilver2, please upload the sublilver2.zip  into the website with the automod.

Step 2, Install module:

            Click install in automod page.

Step 3, Integrate module database:
        
            1, Visit the url http://<phpbb install directory>/fchat_install.php and install the database as the tip.

Step 4, Configure module:

            1. Open: Admin->123 Flash Chat

            2. Configure & save 123 Flash Chat Settings.

step 5, Integrate your chat with phpbb user database

    Intro: For those who chooses running chat on [host mode] or [localhost mode], database integration is necessary,
    please follow the instructions below.

            1. phpbb Admin Panel -> Modules -> 123FlashChat Admin Panel, Log in the Admin Panel of your 123FlashChat server using chat admin account.

	         OPEN: Server Settings-> Integration

            2. For DataBase, SELECT: URL, and click "edit"

            3. Change URL to:

               http://<phpbb install directory>/login_chat.php?username=%username%&password=%password%

            4. Press OK to save the setting.

	    5. OPEN: Server Management -> Restart, to restart server.
	     