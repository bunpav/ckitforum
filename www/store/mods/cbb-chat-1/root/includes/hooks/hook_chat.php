<?php
/*
* @name hook_chat.php
* @package phpBB3 cBB Chat
* @version $Id: hook_chat.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// @ignore
if(!defined('IN_PHPBB'))
{
	exit;
}

if(!empty($config['chat_version']) && !defined('IN_INSTALL'))
{
	require($phpbb_root_path . chat_hook::$path . "includes/functions_chat.$phpEx");
	$chat = new chat();
		
	// Register the hook to execute on user->setup()
	$phpbb_hook->register('phpbb_user_session_handler', array($chat, 'setup'));

	// Register the hook to execute on template->display()
	if(!empty($config['chat_enabled']) && !defined('ADMIN_START'))
	{
		$phpbb_hook->register(array('template', 'display'), array($chat, 'display'));
	}
}

class chat_hook 
{
	static $path = 'chat/';

	static private function setup()
	{
		global $phpbb_root_path, $phpEx, $table_prefix;

		if(!defined('CHAT_USERS_TABLE'))
		{
			include($phpbb_root_path . self::$path . "includes/constants.$phpEx");
		}
	}

	static public function user_login()
	{
		global $config, $db, $user;

		if(!empty($config['chat_version']) && $user->data['user_id'] != ANONYMOUS)
		{
			self::setup();

			$cookie_id = request_var($config['cookie_name'] . '_chat_key', 0, false, true);
		
			$sql = 'DELETE FROM ' . CHAT_USERS_TABLE . '
				WHERE user_id = ' . ANONYMOUS . '
				AND (user_key = ' . $cookie_id . "
					OR user_ip = '" . $user->ip . "')
				AND exclude_time = 0";
			$db->sql_query($sql);
			
			$cookie_expire = $user->time_now - 31536000;
			$user->set_cookie('chat_key', '', $cookie_expire);
			$user->set_cookie('chat_lastcheck', '', $cookie_expire);
		}
	}
	
	static public function user_logout()
	{
		global $db, $user, $config, $chat;
		
		if(!empty($config['chat_version']) && $user->data['user_type'] <> USER_IGNORE)
		{
			self::setup();

			// Delete from chat users if no banned
			$sql = 'DELETE FROM ' . CHAT_USERS_TABLE . '
				WHERE user_id = ' . $user->data['user_id'] . "
					OR user_ip = '" . $user->ip . "'
				AND exclude_time = 0";
			$db->sql_query($sql);
			
			// Set the correct user status if banned
			if(!$db->sql_affectedrows())
			{
				$sql = 'UPDATE ' . CHAT_USERS_TABLE . '
					SET user_online = 0
					WHERE user_id = ' . $user->data['user_id'] . '
					AND exclude_time > 0';
				$db->sql_query($sql);
			}
			
			// Delete private messages
			$room_ary = $chat->obtain_rooms();

			$sql = 'DELETE FROM ' . CHAT_MESSAGES_TABLE . '
				WHERE (poster_id = ' . $user->data['user_id'] . '
					AND ' . $db->sql_in_set('dest_key', array_keys($room_ary), true) . ')
				OR dest_key = ' . $user->data['user_id'];
			$db->sql_query($sql);
			
			$cookie_expire = $user->time_now - 31536000;
			$user->set_cookie('chat_key', '', $cookie_expire);
			$user->set_cookie('chat_lastcheck', '', $cookie_expire);
		}
	}
	
	static public function user_delete($user_data)
	{
		global $db, $config;
		
		if(!empty($config['chat_version']))
		{
			self::setup();

			// Delete from chat users
			$sql = 'DELETE FROM ' . CHAT_USERS_TABLE . '
				WHERE user_id = ' . $user_data['user_id'];
			$db->sql_query($sql);
			
			// Delete private messages
			$sql = 'DELETE FROM ' . CHAT_MESSAGES_TABLE . '
				WHERE poster_id = ' . $user_data['user_id'];
			$db->sql_query($sql);
		}
	}
}
