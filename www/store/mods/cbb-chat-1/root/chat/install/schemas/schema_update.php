<?php
/*
* @name schema_update.php
* @package cBB Chat Installer
* @version $Id: schema_update.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

if(!defined('IN_PHPBB') || !defined('IN_INSTALL'))
{
	exit;
}

switch($this->current_version)
{
	// 1.0.0-beta to 1.0.1
	case '1.0.0-beta':
		$this->umil->config_add(array(
			array('chat_bbcode_format', 'button'),
			array('chat_show_topics', false),
		));
		
		$this->umil->table_column_add(CHAT_USERS_TABLE,	'session_viewonline', array('BOOL', 1));
		
	// no break

	// 1.0.0 to 1.0.1
	case '1.0.0':
		$this->umil->config_add(array(
			array('chat_auto_away', 0),
			array('chat_max_pm', 0),
			array('chat_page_enabled', 1),
			array('chat_remember_status', 1),
			array('chat_show_topics', false),
			array('chat_timeout', 5),
		));
		
		// Add new modules
		$this->umil->module_add('acp', 'ACP_CAT_CHAT', array(
			'module_basename'	=> 'chat',
			'modes'				=> array('rooms'),
		));
		
		// Add new columns
		$this->umil->table_column_add(array(
			array(CHAT_USERS_TABLE,		'user_status',		array('UINT:3', CHAT_STATUS_AVAILABLE)),
			array(CHAT_PAGES_TABLE,		'chat_height', 		array('UINT:4', 0)),
			array(CHAT_PAGES_TABLE,		'page_data',		array('TEXT', ''))
		));
		
		// Add new tables
		$this->umil->table_add(CHAT_ROOMS_TABLE, $this->schema['table_add'][CHAT_ROOMS_TABLE]);
		
		// Add new data
		$this->umil->table_row_insert(CHAT_ROOMS_TABLE, array(
			array(
				'room_key'			=> CHAT_GUEST_ROOM,
				'room_title'		=> 'CHAT_GUEST_ROOM',
				'room_data'			=> '',
				'room_enabled'		=> 1,
				'room_order'		=> 1,
			)
		));
		
		/* 
		 * Change bbcode string to bitfield
		*/
		if(!empty($config['chat_disallowed_bbcode']))
		{
			// Default BBcodes
			$bbcodes = array(
				1	=> 'b',
				2	=> 'i',
				7	=> 'u',
				8	=> 'code',
				6	=> 'color',
				11	=> 'flash',
				4	=> 'img',
				9	=> 'list',
				0	=> 'quote',
				5	=> 'size',
				3	=> 'url'
			);
			
			// Add Custom BBcodes
			$sql = 'SELECT bbcode_id, bbcode_tag
				FROM ' . BBCODES_TABLE . '
				WHERE display_on_posting = 1
				ORDER BY bbcode_tag';
			$result = $db->sql_query($sql);
			while($row = $db->sql_fetchrow($result))
			{
				$bbcodes[$row['bbcode_id']] = $row['bbcode_tag'];
			}
			$db->sql_freeresult($result);
			
			$selected_tags	= explode('|', $config['chat_disallowed_bbcode']);
			$bitfield		= new bitfield();
			
			foreach($bbcodes as $bbcode_id => $bbcode_tag)
			{
				if(in_array($bbcode_tag, $selected_tags))
				{
					$bitfield->set($bbcode_id);
				}	
			}
			
			set_config('chat_disallowed_bbcode', $bitfield->get_base64());
			
			unset($bitfield);
		}
		
	break;
	
	// latest version
	case '1.0.1':
	break;
	
	default:
		$this->update_error = true;
	break;
}
