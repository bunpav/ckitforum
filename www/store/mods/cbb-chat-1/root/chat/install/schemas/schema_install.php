<?php
/*
* @name schema_install.php
* @package cBB Chat Installer
* @version $Id: schema_install.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

if(!defined('IN_PHPBB') || !defined('IN_INSTALL'))
{
	exit;
}

$this->schema = array(
	'table_add' => array(
		CHAT_MESSAGES_TABLE => array(
			'COLUMNS' => array(
				'message_id'		=> array('UINT:8', NULL, 'auto_increment'),
				'poster_key'		=> array('UINT:11', 0),
				'dest_key'			=> array('UINT:11', CHAT_GUEST_ROOM),
				'poster_id'			=> array('UINT:8', ANONYMOUS),
				'poster_ip'			=> array('VCHAR:40', ''),
				'poster_username'	=> array('VCHAR', ''),
				'message_text'		=> array('TEXT', ''),
				'message_time'		=> array('UINT:11', 0),
				'bbcode_bitfield'	=> array('VCHAR', ''),
				'bbcode_uid'		=> array('VCHAR:8', ''),
			),
			'PRIMARY_KEY'	=> 'message_id',
		),
		
		CHAT_PAGES_TABLE => array(
			'COLUMNS' => array(
				'page_id'			=> array('UINT:3', NULL, 'auto_increment'),
				'page_alias'		=> array('VCHAR:50', ''),
				'page_filename'		=> array('VCHAR', ''),
				'page_path'			=> array('VCHAR', ''),
				'page_data'			=> array('TEXT', ''),
				'chat_position'		=> array('VCHAR:10', ''),
				'chat_height'		=> array('UINT:4', 0),
			),
			'PRIMARY_KEY'	=> 'page_id',
		),
		
		CHAT_ROOMS_TABLE => array(
			'COLUMNS' => array(
				'room_id'			=> array('UINT:3', NULL, 'auto_increment'),
				'room_key'			=> array('UINT:11', 0),
				'room_title'		=> array('VCHAR', ''),
				'room_data'			=> array('TEXT', ''),
				'room_enabled'		=> array('BOOL', 1),
				'room_order'		=> array('UINT:3', 0),
			),
			'PRIMARY_KEY'	=> 'room_id',
		),
		
		CHAT_TEXTS_TABLE => array(
			'COLUMNS' => array(
				'text_id'			=> array('UINT:3', NULL, 'auto_increment'),
				'text_content'		=> array('TEXT', ''),
				'text_order'		=> array('UINT:3', 1),
				'text_type'			=> array('UINT:1', 0),
				'bbcode_bitfield'	=> array('VCHAR', ''),
				'bbcode_uid'		=> array('VCHAR:8', ''),
			),
			'PRIMARY_KEY'	=> 'text_id',
		),
		
		CHAT_USERS_TABLE => array(
			'COLUMNS' => array(
				'user_key'				=> array('UINT:11', 0),
				'user_id'				=> array('UINT:8', 0),
				'user_ip'				=> array('VCHAR:40', ''),
				'username'				=> array('VCHAR', ''),
				'session_start'			=> array('UINT:11', 0),
				'session_viewonline'	=> array('BOOL', 1),
				'user_lastjoin'			=> array('UINT:11', 0),
				'user_online'			=> array('BOOL', 0),
				'user_status'			=> array('UINT:3', CHAT_STATUS_AVAILABLE),
				'exclude_time'			=> array('UINT:11', 0),
			),
			'PRIMARY_KEY'	=> 'user_key',
		),
	),
	
	'row_insert' => array(
		CHAT_PAGES_TABLE => array(
			array(
				'page_alias'		=> 'index',
				'page_filename'		=> "index.$phpEx",
				'page_path'			=> '',
				'chat_position'		=> 'bottom',
			)
		),

		CHAT_ROOMS_TABLE => array(
			array(
				'room_key'			=> CHAT_GUEST_ROOM,
				'room_title'		=> 'CHAT_GUEST_ROOM',
				'room_data'			=> '',
				'room_enabled'		=> 1,
				'room_order'		=> 1,
			)
		)
	),

	'config_add' => array(
		array('chat_allow_bbcode', true),
		array('chat_allow_pm', true),
		array('chat_auto_away', 0),
		array('chat_autoconnect', false),
		array('chat_bbcode_format', 'button'),
		array('chat_cron_lock', 0, true),
		array('chat_direction', 'down'),
		array('chat_disallowed_bbcode', ''),
		array('chat_enabled', true),
		array('chat_flood_time', 0),
		array('chat_height', 240),
		array('chat_max_chars', 500),
		array('chat_max_pm', 0),
		array('chat_max_rows', 15),
		array('chat_page_enabled', 1),
		array('chat_refresh', 10),
		array('chat_remember_status', 1),
		array('chat_show_avatars', true),
		array('chat_show_topics', false),
		array('chat_sound', true),
		array('chat_store_time', 31104000), // One year
		array('chat_timeout', 5),
		array('chat_version', $this->new_version),
	),

	// Define modules to install
	'module_check' => 0, // $this->schema['modules'] array ID to check or false to disable

	'modules' => array(
		// Add the ACP modules
		array('acp', 'ACP_CAT_DOT_MODS', 'ACP_CAT_CHAT'),
		array('acp', 'ACP_CAT_CHAT', array(
			'module_basename'	=> 'chat',
			'modes'				=> array('config', 'pages', 'rooms', 'texts'),
		)),
	),
	
	// Define permissions to add and set
	'permission_add' => array(
		array('a_chat', true),
		
		array('m_chat_delete', true),
		array('m_chat_edit', true),
		
		array('u_chat_archive', true),
		array('u_chat_delete', true),
		array('u_chat_edit', true),
		array('u_chat_ignoreflood', true),
		array('u_chat_post', true),
		array('u_chat_sendpm', true),
		array('u_chat_view', true),
	),

	'permission_set' => array(
		array('ROLE_ADMIN_STANDARD', 'a_chat'),
		array('ADMINISTRATORS', 'u_chat_archive', 'group'),
		array('ADMINISTRATORS', 'u_chat_ignoreflood', 'group'),

		array('ROLE_USER_FULL', 'u_chat_delete'),
		array('ROLE_USER_FULL', 'u_chat_edit'),
		array('ROLE_USER_FULL', 'u_chat_post'),
		array('ROLE_USER_FULL', 'u_chat_sendpm'),
		array('ROLE_USER_FULL', 'u_chat_view'),
		
		array('ROLE_MOD_FULL', 'm_chat_delete'),
		array('ROLE_MOD_FULL', 'm_chat_edit'),
	)
);
