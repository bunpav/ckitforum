<?php
/**
*
* @package installer.php
* @package cBB Installer
* @version $Id: installer.php,v1.0.4 15/07/2013 $
*
* @copyright (c) 2013 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* @ignore
*/
if(!defined('IN_PHPBB'))
{
	exit;
}

/**
* Main Check
*/
if(!file_exists($phpbb_root_path . 'common.' . $phpEx))
{
	trigger_error('phpBB Forum not found', E_USER_ERROR);
}

include($phpbb_root_path . 'common.' . $phpEx);


/**
* Create the installer class
*/
function create_installer()
{
	global $user, $phpbb_install_path, $phpEx;
	
	// We only allow a founder install this MOD
	if($user->data['user_type'] != USER_FOUNDER)
	{
		if($user->data['user_id'] == ANONYMOUS)
		{
			login_box();
		}

		trigger_error('NOT_AUTHORISED');
	}

	// Get Some values
	$mode	= (isset($_POST['mode']) ? request_var('mode', 'overview') : 'overview');
	$sub	= request_var('sub', '');

	// Initialize
	if($mode == 'overview')
	{
		$installer = new installer($mode, 'welcome');
	}
	else
	{
		$module_class = 'install_' . $mode;

		if(!file_exists($phpbb_install_path . "$module_class.$phpEx"))
		{
			trigger_error('GENERAL_ERROR', E_USER_ERROR);
		}
	
		include($phpbb_install_path . "$module_class.$phpEx");
		$installer = new $module_class($mode, $sub);
	}
	
	return $installer;
}

/**
* Main Installer Class
*/
class installer
{
	var $package_files	= array(
		'constants'			=> '',
		'schema_install'	=> '',
		'schema_update'		=> '',
	);
	
	var $canidev_docpage	= 'http://www.canidev.com/docs.php?i=%1$s-v%2$s';
	var $default_lang		= 'en';
	var $installer_version	= '1.0.4';
	
	var $umil;
	var $mode;
	var $sub;

	var $buttons		= array();
	var $hidden_fields 	= array();
	var $load_doc		= false;
	var $logo			= '';
	var $mod_id			= '';
	var $mod_title		= '';
	var $new_version;
	var $required_phpbb;
	var $schema			= array();
	var $subs			= array();
	var $version_key	= false;
	var $version_table	= false;

	function installer($mode, $sub)
	{
		global $user, $phpbb_install_path, $phpEx;

		$this->mode	= $mode;
		$this->sub	= (($sub) ? $sub : (!empty($this->subs) ? $this->subs[0] : ''));
		
		$allowed_modes = request_var('allowed_modes', array(0 => ''));

		if((!empty($allowed_modes) && !in_array($this->mode, $allowed_modes)) || ($this->mode != 'overview' && !in_array($this->sub, $this->subs)))
		{
			redirect(append_sid($user->page['page']));
		}

		if(!class_exists('umil'))
		{
			if(!file_exists($phpbb_install_path . "includes/umil.$phpEx"))
			{
				trigger_error('GENERAL_ERROR', E_USER_ERROR);
			}

			include($phpbb_install_path . "includes/umil.$phpEx");
		}
		
		$this->umil = new umil(true);
		
		if(!empty($allowed_modes))
		{
			$this->hidden_fields['allowed_modes'] = $allowed_modes;
		}
		
		// Set basic data
		if($mode != 'overview')
		{
			$this->hidden_fields['mode'] 	= $mode;
			$this->buttons['overview'] 		= 'HOME';
		}
		
		// Load schemas
		$files = array();
		foreach($this->package_files as $package => $null)
		{
			if(strpos($package, 'schema_') !== false)
			{
				$files[$package] = $phpbb_install_path . 'schemas/' . $package . ".$phpEx";
			}
		}
		
		$this->assign_files($files);
		unset($files);
	}
	
	function setup($lang_set = false)
	{
		global $user, $phpEx;

		// Lang Setup
		$lang_name 		= request_var('lang', $user->lang_name);
		$change_lang 	= false;
		
		if($lang_name != $user->lang_name && file_exists($user->lang_path . $lang_name . "/common.$phpEx"))
		{
			$change_lang = true;
		}
		
		// Set the installer default lang as user lang if not find the neccessary files
		if($lang_set !== false)
		{
			if(!is_array($lang_set))
			{
				$lang_set = array($lang_set);
			}
			
			$find = array();
			
			foreach($lang_set as $lang_file)
			{
				$find[] = file_exists($user->lang_path . $lang_name . '/' . $lang_file . ".$phpEx");
			}
			
			if(in_array(false, $find))
			{
				$change_lang = true;
				$lang_name = $this->default_lang;
			}
		}
		
		if($change_lang)
		{
			$user->lang_name = $lang_name;
			$user->lang = array();
			$user->data['user_lang'] = $user->lang_name;
			$user->add_lang('common');
		}
		
		$user->add_lang($lang_set);
	}
	
	function load()
	{
		global $template, $user, $phpbb_install_path;

		$template->set_custom_template($phpbb_install_path . 'style', 'cbbinstaller' . $this->installer_version);

		// the installer template is never stored in the database
		$user->theme['template_storedb'] = false;

		// Include MOD constants if defined
		if(!empty($this->package_files['constants']))
		{
			$this->include_files('constants');
		}
		
		// Set the correct version_table value
		if($this->version_table && defined($this->version_table))
		{
			$defines = get_defined_constants();
			$this->version_table = $defines[$this->version_table];
			unset($defines);
		}
		
		// Load main class
		if($this->mode == 'overview')
		{
			$this->load_overview();
		}
		else if(method_exists($this, 'main'))
		{
			$this->main();
		}
		
		// Display the result
		$this->display();
	}
	
	function load_overview()
	{
		global $user, $config, $phpbb_root_path, $phpEx;

		$this->page_title	= 'WELCOME';
		$this->subs 		= array('welcome');
		
		// Get the mode buttons
		$is_installed = $this->get_installed_version($current_version);
		
		$this->buttons['link1'] = array('FORUM_INDEX', append_sid("{$phpbb_root_path}index.$phpEx"));

		if(!$is_installed)
		{
			$this->buttons['install'] = 'INSTALL';
		}
		elseif(version_compare($current_version, $this->new_version, '<'))
		{
			$this->buttons['update'] = 'UPDATE';
		}
		
		$this->buttons['uninstall'] = 'UNINSTALL';
		
		$this->hidden_fields['allowed_modes'] = array_merge(array('overview'), array_keys($this->buttons));
	
		$checks = array(
			array('legend1', 'VERSION_INFO'),
			array('VERSION_INSTALLED', (($is_installed) ? $current_version : $user->lang['NOMOD']), 'NEUTRAL'),
			array('VERSION_PACKAGE', $this->new_version, 'NEUTRAL')
		);
		
		if(($latest_version = $this->latest_version_info()) !== false)
		{
			$checks[] = array('VERSION_LATEST', $latest_version, true);
		}
		
		$this->build_checks($checks);
		
		$this->assign_vars(array(
			'BODY_TITLE'	=> array('OVERVIEW_TITLE', $this->mod_title, $this->new_version),
			'BODY'			=> array('OVERVIEW_BODY', $this->mod_title),
		));
	}
	
	function display()
	{
		global $db, $template, $user, $phpbb_root_path, $phpbb_install_path, $phpEx;
		
		// Generate the tabs
		foreach($this->subs as $msub)
		{
			$template->assign_block_vars('tab', array(
				'S_TITLE'	=> $user->lang[strtoupper($msub)],
				'S_ACTIVE'	=> ($this->sub == $msub) ? true : false,
			));
		}

		$template->assign_vars(array(
			'S_BUTTONS'				=> $this->build_buttons(),
			'S_LOGO'				=> ($this->logo) ? $this->logo : '',
			'T_TEMPLATE_PATH'		=> './style',
			'S_INSTALLER_VERSION'	=> $this->installer_version,
			'S_LANG_OPTIONS'		=> ($this->mode == 'overview') ? language_select($user->lang_name) : '',

			'U_ACTION'		=> append_sid($phpbb_install_path . "index.$phpEx"),
			'U_DOC'			=> ($this->load_doc) ? sprintf($this->canidev_docpage, $this->mod_id, $this->new_version) : '',
		));
		
		if($this->mode != 'overview')
		{
			$this->hidden_fields['lang'] = $user->lang_name;
		}
		
		if(!empty($this->hidden_fields))
		{
			$template->assign_var('S_HIDDEN_FIELDS', build_hidden_fields($this->hidden_fields));
		}

		// Generate the page
		page_header($user->lang('INSTALLER_TITLE', $this->mod_title, $user->lang[$this->page_title], $this->new_version));
		
		$template->set_filenames(array(
			'body' => 'install_body.html'
		));

		page_footer();
	}
	
	function assign_files($file_ary, $filename = '')
	{
		if(!is_array($file_ary))
		{
			$file_ary = array($file_ary => $filename);
		}
		
		foreach($file_ary as $id => $filename)
		{
			if($filename && file_exists($filename))
			{
				$this->package_files[$id] = $filename;
			}
		}
		
		return true;
	}
	
	function include_files()
	{
		global $table_prefix, $db, $user, $phpbb_root_path, $phpEx;
		
		$packages 		= func_get_args();
		$included_files = get_included_files();
		
		foreach($packages as $package)
		{
			$filename = (!empty($this->package_files[$package]) ? $this->package_files[$package] : '');

			if(!$filename || !file_exists($filename))
			{
				trigger_error('INST_ERR');
			}
			
			if(!in_array($filename, $included_files))
			{
				include($filename);
				$included_files[] = $filename;
			}
		}
		
		return true;
	}
	
	function assign_vars($var_name, $value = '')
	{
		global $user, $template;
		
		if(!is_array($var_name))
		{
			$key = $var_name;

			$var_name = array();
			$var_name[$key]	= $value;
		}
		
		// Parse lang string
		foreach($var_name as $id => $value)
		{
			if(is_array($value))
			{
				$var_name[$id] = call_user_func_array(array($user, 'lang'), $value);
			}
			else if(isset($user->lang[$value]))
			{
				$var_name[$id] = $user->lang[$value];
			}
		}
	
		$template->assign_vars($var_name);
	}
	
	function build_buttons($btn_ary = false)
	{
		global $user;

		$result = array();
		
		if(!empty($this->buttons))
		{
			foreach($this->buttons as $btn_id => $data)
			{
				$type = 'mode';

				// Is link
				if(strpos($btn_id, 'link') === 0)
				{
					$result[] = '<button class="link" value="' . $data[1] . '">' . $user->lang[$data[0]] . '</button>';
					continue;
				}
				
				// Is sub
				if(strpos($btn_id, '_') === 0)
				{
					$btn_id = substr($btn_id, 1, strlen($btn_id));
					$type = 'sub';
				}
				
				$result[] = '<button name="' . $type . '" value="' . $btn_id . '">' . $user->lang[$data] . '</button>';
			}
		}
		
		return implode('&nbsp;', $result);
	}
	
	function build_checks($checks, $display_prefix = false)
	{
		global $template, $user;
		
		$passed = array();
		
		if($display_prefix && strpos($checks[0][0], 'legend') === false)
		{
			array_unshift($checks, array('legend1',	$display_prefix . '_RESULTS'));
		}

		foreach($checks as $check)
		{
			if(strpos($check[0], 'legend') !== false)
			{
				$template->assign_block_vars('checks', array(
					'S_LEGEND'	=> $user->lang[$check[1]]
				));
				continue;
			}
			
			$is_valid = (!empty($check[2]) ? true : false);
			$passed[] = $is_valid;
			
			$template->assign_block_vars('checks', array(
				'S_TITLE'	=> $user->lang('CHK_' . $check[0] . (($display_prefix !== false) ? "_$display_prefix" : ''), $check[1]),
				'S_NEUTRAL'	=> (!empty($check[2]) && $check[2] === 'NEUTRAL') ? true : false,
				'S_VALID'	=> $is_valid,
			));
		}
		
		if($display_prefix !== false)
		{
			$l = 'NOTICE';
			$result = true;

			if(in_array(false, $passed))
			{
				$l = 'ERROR';
				$result = false;
			}
			
			$this->assign_vars("S_$l", "{$display_prefix}_$l");
			
			return $result;
		}
		
		return true;
	}
	
	function get_installed_version(&$version)
	{
		global $db, $config;
		
		$version = false;
		
		if($this->version_key !== false)
		{
			if($this->version_table)
			{
				if($this->umil->table_exists($this->version_table))
				{
					$sql = 'SELECT config_value AS version
						FROM ' . $this->version_table . "
						WHERE config_name = '" . $db->sql_escape($this->version_key) . "'";
					$result = $db->sql_query($sql);
					$version = $db->sql_fetchfield('version');
					$db->sql_freeresult($result);
				}
			}
			else if(!empty($config[$this->version_key]))
			{
				$version = $config[$this->version_key];
			}
		}

		// If no version present, set its as the first version and MOD as Uninstalled
		if(!$version)
		{
			$version = '1.0.0';
			return false;
		}

		return true;
	}
	
	/**
	 * Check the latest version information
	 */
	function latest_version_info()
	{
		global $user, $template, $phpbb_root_path, $phpEx;
		
		if(!function_exists('get_remote_file'))
		{
			include($phpbb_root_path . 'includes/functions_admin.' . $phpEx);
		}

		$errstr = '';
		$errno	= 0;
		$split	= explode('.', $this->new_version);

		$info = get_remote_file('www.canidev.com', '/app/info', $this->mod_id . '-' . $split[0] . $split[1] . 'x.txt', $errstr, $errno);

		if($info !== false && preg_match('#^(\d+)\.(\d+)\.(\d+)#', $info))
		{
			$latest_version_info = explode("\n", $info, 2);
			$latest_version = trim($latest_version_info[0]);
			
			if(version_compare($this->new_version, $latest_version, '<'))
			{
				$this->assign_vars('S_ERROR', array('VERSION_NOT_UP_TO_DATE', $latest_version_info[1]));
				return $latest_version;
			}
		}

		return false;
	}
	
	/**
	* Checks that the server we are installing on meets the requirements for running the mod
	*/
	function check_requirements()
	{
		$req_info = array(
			'passed'			=> array(),
			'required_checks'	=> array(),
			'optional_checks'	=> array(),
		);
		
		if(method_exists($this, '_requirements'))
		{
			$req_info = array_merge($req_info, $this->_requirements());
		}
		
		// Check phpbb version
		if($this->required_phpbb)
		{
			$req_info['optional_checks'][] = array('PHPBB_VERSION', $this->required_phpbb, (version_compare(PHPBB_VERSION, $this->required_phpbb, '>=') ? true : false));
		}
		
		if(!empty($req_info['required_checks']))
		{
			$req_info['required_checks'] = array_merge(array(array('legend1', 'REQUIRED_CHECKS')), $req_info['required_checks']);
		}
		
		if(!empty($req_info['optional_checks']))
		{
			$req_info['optional_checks'] = array_merge(array(array('legend2', 'OPTIONAL_CHECKS')), $req_info['optional_checks']);
		}

		$this->build_checks(array_merge($req_info['required_checks'], $req_info['optional_checks']));
		
		$this->page_title = 'REQUIREMENTS';

		$this->assign_vars(array(
			'TITLE'		=> 'REQUIREMENTS_TITLE',
			'BODY'		=> array('REQUIREMENTS_EXPLAIN', $this->mod_title),
		));
		
		if(in_array(false, $req_info['passed']))
		{
			$this->buttons['_requirements'] = 'TRY_AGAIN';
			$this->assign_vars('S_ERROR', 'REQ_INSTALL_ERROR');
		}
		else
		{
			$this->buttons['_install'] = 'INSTALL_START';
			$this->assign_vars('S_NOTICE', 'REQ_INSTALL_NOTICE');
		}
	}
	
	function insert_tables()
	{
		global $user;
		
		$pass = false;

		// Create the tables
		if(!empty($this->schema['table_add']))
		{
			foreach($this->schema['table_add'] as $table_name => $table_data)
			{
				$this->umil->table_add($table_name, $table_data);
			}
			
			$pass = true;
			unset($this->schema['table_add']);
		}

		// Add Default data to tables
		if(!empty($this->schema['row_insert']))
		{
			foreach($this->schema['row_insert'] as $table_name => $sql_data)
			{
				$this->umil->table_row_insert($table_name, $sql_data);
			}
			
			$pass = true;
			unset($this->schema['row_insert']);
		}
		
		// Do some common operations
		$methods = array('config_add', 'table_column_add');
		
		foreach($methods as $method)
		{
			if(!empty($this->schema[$method]))
			{
				call_user_func(array($this->umil, $method), $this->schema[$method]);
				$pass = true;
			}
		}
		
		return $pass;
	}
	
	function insert_modules()
	{
		if(empty($this->schema['modules']))
		{
			return false;
		}
		
		if(isset($this->schema['module_check']))
		{
			$check = $this->schema['module_check'];
			
			if($check !== false && isset($this->schema['modules'][$check]))
			{
				if(call_user_func_array(array($this->umil, 'module_exists'), $this->schema['modules'][$check]))
				{
					return false;
				}
			}
		}
		
		$this->umil->module_add($this->schema['modules']);

		return true;
	}
	
	function insert_permissions()
	{
		if(!empty($this->schema['permission_add']))
		{
			$this->umil->permission_add($this->schema['permission_add']);

			if(!empty($this->schema['permission_set']))
			{
				$this->umil->permission_set($this->schema['permission_set']);
			}
			
			return true;
		}
		
		return false;
	}
	
	function call_custom_function($function_name = '')
	{
		if(!$function_name && empty($this->schema['custom']))
		{
			return false;
		}
		
		if($function_name)
		{
			if(function_exists($function_name))
			{
				return call_user_func($function_name);
			}
			
			return false;
		}
		
		$functions	= (is_array($this->schema['custom']) ? $this->schema['custom'] : array($this->schema['custom']));
		$results	= array();
		
		foreach($functions as $function)
		{
			if(function_exists($function))
			{
				$results[] = call_user_func($function);
			}
			
			$results[] = false;
		}
		
		if(empty($results) || in_array(false, $results))
		{
			return false;
		}	

		return true;
	}
	
	function remove_tables()
	{
		if(!empty($this->schema['table_add']))
		{
			foreach($this->schema['table_add'] as $table_name => $table_data)
			{
				$this->umil->table_remove($table_name);
			
				// Remove the table from the row_insert array
				if(isset($this->schema['row_insert'][$table_name]))
				{
					unset($this->schema['row_insert'][$table_name]);
				}
			}

			unset($this->schema['table_add']);
		}
		
		if(!empty($this->schema['row_insert']))
		{
			foreach($this->schema['row_insert'] as $table_name => $table_data)
			{
				foreach($table_data as $row)
				{
					$keys = array_keys($row);
					$values = array_values($row);

					$this->umil->table_row_remove($table_name, array($keys[0] => $values[0]));
				}
			}
			unset($this->schema['row_insert']);
		}
		
		// Do some common operations
		$methods = array('config_add', 'table_column_add');
		
		foreach($methods as $method)
		{
			$reverse_method = str_replace('_add', '_remove', $method);

			if(!empty($this->schema[$method]) && method_exists($this->umil, $reverse_method))
			{
				call_user_func(array($this->umil, $reverse_method), $this->schema[$method]);
			}
		}
		
		return true;
	}
	
	function remove_modules()
	{
		if(!empty($this->schema['modules']))
		{
			$this->schema['modules'] = array_reverse($this->schema['modules']);
			$this->umil->module_remove($this->schema['modules']);
			
			return true;
		}
		
		return false;
	}
	
	function remove_permissions()
	{
		if(!empty($this->schema['permission_add']))
		{
			if(!empty($this->schema['permission_set']))
			{
				$this->umil->permission_unset($this->schema['permission_set']);
			}
			
			$this->umil->permission_remove($this->schema['permission_add']);
			
			return true;
		}

		return false;
	}
}

?>
