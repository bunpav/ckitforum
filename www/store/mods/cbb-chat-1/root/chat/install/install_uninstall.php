<?php
/**
* @package install_uninstall.php
* @package cBB Chat Installer
* @version $Id: install_uninstall.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
*/
if(!defined('IN_PHPBB') || !defined('IN_INSTALL'))
{
	exit;
}

/**
* Uninstallation
* @package install
*/
class install_uninstall extends installer
{
	var $subs = array('intro', 'uninstall', 'final');

	function main()
	{
		global $user, $phpbb_root_path, $phpEx;

		switch ($this->sub)
		{
			case 'intro':
				$this->page_title = 'INTRO';

				$this->assign_vars('BODY', array('UNINSTALL_INTRO_BODY', $this->mod_title));
				
				$this->buttons['_uninstall'] = 'UNINSTALL_START';
			break;
			
			case 'uninstall':
				$this->page_title = 'UNINSTALL';
				
				$this->include_files('schema_install');
				
				$checks = array(
					array('DATABASE',		false, $this->remove_tables()),
					array('MODULES',		false, $this->remove_modules()),
					array('PERMISSIONS',	false, $this->remove_permissions()),
				);

				// Check if and error occurs
				if($this->build_checks($checks, 'UNINSTALL'))
				{
					$this->buttons['_final'] = 'NEXT_STEP';
				}
			break;
			
			case 'final':
				$this->page_title = 'UNINSTALL_SUCCEFULL';

				// clear cache
				$this->umil->cache_purge();

				$this->assign_vars(array(
					'BODY_TITLE'	=> 'UNINSTALL_SUCCEFULL',
					'S_NOTICE'		=> array('UNINSTALL_SUCCEFULL_EXPLAIN', $this->mod_title, $this->new_version),
				));
				
				$this->buttons['link2'] = array('FORUM_GO', append_sid("{$phpbb_root_path}index.$phpEx"));
			break;
		}
		
		$this->assign_vars('S_TITLE', 'UNINSTALL');
	}
}

?>