<?php
/**
* @package index.php
* @package cBB Chat Installer
* @version $Id: index.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* @ignore
*/
define('IN_PHPBB', true);
define('IN_INSTALL', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../../';
$phpbb_install_path	= $phpbb_root_path . 'chat/install/';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

// Include installer
include($phpbb_install_path . "includes/installer.$phpEx");

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();

// Initialize installer
$installer = create_installer();
$installer->setup(array('mods/chat/installer', 'mods/info_acp_chat'));

$installer->mod_title		= 'cBB Chat';
$installer->new_version		= '1.0.1';
$installer->required_phpbb	= '3.0.8';

// Main files
$installer->assign_files('constants', $phpbb_root_path . "chat/includes/constants.$phpEx");

/* Values to obtain installed version */
$installer->version_key	 = 'chat_version';

/* Global MOD ID in the canidev web */
$installer->mod_id = 'cbb-chat';

/* Have documentation? */
$installer->load_doc = true;

/* Define installer logo */
$installer->logo = generate_board_url() . '/chat/images/cbb-chat-logo.png';

/* Initialize */
$installer->load();

?>