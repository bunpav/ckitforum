<?php
/**
* @package install_update.php
* @package cBB Chat Installer
* @version $Id: install_update.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
*/
if(!defined('IN_PHPBB') || !defined('IN_INSTALL'))
{
	exit;
}

/**
* Installation
* @package install
*/
class install_update extends installer
{
	var $subs = array('intro', 'update', 'final');

	function main()
	{
		global $user, $phpbb_root_path, $phpEx;

		switch ($this->sub)
		{
			case 'intro':
				$this->page_title = 'INTRO';
				
				$this->get_installed_version($current_version);
				
				$this->assign_vars(array(
					'BODY_TITLE'	=> 'UPDATE_INTRO',
					'BODY'			=> array('UPDATE_INTRO_BODY', $this->mod_title, $current_version, $this->new_version)
				));
				
				$this->buttons['_update'] = 'UPDATE_START';
			break;
			
			case 'update':
				$this->page_title = 'UPDATE';

				$checks = array(
					array('DATABASE', false, $this->perform_update()),
				);
				
				// Check if and error occurs
				if($this->build_checks($checks, 'UPDATE'))
				{
					$this->buttons['_final'] = 'NEXT_STEP';
				}
			break;

			case 'final':
				$this->page_title = 'UPDATE_SUCCEFULL';

				// clear cache
				$this->umil->cache_purge();

				$this->assign_vars(array(
					'BODY_TITLE'	=> 'UPDATE_SUCCEFULL',
					'S_NOTICE'		=> array('UPDATE_SUCCEFULL_EXPLAIN', $this->mod_title, $this->new_version),
				));
				
				$this->buttons['link2'] = array('LOGIN', append_sid($phpbb_root_path . 'adm/index.' . $phpEx, 'i=chat&amp;mode=config', true, $user->session_id));
			break;
		}

		$this->assign_vars('S_TITLE', 'UPDATE');
	}
	
	function perform_update()
	{
		global $db;

		$this->update_error 	= false;
		$this->current_version	= false;
		
		$this->get_installed_version($this->current_version);

		$this->include_files('schema_install', 'schema_update');

		set_config($this->version_key, $this->new_version);
		
		return ($this->update_error) ? false : true;
	}
}

?>