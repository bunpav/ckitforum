<?php
/**
* @package install_install.php
* @package cBB Chat Installer
* @version $Id: install_install.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
*/
if(!defined('IN_PHPBB') || !defined('IN_INSTALL'))
{
	exit;
}

/**
* Installation
* @package install
*/
class install_install extends installer
{
	var $subs = array('requirements', 'install', 'final');

	function main()
	{
		global $user, $phpbb_root_path, $phpEx;

		switch ($this->sub)
		{
			case 'requirements':
				$this->check_requirements();
			break;
			
			case 'install':
				$this->page_title = 'INSTALL';
				
				$this->include_files('schema_install');

				$checks = array(
					array('DATABASE',		false, $this->insert_tables()),
					array('MODULES',		false, $this->insert_modules()),
					array('PERMISSIONS',	false, $this->insert_permissions()),
				);
				
				// Check if any error occurs
				if($this->build_checks($checks, 'INSTALL'))
				{
					$this->buttons['_final'] = 'NEXT_STEP';
				}
			break;

			case 'final':
				$this->page_title = 'INSTALL_CONGRATS';

				// clear cache
				$this->umil->cache_purge();

				$this->assign_vars(array(
					'BODY_TITLE'	=> 'INSTALL_CONGRATS',
					'S_NOTICE'		=> array('INSTALL_CONGRATS_EXPLAIN', $this->mod_title, $this->new_version),
				));
				
				$this->buttons['link2'] = array('LOGIN', append_sid($phpbb_root_path . "adm/index.$phpEx", 'i=chat&amp;mode=config', true, $user->session_id));
			break;
		}

		$this->assign_vars('S_TITLE', 'INSTALL');
	}
	
	function _requirements()
	{
		global $phpbb_root_path, $phpEx;
		
		$checks = array();
		$passed = array('mod' => false);

		// Check the mod
		if(file_exists($phpbb_root_path . "chat/includes/constants.$phpEx"))
		{
			$passed['mod'] = true;
		}

		$checks[] = array('MOD_CLASS', false, $passed['mod']);

		return array(
			'passed'			=> $passed,
			'required_checks'	=> $checks,
		);
	}
}

?>