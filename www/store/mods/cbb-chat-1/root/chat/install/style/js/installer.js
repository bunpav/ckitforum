/*
*
* @name installer.js
* @package cBB Installer
* @version $Id: installer.js,v1.0.4 15/07/2013 $
*
* @copyright (c) 2013 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/
$(function(){function e(e){var t=$("form:first");if(e){$.each(e,function(e,n){if(e.indexOf("input")!=-1){e=n.attr("name");n=n.attr("value")}t.append('<input type="hidden" name="'+e+'" value="'+n+'" />')})}t.attr("onsubmit","").submit()}$("button").click(function(t){if(!$(this).is(".disabled")){if($(this).is(".link")){var n=$(this).attr("value");if($(this).is(".blank")){window.open(n)}else{window.location=n}}else{$(".center-panel").html("").append($("#loading"));$("#loading").show();$("button").addClass("disabled");data={input1:$(this)};setTimeout(function(){e(data)},500)}}t.preventDefault()});$("#lang-select").change(function(t){e();t.preventDefault()});$("a[rel=blank]").click(function(e){window.open(this.href);e.preventDefault()})})