<?php
/** 
* installer.php [English [En]]
*
* @package language cBB Installer
* @version $Id: installer.php v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// DO NOT CHANGE
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

/* General */
$lang = array_merge($lang, array(
	'OVERVIEW_TITLE'		=> '¡Welcome to %1$s %2$s!',
	'OVERVIEW_BODY'			=> 'Welcome to %1$s installer wizard for phpBB 3.0.x.<br /><br />
		This installation system will guide you to install, update or uninstall %1$s.<br />
		For more information, please read the installation guide located in <em>contrib</em> directory.<br /><br />
		To continue, please select one of the options below.',

	'INSTALLER_TITLE'		=> '%1$s %3$s &bull; %2$s',

	'BACK'					=> 'Back',
	'FINAL'					=> 'Final step',
	'FORUM_GO'				=> 'Go to Forum',
	'HOME'					=> 'Home',
	'INTRO'					=> 'Intro',
	'INST_ERR'				=> 'There was an error during installation that forces to cancel.<br />This error may be due to changes made to the files incorrectly on the forum or has corrupted files.',
	'LOADING'				=> 'Loading',
	'NEED_HELP'				=> '¿Need Help?',
	'NOMOD'					=> 'Mod not installed',
	'ONLINE_DOCUMENTATION'	=> 'Online documentation',
	'OPTIONAL_CHECKS'		=> 'Additional requirements',
	'REQUIRED_CHECKS'		=> 'Need Requirements',
	'REQUIREMENTS'			=> 'Requirements',
	'SUPPORT_FORUM'			=> 'Support Forum',
	'TRY_AGAIN'				=> 'Try again',
	'VERSION_INFO'			=> 'Version Information',
	'VERSION_NOT_UP_TO_DATE'	=> 'A newer version of this MOD is available.<br />
		For more details about the new version click on <a href="%s">this link</a>.',
	'WELCOME'				=> 'Welcome',

	'CHK_DIR_PERMISSIONS'		=> 'Writable folder "%s"',
	'CHK_VERSION_INSTALLED'		=> 'Installed version: %s',
	'CHK_VERSION_LATEST'		=> 'Latest version: %s',
	'CHK_VERSION_PACKAGE'		=> 'Package version: %s',
	'CHK_PHPBB_VERSION'			=> 'phpBB %s or higher.',
));

/* Install */
$lang = array_merge($lang, array(
	'INSTALL'					=> 'Install',
	'INSTALL_CONGRATS'			=> 'Congratulations!',
	'INSTALL_CONGRATS_EXPLAIN'	=> 'The installation of %1$s %2$s has successfully completed.<br /><br />
		Enter in your forum and configure %1$s!<br />
		By clicking the button below reach the main configuration screen of %1$s.',
	'INSTALL_ERROR'			=> 'Opss! Errors occurred during installation.<br />
		Try reinstalling the MOD or request help in <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'INSTALL_NOTICE'		=> 'The installation was completed successfully.<br />Proceed to the next step to terminate it.',
	'INSTALL_RESULTS'		=> 'Installation Results',
	'INSTALL_START'			=> 'Install',

	'CHK_CUSTOM_INSTALL'		=> 'Execute aditional operations.',
	'CHK_DATABASE_INSTALL'		=> 'Create tables and insert basic values in the database.',
	'CHK_MOD_CLASS'				=> 'MOD Main Class.',
	'CHK_MODULES_INSTALL'		=> 'Insert or update modules.',
	'CHK_PERMISSIONS_INSTALL'	=> 'Insert or update Permissions.',
	'CHK_SYNC_INSTALL'			=> 'Synchronize data.',

	'REQUIREMENTS_TITLE'		=> 'Installation Compatibility',
	'REQUIREMENTS_EXPLAIN'		=> 'Before proceeding with the full installation, %1$s will perform some tests on your server configuration to ensure that it can be installed without problems.<br />
		Please be sure to read carefully and thoroughly the results and not continue until all required tests are approved.',

	'REQ_INSTALL_NOTICE'	=> 'Your system meets the requirements to install the MOD.',
	'REQ_INSTALL_ERROR'		=> 'Not met the requirements to install the MOD.<br />Check if you followed all the steps of the <em>install.xml</em> file.',
));

/* Uninstall */
$lang = array_merge($lang, array(
	'UNINSTALL'				=> 'Uninstall',
	'UNINSTALL_ERROR'		=> 'Opss! Errors occurred during uninstall.<br />
		Try reinstalling the MOD or request help in <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'UNINSTALL_INTRO'			=> 'Welcome to the Uninstall',
	'UNINSTALL_INTRO_BODY'		=> '<p>With this option, you remove %1$s completely from your phpBB 3.0.x forum.</p>
		<p>This process eliminates the values and tables in the database but the changes in phpBB files must remove manually.</p><br />
		<p>This process can not be undone once started.</p><br />
		<p>To start the uninstallation, please press the button below.</p>',
	'UNINSTALL_NOTICE'		=> 'Uninstall was completed successfully.<br />Proceed to the next step to terminate it.',
	'UNINSTALL_RESULTS'		=> 'Uninstall Results',
	'UNINSTALL_START'		=> 'Uninstall',
	'UNINSTALL_SUCCEFULL'			=> 'Uninstall Complete!',
	'UNINSTALL_SUCCEFULL_EXPLAIN'	=> 'Successfully uninstalled %1$s %2$s.<br /><br />
		Enter in your forum and make sure everything works fine!<br />
		By clicking the button below you reach the General Index of the forum.',

	'CHK_CUSTOM_UNINSTALL'		=> 'Execute aditional operations.',
	'CHK_DATABASE_UNINSTALL'	=> 'Delete Tables and Values in the Database.',
	'CHK_MOD_INSTALLED'			=> 'MOD installed.',
	'CHK_MODULES_UNINSTALL'		=> 'Delete modules.',
	'CHK_PERMISSIONS_UNINSTALL'	=> 'Remove user Permissions.',

	'REQ_UNINSTALL_NOTICE'	=> 'Your system meets the requirements to uninstall the MOD.',
	'REQ_UNINSTALL_ERROR'	=> 'Not met the requirements to uninstall the MOD.',
));

/* Update */
$lang = array_merge($lang, array(
	'CHK_DATABASE_UPDATE'		=> 'Update tables and values in the database.',

	'UPDATE'			=> 'Update',
	'UPDATE_ERROR'		=> 'Opss! Errors occurred during update.<br />
		Try reinstalling the MOD or request help in <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'UPDATE_INTRO'				=> 'Welcome to the Update',
	'UPDATE_INTRO_BODY'			=> '<p>With this option, you update %1$s to the latest version.</p><br />
		<p>Before proceeding, ensure that:</p>
		<ol>
			<li>You copied/uploaded the files of the latest version of %1$s in forum directory (overwriting the old version).</li>
			<li>You have made the necessary changes to phpBB files as explains in the update file <em>contrib/update/%2$s_to_%3$s/update.xml</em>.</li>
		</ol><br />
		<p><strong>Note:</strong> It is recommended to backup the database before continuing with this process.</p>
		<p>To start the update, please press the button below.</p>',
	'UPDATE_NOTICE'		=> 'The update has been successfully completed.<br />Proceed to the next step to terminate it.',
	'UPDATE_RESULTS'	=> 'Update Results',
	'UPDATE_START'		=> 'Update',
	'UPDATE_SUCCEFULL'			=> 'Update completed!',
	'UPDATE_SUCCEFULL_EXPLAIN'	=> 'Enter in your forum and test the new %1$s %2$s !<br />
		By clicking the button below reach the main configuration screen of %1$s.',
));
