<?php
/** 
* permissions_chat.php [English [En]]
* @package language cBB Chat
* @version $Id: permissions_chat.php v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang['permission_cat']['chat'] = 'Chat';

$lang = array_merge($lang, array(
	'acl_a_chat'				=> array('lang'	=> 'Can manage the chat',			'cat'	=> 'misc'),

	'acl_m_chat_delete'			=> array('lang'	=> 'Can delete messages',			'cat'	=> 'chat'),
	'acl_m_chat_edit'			=> array('lang'	=> 'Can edit messages',				'cat'	=> 'chat'),

	'acl_u_chat_archive'		=> array('lang'	=> 'Can view the archive',			'cat'	=> 'chat'),
	'acl_u_chat_delete'			=> array('lang'	=> 'Can delete own messages',		'cat'	=> 'chat'),
	'acl_u_chat_edit'			=> array('lang'	=> 'Can edit own messages',			'cat'	=> 'chat'),
	'acl_u_chat_ignoreflood'	=> array('lang'	=> 'Can ignore flood limit',		'cat'	=> 'chat'),
	'acl_u_chat_post'			=> array('lang'	=> 'Can post messages',				'cat'	=> 'chat'),
	'acl_u_chat_sendpm'			=> array('lang'	=> 'Can send private messages',		'cat'	=> 'chat'),
	'acl_u_chat_view'			=> array('lang'	=> 'Can view the chat',				'cat'	=> 'chat'),
));
