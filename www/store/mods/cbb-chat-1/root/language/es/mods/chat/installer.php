<?php
/** 
* installer.php [Spanish [Es]]
*
* @package language cBB Installer
* @version $Id: installer.php v1.0.4 15/07/2013 $
*
* @copyright (c) 2013 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

/* General */
$lang = array_merge($lang, array(
	'OVERVIEW_TITLE'		=> '¡Bienvenido a %1$s %2$s!',
	'OVERVIEW_BODY'			=> 'Bienvenido al asistente de instalación de %1$s para phpBB 3.0.x.<br /><br />
		Este sistema de instalación le guiará para instalar, actualizar o desinstalar %1$s.<br />
		Para más información, le aconsejamos que lea la guía de instalación alojada en el directorio <em>contrib</em>.<br /><br />
		Para continuar, por favor seleccione una de las opciones inferiores.',

	'INSTALLER_TITLE'		=> '%1$s %3$s &bull; %2$s',

	'BACK'					=> 'Atrás',
	'FINAL'					=> 'Paso Final',
	'FORUM_GO'				=> 'Ir al Foro',
	'HOME'					=> 'Inicio',
	'INTRO'					=> 'Introducción',
	'INST_ERR'				=> 'Ha ocurrido un error durante la instalación que obliga a cancelarla.<br />Este error puede deberse a cambios incorrectamente realizados en los archivos del foro o que haya archivos corruptos.',
	'LOADING'				=> 'Cargando',
	'NEED_HELP'				=> '¿Necesita Ayuda?',
	'NOMOD'					=> 'Mod aún no instalado',
	'ONLINE_DOCUMENTATION'	=> 'Documentación online',
	'OPTIONAL_CHECKS'		=> 'Requisitos adicionales',
	'REQUIRED_CHECKS'		=> 'Requisitios necesarios',
	'REQUIREMENTS'			=> 'Requisitos',
	'SUPPORT_FORUM'			=> 'Foro de Soporte',
	'TRY_AGAIN'				=> 'Probar de nuevo',
	'VERSION_INFO'			=> 'Información de la versión',
	'VERSION_NOT_UP_TO_DATE'	=> 'Está disponible una versión más reciente de este MOD.<br />
		Para conocer más detalles sobre la nueva versión haga clic en <a href="%s">este enlace</a>.',
	'WELCOME'				=> 'Bienvenido',

	'CHK_DIR_PERMISSIONS'		=> 'Permiso de escritura (777) en "%s"',
	'CHK_VERSION_INSTALLED'		=> 'Versión Instalada: %s',
	'CHK_VERSION_LATEST'		=> 'Versión más reciente: %s',
	'CHK_VERSION_PACKAGE'		=> 'Versión de este paquete: %s',
	'CHK_PHPBB_VERSION'			=> 'phpBB %s o superior.',
));

/* Install */
$lang = array_merge($lang, array(
	'INSTALL'					=> 'Instalar',
	'INSTALL_CONGRATS'			=> 'Instalación finalizada',
	'INSTALL_CONGRATS_EXPLAIN'	=> 'La instalación de %1$s %2$s ha finalizado con éxito.<br /><br />
		¡Ingrese en su foro y configure %1$s!<br />
		Haciendo clic en el botón de abajo llegará a la pantalla de configuración principal de %1$s.',
	'INSTALL_ERROR'			=> 'Opss! Han ocurrido errores durante la instalación.<br />
		Intente volver a instalar el MOD o solicite ayuda en <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'INSTALL_NOTICE'		=> 'La instalación se ha completado con éxito.<br />Proceda al siguiente paso para finalizarla.',
	'INSTALL_RESULTS'		=> 'Resultados de la Instalación',
	'INSTALL_START'			=> 'Comenzar instalación',

	'CHK_CUSTOM_INSTALL'		=> 'Ejecutar operaciones adicionales.',
	'CHK_DATABASE_INSTALL'		=> 'Insertar Tablas y Valores en la Base de Datos.',
	'CHK_MOD_CLASS'				=> 'Clase principal del MOD.',
	'CHK_MODULES_INSTALL'		=> 'Insertar o actualizar Módulos.',
	'CHK_PERMISSIONS_INSTALL'	=> 'Insertar o actualizar Permisos de usuarios.',
	'CHK_SYNC_INSTALL'			=> 'Sincronizar datos.',

	'REQUIREMENTS_TITLE'		=> 'Compatibilidad de la instalación',
	'REQUIREMENTS_EXPLAIN'		=> 'Antes de proceder con la instalación completa, %1$s llevará a cabo algunas pruebas de la configuración y archivos en su servidor para asegurarse de que puede instalarse sin problemas.<br />
		Por favor asegúrese de leer completa y cuidadosamente los resultados y no continuar hasta que todos las pruebas requeridas estén aprobadas.',

	'REQ_INSTALL_NOTICE'	=> 'Su sistema cumple los requisitos necesarios para instalar el MOD.',
	'REQ_INSTALL_ERROR'		=> 'No se cumplen los requisitos necesarios para instalar el MOD.<br />Compruebe si ha seguido correctamente todos los pasos del archivo <em>install.xml</em>.',
));

/* Uninstall */
$lang = array_merge($lang, array(
	'UNINSTALL'				=> 'Desinstalar',
	'UNINSTALL_ERROR'		=> 'Opss! Han ocurrido errores durante la desinstalación.<br />
		Intente volver a desinstalar el MOD o solicite ayuda en <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'UNINSTALL_INTRO'			=> 'Bienvenido a la Desinstalación',
	'UNINSTALL_INTRO_BODY'		=> '<p>Con esta opción, eliminará %1$s completamente de su foro phpBB 3.0.x.</p>
		<p>Este proceso elimina los valores de la base de datos y los modulos del Panel de Administración pero los cambios introducidos manualmente en los archivos de phpBB deberá retirarlos manualmente.</p><br />
		<p>Este proceso no puede deshacerse una vez comenzado.</p><br />
		<p>Para comenzar la desinstalación, por favor pulse el botón de abajo.</p>',
	'UNINSTALL_NOTICE'		=> 'La desinstalación se ha completado con éxito.<br />Proceda al siguiente paso para finalizarla.',
	'UNINSTALL_RESULTS'		=> 'Resultados de la Desinstalación',
	'UNINSTALL_START'		=> 'Comenzar Desinstalación',
	'UNINSTALL_SUCCEFULL'			=> '¡Desinstalación completada!',
	'UNINSTALL_SUCCEFULL_EXPLAIN'	=> 'Desinstaló correctamente %1$s %2$s.<br /><br />
		¡Ingrese en su foro y compruebe que todo funciona bien!<br />
		Haciendo clic en el botón de abajo llegará al Índice General de su foro.',

	'CHK_CUSTOM_UNINSTALL'		=> 'Ejecutar operaciones adicionales',
	'CHK_DATABASE_UNINSTALL'	=> 'Eliminar Tablas y Valores en la Base de Datos.',
	'CHK_MOD_INSTALLED'			=> 'MOD instalado.',
	'CHK_MODULES_UNINSTALL'		=> 'Eliminar Módulos.',
	'CHK_PERMISSIONS_UNINSTALL'	=> 'Eliminar Permisos de usuarios.',

	'REQ_UNINSTALL_NOTICE'	=> 'Su sistema cumple los requisitos necesarios para desinstalar el MOD.',
	'REQ_UNINSTALL_ERROR'	=> 'No se cumplen los requisitos necesarios para desinstalar el MOD.',
));

/* Update */
$lang = array_merge($lang, array(
	'CHK_DATABASE_UPDATE'		=> 'Actualizar Tablas y Valores en la Base de Datos.',

	'UPDATE'			=> 'Actualizar',
	'UPDATE_ERROR'		=> 'Opss! Han ocurrido errores durante la actualización.<br />
		Intente volver a actualizar el MOD o solicite ayuda en <a href="http://www.canidev.com" onclick="window.open(this.href); return false;">www.CaniDev.com</a>.',
	'UPDATE_INTRO'				=> 'Bienvenido a la Actualización',
	'UPDATE_INTRO_BODY'			=> '<p>Con esta opción, actualizará la instalación de %1$s de su foro a la última versión.</p><br />
		<p>Antes de proceder, asegúrese de que:</p>
		<ol>
			<li>Ha copiado/subido los archivos de la última versión de %1$s al directorio del foro (sobreescribiendo los de la antigua versión).</li>
			<li>Ha hecho los cambios necesarios en los archivos de phpBB como explica el archivo de actualización <em>contrib/update/%2$s_to_%3$s/update.xml</em>.</li>
		</ol><br />
		<p><strong>Nota:</strong> Se recomienda realizar un backup de la Base de Datos antes de continuar con este proceso.</p>
		<p>Para comenzar la actualización, por favor pulse el botón de abajo.</p>',
	'UPDATE_NOTICE'				=> 'La actualización se ha completado con éxito.<br />Proceda al siguiente paso para finalizarla.',
	'UPDATE_RESULTS'			=> 'Resultados de la Actualización',
	'UPDATE_START'				=> 'Comenzar actualización',
	'UPDATE_SUCCEFULL'			=> '¡Actualización completada!',
	'UPDATE_SUCCEFULL_EXPLAIN'	=> '¡ Ingrese en su foro y pruebe el nuevo %1$s %2$s !<br />
		Haciendo clic en el botón de abajo llegará a la pantalla de configuración principal de %1$s.',
));
