<?php
/**
*
* Module Breizh Shoutbox [English]
*
* @package language
* @version $Id: info_acp_shoutbox.php 150 16:15 16/12/2011 Sylver35 Exp $ 
* @copyright (c) 2010, 2011 Breizh Portal  http://breizh-portal.com
* @copyright (c) 2007 Paul Sohier
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

/**
* NOTE: Most of the language items are used in javascript
* If you want to use quotes or other chars that need escaped, be sure you escape them double
*/
$lang = array_merge($lang, array(
	// Main tab
	'ACP_SHOUTBOX'          		=> 'Чат',
	// General category
	'ACP_SHOUT_GENERAL_CAT'         => 'Общие',
		'ACP_SHOUT_VERSION'				=> 'Обновление версии',
		'ACP_SHOUT_VERSION_T'			=> 'Поиск новых версий чата',
		'ACP_SHOUT_VERSION_T_EXPLAIN'	=> 'На данной странице вы будете уведомлены о выходе новых версий чата.',
		
		'ACP_SHOUT_CONFIGS'				=> 'Основные настройки',
		'ACP_SHOUT_CONFIGS_T'			=> 'Основные настройки чата',
		'ACP_SHOUT_CONFIGS_T_EXPLAIN'  	=> 'На этой странице осуществляются основные настройки чата.',
		
		
		
		
		
		'ACP_SHOUT_RULES'				=> 'Использование правил',
		'ACP_SHOUT_RULES_T'				=> 'Панель с правилами чата',
		'ACP_SHOUT_RULES_T_EXPLAIN'		=> 'На этой странице вы можете написать свои правила для общения в чате',
	// Category for main shoutbox
	'ACP_SHOUT_PRINCIPAL_CAT'		=> 'Чат',
		
		
		
		
		'ACP_SHOUT_OVERVIEW'			=> 'Сообщения и статистика',
		'ACP_SHOUT_OVERVIEW_T'			=> 'Сообщения и статистика чата',
		'ACP_SHOUT_OVERVIEW_T_EXPLAIN'	=> 'На этой странице вы можете увидеть статистику чата.
											<br />Также здесь вы можете очищать чат.',
		'ACP_SHOUT_CONFIG_GEN'			=> 'Настройки чата',
		
		
		'ACP_SHOUT_CONFIG_GEN_T'		=> 'Настройки чата на форуме',
		'ACP_SHOUT_CONFIG_GEN_T_EXPLAIN'=> 'На этой странице вы можете настроить отображение чата на форуме.',
	// Category for private shoutbox
	'ACP_SHOUT_PRIVATE_CAT'			=> 'Приватный чат',
		
		
		
		
		
		
		'ACP_SHOUT_PRIVATE'				=> 'Сообщения и статистика',
		'ACP_SHOUT_PRIVATE_T'			=> 'Сообщения и статистика приватного чата',
		'ACP_SHOUT_PRIVATE_T_EXPLAIN'	=> 'На этой странице вы можете увидеть статистику приватного чата, а так же очистить его.',
		
		'ACP_SHOUT_CONFIG_PRIV'			=> 'Настройки приватного чата',
		'ACP_SHOUT_CONFIG_PRIV_T'		=> 'Настройки приватного чата на форуме',
		'ACP_SHOUT_CONFIG_PRIV_T_EXPLAIN'=> 'На этой странице вы можете настроить отображение приватного чата на форуме.',
	// Category for popup shoutbox
	'ACP_SHOUT_POPUP_CAT'			=> 'Чат в новом окне',
		
		
		
		'ACP_SHOUT_POPUP'				=> 'Настройки чата в новом окне',
		'ACP_SHOUT_POPUP_T'				=> 'Настройки для отображения чата в новом окне',
		'ACP_SHOUT_POPUP_T_EXPLAIN'		=> 'На этой странице вы сможете настроить отображение чата в новом окне',
		// Category for retractable lateral panel
		// Added in version 1.5.0
		
		
		
		'ACP_SHOUT_PANEL'				=> 'Настройки боковой панели',
		'ACP_SHOUT_PANEL_T'				=> 'Основные настройки боковой панели',
		'ACP_SHOUT_PANEL_T_EXPLAIN'		=> 'На этой странице вы сможете настроить отображение боковой панели чата',
	// Category for smilies
	'ACP_SHOUT_SMILIES_CAT'			=> 'Смайлики',
		
		
		
		'ACP_SHOUT_SMILIES'				=> 'Настройки смайликов',
		'ACP_SHOUT_SMILIES_T'			=> 'Настройка панели смайликов в чате',
		'ACP_SHOUT_SMILIES_T_EXPLAIN'	=> 'На этой странице вы сможете настроить панель смайликов, определить какие смайлики будут отображаться сразу, а какие во всплывающем окне',
	// Category for robot
	'ACP_SHOUT_ROBOT_CAT'			=> 'Робот',
		
		
		
		'ACP_SHOUT_ROBOT'				=> 'Настройки робота',
		'ACP_SHOUT_ROBOT_T'				=> 'Настройки робота в чате',
		'ACP_SHOUT_ROBOT_T_EXPLAIN'		=> 'На этой странице вы сможете настроить поведение робота в чате',
		'ACP_SHOUT_ROBOT_MOD'			=> 'Моды робота',
		
		
		'ACP_SHOUT_ROBOT_MOD_T'			=> 'Параметры робота для модов',
		'ACP_SHOUT_ROBOT_MOD_T_EXPLAIN'	=> 'На этой странице вы сможете настроить поведение робота для конкретных модов',
	// Language for Logs
	'LOG_SHOUT_CONFIGS'				=> '<strong>Общие настройки обновлены.</strong>',
	'LOG_SHOUT_CONFIG_GEN'			=> '<strong>Настройки чата обновлены.</strong>',
	'LOG_SHOUT_CONFIG_PRIV'			=> '<strong>Настройки приватного чата обновлены.</strong>',
	'LOG_SHOUT_RULES'				=> '<strong>Правила чата обновлены.</strong>',
	
	'LOG_SHOUT_POPUP'				=> '<strong>Обновлены настройки чата во всплывающем окне.</strong>',
	'LOG_SHOUT_ROBOT'				=> '<strong>Обновлены настройки робота.</strong>',
	'LOG_SHOUT_ROBOT_MOD'			=> '<strong>Обновлены настройки робота для модов.</strong>',
	'LOG_PURGE_SHOUTBOX'			=> '<strong>Очищен чат от всех сообщений</strong>',
	'LOG_PURGE_SHOUTBOX_ROBOT'		=> '<strong>Очищен чат от сообщений робота</strong>',
	'LOG_PURGE_SHOUTBOX_PRIV'		=> '<strong>Очищен приватный чат от сообщений</strong>',
	'LOG_PURGE_SHOUTBOX_PRIV_ROBOT'	=> '<strong>Очищен приватный чат от сообщений робота</strong>',
	'LOG_SELECT_SHOUTBOX'			=> '<strong>Удалено %s выбранное  сообщение из чата</strong>',
	
	
	'LOG_SELECTS_SHOUTBOX'			=> '<strong>Удалено %s выбранных сообщений из приватного чата</strong>',
	'LOG_SELECT_SHOUTBOX_PRIV'		=> '<strong>Удалено %s выбранное  сообщение приватного из чата</strong>',
	'LOG_SELECTS_SHOUTBOX_PRIV'		=> '<strong>Удалено %s выбранное  сообщение из чата</strong>',
	'LOG_LOG_SHOUTBOX'				=> '<strong>Удалено %s выделенный пункт из лога.</strong>',
	'LOG_LOGS_SHOUTBOX'				=> '<strong>Удалено %s выделенных пунктов из лога.</strong>',
	'LOG_LOG_SHOUTBOX_PRIV'			=> '<strong>Удалено %s выделенный пункт из лога.</strong>',
	'LOG_LOGS_SHOUTBOX_PRIV'		=> '<strong>Удалено %s выделенных пунктов из лога.</strong>',
	'LOG_SHOUT_UPDATED'				=> '<strong>Чат обновлен с %1$s до %2$s</strong>',
	'LOG_SHOUT_UPGRADED'			=> '<strong>Обновлен чат с 1.0.x до %1$s</strong>',
	'LOG_SHOUT_INSTALL_DATABASE'	=> '<strong>Создана таблицы чата и добавлены данные в таблицу конфигурации<strong>',
	'LOG_SHOUT_UPDATE_DATABASE'		=> '<strong>Обновлена таблица конфигурации для Breizh чата<strong>',
	'LOG_SHOUT_PANEL'          		=> '<strong>Параметры выдвижной боковой панели обновлены</strong>', // Ajout dans la version 1.5.0
	'LOG_SHOUT_PRUNED'          	=> '<strong>Обрезать Breizh чат</strong>',
	'LOG_SHOUT_PRIV_PRUNED'         => '<strong>Обрезать приватный Breizh чат</strong>',
	'LOG_SHOUT_REMOVED'         	=> '<strong>Автоматическое удаление %s сообщений в чате.</strong>',
	'LOG_SHOUT_REMOVED'         	=> '<strong>Автоматическое удаление %s сообщений в приватном чате.</strong>',
	'LOG_SHOUT_PURGED'         		=> '<strong>Purge time automatic %s messages in the shoutbox.</strong>',
	'LOG_SHOUT_PRIV_PURGED'         => '<strong>Purge time automatic %s messages in the private shoutbox.</strong>',
	'LOG_SHOUT_SCRIPT'				=> '<strong>Attempt to post script in the shoutbox.</strong>',
	'LOG_SHOUT_APPLET'				=> '<strong>Attempt to post applet in the shoutbox.</strong>',
	'LOG_SHOUT_ACTIVEX'				=> '<strong>Attempt to post active x object in the shoutbox.</strong>',
	'LOG_SHOUT_OBJECTS'				=> '<strong>Attempt to post chrome or about object in the shoutbox.</strong>',
	'LOG_SHOUT_IFRAME'				=> '<strong>Attempt to post iframe in the shoutbox.</strong>',
	'LOG_SHOUT_PRUNED_PRIV'         => '<strong>Pruned private Breizh Shoutbox</strong>',
	'LOG_SHOUT_REMOVED_PRIV'        => '<strong>Automatic deletion of %s posts in the private shoutbox.</strong>',
	'LOG_SHOUT_PURGED_PRIV'         => '<strong>Purge time automatic %s messages in the private shoutbox.</strong>',
	'LOG_SHOUT_SCRIPT_PRIV'			=> '<strong>Attempt to post script in the private shoutbox.</strong>',
	'LOG_SHOUT_APPLET_PRIV'			=> '<strong>Attempt to post applet in the private shoutbox.</strong>',
	'LOG_SHOUT_ACTIVEX_PRIV'		=> '<strong>Attempt to post active x object in the private shoutbox.</strong>',
	'LOG_SHOUT_OBJECTS_PRIV'		=> '<strong>Attempt to post chrome or about object in the private shoutbox.</strong>',
	'LOG_SHOUT_IFRAME_PRIV'			=> '<strong>Attempt to post iframe in the private shoutbox.</strong>',
	
	
	
	
	'SHOUT_SMILIES_ON'				=> 'Показывать сразу',
	'SHOUT_SMILIES_POP'				=> 'Отправить во всплывающее окно',
	'DISPLAY_ON_SHOUTBOX'			=> 'Отображать кнопку настроек чата',
	'SHOUT_RULES_ACTIVE'			=> 'Правила чата',
	
	'SHOUT_RULES_ACTIVE_EXPLAIN'	=> 'Включить/выключить правила в чате.',
	'SHOUT_RULES_ON'				=> 'Правила для языка “%s”',
	
	
	
	
	'SHOUT_RULES_ON_EXPLAIN'		=> 'Пожалуйста введите правила общего чата для языка “%s” ',
	'SHOUT_RULES_ON_PRIV_EXPLAIN'	=> 'Пожалуйста введите правила приватного чата для языка “%s” ',
	'SHOUT_RULES_VIEW'				=> 'Правила чата в общем чате:',
	'SHOUT_RULES_VIEW_PRIV'			=> 'Правила чата в приватном чате:',
	'SMILIES_EMOTION'				=> 'Смайлики',
	'SMILIES_OVERVIEW'				=> 'Смайлики в чате',
	
	
	'SMILIES_POPUP'					=> 'Смайлики во всплывающем окне',
	'SMILIES_DISPLAYED'				=> 'Отправить во всплывающее окно',
	'SMILIES_NO_DISPLAYED'			=> 'Показывать в чате',
	
	
	
	
	
	
	'SMILIES_CLIC_NO'				=> 'Кликните сюда, чтобы данный смайл не показывался',
	'SMILIES_CLIC_YES'				=> 'Кликните сюда, чтобы данный смайл показывался',
	'SMILIES_WIDTH'					=> 'Ширина всплывающего окна со смайликами',
	'SMILIES_WIDTH_EXPLAIN'			=> 'Впишите ширину всплывающего окна со смайликами, в пикселях',
	'SMILIES_HEIGHT'				=> 'Высота всплывающего окна со смайликами',
	'SMILIES_HEIGHT_EXPLAIN'		=> 'Впишите высоту всплывающего окна со смайликами, в пикселях',
	'ORDER'							=> 'order',
	
	
	'SHOUT_AVATAR'					=> 'Показывать аватары',
	'SHOUT_AVATAR_EXPLAIN'			=> 'Показывать аватары рядом с сообщениями пользователей',
	'SHOUT_AVATAR_HEIGHT'			=> 'Размеры аватаров',
	
	'SHOUT_AVATAR_HEIGHT_EXPLAIN'	=> 'Введите высоту аватаров, в пикселях, ширина автоматически подсчитывается',
	'SHOUT_AVATAR_ROBOT'			=> 'Аватар робота',
	
	
	
	
	
	'SHOUT_AVATAR_ROBOT_EXPLAIN'	=> 'Включить/выключить аватар для робота',
	'SHOUT_STATS'      				=> 'Сообщения в чате',
	'SHOUT_DATE_LAST_RUN'			=> 'Дата последней автоматической очистки чата',
	'SHOUT_LOGS'      				=> 'Попытки отправить запрещенные сообщения',
	'SHOUT_LOGS_EXPLAIN'      		=> 'Общее число попыток отправить в чат запрещенные сообщения',
	'NUMBER_LOG_TOTAL' 				=> '<strong>%s</strong> отправлена %s',
	'NUMBER_LOGS_TOTAL' 			=> '<strong>%s</strong> отправлены %s',
	
	
	'NUMBER_SHOUTS' 				=> 'Всего сообщений',
	'SHOUT_VERSION'    				=> 'Версия чата',
	'SHOUT_STATISTICS'    			=> 'Статистика',
	
	
	'SHOUT_OPTIONS'    				=> 'Очистить чат',
	'PURGE_SHOUT'      				=> 'Удалить все сообщения',
	'PURGE_SHOUT_MESSAGES'      	=> 'Удалить сообщение',
	
	
	
	'PURGE_SHOUT_ROBOT'      		=> 'Удалить сообщения робота',
	'PURGE_SHOUT_ROBOT_EXPLAIN'     => 'Удаление всех сообщений робота в чате',
	'UNABLE_CONNECT'    			=> 'Невозможно подключиться к серверу обновлений из-за ошибки: %s',
	'NEW_VERSION'       			=> 'Ваша версия чата устарела. Ваша версия <strong>%1$s</strong>, последняя версия <strong>%2$s</strong>. ',
	
	'NO_MESSAGE' 					=> 'Нет сообщений',
	'NO_SHOUT_LOG' 					=> 'Нет данных',
	'NUMBER_MESSAGE' 				=> '<strong>%s</strong> сообщение',
	'NUMBER_MESSAGES' 				=> '<strong>%s</strong> сообщения',
	'NUMBER_LOG' 					=> '<strong>%s</strong> данных',
	'NUMBER_LOGS' 					=> '<strong>%s</strong> данных',
	
	
	
	
	
	'SHOUT_DEL_MAIN'    			=> 'Удалить сообщения',
	'SHOUT_DEL_ACP'    				=> 'Число удаленных сообщений через панель администрирования:',
	'SHOUT_DEL_AUTO'    			=> 'Число сообщений, удаленных автоматически:',
	'SHOUT_DEL_PURGE'    			=> 'Число сообщений, удаленных во время очистки:',
	'SHOUT_DEL_USER'    			=> 'Число сообщений, удаленных пользователями:',
	'SHOUT_DEL_NR'    				=> '<strong>%s</strong> удаленное сообщение',
	'SHOUT_DEL_NRS'    				=> '<strong>%s</strong> удаленных сообщений',
	'SHOUT_DEL_TOTAL'    			=> 'Всего',
	'SHOUT_MAX_CHARS'				=> 'Символов',
	
	
	
	
	
	
	
	
	
	
	'SHOUT_MODS_DISPO'				=> 'Список модов, совместимых с чатом',
	'SHOUT_FILE'    				=> 'Нет совместимых версий, проведите рекомендуемые правки',
	'SHOUT_FILE_OK'    				=> 'Версия мода, которая совместима с чатом: ',
	'SHOUT_FILE_LIEN'    			=> 'Необходимые изменения смотрите здесь: ',
	'SHOUT_FILE_GO'    				=> 'Настройки форума для чата',
	'SHOUT_ENABLE'					=> 'Включить чат',
	'SHOUT_ENABLE_EXPLAIN'			=> 'Включение/выключение чата и всех его функций',
	'SHOUT_WIDTH_POST'				=> 'Размер поля для ввода сообщения',
	'SHOUT_WIDTH_POST_PRO_EXPLAIN'	=> 'Выберите длину поля для ввода текста сообщений (в пикселях) для стилей на основе <strong><em>prosilver</em></strong>',
	'SHOUT_WIDTH_POST_SUB_EXPLAIN'	=> 'Выберите длину поля для ввода текста сообщений (в пикселях) для стилей на основе  <strong><em>subsilver2</em></strong>',
	'SHOUT_CONFIG_TITLE'			=> 'Имя чата',
	
	'SHOUT_CONFIG_TITLE_EXPLAIN'	=> 'Вы можете задать свое имя для чата',
	'SHOUT_PRUNE_TIME'				=> 'Время очистки',
	
	
	
	
	
	
	
	'SHOUT_PRUNE_TIME_EXPLAIN'		=> 'Время автоматической очистки чата. Время указывается в часах. Например, если ввести значение 2, то чат будет очищаться раз в 2 часа',
	'SHOUT_MAX_POSTS'				=> 'Максимальное число сообщений',
	'SHOUT_MAX_POSTS_EXPLAIN'		=> 'Максимальное число сообщений в чате. Значение <strong>0</strong> отключит все ограничения. Если задать нужное значение, то необходимо <strong>настроить</strong> автоматическое удаление сообщений.',
	'SHOUT_MAX_POSTS_ON'			=> 'Число сообщений на страницу',
	'SHOUT_MAX_POSTS_ON_EXPLAIN'	=> 'Число сообщений в чате, отображающихся на одной странице',
	'SHOUT_CORRECT'         		=> 'Коррекция времени',
	'SHOUT_CORRECT_EXPLAIN'         => 'Включение этой функции будет автоматически корректировать время отправки сообщения. Например, 5 минут назад. Действительно только для сообщений, время отправки которых не превышает 1 час',
	'SHOUT_FLOOD_INTERVAL'         	=> 'Задержка флуда',










	'SHOUT_FLOOD_INTERVAL_EXPLAIN' 	=> 'Минимальное время, проходящее между отправкой 2-х сообщений от одного пользователя. Значение 0 отключает данную функцию. В настройках прав доступа для групп можно разрешить или запретить пользователям игнорировать эту настройку',
	'SHOUT_IE_NR'					=> 'Число сообщений на страницу для браузера IE &lt; 9 и мобильных браузеров',
	'SHOUT_IE_NR_EXPLAIN'			=> 'Выберите число сообщений на страницу чата для Internet Explorer &lt; 9 и мобильных браузеров. (Скроллинг невозможен в этих браузерах).',
	'SHOUT_NR_ACP'					=> 'Число сообщений в администраторской панели',
	'SHOUT_NR_ACP_EXPLAIN'			=> 'Выберите число сообщений, отображаемых на одной страницу чата в администраторской панели',
	'SHOUT_MAX_POST_CHARS'			=> 'Максимальное число знаков',
	'SHOUT_MAX_POST_CHARS_EXPLAIN'	=> 'Выберите максимальное число знаков в одном сообщении чата.',
	'SHOUT_IE_NR_POP_EXPLAIN'		=> 'Выберите количество сообщений для Internet Explorer &lt; 9 и мобильных браузеров. <em>Стандарт: 20</em>',
	'SHOUT_NON_IE_NR'				=> 'Количество сообщений на одну страницу',
	'SHOUT_NON_IE_NR_EXPLAIN'		=> 'Выберите количество сообщений для Internet Explorer &lt; 9 и мобильных браузеров.',
	'SHOUT_BACKGROUND_COLOR'		=> 'Фоновая картинка чата',
	'SHOUT_BACKGROUND_COLOR_EXPLAIN'=> 'Выберите фоновую картинку чата для стилей, основанных на <strong><em>prosilver</em></strong>',
	'SHOUT_BACKGROUND_SUB_COLOR_EXPLAIN'=> 'Выберите фоновую картинку чата для стилей, основанных на <strong><em>subsilver2</em></strong>',
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	'SHOUT_BUTTON_BACKGROUND'		=> 'Фоновая картинка для остальных кнопок',
	'SHOUT_BUTTON_BACKGROUND_EXPLAIN'=> 'Выберите фоновую картинку для остальных кнопок',
	'SHOUT_HEIGHT'					=> 'Высота окна сообщений',
	'SHOUT_HEIGHT_EXPLAIN'			=> 'Выберите высоту окна сообщений для IE &lt; 9 и мобильных браузеров. <em>Стандарт: 150</em>',
	'SHOUT_HEIGHT_POP_EXPLAIN'		=> 'Высота окна сообщений для браузеров отличных от IE &lt; 9 и мобильных браузеров. <em>Стандарт: 460</em>',
	'SHOUT_POSITION_INDEX'			=> 'Расположение чата на главной странице',
	'SHOUT_POSITION_INDEX_EXPLAIN'	=> 'Выберите место где будет располагаться окно чата на главной странице форума',
	'SHOUT_POSITION_FORUM'			=> 'Расположение чата на странице просмотра подфорумов',
	'SHOUT_POSITION_FORUM_EXPLAIN'	=> 'Выберите место где будет располагаться окно чата при просмотре подфорумов и разделов.',
	'SHOUT_POSITION_TOPIC'			=> 'Расположение чата на странице просмотра тем',
	'SHOUT_POSITION_TOPIC_EXPLAIN'	=> 'Выберите место где будет располагаться окно чата при просмотре какой-либо темы.',
	'SHOUT_POSITION_PORTAL'			=> 'Расположение чата на странице Портала',
	'SHOUT_POSITION_PORTAL_EXPLAIN'	=> 'Выберите место где будет располагаться окно чата на Портале.',
	'SHOUT_POSITION_ANOTHER'		=> 'Расположение чата на прочих страницах',
	'SHOUT_POSITION_ANOTHER_EXPLAIN' => 'Выберите место где будет располагаться окно чата при просмотре прочих страниц форума.',
	'SHOUT_INDEX_ON'				=> 'Отображать чат на главной странице',
	'SHOUT_INDEX_ON_EXPLAIN'		=> 'Отображать ли окно чата на главной странице ?',
	'SHOUT_FORUM_ON'				=> 'Отображать чат при просмотре подфорумов',
	'SHOUT_FORUM_ON_EXPLAIN'		=> 'Отображать ли окно чата при просмотре подфорумов ?',
	'SHOUT_TOPIC_ON'				=> 'Отображать чат при просмотре тем',
	'SHOUT_TOPIC_ON_EXPLAIN'		=> 'Отображать ли окно чата при просмотре тем ?',
	'SHOUT_ANOTHER_ON'				=> 'Отображать чат на прочих страницах форума',
	'SHOUT_ANOTHER_ON_EXPLAIN'		=> 'Отображать ли окно чата на прочих страницах форума ? Обратите внимание на то, что вам придется вручную прописать все правки мода для этих страниц',
	'SHOUT_PORTAL_ON'				=> 'Отображать чат на портале',
	'SHOUT_PORTAL_ON_EXPLAIN'		=> 'Отображать ли окно чата на Портале ?.<br />(вам придется дописать код в portal.php и portal_body.html)',
	'SHOUT_ON_CRON'					=> 'Включить авто-очистку чата',
	'SHOUT_ON_CRON_EXPLAIN'			=> 'Активировать автоматическую очистку чата от сообщений.',
	'SHOUT_LOG_CRON'				=> 'Лог авто-очистки',
	'SHOUT_LOG_CRON_EXPLAIN'		=> 'Вести лог автоматической очистки чата ?',
	'SHOUT_SEE_BUTTONS'				=> 'Значки на дисплее сверху',
	'SHOUT_SEE_BUTTONS_EXPLAIN'		=> 'Разрешить отображать иконки сверху, если пользователь не имеет разрешения на их использование (см. замок на наведении курсора мыши).',
	'SHOUT_SEE_BUTTONS_LEFT'		=> 'Значки на дисплее слева',
	'SHOUT_SEE_BUTTONS_LEFT_EXPLAIN'=> 'Разрешить отображать иконки слева от сообщений, если пользователь не имеет разрешения на их использование (см. замок на наведении курсора мыши)',
	
	
	
	
	
	
	'SHOUT_POP_HEIGHT'				=> 'Высота всплывающего окна',
	'SHOUT_POP_HEIGHT_EXPLAIN'		=> 'Определите высоту чата всплывающего окна',
	'SHOUT_POP_WIDTH'				=> 'Ширина всплывающего окна',
	'SHOUT_POP_WIDTH_EXPLAIN'		=> 'Определите ширину чата всплывающего окна',
	'SHOUT_MESSAGES_TOTAL'			=> 'Общее число сообщений',
	'SHOUT_MESSAGES_TOTAL_EXPLAIN'	=> 'Количество сообщений в общей сложности с момента установки Breizh Shoutbox.',
	'SHOUT_MESSAGES_TOTAL_NR'		=> '<strong>%s</strong> сообщения с %s',
	
	'SHOUT_POSITION_TOP'			=> 'В верхней части страницы',
	'SHOUT_POSITION_AFTER'			=> 'После списка форумов',
	
	'SHOUT_POSITION_END'			=> 'В нижней части страницы',
	'SHOUTBOX_VERSION_COPY'			=> '<a href="http://breizh-portal.com/mod-breizh-shoutbox-f21.html">Breizh Shoutbox v%s © 2010, 2011</a>',
	'SHOUT_COPY'					=> '<a href="%1$smod-breizh-shoutbox-f21.html">%2$s</a>',
	'SHOUTBOX'						=> '<a href="%1$sindex.html">%2$s</a>',
	'SHOUTBOX_VERSION_ACP_COPY'		=> '- - - - - - - - - - - - - - - - - - - - - -<br /><a href="http://breizh-portal.com/mod-breizh-shoutbox-f21.html" onclick="this.target=\'_blank\';">Breizh Shoutbox v%s</a> © 2010, 2011 - <a href="http://breizh-portal.com/index.html" onclick="this.target=\'_blank\';">Breizh-Portal</a> - The Breizh Touch',
	'SHOUT_TOUCH_COPY'				=> '<span style="font-size: 11px">Breizh Shoutbox © 2010, 2011 <a href="http://breizh-portal.com/index.html">The Breizh touch</a></span>',
	
	
	'SHOUT_VERSION_UP_TO_DATE'		=> 'Ваша установка находится в актуальном состоянии, обновление не нужно для вашей версии Breizh Shoutbox: v%s. Вам не нужно обновлять чат.',
	'SHOUT_NO_VERSION'				=> '<span style="color: red">Не удалось получить последнюю информацию о версии...</span>',
	'SHOUT_MESSAGES'      			=> 'сообщений',
	'SHOUT_PAGES'      				=> 'страниц',
	'SHOUT_SECONDES'      			=> 'секунд',
	'SHOUT_APERCU'      			=> 'обзор: ',
	'SHOUT_DATE'      				=> 'дата',
	
	'SHOUT_USER'      				=> 'пользователь',
	'SHOUT_HOURS'					=> 'часов',
	'SHOUT_PIXELS'					=> 'пикселей',
	'SHOUT_NEVER'      				=> 'Никогда',
	'SHOUT_LOG_ENTRIE'      		=> 'Тип попытки',
	
	'SHOUT_NO_ADMIN'				=> 'У вас нет прав администратора и вы не можете получить доступ к ресурсу',
	'SHOUT_SERVER_HOUR'				=> 'Время сервера: %s час %s',
	'SHOUT_SERVER_HOURS'			=> 'Время сервера: %s часов %s',
	
	
	
	
	'SHOUT_BAR'						=> 'Положение почтового ящика',
	'SHOUT_BAR_EXPLAIN'				=> 'Выберите положение почтового ящика, вверху или внизу чата.',
	'SHOUT_PAGIN'					=> 'Положение нумерации',
	'SHOUT_PAGIN_EXPLAIN'			=> 'Выберитер положение нумерации, внизу чата или слева от строки статуса.',
	
	
	
	
	'SHOUT_BAR_TOP'					=> 'В верхней части чата',
	'SHOUT_BAR_BOTTOM'				=> 'В нижней части чата',
	'SHOUT_PAGIN_IN'				=> 'В правой части чата',
	'SHOUT_PAGIN_BOTTOM'			=> 'В нижней части чата',
	
		
	'SHOUT_SOUND_NEW'				=> 'Звук нового сообщения',
	'SHOUT_SOUND_NEW_EXPLAIN'		=> 'Выберите звук, который будет воспроизводиться при поступлении нового сообщения.',
	'SHOUT_SOUND_ERROR'				=> 'Звук ошибки',
	
	'SHOUT_SOUND_ERROR_EXPLAIN'		=> 'Выберите звук, который будет воспроизводиться при сообщении об ошибке.',
	'SHOUT_SOUND_DEL'				=> 'Звук удаления',
	
	
	
	
	
	
	'SHOUT_SOUND_DEL_EXPLAIN'		=> 'Выберите звук, который будет воспроизводиться при удалении сообщений',
	'SHOUT_ALL_MESSAGES'			=> ' все сообщения из чата,',
	'SHOUT_PANEL'					=> 'Выдвижная Боковая панель',
	'SHOUT_PANEL_EXPLAIN'			=> 'Активируйте выдвижную боковую панель на всех страницах форума, за исключением отключенных страниц.',
	'SHOUT_PANEL_ALL'				=> 'Выдвижная боковая панель в любом месте',
	'SHOUT_PANEL_ALL_EXPLAIN'		=> 'Активируйте выдвижную боковую панель над страницами.',
	// Robot
	'SHOUT_ROBOT_ACTIVATE'			=> 'Включить робота',
	'SHOUT_ROBOT_ACTIVATE_EXPLAIN'	=> 'Активировать робота для чата ?',
	'SHOUT_ROBOT_MESSAGE'			=> 'Сообщения робота',
	
	'SHOUT_ROBOT_MESSAGE_EXPLAIN'	=> 'Включить оповещения роботом о новых темах и сообщениях',
	'SHOUT_ROBOT_REP'				=> 'Ответы в темах',
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	'SHOUT_ROBOT_REP_EXPLAIN'		=> 'Включить оповещения роботом о новых ответах в темах.',
	'SHOUT_ROBOT_EDIT'				=> 'Редактирование сообщений',
	'SHOUT_ROBOT_EDIT_EXPLAIN'		=> 'Включить оповещения роботом о редактировании сообщений в темах.',
	'SHOUT_ROBOT_MES_PRIV'			=> 'Сообщения робота в приватном чате',
	'SHOUT_ROBOT_MES_PRIV_EXPLAIN'	=> 'Включить оповещения роботом о новых темах и сообщениях в приватном чате',
	'SHOUT_ROBOT_REP_PRIV'			=> 'Ответы в темах(приватный чат)',
	'SHOUT_ROBOT_REP_PRIV_EXPLAIN'	=> 'Включить оповещения роботом о новых ответах в темах в приватном чате.',
	'SHOUT_ROBOT_EDIT_PRIV'			=> 'Редактирование сообщений(приватный чат)',
	'SHOUT_ROBOT_EDIT_PRIV_EXPLAIN'	=> 'Включить оповещения роботом о редактировании сообщений в темах в приватном чате.',
	'SHOUT_ROBOT_DEL'				=> 'Автоматическое удаление',
	'SHOUT_ROBOT_DEL_EXPLAIN'		=> 'Включить\выключить автоматическое удаление сообщений в чате',
	'SHOUT_ROBOT_PREZ'				=> 'Форум для презентаций',
	'SHOUT_ROBOT_PREZ_EXPLAIN'		=> 'Выберите форум для презентаций',
	'SHOUT_ROBOT_EXCLU'				=> 'Исключенные форумы',
	'SHOUT_ROBOT_EXCLU_EXPLAIN'		=> 'Выберите форумы из которых не будут выводится презентации, обратите внимание также на права доступа к данным форумам',
	'SHOUT_ROBOT_COLOR'				=> 'Цвет Робота',
	'SHOUT_ROBOT_COLOR_INFO'		=> 'Цвет сообщений Робота:',
	
	
	
	
	'SHOUT_ROBOT_SESSION'			=> 'Робот и Вход пользователей на форум',
	'SHOUT_ROBOT_SESSION_EXPLAIN'	=> 'Включить\выключить приветствие от Робота вошедших на форум пользователей',
	'SHOUT_ROBOT_SESSION_PRIV'		=> 'Робот и Вход пользователей на форум в приватной комнате',
	'SHOUT_ROBOT_SESSION_PRIV_EXPLAIN'=> 'Включить\выключить приветствие от Робота вошедших на форум пользователей в приватной комнате',
	'SHOUT_ROBOT_SESSION_R'			=> 'Робот и Поисковые боты',
	
	
	
	
	
	
	
	'SHOUT_ROBOT_SESSION_R_EXPLAIN'	=> 'Включить\выключить уведомления о входе поисковых ботов на форум',
	'SHOUT_ROBOT_SESSION_R_PRIV'	=> 'Робот и Поисковые боты в приватной комнате',
	'SHOUT_ROBOT_SESSION_R_PRIV_EXPLAIN'=> 'Включить\выключить уведомления о входе поисковых ботов на форум в приватной комнате',
	'SHOUT_ROBOT_HELLO'				=> 'Робот и Сегодняшняя дата',
	'SHOUT_ROBOT_HELLO_EXPLAIN'		=> 'Включить\выключить уведомления о Сегодняшней дате.',
	'SHOUT_ROBOT_HELLO_PRIV'		=> 'Робот и Сегодняшняя дата в приватной комнате',
	'SHOUT_ROBOT_HELLO_PRIV_EXPLAIN'=> 'Включить\выключить уведомления о Сегодняшней дате в приватной комнате',
	'SHOUT_ROBOT_BIRTHDAY'			=> 'Робот и Дни Рождения',
	
	
	
	
	
	
	
	
	
	
	
	'SHOUT_ROBOT_BIRTHDAY_EXPLAIN'	=> 'Включить\выключить уведомления о Днях Рождения пользователей',
	'SHOUT_ROBOT_BIRTHDAY_PRIV'		=> 'Робот и Дни Рождения в приватной комнате',
	'SHOUT_ROBOT_BIRTHDAY_PRIV_EXPLAIN'	=> 'Включить\выключить уведомления о Днях Рождения пользователей в приватной комнате',
	'SHOUT_ROBOT_CRON_H'			=> 'Расписание уведомлений',
	'SHOUT_ROBOT_CRON_H_EXPLAIN'	=> 'Введите время, в которое будет выводится информация от Робота о Днях Рождения и Сегодняшней дате',
	'SHOUT_ROBOT_NEWEST'			=> 'Робот и новые пользователи',
	'SHOUT_ROBOT_NEWEST_EXPLAIN'	=> 'Включить\выключить уведомления о регистрации новых пользователей.',
	'SHOUT_ROBOT_NEWEST_PRIV'		=> 'Робот и новые пользователи в приватной комнате',
	'SHOUT_ROBOT_NEWEST_PRIV_EXPLAIN'=> 'Включить\выключить уведомления о регистрации новых пользователей в приватной комнате.',
	'SHOUT_ROBOT_CHOICE'			=> 'Параметры чистки робота перед',
	'SHOUT_ROBOT_CHOICE_EXPLAIN'	=> 'Выберите здесь робота с информацией для обслуживания.<br />Вы можете добавить столько вариантов сколько пожелаете.<br />Обратите внимание, что информация для чистки и сброса нагрузки будут стерты.',
	'SHOUT_ON_CONNECT'				=> 'Вход на форум',
	'SHOUT_ON_SUBJET'				=> 'Новые темы',
	
	'SHOUT_ON_REPONSE'				=> 'Ответы и редактирования сообщений',
	'SHOUT_ON_BIRTHDAY'				=> 'Дни Рождения',
	
	
	
	
	'SHOUT_ON_DAY'					=> 'Сегодняшняя дата',
	'SHOUT_ON_NEWS'					=> 'Новые пользователи',
	'SHOUT_ON_PRIV'					=> 'Вход в приватную комнату',
	'SHOUT_ON_MODS'					=> 'Информация о дополнительных модах',
	'SHOUT_PURGE_ON'				=> 'Очистить',
	
	
	
	
	'SHOUT_NO_MOD_ROBOT'			=> 'В данный момент вы не имеете модов на вашем форуме, совместимые с роботом Breizh чата...',
	'SHOUT_MOD_ROBOT'				=> 'Здесь Вы можете задать параметры для модов на вашем форуме которые совместимые с роботом Breizh чата...',
	
		// Add-ons for another Mods
	// Mod Ultimate points system
	'MOD_UPS'						=> 'Мод Ultimate points system',
	
	
	
	
	
	
	
	
	'SHOUT_ROBBERY'					=> 'Сообщения об грабежах',
	'SHOUT_ROBBERY_EXPLAIN'			=> 'Включить/выключить функцию сообщений об успешных грабежах',
	'SHOUT_LOTTERY'					=> 'Сообщения об лотерее',
	'SHOUT_LOTTERY_EXPLAIN'			=> 'Включить/выключить функцию сообщений об выигравших в лотерею пользователях',
	'SHOUT_ROBBERY_PRIV'			=> 'Сообщения об грабежах(приват)',
	'SHOUT_ROBBERY_PRIV_EXPLAIN'	=> 'Включить/выключить функцию сообщений об успешных грабежах в приватном чате',
	'SHOUT_LOTTERY_PRIV'			=> 'Сообщения об лотерее',
	'SHOUT_LOTTERY_PRIV_EXPLAIN'	=> 'Включить/выключить функцию сообщений об выигравших в лотерею пользователях в приватном чате',
	// Mod Hangman
	'MOD_HANGMAN'					=> 'Виселица',
	
	
	
	
	
	
	
	
	'SHOUT_HANGMAN'					=> 'Сообщения из виселицы',
	'SHOUT_HANGMAN_EXPLAIN'			=> 'Включить/выключить сообщения в чате об выигравших пользователях.',
	'SHOUT_HANGMAN_PRIV'			=> 'Сообщения из виселицы(приват)',
	'SHOUT_HANGMAN_PRIV_EXPLAIN'	=> 'Включить/выключить сообщения об выигравших пользователях в приватном чате',
	'SHOUT_HANGMAN_CR'				=> 'Сообщения об созданных виселицах',
	'SHOUT_HANGMAN_CR_EXPLAIN'		=> 'Включить/выключить сообщения в чате про новые виселицы',
	'SHOUT_HANGMAN_CR_PRIV'			=> 'Сообщения об созданных виселицах(приват)',
	'SHOUT_HANGMAN_CR_PRIV_EXPLAIN'	=> 'Включить/выключить сообщения про новые виселицы в приватном чате',
	// Mod tracker
	'MOD_TRACKER'					=> 'Mod Traqueur',
	'SHOUT_TRACKER'					=> 'Robot Ticket Tracker',
	'SHOUT_TRACKER_EXPLAIN'			=> 'Enables/disables Notifications of new tickets in the tracker.',
	'SHOUT_TRACKER_REP'				=> 'Robot replies Tracker',
	'SHOUT_TRACKER_REP_EXPLAIN'		=> 'Enables/disables notifications of replies to tickets in the tracker.',
	'SHOUT_TRACKER_EDIT'			=> 'Robot editions Tracker',
	'SHOUT_TRACKER_EDIT_EXPLAIN'	=> 'Enables/disables notifications editions tickets and answers in the tracker.',
	'SHOUT_TRACKER_PRIV'			=> 'Robot Ticket Tracker private shoutbox',
	'SHOUT_TRACKER_PRIV_EXPLAIN'	=> 'Enables/disables Notifications of new tickets in the tracker in private shoutbox.',
	'SHOUT_TRACKER_REP_PRIV'		=> 'Robot replies Tracker private shoutbox',
	'SHOUT_TRACKER_REP_PRIV_EXPLAIN'=> 'Enables/disables notifications of replies to tickets in the tracker in private shoutbox.',
	'SHOUT_TRACKER_EDIT_PRIV'		=> 'Robot editions Tracker private shoutbox',
	'SHOUT_TRACKER_EDIT_PRIV_EXPLAIN'=> 'Enables/disables notifications editions tickets and answers in the tracker in private shoutbox.',
	
	// Installation
	'SHOUT_WELCOME'					=> '[color=#008000]Это ваше первое сообщение. Приветствуем в %s[/color]... [i]from Sylver35[/i] ...', // Will be modified by generate_text_for_storage()
	'SHOUT_WELCOME_INSTALL'			=> '[i][b][color=#ff0000]Установка %s прошла успешно![/color][/b][/i]', // Will be modified by generate_text_for_storage()
	'SHOUT_USE_NEXT'				=> 'The version 1.0.0 of Breizh Shoutbox is detected on your board, please read first <a href="http://breizh-portal.com/mod-breizh-shoutbox-version-1-1-0-p1074.html#p1074">this message</a> and download the update v1.0.0 to 1.1.0 before continuing.',
	
	'SHOUT_PRIV'					=> 'Приватный чат',
	'SHOUT_NAME_FULL'				=> '© чат v%s',
	
	// Update instructions
	'SHOUTBOX_INSTRUCTIONS'			=> '<br /><h1>Use Of Breizh Shoutbox v%1$s</h1><br />
										<p>Team Breizh Portal wishes you well enjoy the features of this Mod.<br />
										We have made every effort so that you can have a shoutbox efficient and responsive to your needs maximizing choice of personalization.<br />
										Feel free to donate to perpetuate the development and support... Go <strong><a href="%2$s" title="Mod Breizh Shoutbox Tracker">here</a></strong></p>
										<p>For any support request, go in the <strong><a href="%3$s" title="Support forum dedicated to Breizh Shoutbox Mod">Support forum dedicated to Breizh Shoutbox Mod</a></strong></p>
										<p>Visit the Tracker <strong><a href="%4$s" title="Mod Breizh Shoutbox Tracker">on this page</a></strong>. Keep you informed of any bugs, additions or feature requests, security...</p>',
	'SHOUTBOX_UPDATE_INSTRUCTIONS'	=> '<br /><br /><h1>Release announcement version %2$s</h1><br /><br />
		<p>Please read <strong><a href="%1$s" title="%1$s">the release announcement of the latest version</a></strong> before continuing the process of updating, it may contain useful information. It also contains complete links to download and the change log.</p>
		<p>For any support request, go in the <strong><a href="%3$s" title="Support forum dedicated to Breizh Shoutbox Mod">Support forum dedicated to Breizh Shoutbox Mod</a></strong></p>
		<p>Visit the Tracker <strong><a href="%4$s" title="Mod Breizh Shoutbox Tracker">on this page</a></strong>. Keep you informed of any bugs, additions or feature requests, security...</p>',
	
	// Added in version 1.4.0
	'SHOUT_USERS_CAN_CHANGE'		=> 'Note that users can enable/disable this setting individually',
	'SHOUT_AVATAR_USER'				=> 'User’s avatars',
	'SHOUT_AVATAR_USER_EXPLAIN'		=> 'Enable/disable default avatar for users without an avatar.',
	'SHOUT_AVATAR_IMG'				=> 'Avatar image default',
	'SHOUT_AVATAR_IMG_EXPLAIN'		=> 'Specify here the image chosen for the avatar by default for users who do not choose.<br />This image should be in the folder “images/shoutbox/” <em>defaut</em>: “no_avatar.gif”',
	'SHOUT_AVATAR_IMG_BOT'			=> 'Avatar image of the robot',
	'SHOUT_AVATAR_IMG_BOT_EXPLAIN'	=> 'Specify here the image chosen for the avatar of the robot.<br />This image should be in the folder “images/shoutbox/”<br /><em>defaut</em>: “avatar_robot.png”',
	'SHOUT_SOUND_EMPTY'				=> 'Без звука',
	
	
	'SHOUT_SOUND_ON'				=> 'Активировать звуки',
	'SHOUT_SOUND_ON_EXPLAIN'		=> 'Включить/выключить звуки в чате',
	
	// Added in version 1.5.0
	'SHOUT_BBCODE'					=> 'Prohibition bbcodes',
	'SHOUT_BBCODE_EXPLAIN'			=> 'Enter here the list of bbcodes that you do not want in the shoutbox.<br />Some bbcodes can cause bugs, your experience will allow you to list them here.<br />You must enter them without the brackets, separated by a comma and a space.<br />Ex:&nbsp;&nbsp;<em>list, code, quote</em>',
	'SHOUT_BBCODE_USER_EXPLAIN'		=> 'Enter the list of bbcode that you do not want in the format of messages users.<br />The list of prohibited bbcodes above is already considered, this list is a complement. The videos are already prohibited.<br />You must enter them without the brackets, separated by a comma and a space.<br />Eg:&nbsp;&nbsp;<em>list, code, quote</em>',
	'SHOUT_BBCODE_SIZE'				=> 'Font size',
	'SHOUT_BBCODE_SIZE_EXPLAIN'		=> 'Enter here the maximum font size allowed for the bbcode size= in the formatting user’s messages.<br />The number 100 corresponds to the overall size of the police, 150 corresponds to one and half times that size.',
	'SHOUT_PANEL_PERMISSIONS'		=> 'To see this panel, must have permissions: “<em>A user can view the shoutbox in a lateral panel</em>” and “<em>A user can use the shoutbox in popup</em>” to yes.<br />Not enabled for mobile phones.',
	'SHOUT_PANEL_KILL'				=> 'pages excluded',
	'SHOUT_PANEL_KILL_EXPLAIN'		=> 'You can select or exclude pages displaying the retractable lateral panel.<br />Enter the name of the php page with the settings and the path if different from root.<br />One page per line. Ex: <em>ucp.php?mode=register&nbsp;&nbsp;gallery/index.php</em><br />Pages automatically excluded: errors, informations, redirections and connexion.',
	'SHOUT_PANEL_IMG'				=> 'Opening Image',
	'SHOUT_PANEL_IMG_EXPLAIN'		=> 'Choose the opening image for the retractable lateral panel.<br />Images of directory root/images/shoutbox/panel/',
	'SHOUT_PANEL_EXIT_IMG'			=> 'Closing Image',
	'SHOUT_PANEL_EXIT_IMG_EXPLAIN'	=> 'Choose the closing image for the retractable lateral panel.<br />Images of directory root/images/shoutbox/panel/',
	'SHOUT_PANEL_WIDTH'				=> 'Lateral panel width',
	'SHOUT_PANEL_WIDTH_EXPLAIN'		=> 'Specify the width of the retractable lateral panel.<br />Note that it must contain in the shoutbox in popup.',
	'SHOUT_PANEL_HEIGHT'			=> 'Lateral panel height',
	'SHOUT_PANEL_HEIGHT_EXPLAIN'	=> 'Specify the height of the retractable lateral panel.',
	'SHOUT_ROBOT_CHOICE_PRIV'		=> 'Parameters of the purge Robot in front private shoutbox',
	'SHOUT_ROBOT_CHOICE_PRIV_EXPLAIN'=> 'Choose here all infos Robot you want to serve on the front.<br />You can add as many options as desired in the private shoutbox.<br />You can add as many choices as desired.<br />Note that infos for purge and load shedding will always be erased.',
	'SHOUT_TEMP'					=> 'Refresh time',
	'SHOUT_TEMP_TITLE'				=> 'setting the time of updating for shoutbox depending on the status connected/not connected.<br />Too short, there are risks that the server can not respond within the allotted time, too long, you lose responsiveness.<br />change the value until a satisfactory performance according to your server.',
	'SHOUT_TEMP_USERS'				=> 'Refresh time for members',
	'SHOUT_TEMP_USERS_EXPLAIN'		=> 'Choose here the time to refresh the shoutbox for members online.',
	'SHOUT_TEMP_ANONYMOUS'			=> 'Refresh time for guests',
	'SHOUT_TEMP_ANONYMOUS_EXPLAIN'	=> 'Choose here the time to refresh the shoutbox for guests.',
	'SHOUT_TEMP_BOT'				=> 'Refresh time for bots',
	'SHOUT_TEMP_BOT_EXPLAIN'		=> 'Time to refresh for bots is not adjustable, it is set by default to 120 seconds in order not to unnecessarily consume resources.',
	'SHOUT_BIRTHDAY_EXCLUDE'		=> 'Исключенные группы',
	'SHOUT_BIRTHDAY_EXCLUDE_EXPLAIN'=> 'You can select one or more groups will be excluded from birthdays to wish.<br />Banned members are automatically excluded.<br /><br />Use ctrl+click to select more than one group.',
	'SHOUT_INACTIV_A'				=> 'Inactivity time of guests',
	'SHOUT_INACTIV_A_EXPLAIN'		=> 'Here you determine the time of inactivity of the guests, after this period, the shoutbox will automatically standby and so will not do more requests.',
	'SHOUT_INACTIV_B'				=> 'Inactivity time of registered users',
	'SHOUT_INACTIV_B_EXPLAIN'		=> 'Here you determine the time of inactivity of the registered users, after this period, the shoutbox will automatically standby and so will not do more requests.<br />Note that there is a permission to skip this.',
));

?>