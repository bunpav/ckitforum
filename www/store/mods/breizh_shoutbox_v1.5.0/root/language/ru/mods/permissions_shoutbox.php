<?php
/**
*
* permission Breizh Shoutbox [English]
*
* @package language
* @version $Id: permissions_shoutbox.php 140 20:08 31/12/2010 Sylver35 Exp $ 
* @copyright (c) 2010, 2011 Breizh Portal  http://breizh-portal.com
* @copyright (c) 2007 Paul Sohier
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

/**
*	MODDERS PLEASE NOTE
*
*	You are able to put your permission sets into a separate file too by
*	prefixing the new file with permissions_ and putting it into the acp
*	language folder.
*
*	An example of how the file could look like:
*
*	<code>
*
*	if (empty($lang) || !is_array($lang))
*	{
*		$lang = array();
*	}
*
*	// Adding new category
*	$lang['permission_cat']['bugs'] = 'Bugs';
*
*	// Adding new permission set
*	$lang['permission_type']['bug_'] = 'Bug Permissions';
*
*	// Adding the permissions
*	$lang = array_merge($lang, array(
*		'acl_bug_view'		=> array('lang' => 'Can view bug reports', 'cat' => 'bugs'),
*		'acl_bug_post'		=> array('lang' => 'Can post bugs', 'cat' => 'post'), // Using a phpBB category here
*	));
*
*	</code>
*/

// Adding new category
$lang['permission_cat']['shoutbox'] = 'Breizh чат';

// Adding new permission set
$lang['permission_type']['shout_'] = 'Права доступа к Breizh чату';

// Adding the permissions
$lang = array_merge($lang, array(
	// Admin permission
	'acl_a_shout_manage'			=> array('lang' => 'Может изменять настройки чата', 				'cat' => 'misc'),
	'acl_a_shout_priv'				=> array('lang' => 'Может изменять настройки приватного чата', 		'cat' => 'misc'),
	
	// User permissions
	'acl_u_shout_bbcode'			=> array('lang' => 'Может использовать ВВ-коды', 										'cat' => 'shoutbox'),
	'acl_u_shout_bbcode_change'		=> array('lang' => 'Может пользоваться форматированием текста', 						'cat' => 'shoutbox'),
	'acl_u_shout_chars'				=> array('lang' => 'Может использовать спец.символы', 									'cat' => 'shoutbox'),
	'acl_u_shout_color'				=> array('lang' => 'Может использовать палитру цветов', 								'cat' => 'shoutbox'),
	'acl_u_shout_delete'			=> array('lang' => 'Может удалять сообщения других пользователей', 						'cat' => 'shoutbox'),
	'acl_u_shout_delete_s'			=> array('lang' => 'Может удалять свои сообщения', 										'cat' => 'shoutbox'),
	'acl_u_shout_edit'				=> array('lang' => 'Может редактировать свои сообщения', 								'cat' => 'shoutbox'),
	'acl_u_shout_edit_mod'			=> array('lang' => 'Может редактировать сообщения других пользователей', 				'cat' => 'shoutbox'),
	'acl_u_shout_hide'				=> array('lang' => 'Может игнорировать сообщения от робота', 							'cat' => 'shoutbox'),
	'acl_u_shout_ignore_flood'		=> array('lang' => 'Может игнорировать ограничения по Флуду', 							'cat' => 'shoutbox'),
	'acl_u_shout_image'				=> array('lang' => 'Может добавлять картинки в сообщение', 								'cat' => 'shoutbox'),
	'acl_u_shout_inactiv'			=> array('lang' => 'Может игнорировать сон и бездеятельность в чате', 					'cat' => 'shoutbox'),
	'acl_u_shout_info'				=> array('lang' => 'Может видеть IP других пользователей', 								'cat' => 'shoutbox'),
	'acl_u_shout_info_s'			=> array('lang' => 'Может видеть свой IP', 												'cat' => 'shoutbox'),
	'acl_u_shout_lateral'			=> array('lang' => 'Может видеть чат в боковой панели', 								'cat' => 'shoutbox'),
	'acl_u_shout_limit_post'		=> array('lang' => 'Может игнорировать лимит знаков в сообщении',						'cat' => 'shoutbox'),
	'acl_u_shout_popup'				=> array('lang' => 'Может использовать чат во всплывающем окне', 						'cat' => 'shoutbox'),
	'acl_u_shout_post'				=> array('lang' => 'Может оставлять сообщения в чате', 									'cat' => 'shoutbox'),
	'acl_u_shout_post_inp'			=> array('lang' => 'Может отправлять <strong>приватные</strong> сообщения в чат',		'cat' => 'shoutbox'),
	'acl_u_shout_priv'				=> array('lang' => 'Может заходить в приватную комнату чата', 							'cat' => 'shoutbox'),
	'acl_u_shout_purge'				=> array('lang' => 'Может очищать чат', 												'cat' => 'shoutbox'),
	'acl_u_shout_smilies'			=> array('lang' => 'Может использовать смайлики в сообщениях', 							'cat' => 'shoutbox'),
	'acl_u_shout_view'				=> array('lang' => 'Может <strong>просматривать</strong> чат', 							'cat' => 'shoutbox'),
));

?>