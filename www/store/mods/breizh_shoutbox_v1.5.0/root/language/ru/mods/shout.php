<?php
/** 
*
* Module Breizh Shoutbox [English]
*
* @package language
* @version $Id: shout.php 150 16:15 16/12/2011 Sylver35 Exp $ 
* @copyright (c) 2010, 2011 Breizh Portal  http://breizh-portal.com
* @copyright (c) 2007 Paul Sohier
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'SHOUT_ROBOT'			=> 'Робот',
	'SHOUT_TOUCH'			=> 'The Breizh Touch',
	'MISSING_DIV' 			=> 'Div чата не найден ',
	'NO_POST_GUEST'         => 'Гости могут оставлять сообщение.',
	'LOADING' 				=> 'Грузится...',
	'SHOUT_MESSAGE'			=> 'Сообщение',
	'POST_MESSAGE'			=> 'Отправить',
	'POST_MESSAGE_ALT'		=> 'Отправить сообщение',
	'SENDING' 				=> 'Отправка сообщения...',
	'MESSAGE_EMPTY'			=> 'Сообщение пусто.',
	'XML_ER' 				=> 'Ошибка XML, обновите страницу...',
	'NO_MESSAGE' 			=> 'Нет сообщений.',
	'NO_AJAX' 				=> 'Нет ajax',
	'NO_AJAX_USER' 			=> 'Для просмотра чата вам необходимо включить Javascript.',
	'NO_SHOUT_ID'	 		=> 'Нет идентификатора сообщения.',
	'JS_ERR' 				=> 'Ошибка Java скрипта',
	'LINE' 					=> 'Линия',
	'FILE' 					=> 'Файл',
	'FLOOD_ERROR'	 		=> 'Флуд',
	'POSTED' 				=> 'Сообщение добавлено...',
	'NO_CODE' 				=> 'Код не принят.',
	'NO_QUOTE' 				=> 'Не использовать листы, цитаты или ББкоды.',
	'NO_SCRIPT' 			=> 'Скрипты не допускаются в этом чате !!  Пожалуйста, обратите внимание, что эта попытка была зарегистрирована с вашим ip.',
	'NO_APPLET' 			=> 'Апплеты не допускаются в этом чате !!  Пожалуйста, обратите внимание, что эта попытка была зарегистрирована с вашим ip.',
	'NO_ACTIVEX' 			=> 'Объекты Актив Х не допускаются в этом чате !! Пожалуйста, обратите внимание, что эта попытка была зарегистрирована с вашим ip.',
	'NO_OBJECTS' 			=> 'Хром и похожие объекты не допускаются в этом чате !! Пожалуйста, обратите внимание, что эта попытка была зарегистрирована с вашим ip.',
	'NO_IFRAME' 			=> 'Фреймы не допускаются в этом чате !! Пожалуйста, обратите внимание, что эта попытка была зарегистрирована с вашим ip.',
	'SHOUT_AVERT'			=> 'Уважайте работу девелоперов ... Оставьте копирайты на их сайты!',
	'SHOUT_DEL'				=> 'Удалить сообщение',
	'DEL_SHOUT' 			=> 'Вы уверены, что хотите удалить это сообщение?',
	'MSG_DEL_DONE' 			=> 'Сообщение удаляется...',
	'NO_SHOUT_ID'	 		=> 'Нет идентификатора сообщения.',
    'SHOUT_PAGE'            => 'Страница N° ',
	'CODE'                  => 'Код',
	'EDIT'                  => 'Изменить',
	'CANCEL'                => 'Отменить',
	'COLORS'                => 'Цвета',
	'SHOUT_IP'              => 'Посмотреть Ip автора сообщения',
	'SHOUT_EDIT'            => 'Редактировать сообщение',
	'SENDING_EDIT'          => 'Отправлять после редактирования...',
    'EDIT_DONE'             => 'Сообщение было отредактировано',
	'ONLY_ONE_OPEN'         => 'Вы можете редактировать только 1 открытое окно',
	'SHOUT_AVATAR_SHORT'	=> 'Аватар',
	'SHOUT_AVATAR_TITLE'	=> 'Аватар %s',
	'SHOUT_COPY'			=> '<a href="%1$smod-breizh-shoutbox-f21.html">%2$s</a>',
	'SHOUT_COLOR'			=> 'Раскрасить текст',
	'SHOUT_COLOR_CLOSE'		=> 'Свернуть раскрашивание текста',
	'SHOUT_CHARS'			=> 'Добавить специальные символы',
	'SHOUT_CHARS_CLOSE'		=> 'Закройте панель специальных символов',
	'SHOUT_BOLD'			=> 'Жирный текст',
	'SHOUT_ITALIC'			=> 'Выделить курсивом',
	'SHOUT_UNDERLINE'		=> 'Подчеркнуть текст',
		'SHOUT_IMAGE'			=> 'Добавить изображения',
	'SHOUT_URL'				=> 'Добавить веб-адрес',
	'SMILIES' 				=> 'Добавить смайлики', 
	'SMILIES_CLOSE' 		=> 'Свернуть панель смайликов',
	'SHOUT_MORE_SMILIES'	=> 'Больше смайликов',
	'SHOUT_MORE_SMILIES_ALT' => 'Нажмите здесь, чтобы видеть больше смайликов',
	'SHOUT_POST_IP'			=> 'IP автора сообщения:',
	'SHOUTBOX'				=> '<a href="%1$sindex.html">%2$s</a>',
	'SHOUTBOX_VER'			=> 'Breizh Shoutbox v%s',
	'SHOUTBOX_VER_ALT'		=> 'Breizh Shoutbox v%s © 2011',
	'SHOUTBOX_POPUP'		=> 'Breizh Shoutbox в всплывающем окне',
	'SHOUT_POP'				=> 'Открыть мини-чат в новом окне',
	'SHOUT_RULES'			=> 'Правила пользования чатом',
	'SHOUT_RULES_PRIV'		=> 'Правила пользования частным чатом',
	'SHOUT_RULES_CLOSE'		=> 'Закрыть панель правил',
	'SHOUTBOX_SECRET'		=> 'Приватный мини чат',
	'SHOUT_PRIV'			=> 'Ввести в частный чат',
	'SHOUT_PURGE'			=> 'Очистить чат',
	'SHOUT_PURGE_ALT'		=> 'Нажмите для полной очистки чата',
	'SHOUT_PURGE_BOX'		=> "Вы действительно хотите очистить чат? Пожалуйста, обратите внимание, это действие необратимо...",
	'PURGE_PROCESS'			=> 'Идет процесс очистки...',
	'SHOUT_PURGE_ROBOT_ALT'	=> 'Нажмите для очистки сообщений Бота',
	'SHOUT_PURGE_ROBOT_BOX'	=> 'Вы действительно хотите очистить сообщения Бота ?',
	'SERVER_ERR'			=> 'Произошла ошибка при попытки связи с сервером, пожалуйста обновите страницу...',
	'SHOUT_ERROR' 			=> 'Ошибка: ',
	'SHOUT_IMG_POST_ERROR'	=> 'Ошибка: для вставки изображения, вы должны кликнуть на иконке добавки изображения...',
	'SHOUT_IMG_DIM_ERROR'	=> 'Ошибка: файл изображения поврежден или это не изображение...',
	'SHOUT_IMG_FOPEN_ERROR'	=> 'Ошибка: невозможно связаться с сервером хостинга изображений...',
	'SHOUT_PROCESS_IMG'		=> 'Идет процесс проверки изображений...',

	// User panel
		'MINUTES_CHOICE'		=> 'Вы выбрали формат с минутным обновлением(?): “%s”<br />Вы можете выбрать из 2-х вариантов: будет проходить звуковой сигнал или ежеминутное обновление сообщений(?)',
	'DISPLAY_SOUND_CHOICE'	=> 'Вы можете включить или выключить звук входящих сообщений',
	'SOUND_OR_NOT'			=> 'Выберите настройки для себя',
	'CHOOSE_NEW_SOUND'		=> 'Выберите мелодию которая будет проиграна при появлении нового сообщения',
	'CHOOSE_ERROR_SOUND'	=> 'Выберите мелодию которая будет проиграна при ошибке',
	'CHOOSE_DEL_SOUND'		=> 'Выберите мелодию которая будет проиграна при удалении сообщения',
	'CHOOSE_NEW_YES'		=> 'Выбранная мелодия будет проиграна при появлении нового сообщения',
	'CHOOSE_ERROR_YES'		=> 'Выбранная мелодия будет проиграна при ошибке',
	'CHOOSE_DELETE_YES'		=> 'Выбранная мелодия будет проиграна при удалении сообщения',
	'CHOOSE_POSITIONS'		=> 'Расположение мини-чата',
	'CHOOSE_NEW_NO'			=> 'При входящих сообщениях звук проигрываться не будет',
	'SHOUT_MINUTES_YES'		=> 'Коррекция в минутах будет эффективнее',
	'SHOUT_SOUND_MINUTES'	=> 'Изменение времени отображения',
	'SHOUT_SOUND_YES'		=> 'Включить звук',
	'SHOUT_SOUND_NO'		=> 'Выключить звук',
	'SHOUT_SOUND_ECOUTE'	=> 'Проиграть звук',
	'SHOUT_CONFIG_OPEN'		=> 'Открыть панель настроек чата',
	'SHOUT_PANEL_USER'		=> 'Настройки профиля',
	'SHOUT_PREF_UPDATED'	=> 'Ваши настройки чата сохранены',
	'RETURN_SHOUT_PREF'		=> '%s« Вернуться к панели настроек%s',
	'SHOUT_DEF_VAL'			=> 'Значения по умолчанию',
	'SHOUT_DEF_VAL_EXPLAIN'	=> 'Вернуться к настройкам по умолчанию',

	// No permission errors
	'NO_ADMIN_PERM'     	=> 'Нет прав доступа...',
	'NO_EDIT_PERM'			=> 'Вы не можете редактировать сообщения...',
	'NO_DELETE_PERM'    	=> 'Вам не разрешено удалять сообщения...',
	'NO_DELETE_PERM_S'  	=> 'Вам не разрешено удалять свои сообщения...',
	'NO_DELETE_PERM_T'  	=> 'Вам не разрешено удалять сообщения других пользователей...',
	'NO_POST_PERM'			=> 'Вам не разрешено оставлять сообщения...',
	'NO_POST_PERM_GUEST'	=> '...Для отправки сообщений вы должны авторизоваться...',
	'NO_PURGE_PERM'			=> 'Вам не разрешено очищать  Мини-чат...',
	'NO_PURGE_ROBOT_PERM'	=> 'Вы не можете удалять сообщения Робота...',
	'NO_SHOUT_BBCODE' 		=> 'Вам не разрешено использовать BBCode...',
	'NO_SHOUT_CHARS'		=> 'Вы не можете использовать специальные символы...',
	'NO_SHOUT_COLOR'		=> 'Вы не можете писать цветным текстом...',
	'NO_SHOUT_DEL'			=> 'Вам не разрешено удалять сообщение...',
	'NO_SHOUT_EDIT'         => 'Вы не можете редактировать сообщения...',
	'NO_SHOUT_IMG'			=> 'Вы не можете добавлять изображения...',
	'NO_SHOUT_POP'			=> 'Вам не разрешено открывать мини-чат в новом окне...',
	'NO_SHOW_IP_PERM'   	=> 'Вам не разрешено видеть IP-отправителя сообщения. Вы можете увидеть только свой IP...',
	'NO_SMILIES' 			=> 'Вам не разрешено использовать смайлики...',
	'NO_SMILIE_PERM'    	=> 'Вам не разрешено вставлять смайлы в сообщение...',
	'NO_VIEW_PERM'      	=> 'Вам не разрешено просматривать мини-чат...',
	'NO_VIEW_PRIV_PERM'     => 'Вам не разрешено просматривать приватный мини-чат...',

	// Post infos Robot
	'SHOUT_SELECT_ROBOT'		=> 'Добавить уведомление в чат',
	// Info messages of the robot
	'SHOUT_GLOBAL_ROBOT'		=> '%1$s %2$s создал важную тему: %3$s',
	'SHOUT_ANNOU_ROBOT'			=> '%1$s %2$s создал объявление: %3$s',
	'SHOUT_POST_ROBOT'			=> '%1$s %2$s создал новую тему: %3$s',
	'SHOUT_REPLY_ROBOT'			=> '%1$s %2$s ответил в теме: %3$s',
	'SHOUT_EDIT_ROBOT'			=> '%1$s %2$s отредактировал сообщение: %3$s',
	'SHOUT_TOPIC_ROBOT'			=> '%1$s %2$s отредактировал тему: %3$s',
	'SHOUT_LAST_ROBOT'			=> '%1$s %2$s отредактировал последнее сообщение в теме: %3$s',
	'SHOUT_QUOTE_ROBOT'			=> '%1$s %2$s ответил с цитированием в теме: %3$s',
	'SHOUT_PREZ_ROBOT'			=> '%1$s %2$s создал презентацию: %3$s',
	'SHOUT_PREZ_F_ROBOT'		=> '%1$s %2$s отредактировал презентацию: %3$s',
	'SHOUT_PREZ_FS_ROBOT'		=> '%1$s %2$s отредактировал свою презентацию: %3$s',
	'SHOUT_PREZ_E_ROBOT'		=> '%1$s %2$s отредактировал сообщение в презентации: %3$s',
	'SHOUT_PREZ_ES_ROBOT'		=> '%1$s %2$s отредактировал свое сообщение в презентации: %3$s',
	'SHOUT_PREZ_L_ROBOT'		=> '%1$s %2$s отредактировал последнее сообщение в презентации: %3$s',
	'SHOUT_PREZ_LS_ROBOT'		=> '%1$s %2$s отредактировал последнее сообщение в своей презентации: %3$s',
	'SHOUT_PREZ_Q_ROBOT'		=> '%1$s %2$s ответил в презентации с цитированием: %3$s',
	'SHOUT_PREZ_R_ROBOT'		=> '%1$s %2$s ответил в презентации: %3$s',
	'SHOUT_PREZ_RS_ROBOT'		=> '%1$s %2$s ответил в своей презентации: %3$s',
	'SHOUT_ENTER_PRIV'			=> '%1$s %2$s вошел в приватную комнату чата',
	'SHOUT_PURGE_SHOUT'			=> '%s История Чата очищена. Приветствуем вас на нашем Форуме. Ведите себя достойно и дружелюбно !!!',
	'SHOUT_PURGE_PRIV'			=> '%s История Чата очищена. Приветствуем вас на нашем Форуме. Ведите себя достойно и дружелюбно !!!...',
	'SHOUT_PURGE_ROBOT'			=> '%s Сообщения робота очищены...',
	'SHOUT_PURGE_AUTO'			=> '%s Автоматическая очистка %s сообщений окончена...',
	'SHOUT_PURGE_PRIV_AUTO'		=> '%s Автоматическая очистка of %s сообщений окончена...',
	'SHOUT_DELETE_AUTO'			=> '%s Автоматический сброс of %s сообщений окончен...',
	'SHOUT_DELETE_PRIV_AUTO'	=> '%s Автоматический сброс %s сообщений окончен...',
	'SHOUT_BIRTHDAY_ROBOT'		=> 'Вся команда форума %1$s поздравляет с Днем Рождения пользователя %2$s , всего самого наилучшего в этот отличный день !!!',
	'SHOUT_BIRTHDAY_ROBOT_FULL'	=> 'Вся команда форума %1$s поздравляет с Днем Рождения пользователя %2$s , которому исполнилось %3$s %4$s лет !!! Долгих лет счастливой и беззаботной жизни тебе, дорогой %2$s !!! ',
	'SHOUT_HELLO_ROBOT'			=> 'Привет, мы %1$s %2$s',
	'SHOUT_NEWEST_ROBOT'		=> 'Просим любить и жаловать нового пользователя: %1$s ...',
	'SHOUT_ROBBERY_ROBOT'		=> '%1$s %2$s успешно ограбил %3$s на %4$s',
	'SHOUT_LOTTERY_ROBOT'		=> '%1$s %2$s выиграл джекпот в лотерее %3$s %4$s',
	'SHOUT_HANGMAN_ROBOT'		=> '%1$s %2$s правильно ответил в Викторине: %3$s',
	'SHOUT_HANGMAN_C_ROBOT'		=> '%1$s %2$s создал новый вопрос в Викторине: %3$s',
	'SHOUT_SESSION_ROBOT'		=> 'Привет %s и добро пожаловать на форум...',
	'SHOUT_SESSION_ROBOT_BOT' 	=> '%1$s %2$s зашел на форум...',
	'SHOUT_TRACKER_ADD_ROBOT' 	=> '%1$s %2$s отправил новый тикет: %3$s',
	'SHOUT_TRACKER_REPLY_ROBOT' => '%1$s %2$s ответил на тикет: %3$s',
	'SHOUT_TRACKER_EDIT_ROBOT' 	=> '%1$s %2$s отредактировал тикет: %3$s',
	'SHOUT_TRACKER_EDIT_P_ROBOT'=> '%1$s %2$s отредактировал сообщение в тикете: %3$s',
	'SHOUT_SUDOKU_ALL_ROBOT'	=> '%1$s %2$s выиграл в судоку !!',
	'SHOUT_SUDOKU_WIN_ROBOT'	=> '%1$s %2$s выиграл в судоку !!!',
	'SHOUT_VERSION_ROBOT'		=> '%1$s ваша версия чата устарела, у вас версия %2$s , а последняя версия %3$s. Посетите %4$s для обновления...',

	// Added in version 1.4.0
	'SHOUT_PROTECT'				=> '’',
	'SHOUT_START'				=> 'Чат',
	'SHOUT_AVATAR_NONE'			=> '%s без аватара',
	'SHOUT_ANY'					=> 'Нет звука',
	'CHOOSE_ERROR_NO'			=> 'Мелодия не может быть озвучена из-за ошибки',
	'CHOOSE_DELETE_NO'			=> 'Не проигрывать звук при удалении сообщения',
	'SHOUT_USER_CONFIG'			=> 'Выбор позиции (сверху\снизу) для панели смайликов, цветов, спецсимволов......',
	'SHOUT_USER_PAGIN'			=> 'Выбор позиции размещения, внизу чата или слева от строки статуса.',
	
	// Added in version 1.5.0
	'SHOUT_SEP'					=> ' ¦ ',
	'SHOUT_CLOSE'				=> 'Свернуть',
	'SHOUT_DIV_CLOSE'			=> 'Свернуть панель',
	'SHOUT_ROBOT_START'			=> 'Информация:', // At the beginning of infos robot
	'SHOUT_ROBOT_DATE'			=> 'l F j, Y', // Form of the info date
	'SHOUT_AUTO'				=> 'Введите сообщение...',
	'PICK_COLOR'				=> 'Выберите цвет, кликнув по области цвета',
	'PICK_BUTON'				=> 'Цвет текста',
	'SHOUT_CLICK_HERE'			=> 'Клик сюда чтобы войти',
	'SHOUT_LOG_ME_IN'			=> 'Авто-вход',
	'SHOUT_HIDE_ME'				=> 'Скрыть меня',
	'SHOUT_CHOICE_COLOR'		=> 'Изменить палитру',
	'SHOUT_JSCOLOR'				=> 'Jscolor палитра',
	'SHOUT_PHPBBCOLOR'			=> 'Форумная палитра',
	'SHOUT_PHPBB2COLOR'			=> 'Форумная палитра',
	'SHOUT_ONLINE_TITLE'		=> 'Пользователи в он-лайне',
	'SHOUT_ONLINE'				=> 'Посмотреть кто в он-лайне',
	'SHOUT_ONLINE_CLOSE'		=> 'Закрыть панель',
	'SHOUT_EXEMPLE'				=> 'В данных ВВ-кодах ваш текст будет выглядеть именно так',
	'SHOUT_PERSO'				=> 'Форматирование текста в сообщении',
	'SHOUT_PERSO_GO'			=> 'Применить',
	'SHOUT_BBCODE_OPEN'			=> 'Открыть теги ВВ-кода',
	'SHOUT_BBCODE_CLOSE'		=> 'Закрыть теги ВВ-кода',
	'SHOUT_BBCODE_SUCCESS'		=> 'Изменения применены',
	'SHOUT_BBCODE_SUP'			=> 'Форматирование отменено',
	'SHOUT_BBCODE_ERROR'		=> 'Должны быть заполнены 2 поля',
	'SHOUT_BBCODE_ERROR_COUNT'	=> 'Вы должны вписать открывающие и закрывающие теги ВВ-кодов',
	'SHOUT_BBCODE_ERROR_SHAME'	=> 'Изменений в отображении нет',
	'SHOUT_DIV_BBCODE_CLOSE'	=> 'Закрыть панель форматирования текста',
	'SHOUT_DIV_BBCODE_EXPLAIN'	=> 'Вы можете посмотреть как будет выглядеть ваш текст в чате, если его обрамить разными ВВ-кодами<br />Введите открывающие теги(например [b]) и закрывающие (например [/b]) в соответствующие поля .<br /> После нажмите кнопку <strong>Применить</strong> и вы увидите как будет выглядеть ваш текст.',
	'SHOUT_ACTION_TITLE'		=> 'Действия с Пользователем:',
	'SHOUT_ACTION_PROFIL'		=> 'Смотреть профиль',
	'SHOUT_ACTION_CITE'			=> 'Написать публичное сообщение Пользователю',
	'SHOUT_ACTION_CITE_ON'		=> 'Для ',
	'SHOUT_ACTION_CITE_EXPLAIN'	=> 'Написать сообщение адресованное этому Пользователю',
	'SHOUT_ACTION_MSG'			=> 'Отправить приватное сообщение Пользователю',
	'SHOUT_ACTION_MSG_ROBOT'	=> 'Отправить сообщение от имени Робота',
	'SHOUT_ACTION_DELETE'		=> 'Удалить приватную переписку',
	'SHOUT_ACTION_DELETE_EXPLAIN'=> 'Вы действительно хотите удалить все свои приватные сообщения?',
	'SHOUT_ACTION_DEL_REP'		=> 'Все ваши приватные сообщения были удалены',
	'SHOUT_ACTION_DEL_NO'		=> 'Все ваши приватные сообщения были удалены',
	'SHOUT_ACTION_MCP'			=> 'Заметки о пользователе',
	'SHOUT_ACTION_BAN'			=> 'Заблокировать',
	'SHOUT_USER_ADMIN'			=> 'Администрировать',
	'SHOUT_USER_POST'			=> 'Лично для ',
	'SHOUT_TOO_BIG'				=> 'Ваше сообщение слишком длинное: ',
	'SHOUT_TOO_BIG2'			=> 'Максимальное количество знаков: ',
	'SHOUT_CLICK_SOUND_ON'		=> 'Включить звук',
	'SHOUT_CLICK_SOUND_OFF'		=> 'Выключить звук',
	'SHOUT_CHOICE_NAME'			=> 'Выбрать ник',
	'SHOUT_CHOICE_YES'			=> 'Ник обновлен',
	'SHOUT_CHOICE_NAME_RETURN'	=> 'Сначала надо выбрать имя пользователя.',
	'SHOUT_CHOICE_NAME_ERROR'	=> 'Сначала надо выбрать имя пользователя',
	'SHOUT_OUT_TIME'			=> 'чат в режиме ожидания...',
));

?>