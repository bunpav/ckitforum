<?php
/**
*
* @package breizh Shoutbox - Breizh installer -
* @version $Id: install/install_main.php 120 16:15 16/12/2011 Sylver35 Exp $ 
* @copyright (c) 2010, 2011 Breizh Portal  http://breizh-portal.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
*/
// Someone has tried to access the file direct. This is not a good idea, so exit
if (!defined('IN_PHPBB'))
{
	exit;
}
if (!defined('IN_INSTALL'))
{
	exit;
}

class install_main
{
	function affiche_gnu($lang = false)
	{
		global $phpbb_root_path, $phpEx, $user, $template;
		
		$sort = ($user->lang_name == 'fr' && !$lang) ? '_fr' : '';
		$file = file_get_contents($phpbb_root_path. 'install/license' .$sort. '.txt');
		$lines = str_replace("\n", '<br />', $file);
		$template->assign_vars(array(
			'IN_LICENSE'   	=> true,
			'CONTENT'		=> $lines,
		));
	}
	
	function permission_exists($auth_option, $global = true)
	{
		global $db;
		
		$type_sql = ($global) ? ' AND is_global = 1' : ' AND is_local = 1';
		$sql = 'SELECT auth_option_id
			FROM ' . ACL_OPTIONS_TABLE . "
			WHERE auth_option = '" . $db->sql_escape($auth_option) . "'"
			. $type_sql;
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if ($row)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function permission_remove($auth_option, $global = true)
	{
		global $auth, $cache, $db;

		$type_sql = ($global) ? ' AND is_global = 1' : ' AND is_local = 1';
		$sql = 'SELECT auth_option_id, is_global, is_local FROM ' . ACL_OPTIONS_TABLE . "
			WHERE auth_option = '" .$db->sql_escape($auth_option). "'" .
			$type_sql;
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if ($row)
		{
			$db->sql_query('DELETE FROM ' . ACL_GROUPS_TABLE . ' WHERE auth_option_id = ' .$row['auth_option_id']);
			$db->sql_query('DELETE FROM ' . ACL_ROLES_DATA_TABLE . ' WHERE auth_option_id = ' .$row['auth_option_id']);
			$db->sql_query('DELETE FROM ' . ACL_USERS_TABLE . ' WHERE auth_option_id = ' .$row['auth_option_id']);
			$db->sql_query('DELETE FROM ' . ACL_OPTIONS_TABLE . ' WHERE auth_option_id = ' .$row['auth_option_id']);
			// Purge the auth cache
			$cache->destroy('_acl_options');
			$auth->acl_clear_prefetch();
		}
		return;
	}
	
	function select_perm($perm)
	{
		global $db;
		
		$sql = 'SELECT auth_option_id 
			FROM ' . ACL_OPTIONS_TABLE . "
			WHERE auth_option = '" .$db->sql_escape($perm). "'";
		$result = $db->sql_query_limit($sql, 1);
		$auth_option_id = $db->sql_fetchfield('auth_option_id');
		$db->sql_freeresult($result);
		
		return $auth_option_id;
	}
	
	// Function for give the wanted role its option for admin standard
	function give_perm_a_standard($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_ADMIN_STANDARD'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for admin full
	function give_perm_a_full($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_ADMIN_FULL'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for user standard
	function give_perm_standard($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_STANDARD'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for user full
	function give_perm_full($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_FULL'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for user limited
	function give_perm_limited($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_LIMITED'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for user no pm
	function give_perm_nopm($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_NOPM'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for user no avatar
	function give_perm_noavatar($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_NOAVATAR'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for new member (since 3.0.6)
	function give_perm_new_member($number_option, $setting)
	{
		global $db;
		
		$role_id = '';
		$sql = 'SELECT role_id FROM ' . ACL_ROLES_TABLE . "
			WHERE role_name = 'ROLE_USER_NEW_MEMBER'";
		$result = $db->sql_query_limit($sql, 1);
		$role_id = (int) $db->sql_fetchfield('role_id');
		$db->sql_freeresult($result);
	
		if ($role_id)
		{
			$roles_data = array(
				'role_id'			=> $role_id,
				'auth_option_id'	=> $number_option,
				'auth_setting'		=> $setting,
			);
		
			$sql = 'INSERT INTO ' . ACL_ROLES_DATA_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
			$db->sql_query($sql);
		}
	}
	
	// Function for give the wanted role its option for anonymous
	function give_perm_anonymous($number_option, $setting)
	{
		global $db;
		
		$roles_data = array(
			'user_id'			=> 1,
			'forum_id'			=> 0,
			'auth_option_id'	=> $number_option,
			'auth_role_id'		=> 0,
			'auth_setting'		=> $setting,
		);

		$sql = 'INSERT INTO ' . ACL_USERS_TABLE . ' ' . $db->sql_build_array('INSERT', $roles_data);
		$db->sql_query($sql);
	}
	
	function module_exists($class, $module)
	{
		global $db;
		
		if (!$module || !$class)
		{
			return true;
		}
		$class = $db->sql_escape($class);
		$module = $db->sql_escape($module);
		$sql = 'SELECT module_id 
			FROM ' . MODULES_TABLE . "
			WHERE module_langname = '$module'
			AND module_class = '$class'";
		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);
		if ($row)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function delete_modules($module_name, $class)
	{
		global $db, $cache, $user;
		
		$module_name = $db->sql_escape($module_name);
		$class = $db->sql_escape($class);
		$sql = 'SELECT module_langname, module_id
			FROM ' . MODULES_TABLE . "
			WHERE module_langname LIKE '{$module_name}%'
			AND module_class = '$class'";
		$result = $db->sql_query($sql);
		if ($row = $db->sql_fetchrow($result))
		{
			do
			{
				add_log('admin', 'LOG_INSTALL_MODULE_DEL', $class, $this->lang_install($row['module_langname']));
				$sql = "DELETE FROM " . MODULES_TABLE . " WHERE module_id = " .$row['module_id'];
				$db->sql_query($sql);
			}
			while ($row = $db->sql_fetchrow($result));
		}
		$db->sql_freeresult($result);
		$cache->destroy("_modules_$class");
	}
	
	function delete_auth_option($auth_option)
	{
		global $db, $auth, $cache, $user;
		
		$i = 0;
		$auth_option = $db->sql_escape($auth_option);
		$sql = 'SELECT auth_option_id, auth_option
			FROM ' . ACL_OPTIONS_TABLE . "
			WHERE auth_option LIKE '{$auth_option}%'";
		$result = $db->sql_query($sql);
		if ($row = $db->sql_fetchrow($result))
		{
			do
			{
				$this->delete_auth_groups($row['auth_option_id']);
				$this->delete_auth_roles_data($row['auth_option_id']);
				$this->delete_acl_users($row['auth_option_id']);
				$sql = "DELETE FROM " . ACL_OPTIONS_TABLE . " WHERE auth_option_id = " .$row['auth_option_id'];
				$db->sql_query($sql);
				$i++;
			}
			while ($row = $db->sql_fetchrow($result));
		}
		$db->sql_freeresult($result);
		if ($i > 0)
		{
			add_log('admin', 'LOG_INSTALL_AUTH_DEL_ALL', $auth_option. '%', MOD_VERSION_FULL);
			$cache->destroy('_acl_options');
			$auth->acl_clear_prefetch();
		}
	}
	
	function delete_auth_groups($auth_option)
	{
		global $db;
		
		$sql = "DELETE FROM " . ACL_GROUPS_TABLE . " WHERE auth_option_id = " .(int)$auth_option;
		$db->sql_query($sql);
	}
	
	function delete_auth_roles_data($auth_option)
	{
		global $db;
		
		$sql = "DELETE FROM " . ACL_ROLES_DATA_TABLE . " WHERE auth_option_id = " .(int)$auth_option;
		$db->sql_query($sql);
	}
	
	function delete_acl_users($auth_option)
	{
		global $db;
		
		$sql = "DELETE FROM " . ACL_USERS_TABLE . " WHERE auth_option_id = " .(int)$auth_option;
		$db->sql_query($sql);
	}
	
	function delete_log($log_name)
	{
		global $db;
		
		$log_name = $db->sql_escape($log_name);
		$sql = "DELETE FROM " . LOG_TABLE . " WHERE log_operation LIKE '{$log_name}%'
			AND log_operation NOT IN ('LOG_INSTALL_MOD_ADD', 'LOG_INSTALL_ALTER_END_NO', 'LOG_INSTALL_MOD_UPDATE')";
		$db->sql_query($sql);
	}
	
	function delete_all_log()
	{
		global $db;
		
		$sql = "DELETE FROM " . LOG_TABLE . " WHERE log_operation LIKE 'LOG_INSTALL_%'";
		$db->sql_query($sql);
	}
	
	function style_name($style_id)
	{
		global $db;
		
		$style_id = (int)$style_id;
		$sql = 'SELECT t.theme_path AS theme 
			FROM ' . STYLES_TABLE . ' AS s
			LEFT JOIN ' . STYLES_THEME_TABLE . " AS t ON t.theme_id = s.theme_id
			WHERE s.style_id = $style_id";
		$result = $db->sql_query_limit($sql, 1);
		$style = $db->sql_fetchfield('theme');
		$db->sql_freeresult($result);
		
		return $style;
	}
	
	function template_name($template_id)
	{
		global $db;
		
		$template_id = (int)$template_id;
		$sql = 'SELECT t.template_path as template
			FROM ' . STYLES_TABLE . ' AS s
			LEFT JOIN ' . STYLES_TEMPLATE_TABLE . " AS t ON t.template_id = s.template_id
			WHERE s.style_id = $template_id";
		$result = $db->sql_query_limit($sql, 1);
		$template = $db->sql_fetchfield('template');
		$db->sql_freeresult($result);
		
		return $template;
	}
	
	function all_files_exist()
	{
		global $config, $user, $phpbb_root_path, $phpEx;
		
		$error = '';
		$shout_dir 		= '/theme/images/shout/';
		$style 			= utf8_normalize_nfc($this->style_name($config['default_style']));
		$template 		= utf8_normalize_nfc($this->template_name($config['default_style']));
		$template_user	= utf8_normalize_nfc($user->theme['template_path']);
		$filelist = array (
			'2'		=> 'ajax_shoutbox.php',
			'3'		=> 'shout_js.php',
			'6'		=> 'shout_popup.php',
			'8'		=> 'images/shoutbox/chars.js',
			'9'		=> 'adm/style/acp_shoutbox.html',
			'10'	=> 'includes/functions_shoutbox.php',
			'11'	=> 'includes/acp/acp_shoutbox.php',
			'12'	=> 'includes/acp/info/acp_shoutbox.php',
			'13'	=> 'language/' .$config['default_lang']. '/mods/info_acp_shoutbox.php',
			'14'	=> 'language/' .$config['default_lang']. '/mods/permissions_shoutbox.php',
			'15'	=> 'language/' .$config['default_lang']. '/mods/shout.php',
			'16'	=> ($user->data['user_lang'] != $config['default_lang']) ? 'language/' .$user->data['user_lang']. '/mods/info_acp_shoutbox.php' : '',
			'17'	=> ($user->data['user_lang'] != $config['default_lang']) ? 'language/' .$user->data['user_lang']. '/mods/permissions_shoutbox.php' : '',
			'18'	=> ($user->data['user_lang'] != $config['default_lang']) ? 'language/' .$user->data['user_lang']. '/mods/shout.php' : '',
			'19'	=> 'styles/' .$template. '/template/shout_body.html',
			'20'	=> 'styles/' .$template. '/template/shout_template.html',
			'22'	=> 'styles/' .$template. '/template/shoutbox.js',
			'23'	=> ($user->theme['template_path'] != $template) ? 'styles/' .$user->theme['template_path']. '/template/shout_body.html' : '',
			'24'	=> ($user->theme['template_path'] != $template) ? 'styles/' .$user->theme['template_path']. '/template/shout_template.html' : '',
			'26'	=> ($user->theme['template_path'] != $template) ? 'styles/' .$user->theme['template_path']. '/template/shoutbox.js' : '',
			'27' 	=> (file_exists($phpbb_root_path. 'styles/' .$style. '/theme/common.css')) ? 'styles/' .$style. '/theme/shoutbox.css' : '',
			'28' 	=> ($user->theme['theme_path'] != $style && file_exists($phpbb_root_path. 'styles/' .$user->theme['theme_path']. '/theme/common.css')) ? 'styles/' .$user->theme['theme_path']. '/theme/shoutbox.css' : '',
		);

		foreach ($filelist as $key => $file)
		{
			if (!file_exists($phpbb_root_path . $file))
			{
				$error .= sprintf($user->lang['INSTALL_NO_FILE'], $file);
			}
		}
		
		if (!is_dir($phpbb_root_path. 'styles/' . $style . $shout_dir))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'styles/' . $style . $shout_dir);
		}
		if (!is_dir($phpbb_root_path. 'styles/' . $user->theme['theme_path'] . $shout_dir) && $user->theme['theme_path'] != $style)
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'styles/' . $user->theme['theme_path'] . $shout_dir);
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/');
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/error/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/error/');
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/del/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/del/');
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/new/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/new/');
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/jscolor/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/jscolor/');
		}
		if (!is_dir($phpbb_root_path. 'images/shoutbox/panel/'))
		{
			$error .= sprintf($user->lang['INSTALL_NO_PATH'], 'images/shoutbox/panel/');
		}
		
		return $error;
	}
	
	function stat_install()
	{
		global $db, $config, $user, $phpbb_root_path, $phpEx, $db_tools, $img_yes, $img_no;
		
		$message = '';
		$open = '<tr><td><span style="color:';
		$close = '</span></td></tr>';
		
		$config_row 	= (isset($config['shout_cron_run'])) ? true : false;
		$auth_row		= $this->permission_exists('u_shout_inactiv', true);
		$module_row 	= ($this->module_exists('acp', 'ACP_SHOUTBOX') && $this->module_exists('acp', 'ACP_SHOUT_PANEL')) ? true : false;
		$table_true 	= ($db_tools->sql_table_exists(SHOUTBOX_TABLE) && $db_tools->sql_column_exists(SHOUTBOX_TABLE, 'shout_inp')) ? true : false;
		$table_p_true 	= ($db_tools->sql_table_exists(SHOUTBOX_PRIV_TABLE) && $db_tools->sql_column_exists(SHOUTBOX_PRIV_TABLE, 'shout_inp')) ? true : false;
		$exist_user 	= ($db_tools->sql_column_exists(USERS_TABLE, 'user_shout') && $db_tools->sql_column_exists(USERS_TABLE, 'user_shoutbox') && $db_tools->sql_column_exists(USERS_TABLE, 'shout_bbcode')) ? true : false;
		$exist_smil 	= ($db_tools->sql_column_exists(SMILIES_TABLE, 'display_on_shout')) ? true : false;
		$error 			= ($this->all_files_exist()) ? true : false;
		
		$message .= $open.((!$config_row) ? 'red' : 'green'). ';">' .$user->lang['INSTALL_ENTER_CONFIG'] . ((!$config_row) ? $img_no : $img_yes).$close;
		$message .= $open.((!$table_true) ? 'red' : 'green'). ';">' .sprintf($user->lang['INSTALL_TABLES'], 'shoutbox') . ((!$table_true) ? $img_no : $img_yes).$close;
		$message .= $open.((!$table_p_true) ? 'red' : 'green'). ';">' .sprintf($user->lang['INSTALL_TABLES'], 'shoutbox_priv') . ((!$table_p_true) ? $img_no : $img_yes).$close;
		$message .= $open.((!$exist_smil) ? 'red' : 'green'). ';">' .sprintf($user->lang['INSTALL_ALTER_TABLES'], 'smilies') . ((!$exist_smil) ? $img_no : $img_yes).$close;
		$message .= $open.((!$exist_user) ? 'red' : 'green'). ';">' .sprintf($user->lang['INSTALL_ALTER_TABLES'], 'users') . ((!$exist_user) ? $img_no : $img_yes).$close;
		$message .= $open.((!$module_row) ? 'red' : 'green'). ';">' .sprintf($user->lang['INSTALL_MODULES'], 'ACP') . ((!$module_row) ? $img_no : $img_yes).$close;
		$message .= $open.((!$auth_row) ? 'red' : 'green'). ';">' .$user->lang['INSTALL_AUTH'] . ((!$auth_row) ? $img_no : $img_yes).$close;
		$message .= $open.(($error) ? 'red' : 'green'). ';"> » ' .(($error == '') ? $user->lang['INSTALL_NO_ERRORS'] . $img_yes : $user->lang['INSTALL_IS_ERRORS'] . $img_no).$close;
		
		return $message;
	}
	
	function redirect($meta_url, $delay)
	{
		global $phpbb_root_path, $user, $config;
		
		$img_progress = '<img src="./images/loading.gif" alt="' .$user->lang['INSTALL_PROGRESS']. '" title="' .$user->lang['INSTALL_PROGRESS']. '" />';
		meta_refresh($delay, $meta_url);
		trigger_error('<br /><br /><p style="text-align:center;font-size:13px;">' .$img_progress. '<span style="margin-left:30px;">' .$user->lang['INSTALL_PROGRESS']. '</span></p><br /><br />');
	}
	
	function get_log($out_mode)
	{
		global $user, $db;
		
		$i = 0;
		$msg_log = '';
		$log = array();
		switch ($out_mode)
		{
			case 'del':
				$where_sql = array('LOG_INSTALL_MOD_DELETE');
				$nb = 1;
			break;
			case 'delete':
				$where_sql = array('LOG_INSTALL_MODULE_DEL', 'LOG_INSTALL_AUTH_DEL', 'LOG_INSTALL_AUTH_DEL_ALL', 'LOG_INSTALL_TABLE_DEL', 'LOG_INSTALL_COLUMN_DEL', 'LOG_INSTALL_CONFIG_DEL');
				$nb = 300;
			break;
			case 'modules':
				$where_sql = array('LOG_INSTALL_MODULE_ADD', 'LOG_INSTALL_MODULE_EDIT', 'LOG_INSTALL_MODULE_DEL', 'LOG_INSTALL_ALTER_MOD_NO');
				$nb = 60;
			break;
			case 'auth':
				$where_sql = array('LOG_INSTALL_AUTH_ADM', 'LOG_INSTALL_AUTH', 'LOG_INSTALL_AUTH_ADDED', 'LOG_INSTALL_AUTH_DEL', 'LOG_INSTALL_AUTH_DEL_ALL', 'LOG_INSTALL_ALTER_AUTH_NO');
				$nb = 40;
			break;
			case 'tables':
				$where_sql = array('LOG_INSTALL_TABLE_ADD', 'LOG_INSTALL_TABLE_DEL', 'LOG_INSTALL_COLUMN_ADD', 'LOG_INSTALL_COLUMN_DEL', 'LOG_INSTALL_ALTER_NO', 'LOG_INSTALL_CONFIG_UDP', 'LOG_INSTALL_CONFIG_ADD', 'LOG_INSTALL_ALTER_CONFIG_NO', 'LOG_INSTALL_UPDATE_CONFIG');
				$nb = 250;
			break;
			case 'config':
				$where_sql = array('LOG_INSTALL_CONFIG_UDP', 'LOG_INSTALL_CONFIG_ADD', 'LOG_INSTALL_ALTER_CONFIG_NO', 'LOG_INSTALL_UPDATE_CONFIG');
				$nb = 200;
			break;
			case 'end':
				$where_sql = array('LOG_INSTALL_CONFIG_UDP', 'LOG_INSTALL_CONFIG_ADD', 'LOG_INSTALL_MOD_ADD', 'LOG_INSTALL_ALTER_END_NO');
				$nb = 3;
			break;
			case 'check':
				$where_sql = array('LOG_INSTALL_CHECK_OK', 'LOG_INSTALL_CHECK_NO');
				$nb = 20;
			break;
			default:
				trigger_error('INSTALL_NO_EXIST');
			break;
		}
		
		$sql = 'SELECT log_operation, log_data, log_id
			FROM ' . LOG_TABLE . "
			WHERE (" .$db->sql_in_set('log_operation', $where_sql, false, true). ")
			ORDER BY log_id ASC";
		$result = $db->sql_query_limit($sql, $nb);
		while ($row = $db->sql_fetchrow($result))
		{
			$log[$i] = array('action' => (isset($user->lang[$row['log_operation']])) ? $user->lang[$row['log_operation']] : '{' . ucfirst(str_replace('_', ' ', $row['log_operation'])) . '}');
			$log_data_ary = @unserialize($row['log_data']);
			$log_data_ary = ($log_data_ary === false) ? array() : $log_data_ary;
			if ((substr_count($log[$i]['action'], '%') - sizeof($log_data_ary)) > 0)
			{
				$log_data_ary = array_merge($log_data_ary, array_fill(0, substr_count($log[$i]['action'], '%') - sizeof($log_data_ary), ''));
			}
			$log[$i]['action'] = vsprintf($log[$i]['action'], $log_data_ary);
			$log[$i]['action'] = bbcode_nl2br(censor_text($log[$i]['action']));
			$log[$i]['action'] = str_replace('<br />', ' ', $log[$i]['action']);
			$msg_log .= $log[$i]['action']. '<br />';
			$i++;
		}
		$db->sql_freeresult($result);
		
		return $msg_log;
	}
	
	function unlink_old_files($_filelist)
	{
		global $phpbb_root_path;
		
		foreach ($_filelist as $_key => $_file)
		{
			if (file_exists($phpbb_root_path . $_file))
			{
				@unlink($phpbb_root_path . $_file);
			}
		}
	}
	
	function add_config($config_data, $speed = false)
	{
		global $config, $db, $cache, $user;
		
		foreach ($config_data as $config_name => $config_array)
		{
			if (!$speed)
			{
				if (isset($config[$config_name]))
				{
					add_log('admin', 'LOG_INSTALL_CONFIG_UDP', $config_name);
				}
				else
				{
					add_log('admin', 'LOG_INSTALL_CONFIG_ADD', $config_name);
				}
			}
			set_config("{$config_name}", "{$config_array[0]}", "{$config_array[1]}");
		}
		$cache->purge();
		return true;
	}
	
	function delete_config($config_name)
	{
		global $config, $db, $cache;
		
		if (isset($config[$config_name]))
		{
			$sql = 'DELETE FROM ' . CONFIG_TABLE . " WHERE config_name = '" . $db->sql_escape($config_name) . "'";
			$db->sql_query($sql);
			
			add_log('admin', 'LOG_INSTALL_CONFIG_DEL', $config_name);
			
			unset($config[$config_name]);
			$cache->destroy('config');
			
			return true;
		}
		return false;
	}
	
	function create_tables_mod($speed = false)
	{
		global $db, $user, $db_tools;
		
		$post_shout = $post_shout_priv = false;
		
		$this->add_new_column(SHOUTBOX_TABLE, 'shout_robot_user', array('UINT', 0), $speed);
		$this->add_new_column(SHOUTBOX_TABLE, 'shout_forum', array('UINT', 0), $speed);
		$this->add_new_column(SHOUTBOX_TABLE, 'shout_info', array('UINT:2', 0), $speed);
		$this->add_new_column(SHOUTBOX_TABLE, 'shout_inp', array('UINT', 0), $speed);
		$this->add_new_column(SHOUTBOX_PRIV_TABLE, 'shout_robot_user', array('UINT', 0), $speed);
		$this->add_new_column(SHOUTBOX_PRIV_TABLE, 'shout_forum', array('UINT', 0), $speed);
		$this->add_new_column(SHOUTBOX_PRIV_TABLE, 'shout_info', array('UINT:2', 0), $speed);
		$this->add_new_column(SHOUTBOX_PRIV_TABLE, 'shout_inp', array('UINT', 0), $speed);

		$schema_shoutbox = array(
			'COLUMNS'   => array(
				'shout_id'				=> array('UINT', NULL, 'auto_increment'),
				'shout_user_id'			=> array('UINT', 0),
				'shout_time'			=> array('TIMESTAMP', 0),
				'shout_ip'				=> array('VCHAR:40', ''),
				'shout_text'    		=> array('MTEXT_UNI', ''),
				'shout_bbcode_bitfield'	=> array('VCHAR:255', ''),
				'shout_bbcode_uid'  	=> array('VCHAR_UNI:8', ''),
				'shout_bbcode_flags'  	=> array('UINT:11', 7),
				'shout_robot'        	=> array('UINT:1', 0),
				'shout_robot_user'      => array('UINT', 0),
				'shout_forum'        	=> array('UINT', 0),
				'shout_info'        	=> array('UINT:2', 0),
				'shout_inp'        		=> array('UINT', 0),
			),
			'PRIMARY_KEY'	=> 'shout_id',
		);
		
		if (!$db_tools->sql_table_exists(SHOUTBOX_TABLE))
		{
			$db_tools->sql_create_table(SHOUTBOX_TABLE, $schema_shoutbox);
			if (!$speed)
			{
				add_log('admin', 'LOG_INSTALL_TABLE_ADD', SHOUTBOX_TABLE);
			}
		}
		if (!$db_tools->sql_table_exists(SHOUTBOX_PRIV_TABLE))
		{
			$db_tools->sql_create_table(SHOUTBOX_PRIV_TABLE, $schema_shoutbox);
			if (!$speed)
			{
				add_log('admin', 'LOG_INSTALL_TABLE_ADD', SHOUTBOX_PRIV_TABLE);
			}
		}

		$uid 		= $bitfield = $options = $uid2 = $bitfield2 = $options2 = '';
		$message 	= utf8_normalize_nfc(sprintf($user->lang['SHOUT_WELCOME'], MOD_VERSION_FULL));
		$message2 	= utf8_normalize_nfc(sprintf($user->lang['SHOUT_WELCOME_INSTALL'], MOD_VERSION_FULL));
		generate_text_for_storage($message, $uid, $bitfield, $options, true, true, true);
		generate_text_for_storage($message2, $uid2, $bitfield2, $options2, true, true, true);

		$insert = "(shout_user_id, shout_time, shout_ip, shout_text, shout_bbcode_bitfield, shout_bbcode_uid, shout_bbcode_flags, shout_robot, shout_robot_user, shout_forum, shout_info, shout_inp) VALUES
			('0', '" .time(). "', '{$user->ip}', '{$message2}', '{$bitfield2}', '{$uid2}', '{$options2}', '0', '{$user->data['user_id']}', '0', '0', '0'),
			('0', '" .time(). "', '{$user->ip}', '{$message}', '{$bitfield}', '{$uid}', '{$options}', '0', '{$user->data['user_id']}', '0', '0', '0')";
		
		$sql = "INSERT INTO " . SHOUTBOX_TABLE . " $insert";
		$db->sql_query($sql);

		$sql = "INSERT INTO " . SHOUTBOX_PRIV_TABLE . " $insert";
		$db->sql_query($sql);

		return;
	}
	
	function add_shoutbox_config($speed = false)
	{
		global $db, $user, $config;
		
		if (CUR_VERSION == 1)
		{
			$config_data = array(
				'shout_another'				=> array('0', 0),
				'shout_color_background'	=> array('transparent', 0),
				'shout_color_background_pop'=> array('transparent', 0),
				'shout_color_background_priv'=> array('transparent', 0),
				'shout_color_message'		=> array('008000', 0),
				'shout_color_robot'			=> array('990033', 0),
				'shout_del_acp'				=> array('0', 1),
				'shout_del_acp_priv'		=> array('0', 1),
				'shout_del_auto'			=> array('0', 1),
				'shout_del_auto_priv'		=> array('0', 1),
				'shout_del_purge'			=> array('0', 1),
				'shout_del_purge_priv'		=> array('0', 1),
				'shout_del_user'			=> array('0', 1),
				'shout_del_user_priv'		=> array('0', 1),
				'shout_enable_robot'		=> array('1', 0),
				'shout_exclude_forums'		=> array('0', 0),
				'shout_flood_interval'		=> array('5', 0),
				'shout_forum'				=> array('1', 0),
				'shout_height'				=> array('160', 0),
				'shout_ie_nr'				=> array('5', 0),
				'shout_ie_nr_pop'			=> array('15', 0),
				'shout_ie_nr_priv'			=> array('15', 0),
				'shout_index'				=> array('1', 0),
				'shout_interval'			=> array('3600', 0),
				'shout_last_run'			=> array(time(), 1),
				'shout_last_run_priv'		=> array(time(), 1),
				'shout_log_cron'			=> array('0', 0),
				'shout_log_cron_priv'		=> array('0', 0),
				'shout_lottery'				=> array('0', 0),
				'shout_max_posts'			=> array('450', 0),
				'shout_max_posts_on'		=> array('200', 0),
				'shout_max_posts_on_priv'	=> array('200', 0),
				'shout_max_posts_priv'		=> array('450', 0),
				'shout_non_ie_height_pop'	=> array('415', 0),
				'shout_non_ie_height_priv'	=> array('460', 0),
				'shout_non_ie_nr'			=> array('20', 0),
				'shout_non_ie_nr_pop'		=> array('22', 0),
				'shout_non_ie_nr_priv'		=> array('20', 0),
				'shout_nr'					=> array('2', 1),
				'shout_nr_acp'				=> array('20', 0),
				'shout_nr_log'				=> array('0', 1),
				'shout_nr_log_priv'			=> array('0', 1),
				'shout_nr_priv'				=> array('2', 1),
				'shout_on_cron'				=> array('1', 0),
				'shout_on_cron_priv'		=> array('1', 0),
				'shout_popup_height'		=> array('510', 0),
				'shout_popup_width'			=> array('850', 0),
				'shout_portal'				=> array('1', 0),
				'shout_position_another'	=> array('0', 0),
				'shout_position_index'		=> array('0', 0),
				'shout_position_portal'		=> array('0', 0),
				'shout_post_robot'			=> array('1', 0),
				'shout_post_robot_priv'		=> array('1', 0),
				'shout_prez_form'			=> array('0', 0),
				'shout_prune'				=> array('0', 0),
				'shout_prune_priv'			=> array('0', 0),
				'shout_robbery'				=> array('0', 0),
				'shout_see_buttons'			=> array('0', 0),
				'shout_see_buttons_left'	=> array('0', 0),
				'shout_sessions'			=> array('1', 0),
				'shout_sessions_priv'		=> array('0', 0),
				'shout_sessions_bots'		=> array('0', 0),
				'shout_smilies_height'		=> array('430', 0),
				'shout_smilies_width'		=> array('600', 0),
				'shout_time'				=> array(time(), 0),
				'shout_time_priv'			=> array(time(), 0),
				'shout_title'				=> array($user->lang['SHOUT_START'], 0),
				'shout_title_priv'			=> array($user->lang['SHOUT_PRIV'], 0),
				'shout_topic'				=> array('1', 0),
			);
			$this->add_config($config_data, $speed);
		}
		
		if (CUR_VERSION < 3)
		{
			$config_data = array(
				'shout_avatar'					=> array('1', 0),
				'shout_avatar_height'			=> array('18', 0),
				'shout_avatar_robot'			=> array('1', 0),
				'shout_bar_option'				=> array('1', 0),
				'shout_bar_option_pop'			=> array('1', 0),
				'shout_bar_option_priv'			=> array('1', 0),
				'shout_birthday'				=> array('1', 0),
				'shout_birthday_priv'			=> array('1', 0),
				'shout_button_background'		=> array('1', 0),
				'shout_button_background_pop'	=> array('1', 0),
				'shout_button_background_priv'	=> array('0', 0),
				'shout_color_background_sub'	=> array('grey', 0),
				'shout_color_background_sub_pop'=> array('grey', 0),
				'shout_color_background_sub_priv'=> array('grey', 0),
				'shout_correct_minutes'			=> array('1', 0),
				'shout_delete_robot'			=> array('0', 0),
				'shout_edit_robot'				=> array('0', 0),
				'shout_edit_robot_priv'			=> array('0', 0),
				'shout_enable'					=> array('1', 0),
				'shout_hangman'					=> array('0', 0),
				'shout_hangman_priv'			=> array('0', 0),
				'shout_hello'					=> array('1', 0),
				'shout_hello_priv'				=> array('1', 0),
				'shout_newest'					=> array('1', 0),
				'shout_newest_priv'				=> array('1', 0),
				'shout_pagin_option'			=> array('0', 0),
				'shout_pagin_option_pop'		=> array('0', 0),
				'shout_pagin_option_priv'		=> array('0', 0),
				'shout_panel'					=> array('1', 0),
				'shout_panel_all'				=> array('0', 0),
				'shout_position_forum'			=> array('0', 0),
				'shout_position_topic'			=> array('0', 0),
				'shout_rep_robot'				=> array('1', 0),
				'shout_rep_robot_priv'			=> array('0', 0),
				'shout_robbery_priv'			=> array('0', 0),
				'shout_robot_choice'			=> array('1, 2, 3, 5, 6, 7, 8', 0),
				'shout_rules'					=> array('1', 0),
				'shout_sessions_bots_priv'		=> array('0', 0),
				'shout_sound_del'				=> array('cartoon1.swf', 0),
				'shout_sound_error'				=> array('failure.swf', 0),
				'shout_sound_new'				=> array('elegance.swf', 0),
				'shout_source'					=> array('http://breizh-portal.com/', 0),
				'shout_tracker'					=> array('0', 0),
				'shout_tracker_edit'			=> array('0', 0),
				'shout_tracker_edit_priv'		=> array('0', 0),
				'shout_tracker_priv'			=> array('0', 0),
				'shout_tracker_rep'				=> array('0', 0),
				'shout_tracker_rep_priv'		=> array('0', 0),
				'shout_width_post'				=> array('325', 0),
				'shout_width_post_pop'			=> array('325', 0),
				'shout_width_post_priv'			=> array('325', 0),
			);
			$this->add_config($config_data, $speed);
		}
		
		if (CUR_VERSION < 4)
		{
			$config_data = array(
				'shout_avatar_none'				=> array('1', 0),
				'shout_avatar_user'				=> array('1', 0),
				'shout_avatar_img'				=> array('no_avatar.gif', 0),
				'shout_avatar_img_robot'		=> array('avatar_robot.png', 0),
				'shout_max_post_chars'			=> array('300', 0),
				'shout_sound_on'				=> array('1', 0),
			);
			$this->add_config($config_data, $speed);
		}
		
		if (CUR_VERSION < 5)
		{
			$config_data = array(
				'shout_bbcode'					=> array('code, list, mod, schild, testlink, tabs, table', 0),
				'shout_bbcode_size'				=> array('140', 0),
				'shout_bbcode_user'				=> array('', 0),
				'shout_birthday_exclude'		=> array('', 0),
				'shout_color_panel'				=> array('3', 0),
				'shout_cron_hour'				=> array('09', 0),
				'shout_cron_run'				=> array('', 1),
				'shout_hangman_cr'				=> array('0', 0),
				'shout_hangman_cr_priv'			=> array('0', 0),
				'shout_inactiv_anony'			=> array('15', 0),
				'shout_inactiv_member'			=> array('45', 0),
				'shout_lottery_priv'			=> array('0', 0),
				'shout_page_exclude'			=> array('ucp.php?mode=register', 0),
				'shout_panel_exit_img'			=> array('out.png', 0),
				'shout_panel_height'			=> array('510', 0),
				'shout_panel_img'				=> array('shout-open.png', 0),
				'shout_panel_width'				=> array('800', 0),
				'shout_robot_choice_priv'		=> array('1, 2, 3, 5, 6, 7, 8', 0),
				'shout_start'					=> array('1', 1),
				'shout_sudoku'					=> array('0', 0),
				'shout_sudoku_priv'				=> array('0', 0),
				'shout_temp_anonymous'			=> array('10', 0),
				'shout_temp_users'				=> array('5', 0),
				'shout_update'					=> array('', 0),
				'shout_update_url'				=> array('', 0),
				'shout_version_display'			=> array('PGEgaHJlZj0iaHR0cDovL2JyZWl6aC1wb3J0YWwuY29tL21vZC1icmVpemgtc2hvdXRib3gtZjIxLmh0bWwiIHRpdGxlPSJCeSBCcmVpemggcG9ydGFsIj7CqSBCcmVpemggU2hvdXRib3ggdg==', 0),
				'shout_version_run'				=> array('0', 1),
				'shout_version_url'				=> array('<a href="%1$sindex.html">%2$s</a>', 0),
				'shout_width_post_sub'			=> array('325', 0),
				'shout_width_post_sub_pop'		=> array('325', 0),
				'shout_width_post_sub_priv'		=> array('325', 0),
			);
			$this->add_config($config_data, $speed);
		}
		if (!$speed)
		{
			add_log('admin', 'LOG_INSTALL_UPDATE_CONFIG');
		}
	}
	
	function modifie_tables($speed = false)
	{
		global $db, $db_tools;
		
		$db_tools->sql_column_change(CONFIG_TABLE, 'config_value', array('TEXT', 0));
		$this->add_new_column(USERS_TABLE, 'user_shout', array('VCHAR:100', '0, , , , 3, 3, 3, 3, 3'), $speed);
		$this->add_new_column(USERS_TABLE, 'user_shoutbox', array('VCHAR:100', 'N,N,N,N,N,N'), $speed);
		$this->add_new_column(USERS_TABLE, 'shout_bbcode', array('VCHAR:350', 0), $speed);
		$this->add_new_column(SMILIES_TABLE, 'display_on_shout', array('BOOL', 1), $speed);
		return;
	}
	
	// add new column
	function add_new_column($table_name, $column, $data, $speed = false)
	{
		global $db, $db_tools;
		
		if ($db_tools->sql_table_exists($table_name))
		{
			if (!$db_tools->sql_column_exists($table_name, $column))
			{
				$db_tools->sql_column_add($table_name, $column, $data);
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_COLUMN_ADD', $column, $table_name);
				}
			}
		}
	}
	
	// Delete config for this mod
	function delete_config_mod($sort_config, $nb_del = false)
	{
		global $db, $config, $db_tools;
		
		$sql = 'SELECT * FROM ' . CONFIG_TABLE . " WHERE config_name LIKE '" .$db->sql_escape($sort_config). "'";
		$result = $db->sql_query($sql);
		if ($row = $db->sql_fetchrow($result))
		{
			do
			{
				$this->delete_config($row['config_name']);
			}
			while ($row = $db->sql_fetchrow($result));
		}
		$db->sql_freeresult($result);
		
		if ($nb_del)
		{
			if (isset($config[$nb_del]))
			{
				set_config_count($nb_del, 1, false);
			}
			else
			{
				$config_data = array($nb_del => array('1', 0));
				$this->add_config($config_data);
			}
		}
	}
	
	// delete columns in tables
	function delete_column_mod($column_data)
	{
		global $db, $db_tools;
		
		foreach ($column_data as $nb => $table_array)
		{
			$column_name = $db->sql_escape($table_array[1]);
			$table_name = $db->sql_escape($table_array[0]);
			if ($db_tools->sql_column_exists($table_name, $column_name))
			{
				$db_tools->sql_column_remove($table_name, $column_name);
				add_log('admin', 'LOG_INSTALL_COLUMN_DEL', $column_name, $table_name);
			}
		}
	}
	
	// delete tables
	function delete_table_mod($tables_name)
	{
		global $db, $db_tools;
		
		foreach ($tables_name as $key => $table_name)
		{
			if ($db_tools->sql_table_exists($table_name))
			{
				add_log('admin', 'LOG_INSTALL_TABLE_DEL', $table_name);
				$db_tools->sql_table_drop($table_name);
			}
		}
	}
	
	function update_module_install(&$module_data, $speed = false)
	{
		global $db, $user, $modules;

		if (!isset($module_data['module_id']))
		{
			// no module_id means we're creating a new category/module
			if ($module_data['parent_id'])
			{
				$sql = 'SELECT left_id, right_id
					FROM ' . MODULES_TABLE . "
					WHERE module_class = '" . $db->sql_escape($module_data['module_class']) . "'
						AND module_id = " . (int) $module_data['parent_id'];
				$result = $db->sql_query($sql);
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);

				if (!$row)
				{
					return 'PARENT_NO_EXIST';
				}

				// Workaround
				$row['left_id'] = (int) $row['left_id'];
				$row['right_id'] = (int) $row['right_id'];

				$sql = 'UPDATE ' . MODULES_TABLE . "
					SET left_id = left_id + 2, right_id = right_id + 2
					WHERE module_class = '" . $db->sql_escape($module_data['module_class']) . "'
						AND left_id > {$row['right_id']}";
				$db->sql_query($sql);

				$sql = 'UPDATE ' . MODULES_TABLE . "
					SET right_id = right_id + 2
					WHERE module_class = '" . $db->sql_escape($module_data['module_class']) . "'
						AND {$row['left_id']} BETWEEN left_id AND right_id";
				$db->sql_query($sql);

				$module_data['left_id'] = (int) $row['right_id'];
				$module_data['right_id'] = (int) $row['right_id'] + 1;
			}
			else
			{
				$sql = 'SELECT MAX(right_id) AS right_id
					FROM ' . MODULES_TABLE . "
					WHERE module_class = '" . $db->sql_escape($module_data['module_class']) . "'";
				$result = $db->sql_query($sql);
				$row = $db->sql_fetchrow($result);
				$db->sql_freeresult($result);

				$module_data['left_id'] = (int) $row['right_id'] + 1;
				$module_data['right_id'] = (int) $row['right_id'] + 2;
			}

			$sql = 'INSERT INTO ' . MODULES_TABLE . ' ' . $db->sql_build_array('INSERT', $module_data);
			$db->sql_query($sql);

			$module_data['module_id'] = $db->sql_nextid();

			if (!$speed)
			{
				add_log('admin', 'LOG_INSTALL_MODULE_ADD', $module_data['module_class'], $this->lang_install($module_data['module_langname']));
			}
		}
		else
		{
			$row = $modules->get_module_row($module_data['module_id']);

			if ($module_data['module_basename'] && !$row['module_basename'])
			{
				// we're turning a category into a module
				$branch = $modules->get_module_branch($module_data['module_id'], 'children', 'descending', false);

				if (sizeof($branch))
				{
					return array($user->lang['NO_CATEGORY_TO_MODULE']);
				}
			}

			if ($row['parent_id'] != $module_data['parent_id'])
			{
				$modules->move_module($module_data['module_id'], $module_data['parent_id']);
			}

			$update_ary = $module_data;
			unset($update_ary['module_id']);

			$sql = 'UPDATE ' . MODULES_TABLE . '
				SET ' . $db->sql_build_array('UPDATE', $update_ary) . "
				WHERE module_class = '" . $db->sql_escape($module_data['module_class']) . "'
					AND module_id = " . (int) $module_data['module_id'];
			$db->sql_query($sql);

			if (!$speed)
			{
				add_log('admin', 'LOG_INSTALL_MODULE_EDIT', $module_data['module_class'], $this->lang_install($module_data['module_langname']));
			}
		}
		return;
	}
	
	function lang_install($langname)
	{
		global $user;

		return (!empty($user->lang[$langname])) ? $user->lang[$langname] : $langname;
	}
	
	function lang_install_acl($langname)
	{
		global $user;

		return (!empty($user->lang['acl_' .$langname]['lang'])) ? $user->lang['acl_' .$langname]['lang'] : $langname;
	}
}

?>