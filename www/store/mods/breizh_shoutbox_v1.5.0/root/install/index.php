<?php
/**
*
* @package breizh Shoutbox - Breizh installer -
* @version $Id: install/index.php 120 16:15 16/12/2011 Sylver35 Exp $ 
* @copyright (c) 2010, 2011 Breizh Portal  http://breizh-portal.com
* @license http://opensource.org/licenses/gpl-license.php GNU Public License 
*
*/

/**
* @ignore
*/

define('IN_PHPBB', true);
define('IN_INSTALL', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../'; //relative path to phpBB3 forum directory
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/acp/acp_modules.' . $phpEx);
include($phpbb_root_path . 'includes/acp/auth.' . $phpEx);
include($phpbb_root_path . 'install/install_main.' . $phpEx);
include($phpbb_root_path . 'includes/db/db_tools.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();
$user->add_lang(array('mods/shout', 'mods/info_acp_shoutbox', 'mods/permissions_shoutbox', 'mods/info_acp_installer'));

// Mod and Version in use
define('MOD_VERSION', '1.5.0');
define('NAME_MOD', $user->lang['ACP_SHOUTBOX']);
define('MOD_VERSION_FULL', sprintf($user->lang['SHOUT_NAME_FULL'], MOD_VERSION));
$version = 'shout_version';

$accept 	= request_var('accept', -1);
$mode 		= request_var('mode', '');
$out_mode 	= request_var('out', '');
$delete 	= request_var('delete', 0);
$in_del 	= request_var('del', 0);
$in_tables 	= request_var('tab', 0);
$in_config 	= request_var('conf', 0);
$in_modul 	= request_var('modul', 0);
$in_auth 	= request_var('aut', 0);
$in_end 	= request_var('end', 0);
$license_en = request_var('l', 0);
$go_action	= request_var('f', 0);
$in_check 	= request_var('check', 0);
$speed		= request_var('s', 0);
$speed		= ($speed == 1) ? true : false;
$go_speed 	= $speed ? '&amp;s=1' : '&amp;s=0';
$error 		= $msg_log = $message_etat = $all_files_exist = '';
$url_go 	= 'mode=first';
$stats		= true;

// First, is user any type of admin? No, then stop here, each script needs to
// check specific permissions but this is a catchall
if (!$auth->acl_get('a_'))
{
	if ($user->data['is_bot'])
	{
		redirect(append_sid("{$phpbb_root_path}index.$phpEx"));
	}
	trigger_error('INSTALL_NO_ADMIN');
}

// The install file html exist?
// otherwise, the installation may not be done!
$isfile = 'styles/' .$user->theme['template_path']. '/template/breizh_installer.html';
if (!file_exists($phpbb_root_path . $isfile))
{
	trigger_error(sprintf($user->lang['INSTALL_NO_FILE'], '" ' .$isfile. ' "'));
}

$install 	= new install_main();
$db_tools 	= new phpbb_db_tools($db);
$modules 	= new acp_modules();
$auth_admin = new auth_admin();

// Checks if one CSS file is on too...
// Too many errors by so confusing sort of style
$defaut_theme_pro = $user_theme_pro = $error_theme = false;
$message_error_theme = '';
$defaut_template = $install->style_name($config['default_style']);
if (file_exists($phpbb_root_path. 'styles/' .$defaut_template. '/theme/common.css'))
{
	$defaut_theme_pro = true;
	if (!$defaut_theme_pro && file_exists($phpbb_root_path. 'styles/' .$defaut_template. 'theme/shoutbox.css'))
	{
		$error_theme = true;
		$message_error_theme .= sprintf($user->lang['INSTALL_THEME_ERROR'], 'shoutbox.css', $defaut_template);
	}
}
if (file_exists($phpbb_root_path. 'styles/' .$user->theme['theme_path']. '/theme/common.css'))
{
	$user_theme_pro = true;
	if (!$user_theme_pro && file_exists($phpbb_root_path. 'styles/' .$user->theme['theme_path']. '/theme/shoutbox.css') && $defaut_template != $user->theme['theme_path'])
	{
		$error_theme = true;
		$message_error_theme .= (($message_error_theme) ? '<br />' : '') . sprintf($user->lang['INSTALL_THEME_ERROR'], 'shoutbox.css', $user->theme['theme_path']);
	}
}
if ($defaut_template == $user->theme['theme_path'])
{
	$styles_right = true;
	$message_style = sprintf($user->lang['INSTALL_INFO_STYLE'], $defaut_template, $user->lang['INSTALL_' .(($defaut_theme_pro) ? 'PROSILVER' : 'SUBSILVER')], (($defaut_theme_pro = false) ? $user->lang['INSTALL_INFO_SUB_XML'] : ''));
}
else
{
	$styles_right = false;
	$message_style .= '<br />' .sprintf($user->lang['INSTALL_INFO_STYLE'], $user->theme['theme_path'], $user->lang['INSTALL_' .(($defaut_theme_pro) ? 'PROSILVER' : 'SUBSILVER')], (($defaut_theme_pro = false) ? $user->lang['INSTALL_INFO_SUB_XML'] : ''));
}

$img_yes	= '<img src="./images/shout_ok.png" alt="' .$user->lang['YES']. '" title="' .$user->lang['YES']. '" />';
$img_no		= '<img src="./images/shout_no.png" alt="' .$user->lang['NO']. '" title="' .$user->lang['NO']. '" />';

// Check if tables and constant are defined...
if (!defined('SHOUTBOX_TABLE'))
{
	$error .= sprintf($user->lang['INSTALL_TABLE_UNDEFINED'], 'shoutbox');
}
if (!defined('SHOUTBOX_PRIV_TABLE'))
{
	$error .= sprintf($user->lang['INSTALL_TABLE_UNDEFINED'], 'shoutbox_priv');
}
if (!defined('ROBOT'))
{
	$error .= sprintf($user->lang['INSTALL_CONSTANT_UNDEFINED'], 'ROBOT');
}

// Check if old version is present or not
// Current version already installed?
if (isset($config[$version]))
{
	if ($config[$version] == '1.0.0')
	{
		trigger_error('SHOUT_USE_NEXT');
	}
	else if ($config[$version] == '1.1.0')
	{
		define('CUR_VERSION', 2);
		if (isset($config['del_shout']))
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], sprintf($user->lang['INSTALL_FIRST_UNINSTAL'], NAME_MOD, $config['del_shout']));
		}
		else
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], '');
		}
	}
	else if ($config[$version] == '1.3.0')
	{
		define('CUR_VERSION', 3);
		if (isset($config['del_shout']))
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], sprintf($user->lang['INSTALL_FIRST_UNINSTAL'], NAME_MOD, $config['del_shout']));
		}
		else
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], '');
		}
	}
	else if ($config[$version] == '1.4.0')
	{
		define('CUR_VERSION', 4);
		if (isset($config['del_shout']))
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], sprintf($user->lang['INSTALL_FIRST_UNINSTAL'], NAME_MOD, $config['del_shout']));
		}
		else
		{
			$message_first = sprintf($user->lang['INSTALL_FIRST_YES'], $config[$version], '');
		}
	}
	else if ($config[$version] == MOD_VERSION)
	{
		define('CUR_VERSION', 10);
		$message_first = sprintf($user->lang['INSTALL_FIRST_CUR'], $user->lang['SHOUT_ALL_MESSAGES']);
	}
}
else
{
	define('CUR_VERSION', 1);
	if (isset($config['del_shout']))
	{
		$message_first = sprintf($user->lang['INSTALL_FIRST_NONE'], sprintf($user->lang['INSTALL_FIRST_UNINSTAL'], NAME_MOD, $config['del_shout']));
	}
	else
	{
		$message_first = sprintf($user->lang['INSTALL_FIRST_NONE'], '');
	}
}

switch ($mode)
{
	case 'first':
		$stats = false;
		if ($delete == 1)
		{
			redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", 'mode=delete&amp;f=1'));
		}
		else if (CUR_VERSION == 10 && $delete == 0)
		{
			redirect(append_sid("{$phpbb_root_path}install/index.$phpEx"));
		}
		else
		{
			redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", 'mode=license'));
		}
	break;
	case 'license':
		$template->assign_vars(array(
			'INSTALL_TERMS'		=> sprintf($user->lang['INSTALL_TERMS'], MOD_VERSION_FULL),
			'INSTALL_TERMS_LANG'=> $user->lang['INSTALL_TERMS_' .(($license_en) ? 'FR' : 'EN')],
			'U_TERMS_LANG'		=> append_sid("{$phpbb_root_path}install/index.$phpEx", 'mode=license&amp;l=' .(($license_en) ? '0' : '1')),
		));
		$url_go = 'mode=license';
		$stats = false;
		if ($license_en == 1 && $accept == -1)
		{
			$install->affiche_gnu(true);
		}
		else if ($accept == -1)
		{
			$install->affiche_gnu(false);
		}
		else if ($accept == 1)
		{
			$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=check&amp;check=0"), 2);
		}
		else if ($accept == 0)
		{
			trigger_error(sprintf($user->lang['INSTALL_NO_ACCEPT_END'], MOD_VERSION_FULL));
		}
	break;
	case 'delete':
		if ($in_del == 0 && $go_action == 1)
		{
			$install->delete_all_log();
			$template->assign_vars(array(
				'S_IN_CHECK'	=> true,
				'DEL_BUTTON'	=> true,
				'CHECK_TITLE'	=> sprintf($user->lang['INSTALL_CHECK_DELETE_TITLE'], NAME_MOD),
				'CHECK_MESSAGE'	=> $user->lang['INSTALL_CHECK_DELETE_MESSAGE'],
			));
			$url_go = 'mode=delete';
		}
		else if ($in_del == 0 && $go_action == 0)
		{
			redirect(append_sid("{$phpbb_root_path}install/index.$phpEx"));
		}
		else if ($in_del == 1 && $go_action == 0)
		{
			// Delete the config mod -> 'config_mod_%' and add a counter of deletions
			$install->delete_config_mod('shout_%', 'del_shout');
			
			// Delete columns in phpbb tables
			$column_data = array(
				'1' => array(USERS_TABLE, 'user_shout'),
				'2' => array(USERS_TABLE, 'user_shoutbox'),
				'3' => array(USERS_TABLE, 'shout_bbcode'),
				'4' => array(SMILIES_TABLE, 'display_on_shout'),
			);
			$install->delete_column_mod($column_data);
			
			// Delete Tables
			$tables = array(
				'1'	=> SHOUTBOX_TABLE,
				'2'	=> SHOUTBOX_PRIV_TABLE,
			);
			$install->delete_table_mod($tables);
			
			// Remove modules
			$install->delete_modules('ACP_SHOUT', 'acp');
			
			// Remove all traces of permissions
			$install->delete_auth_option('u_shout_');
			$install->delete_auth_option('a_shout_');
			
			// Shows the results of the removal of the Mod
			$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", 'out=delete'), 2);
		}
		else if ($in_del == 2)
		{
			// Removes and deletes log mounts
			$install->delete_all_log();
			// And enter in the log the Mod deletion
			add_log('admin', 'LOG_INSTALL_MOD_DELETE', MOD_VERSION_FULL);
			
			// Shows the results of the removal of the final Mod
			$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", 'out=del'), 2);
		}
	break;
	case 'check':
		if (!$in_check)
		{
			$template->assign_vars(array(
				'S_IN_CHECK'		=> true,
				'CHECK_TITLE'		=> $user->lang['INSTALL_CHECK_TITLE'],
				'CHECK_MESSAGE'		=> $user->lang['INSTALL_CHECK_MESSAGE'],
			));
			$url_go = "mode=check&amp;check=1";
			$stats = false;
		}
		if ($in_check)
		{
			$install->delete_all_log();
			// Obsolete files from previous versions to remove
			$_filelist = array (
				'1'	=> 'ajax2.php',
				'2'	=> 'js.php',
				'3'	=> 'js2.php',
				'4'	=> 'js_priv.php',
				'5'	=> 'static.js',
				'6'	=> "styles/{$user->theme['template_path']}/template/shout_popup.html",
				'7' => 'adm/mods/breizh_shoutbox_version.php',
			);
			$install->unlink_old_files($_filelist);
			
			// Check for presence Mod files
			$all_files_exist = $install->all_files_exist();
			if ($all_files_exist)
			{
				$error .= $all_files_exist;
				add_log('admin', 'LOG_INSTALL_CHECK_NO', NAME_MOD);
			}
			else
			{
				add_log('admin', 'LOG_INSTALL_CHECK_OK', NAME_MOD);
			}
			// Displays search results
			$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "out=check"), 2);
		}
	break;
	case 'tables':
		if (!$in_tables && !$speed)
		{
			$template->assign_vars(array(
				'S_IN_CHECK'	=> true,
				'CHECK_TITLE'	=> $user->lang['INSTALL_CHECK_TABLES_TITLE'],
				'CHECK_MESSAGE'	=> sprintf($user->lang['INSTALL_CHECK_TABLES'], 'shoutbox'). '<br />' .sprintf($user->lang['INSTALL_CHECK_TABLES'], 'shoutbox_priv'). '<br />' .$user->lang['INSTALL_CHECK_CONFIG']. '<br />' .$user->lang['INSTALL_CHECK_USERS'],
			));
			$url_go = "mode=tables&amp;tab=1$go_speed";
		}
		else
		{
			if (CUR_VERSION != 10)
			{
				$install->modifie_tables($speed);
				$install->create_tables_mod($speed);
			}
			else
			{
				add_log('admin', 'LOG_INSTALL_ALTER_NO');
			}
			// Removes obsolete configuration items
			$is_on = false;
			$data_delete = array(
				'0' => 'shout_defaut_sound',
				'1'	=> 'shout_position',
				'2'	=> 'shout_priv_height',
				'3'	=> 'shout_priv_width',
				'4'	=> 'shout_birthday_hour',
				'5'	=> 'shout_hello_hour',
				'6'	=> 'shout_pos_chars',
				'7'	=> 'shout_pos_chars_pop',
				'8'	=> 'shout_pos_chars_priv',
				'9'	=> 'shout_pos_color',
				'10' => 'shout_pos_color_pop',
				'11' => 'shout_pos_color_priv',
				'12' => 'shout_pos_rules',
				'13' => 'shout_pos_rules_pop',
				'14' => 'shout_pos_rules_priv',
				'15' => 'shout_pos_smil',
				'16' => 'shout_pos_smil_pop',
				'17' => 'shout_pos_smil_priv',
			);
			foreach ($data_delete as $key => $data)
			{
				$is_on .= $install->delete_config($data);
			}
			// No changes for the current version
			if (CUR_VERSION == 10 && !$is_on && !$speed)
			{
				add_log('admin', 'LOG_INSTALL_ALTER_CONFIG_NO');
			}
			// Change the configuration depending on the version
			$install->add_shoutbox_config($speed);
			
			if (!$speed)
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "out=tables$go_speed"), 2);
			}
			else
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=modules$go_speed"), 2);
			}
		}
	break;
	case 'modules':
		if (!$in_modul && !$speed)
		{
			$template->assign_vars(array(
				'S_IN_CHECK'	=> true,
				'CHECK_TITLE'	=> sprintf($user->lang['INSTALL_CHECK_MODUL_TITLE'], 'ACP'),
				'CHECK_MESSAGE'	=> sprintf($user->lang['INSTALL_CHECK_MODULES'], 'ACP'),
			));
			$url_go = "mode=modules&amp;modul=1$go_speed";
		}
		else
		{
			$install->delete_log('LOG_INSTALL_');
			// First delete old modules
			if (CUR_VERSION != 10)
			{
				$install->delete_modules('ACP_SHOUT_%', 'acp');
			}
			
		// Create the base module
			if (!$install->module_exists('acp', 'ACP_SHOUTBOX'))
			{
				$acp_shout_parent = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> 0,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUTBOX',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shout_parent, $speed);
			}

		// Install the modules in ACP_SHOUTBOX
			$sql = 'SELECT module_id
				FROM ' . MODULES_TABLE . "
				WHERE module_langname = '" .$db->sql_escape('ACP_SHOUTBOX'). "'";
			$result = $db->sql_query_limit($sql, 1);
			$cat_parent = (int) $db->sql_fetchfield('module_id');
			$db->sql_freeresult($result);

		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_GENERAL_CAT'))
			{
				$acp_shoutbox = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_GENERAL_CAT',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shoutbox, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_VERSION'))
			{
				$acp_shout_version = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shoutbox['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_VERSION',
					'module_mode'		=> 'version',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_version, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_CONFIGS'))
			{
				$acp_shout_configs = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shoutbox['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_CONFIGS',
					'module_mode'		=> 'configs',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_configs, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_RULES'))
			{
				$acp_shout_configs = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shoutbox['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_RULES',
					'module_mode'		=> 'rules',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_configs, $speed);
			}
			
		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_PRINCIPAL_CAT'))
			{
				$acp_shout_princ = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_PRINCIPAL_CAT',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shout_princ, $speed);
			}

			if (!$install->module_exists('acp', 'ACP_SHOUT_OVERVIEW'))
			{
				$acp_shout_overview = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_princ['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_OVERVIEW',
					'module_mode'		=> 'overview',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_overview, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_CONFIG_GEN'))
			{
				$acp_shout_overview = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_princ['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_CONFIG_GEN',
					'module_mode'		=> 'config_gen',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_overview, $speed);
			}
			
		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_PRIVATE_CAT'))
			{
				$acp_shout_priv = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_PRIVATE_CAT',
					'module_mode'		=> '',
					'module_auth'		=> 'acl_a_shout_priv'
				);
				$install->update_module_install($acp_shout_priv, $speed);
			}

			if (!$install->module_exists('acp', 'ACP_SHOUT_PRIVATE'))
			{
				$acp_shout_private = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_priv['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_PRIVATE',
					'module_mode'		=> 'private',
					'module_auth'		=> 'acl_a_shout_priv'
				);
				$install->update_module_install($acp_shout_private, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_CONFIG_PRIV'))
			{
				$acp_shout_private = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_priv['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_CONFIG_PRIV',
					'module_mode'		=> 'config_priv',
					'module_auth'		=> 'acl_a_shout_priv'
				);
				$install->update_module_install($acp_shout_private, $speed);
			}
			
		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_POPUP_CAT'))
			{
				$acp_shout_pop = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_POPUP_CAT',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shout_pop, $speed);
			}

			if (!$install->module_exists('acp', 'ACP_SHOUT_POPUP'))
			{
				$acp_shout_popup = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_pop['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_POPUP',
					'module_mode'		=> 'popup',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_popup, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_PANEL'))
			{
				$acp_shout_panel = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_pop['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_PANEL',
					'module_mode'		=> 'panel',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_panel, $speed);
			}
			
		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_SMILIES_CAT'))
			{
				$acp_shout_smil = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_SMILIES_CAT',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shout_smil, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_SMILIES'))
			{
				$acp_shout_smilies = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_smil['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_SMILIES',
					'module_mode'		=> 'smilies',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shout_smilies, $speed);
			}
			
		// Create a category module
			if (!$install->module_exists('acp', 'ACP_SHOUT_ROBOT_CAT'))
			{
				$acp_shout_robot = array(
					'module_basename'	=> '',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $cat_parent,
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_ROBOT_CAT',
					'module_mode'		=> '',
					'module_auth'		=> ''
				);
				$install->update_module_install($acp_shout_robot, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_ROBOT'))
			{
				$acp_shoutbox_robot = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_robot['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_ROBOT',
					'module_mode'		=> 'robot',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shoutbox_robot, $speed);
			}
			
			if (!$install->module_exists('acp', 'ACP_SHOUT_ROBOT_MOD'))
			{
				$acp_shoutbox_robot = array(
					'module_basename'	=> 'shoutbox',
					'module_enabled'	=> 1,
					'module_display'	=> 1,
					'parent_id'			=> $acp_shout_robot['module_id'],
					'module_class'		=> 'acp',
					'module_langname'	=> 'ACP_SHOUT_ROBOT_MOD',
					'module_mode'		=> 'robot_mod',
					'module_auth'		=> 'acl_a_shout_manage'
				);
				$install->update_module_install($acp_shoutbox_robot, $speed);
			}
			
			$cache->destroy("_modules_acp");

			if (CUR_VERSION == 10 && !$speed)
			{
				add_log('admin', 'LOG_INSTALL_ALTER_MOD_NO');
			}
			if (!$speed)
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "out=modules$go_speed"), 2);
			}
			else
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=auth$go_speed"), 2);
			}
		}
	break;
	case 'auth':
		if (!$in_auth && !$speed)
		{
			$template->assign_vars(array(
				'S_IN_CHECK'	=> true,
				'CHECK_TITLE'	=> $user->lang['INSTALL_CHECK_AUTH_TITLE'],
				'CHECK_MESSAGE'	=> $user->lang['INSTALL_CHECK_AUTH'],
			));
			$url_go = 'mode=auth&amp;aut=1' .$go_speed;
		}
		else
		{
			$install->delete_log('LOG_INSTALL_');
		// Add permissions to administrators
		// Permission to manage shoutbox
			$auth_option = 'a_shout_manage';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH_ADM', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for admin standard
				$install->give_perm_a_standard($auth_option_id, 1);
				// Give permission for admin full
				$install->give_perm_a_full($auth_option_id, 1);
			}

		// Permission to manage private shoutbox
			$auth_option = 'a_shout_priv';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH_ADM', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for admin standard
				$install->give_perm_a_standard($auth_option_id, 1);
				// Give permission for admin full
				$install->give_perm_a_full($auth_option_id, 1);
			}

		// Add permissions to users
		// permission to post
			$auth_option = 'u_shout_post';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}

		// permissions to view user infos
			$auth_option = 'u_shout_info_s';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permissions to view all infos
			$auth_option = 'u_shout_info';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to delete user messages
			$auth_option = 'u_shout_delete_s';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}

		// permission to delete all messages
			$auth_option = 'u_shout_delete';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}

		// permission to edit user messages
			$auth_option = 'u_shout_edit';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}

		// permission to edit all the messages
			$auth_option = 'u_shout_edit_mod';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}

		// permission to use smilies
			$auth_option = 'u_shout_smilies';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}

		// permission to use colour
			$auth_option = 'u_shout_color';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}

		// permission to use bbcodes
			$auth_option = 'u_shout_bbcode';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}

		// permission to ignore flood
			$auth_option = 'u_shout_ignore_flood';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}

		// permission to use popup
			$auth_option = 'u_shout_popup';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for new member
				$install->give_perm_new_member($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 1);
			}

		// permission to view shoutbox
			$auth_option = 'u_shout_view';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for new member
				$install->give_perm_new_member($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 1);
			}
		
		// permission to view private shoutbox
			$auth_option = 'u_shout_priv';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to purge shoutbox
			$auth_option = 'u_shout_purge';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to ignore shoutbox's robot
			$auth_option = 'u_shout_hide';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to use chars
			$auth_option = 'u_shout_chars';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}
			
		// permission to post images
			$auth_option = 'u_shout_image';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
			}
			
		// permission to view shoutbox in lateral box
			$auth_option = 'u_shout_lateral';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for user limited
				$install->give_perm_limited($auth_option_id, 1);
				// Give permission for user nopm
				$install->give_perm_nopm($auth_option_id, 1);
				// Give permission for user noavatar
				$install->give_perm_noavatar($auth_option_id, 1);
				// Give permission for new member
				$install->give_perm_new_member($auth_option_id, 1);
			}
			
		// permission to ignore caracters post limit
			$auth_option = 'u_shout_limit_post';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to ignore inactivity
			$auth_option = 'u_shout_inactiv';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to post personnal messages
			$auth_option = 'u_shout_post_inp';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
		// permission to use formatting of messages
			$auth_option = 'u_shout_bbcode_change';
			if (!$install->permission_exists($auth_option, true))
			{
				$auth_admin->acl_add_option(array(
					'local'     => array(),
					'global'   	=> array($auth_option),
				));
				if (!$speed)
				{
					add_log('admin', 'LOG_INSTALL_AUTH', $install->lang_install_acl($auth_option));
				}
				// Select auth_option_id
				$auth_option_id = $install->select_perm($auth_option);
				// Give permission for user full
				$install->give_perm_full($auth_option_id, 1);
				// Give permission for user standard
				$install->give_perm_standard($auth_option_id, 1);
				// Give permission for anonymous
				$install->give_perm_anonymous($auth_option_id, 0);
			}
			
			$cache->purge();
			if (!$speed)
			{
				if (CUR_VERSION != 10)
				{
					add_log('admin', 'LOG_INSTALL_AUTH_ADDED', MOD_VERSION_FULL);
				}
				else
				{
					add_log('admin', 'LOG_INSTALL_ALTER_AUTH_NO');
				}
			}
			if (!$speed)
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "out=auth"), 2);
			}
			else
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=end$go_speed"), 2);
			}
		}
	break;
	case 'end':
		if (!$in_end && !$speed)
		{
			$template->assign_vars(array(
				'S_IN_CHECK'	=> true,
				'CHECK_TITLE'	=> $user->lang['INSTALL_CHECK_END_TITLE'],
				'CHECK_MESSAGE'	=> $user->lang['INSTALL_CHECK_END'],
			));
			$url_go = 'mode=end&amp;end=1'.$go_speed;
		}
		else
		{
			if (CUR_VERSION != 10)
			{
				$config_data = array(
					'shout_version' 		=> array(MOD_VERSION, 0),
					'shout_version_full' 	=> array(MOD_VERSION_FULL, 0),
				);
				$install->add_config($config_data, $speed);
				if (CUR_VERSION == 1)
				{
					add_log('admin', 'LOG_INSTALL_MOD_ADD', MOD_VERSION_FULL);
				}
				else
				{
					add_log('admin', 'LOG_INSTALL_MOD_UPDATE', MOD_VERSION_FULL);
				}
			}
			else
			{
				add_log('admin', 'LOG_INSTALL_ALTER_END_NO', NAME_MOD);
			}
			if (!$speed)
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "out=end$go_speed"), 2);
			}
			else
			{
				$install->redirect(append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=finish$go_speed"), 2);
			}
		}
	break;
	case 'finish':
		$install->delete_log('LOG_INSTALL_');
		$template->assign_vars(array(
			'S_IN_CHECK'	=> true,
			'S_IN_FINISH'	=> true,
			'CHECK_TITLE'	=> sprintf($user->lang['INSTALL_CHECK_FINISH_TITLE'], NAME_MOD),
			'CHECK_MESSAGE'	=> sprintf($user->lang['INSTALL_CHECK_FINISH'], MOD_VERSION_FULL),
			'FINISH_MESSAGE'=> $user->lang['INSTALL_FINISH_MESSAGE'],
		));
	break;
}

// Redirects after informations
switch ($out_mode)
{
	case 'delete':
		$url_go = 'mode=delete&amp;del=2'.$go_speed;
	break;
	case 'del':
		$url_go = '';
	break;
	case 'check':
		$template->assign_var('RUN_FAST', true);
		$error .= $install->all_files_exist();
		$url_go = "mode=tables";
	break;
	case 'tables':
		$url_go = "mode=modules$go_speed";
	break;
	case 'modules':
		$url_go = "mode=auth$go_speed";
	break;
	case 'auth':
		$url_go = "mode=end$go_speed";
	break;
	case 'end':
		$url_go = "mode=finish$go_speed";
	break;
}

$active = false;
if (!$out_mode && !$mode)
{
	$active = true;
}
if ($active && CUR_VERSION != 1)
{
	$url_go = 'mode=first';
}
// the url of the next phase of installation
$url_go_on = ($error) ? append_sid("{$phpbb_root_path}install/index.$phpEx", "mode=check$go_speed") : (($mode == 'finish') ? append_sid("{$phpbb_root_path}index.$phpEx") : append_sid("{$phpbb_root_path}install/index.$phpEx", $url_go));

// Now we displays everything!
$template->assign_vars(array(
	'S_IN_INSTALL'			=> true, // Enable install part in the html file
	'ERROR'					=> $error, // Displays errors
	'MESSAGE_TEXT'			=> ($active) ? $message_first : '', // Displays the start of install
	'MESSAGE_LOG'			=> ($out_mode) ? $install->get_log($out_mode) : '', // Displays the log after each operation if needed
	'MESSAGE_ETAT'			=> ($stats) ?  $install->stat_install() : '', // Displays the progress of changes if needed
	'INSTALL_WELCOME_TEXT'	=> sprintf($user->lang['INSTALL_WELCOME_TEXT'], NAME_MOD), // install title for the current Mod
	'INSTALL_UPDATE'		=> (CUR_VERSION == 1) ? $user->lang['INSTALL_GO_ON'] : ((CUR_VERSION > 1) ? $user->lang['INSTALL_UPDATE'] : ''), // Install or update?
	'S_IN_MODE_DEL'			=> ($out_mode == 'delete') ? true : false, // In progress of removal?
	'S_IN_MODE'				=> ($out_mode || $mode) ? true : false, // If not, go go!
	'SUBMIT_BUTTON'			=> ($mode == 'license') ? $user->lang['INSTALL_ACCEPT'] : (($error) ? $user->lang['INSTALL_RELOAD'] : (($out_mode) ? $user->lang['INSTALL_CONTINUE'] : (($mode == 'finish') ? $user->lang['INSTALL_GO_INDEX'] : $user->lang['INSTALL_ACCEPT']))),
	'INFO_TEXT'				=> sprintf($user->lang['INSTALL_INFO_MESSAGE'], (($styles_right) ? $user->lang['INSTALL_INFO_IDENTIQUE'] : $user->lang['INSTALL_INFO_DIFFERENT'])), // Well say what you can...
	'INSTALL_INFO_STYLE'	=> $message_style, // Rapport for the style
	'ERROR_THEME'			=> ($error_theme) ? $message_error_theme : '', // error in the theme? gloups!
	'INSTALL_BUTTON'		=> ($active && CUR_VERSION != 10) ? true : false, // can install it?
	'DELETE_BUTTON'			=> ($active && CUR_VERSION != 1) ? true : false, // can uninstall it?
	'S_LANG_FR'				=> ($user->lang_name == 'fr') ? true : false, // special for French :)
	'INSTALL_ETAT_INFO'		=> sprintf($user->lang['INSTALL_ETAT_INFO'], $img_no, $img_yes),
	'S_CONFIRM_ACTION'		=> $url_go_on,
	'MESSAGE_TITLE'			=> sprintf($user->lang['INSTALL_WELCOME_TITLE'], '<img src="./images/shout.png" alt="' .$user->lang['ACP_SHOUTBOX']. '" title="' .$user->lang['ACP_SHOUTBOX']. '" />', MOD_VERSION_FULL),
	'INSTALL_COPYRIGHT'		=> $user->lang['INSTALL_INSTALLER']. '<br />' .sprintf($user->lang['SHOUTBOX_VERSION_ACP_COPY'], MOD_VERSION),
));

// Output page
page_header(sprintf($user->lang['INSTALL_PAGE_TITLE'], MOD_VERSION_FULL). ' - ' .$user->lang['INSTALL_INSTALLER']. ' - ' .(($mode) ? $mode : (($out_mode) ? $out_mode : 'index')));

$template->set_filenames(array(
	'body' => 'breizh_installer.html'
));

page_footer();

?>