**************************************************************************
** PHPBB3                                                               **
** Module for 123 Flash Chat Server software                            **
** ==============================================                       **
**                                                                      **
** Copyright (c) by TopCMM                                              **
** Daniel Jiang (support@123flashchat.com)                              **
** http://www.topcmm.com                                                **
** http://www.123flashchat.com                                          **
** http://www.phpbb.com/	                                        **
**                                                                      **
** Version : 123FlashChat for PHPBB3 Module 3.0                         **
** Author : TOPCMM SOFTWARE CORP.                                       **
** Contributors : 123FlashChat.com                                      **
** Requires at least : 3.0.X                                           **
** Tested up to : 3.0.10                                                **
** Donate link : www.123flashchat.com                                   **
** Tags : 123 Flash Chat, Chat Software, Flash Chat Hosting,            **
**        Video Webcam Audio, Chat Server, Live Help, IM,               **
**                                                                      **
**************************************************************************


############################################################################################################
## Notes:                                                                                                 ##
##                                                                                                        ##
##     IMPORTANT!!!!                                                                                      ##
##     This module need the 123FlashChat Server Service running.                                          ##
##     And we provide 3 options for the chat server:                                                      ##
##     1. Chat server hosted by your own server.                                                          ##
##     2. Chat server hosted by 123flashchat.com(paid or 15 days free trial)                              ##
##     3. Chat server hosted by 123flashchat.com for free demo(by default)                                ##
##                                                                                                        ##
##     If you need to run the chat service on your own server, before installing this module, please      ##
##     download 123FlashChat Server software first from this page:                                        ##
##     http://www.123flashchat.com/download.html                                                          ##
##     You may get the latest free demo version of 123 flash chat server software at there.               ##
##                                                                                                        ##
##     Or you can purchase 123flashchat hosting service here:                                             ##
##     http://www.123flashchat.com/host.html                                                              ##
##     To apply a free trial host service:                                                                ##
##     http://www.123flashchat.com/host/apply.php                                                         ##
##                                                                                                        ##
##     And get professional support from: http://www.123flashchat.com/support.html                        ##
##     We provide email support, online live support, and phone support                                   ##
##     etc.                                                                                               ##
##                                                                                                        ##
############################################################################################################


== Simple Installation ==
Step 1. Upload all the files in the folder "upload" to your phpbb root directory.

Step 2.	1). If you use the default config ( Chat server mode is 3) install , just go to Step 3.

	2). If you want to install Chat server on your own server , you need to download 123flashchat software at http://www.123flashchat.com/download.html and install in your server.
		
		Edit <root path>/123flashchat_config.php
  
		  a) Edit the value of $fchatconfig['fc_server'] set to 1;

		  b).If your chat server and website server are located in the same server, please set "$fchatconfig['fc_server_host']" to "$user->host", otherwise please set it with your chat server hostname.
                 
                  c) Go to setp 3.
				
	3). If you do not want to install the chat server by yourself, 
            you can also buy a host here http://www.123flashchat.com/host.html, 
            or apply a free host trial:  http://www.123flashchat.com/host/apply.php
            Once you get host info , you can edit <root path>/123flashchat_config.php 
	
		Edit <root path>/123flashchat_config.php  

		 a).Edit the value of $fchatconfig['fc_server'] set to 2;

		 b).Please set $fchatconfig['fc_client_loc'] with your hosted url, 
                    for example: http://host71200.123flashchat.com/phpchat/ (paid version) 
                             or: http://trial.123flashchat.com/phpchat/ (trial version)

                 c) Go to step 3
				
	4). For those who chooses running chat on [host mode](Chat server mode 2) or [localhost mode](Chat server mode 1), database integration is necessary,
             
            please follow the instructions below.
	
		a). Visit chat server Admin Panel , Log in the Admin Panel of your 123FlashChat server using chat admin account.
				OPEN: Server Settings-> Integration
		b). For DataBase, SELECT: URL, and click "edit"
		
        	c). Change URL to:
				http://<phpbb install directory>/login_chat.php?username=%username%&password=%password%
		d). Press OK to save the setting.	
		
                e). OPEN: Server Management -> Restart, to restart server.		
 
Step 3. add link url to your website:

	1). Edit file <root path>/includes/functions.php
		Find code :

			'U_FEED'		=> generate_board_url() . "/feed.$phpEx",

		After Add :

			'U_BASE_URL'		=> append_sid("{$phpbb_root_path}"),



	2). Edit file <root path>/styles/<your theme>/template/overall_header.html
		(1). If your theme is prosilver
			Find code :

				<ul class="linklist rightside">

			After add :

				<li class="icon-chat"><a href="{U_BASE_URL}123flashchat.php" target=_"blank">Chat</a>
				<style>
					.icon-chat {
						background: url("{U_BASE_URL}images/icon_fchat.gif") 0 50% no-repeat;
						padding: 1px 0 0 17px;
					}
				</style>
				</li>



		(2). If your theme is sublilver2
			Find code:

				<td class="genmed" align="{S_CONTENT_FLOW_END}">

			After Add :
				<a href="{U_BASE_URL}123flashchat.php" target="_blank"><img src="{U_BASE_URL}images/icon_mini_fchat.gif" width="12" height="13" /> Chat</a>&nbsp; &nbsp;

Step 4 : clear website cache .
	Login Administration Control Panel use your admin account,
	STYLES -> Templates -> Refresh (your active templates)

== Automod  Installation ==
	If you want to achieve more functions, you can use automod. Please read the readme.txt in the automod folder to finish the installation.
