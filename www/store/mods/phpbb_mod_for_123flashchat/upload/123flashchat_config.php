<?php
global	$fchatconfig;

//Chat server mode, 3 options:  1, 2, 3
//  1: Chat Server Host By Own, you should download and install 123FlashChat in your server: http://www.123flashchat.com/download.html
//	2: Chat Server Host By 123 Flash Chat, you should purchase a chat hosting here: http://www.123flashchat.com/host.html , or apply a free host trial here: http://www.123flashchat.com/host/apply.php
//	3: Chat Server Host By 123 Flash Chat For Free, for testing only, only 1 room supported
$fchatconfig['fc_server'] 			= 3;

//if choose Server mode 1, you can iggnore 2 and 3
if($fchatconfig['fc_server'] == 1){

	//you can visit http://www.123flashchat.com/download.html to download the latest 123flashchat software,
	//the domian of chat server, It must be required , if your chat server and website are the same, you can use $user->host
	$fchatconfig['fc_server_host']		= $user->host;	
			
	//unless you know what you are doing, do not change the value of bellow
	
	//could be 51127,21127, It must be required 
	$fchatconfig['fc_server_port']		= 51127;				
	//default 35555, It must be required 
	$fchatconfig['fc_http_port']		= 35555;	
	// Chat client Location, It must be required 
	$fchatconfig['fc_client_loc']		= 'http://'.$fchatconfig['fc_server_host'].':35555/';					
	//Chat api url, It must be required 
	$fchatconfig['fc_api_url']			= 'http://'.$fchatconfig['fc_server_host'].':35555/';	

}
//if choose Server mode 2
if($fchatconfig['fc_server'] == 2){

	// you can visit http://www.123flashchat.com/host.html  to buy a  host.
	//example: if your "fc_client_loc" is "http://host123.123flashchat.com/myhost/", "fc_group" should be "myhost"	
	// Chat client Location, It must be required 
	$fchatconfig['fc_client_loc']		= 'http://host71200.123flashchat.com/phpchat/';
	
	//unless you know what you are doing, do not change the value of bellow
	
	// the domian of chat server, It must be required 
	$fchatconfig['fc_server_host']		= @parse_url($fchatconfig['fc_client_loc'], PHP_URL_HOST);
	//Chat api url, It must be required 
	$fchatconfig['fc_api_url']			= 'http://' . $fchatconfig['fc_server_host'] . '/';
		
	$fchatconfig['fc_group']			= substr(@parse_url($fchatconfig['fc_client_loc'], PHP_URL_PATH),1,-1);
	
	if($fchatconfig['fc_server_host'] == 'trial.123flashchat.com'){
		// trial host port could only be 21125, It must be required
		$fchatconfig['fc_server_port']		= 21125;
	}else{
		// could be 51127,21127, It must be required 
		$fchatconfig['fc_server_port']		= 21127;	
	}
							
	// default 35555, It must be required 
	$fchatconfig['fc_http_port']		= 35555;		
}

//if choose Server mode 3
if($fchatconfig['fc_server'] == 3){
	//For server mode "3" : room name
	$fchatconfig['fc_room']				= 'lobby';	
	
	//For server mode "3" :	Chat language 
	//"en" =>English,"zh-CN" =>GB Chinese,"zh-TW" =>Big5 Chinese,"fr" =>French,"it" =>Italian,"de" =>German,"nl" =>Dutch,"hu" =>Hungarian,"es" =>Spanish,
	//"hr" =>Croatian,"tr" =>Turkish,"ar" =>Arabic,"pt" =>Portuguese,"ru" =>Russian,"ko" =>Korean,"serbian" =>Serbian,"no" =>Norwegian,"ja" =>Japanese
	$fchatconfig['fc_client_lang']		= '*';	
											
	//For server mode "3" : Chat skin
	//"default" => Default, "green" => Green, "orange" => Orange,"red" => Red, "black" => Black, "beige" => Beige, "standard" => Standard, "clean" => Clean, "artistic" => Artistic
	$fchatconfig['fc_client_skin']		= 'default';	

}

//show userlist	
$fchatconfig['fc_user_list']		= 1;
		
//show room list
$fchatconfig['fc_room_list']		= 1;
		
//end for config.php
