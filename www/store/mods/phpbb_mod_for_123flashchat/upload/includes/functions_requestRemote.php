<?php
/**
 *
 * @author TopCMM daniel@topcmm.com
 * @copyright (c) 2010 TopCMM
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

$timeout = 3;


class topcmmRequestRemote
{
	function checkConfig()
	{
			return $this->checkCurl() || $this->checkAllowUrlFopen();
	}
	
	function checkCurl()
	{
		return function_exists("curl_init");
	}
	
	function checkAllowUrlFopen()
	{
		return ini_get("allow_url_fopen");
	}
		
    function requestRemote($url,$mode=1)
    {
        if(!empty($url))
        {
            if(self::checkCurl())
            {   	
                return self::curlRequestRemote($url,$mode);
            }
            else if($this->checkAllowUrlFopen())
            {
                if($mode == "1")
                {
                    return $this->filegetContentsRequestRemote($url);
                }
                else
                {
                    return $this->fsockopenRequestRemote($url);
                }

            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function curlRequestRemote($url,$mode)
    { 		
    		global $timeout;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
        curl_setopt($ch, CURLOPT_REFERER,_REFERER_);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($mode == "0")
        {
 
            curl_setopt($ch, CURLOPT_HEADER, true);
        }

        $r = curl_exec($ch);

        curl_close($ch);
        if($mode == "0")
        {
            return substr($r,0,17);
        }
        return $r;
    }

    function filegetContentsRequestRemote($url)
    {
    	 	if(PHP_VERSION > 5)
    	 	{
    			global $timeout;
        	$context = stream_context_create(array(
                'http' => array(
                        'timeout' => $timeout      // Timeout in seconds
                )
        	));
        	return @file_get_contents($url,0,$context);
        }
        else
        {
        	return @file_get_contents($url);
        }
    }

    function fsockopenRequestRemote($url)
    {
    		global $timeout;
        $url = @parse_url($url);
        if($fp = @fsockopen($url['host'],empty($url['port'])?80:$url['port'],$errorno,$error,$timeout))
        {

            fputs($fp,"GET ".(empty($url['path'])?'/':$url['path'])." HTTP/1.1\r\n");
            fputs($fp,"Host:$url[host]\r\n\r\n");
            while(!feof($fp))
            {
                $tmp = fgets($fp);

                if(trim($tmp) == '')
                {
                    break;
                }
                else if(preg_match_all('|HTTP(.*)|U',$tmp,$arr))
                {
                    return $tmp;

                }
            }
            return false;
        }
        else
        {
            return false;
        }
   }
}
?>