<?php
/**
 *
 * @author TopCMM daniel@topcmm.com
 * @copyright (c) 2010 TopCMM
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

include($phpbb_root_path . 'includes/functions_requestRemote.' . $phpEx);
define ("CACHE_PATH",$phpbb_root_path.'cache/');
define ("CHAT_PLUGIN_URL", append_sid($phpbb_root_path."123flashchat"));
if (!function_exists('json_encode')) 
{
	include($phpbb_root_path . 'includes/functions_json.' . $phpEx);
	function json_encode($arg)
	{
		global $services_json;
		if (!isset($services_json)) 
		{
			$services_json = new Services_JSON();
		}
		return $services_json->encode($arg);
	}

	function json_decode($arg)
	{
		global $services_json;
		if (!isset($services_json)) 
		{
			$services_json = new Services_JSON();
		}
		$decodeObjects = $services_json->decode($arg);
		
		if(!empty($decodeObjects))
		{
			if(is_array($decodeObjects))
			{
				$decodeArray = array();
				foreach($decodeObjects as $objkey => $decodeObject)
				{
					if(is_object($decodeObject))
					{
						$decodeArray[$objkey] = array();
						foreach($decodeObject as $key => $val)
						{
							$decodeArray[$objkey][$key] = $val;
						}
					}
				}
			}
			else
			{
				$decodeArray = array();
				foreach($decodeObjects as $key => $val)
				{
					$decodeArray[$key] = $val;
				}
			}
		}
		return !empty($decodeArray) ? $decodeArray : false;
	}
}

function fc_get_chat_url()
{
	global $fchatconfig;
	$server = $fchatconfig['fc_server'];
	$client_name = '123flashchat.swf';
	$chat_url = $fchatconfig['fc_client_loc'] . $client_name . "?init_host=" . $fchatconfig['fc_server_host'] . "&init_port=" . $fchatconfig['fc_server_port'];
	if ($server == 3)
	{
		$chat_url .= "&init_skin=" . $fchatconfig['fc_client_skin'] . "&init_lang=" . $fchatconfig['fc_client_lang'];
	}
	if ($server == 2)
	{
		$chat_url .= "&init_group=" . $fchatconfig['fc_group'];
	}
	return $chat_url;
}

function serviceGetChatStats()
{
	global $user, $fchatconfig;
	
    $sOutputCode = '';
    $c_full = $fchatconfig["fc_fullscreen"];
    if($c_full)
    {
        $fc_client_width = "screen.width";
        $fc_client_height = "screen.height";
    }else
    {
        $fc_client_width = $fchatconfig['fc_client_width'];
        $fc_client_height = $fchatconfig['fc_client_height'];
    }
    $status = getStatus($fchatconfig);
    if ($fchatconfig['fc_user_list'])
    {
        $users  = getUsers($fchatconfig);
    }
    if (($fchatconfig['fc_room_list']) && ($fchatconfig['fc_server'] != "3"))
    {
        $rooms  = getRooms($fchatconfig);
    }

    if (isset($status['ln'])){
        $sOutputCode .= '<b>' . $status['ln'] . '</b> login users.';
        if (isset($status['rn'])){
            $sOutputCode = '<b>' . $status['rn'] . '</b> rooms, ' . '<b>' . $status['cn'] . '</b> connections, ' . $sOutputCode;
        }
    }
    if(isset($rooms)){
        $sOutputCode .= "<b><p>Rooms:</b> ";
        if(!$rooms){
            $sOutputCode .= "None";
        } else{
            $xa = '';
            foreach($rooms as $room){                
				$chaturl = CHAT_PLUGIN_URL.'/123flashchat.php?room=' . $room['id'];
				$js = 'onclick="window.open(\''.$chaturl.'\');return false;"';
				$sOutputCode .= $xa . '<a target="_block" href="http://wwww.123flashchat.com" ' .  $js . ">" .$room['name'] ."</a>(" . $room['count'] . ")";
                $xa = ", ";
            }
        }

    }
    if(isset($users))
    {
        $sOutputCode .= "</p><p><b>Users:</b> ";
        if(!$users)
        {
            $sOutputCode .= "None</p>";
        }
        else
        {
            $xb = '';
            foreach($users as $userinfo)
            {
                $sOutputCode .= $xb . $userinfo['name'];
                $xb = ', ';
            }
			//exit;
            $sOutputCode .= "</p>";
        }

    }
	$thishref = CHAT_PLUGIN_URL.'/123flashchat.php?room=';
    $js = ' onclick="window.open(\''.$thishref.'\');return false;"';
	$sOutputCode .=  '<p><a href="http://www.123flashchat.com"' .$js.">" ."<b>Chat Now!</b>" ."</a></p>";
    $sOutputCode = '<div class="dbContentHtml">' . $sOutputCode . '</div>';
    return $sOutputCode;

}

function getStatus($fchatconfig)
{
    $data = topcmmRequestRemote::requestRemote(CACHE_PATH . "status.js",1);
    $status_json = substr($data,10);
    if ((time() > substr($data,0,10)) || !$status_json)
    {
        $server =  $fchatconfig['fc_server'];
        switch ($server)
        {
            case "1":
                $status_js = $fchatconfig['fc_api_url'] . "online.js";
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    $status_json = substr($rs,11,-1);
                }
                break;
            case "2":
                $status_js = $fchatconfig['fc_api_url'] . "online.js?group=" . $fchatconfig['fc_group'];
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    $status_json = substr($rs,11,-1);
                }
                break;
            case "3":
                $status_js = "http://free.123flashchat.com/freeroomnum.php?roomname=" . $fchatconfig['fc_room'];
                if($rs = topcmmRequestRemote::requestRemote($status_js,1))
                {
                    preg_match("/document.write\('(.*)'\);/",$rs,$matches);
                    $status['ln'] = $matches[1];
                    $status_json = json_encode($status);
                }
                break;
        }
       // @file_put_contents(CACHE_PATH . "status.js",(time() + 120) . $status_json);
    }
    return json_decode($status_json,true);
}

function getRooms($fchatconfig)
{
    $data = topcmmRequestRemote::requestRemote(CACHE_PATH ."rooms.js", 1);
    $rooms_json = substr($data,10);
    //var_dump($data);
    if ((time() > substr($data,0,10)) || !$rooms_json)
    {
        $server =  $fchatconfig['fc_server'];
     
	  switch ($server)
        {
            case "1":
                $room_js = $fchatconfig['fc_api_url'] . "rooms.js";
                break;
            case "2":
                $room_js = $fchatconfig['fc_api_url'] . "rooms.js?group=" . $fchatconfig['fc_group'];
                break;
        }
        if($rs = topcmmRequestRemote::requestRemote($room_js,1))
        {
            $rooms_json = substr($rs,10,-1);
        }
       // @file_put_contents(CACHE_PATH ."rooms.js",(time() + 120) . $rooms_json);
    }
    return json_decode($rooms_json, true);
}
/**
 * Function will get users;
 */
function getUsers($fchatconfig)
{
    $context = stream_context_create(array(
            'http' => array(
                    'timeout' => 3      // Timeout in seconds
            )
    ));
    $data =  topcmmRequestRemote::requestRemote(CACHE_PATH ."users.js",1);
    $users_json = substr($data,10);
    if ((time() > substr($data,0,10)) || !$users_json)
    {
        $server =  $fchatconfig['fc_server'];
        switch ($server)
        {
            case "1":
                $rooms = getRooms($fchatconfig);
                $users = array();
                foreach ($rooms as $room)
                {
                    $user_js = $fchatconfig['fc_api_url'] . "roomonlineusers.js?roomid=" . $room['id'];
					if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                    {
                        $users = array_merge($users, json_decode(substr($rs,20,-1),true));
                    }
                }
                $users_json = json_encode($users);
			break;
            case "2":
                $user_js = $fchatconfig['fc_api_url'] . "roomonlineusers.js?group=" . $fchatconfig['fc_group'];
                if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                {
                    $users_json = substr($rs,20,-1);
                }
            break;
            case "3":
                $user_js = "http://free.123flashchat.com/freeroomuser.php?roomname=" . $fchatconfig['fc_room'];
                if($rs = topcmmRequestRemote::requestRemote($user_js,1))
                {
                    preg_match("/document.write\('(.*)'\);/",$rs,$matches);
                    foreach (explode(',', $matches[1]) as $userinfo)
                    {
                        if($userinfo != 'None'){
							$users[] = array('name' => $userinfo);
						}
                    }
                }
                $users_json = json_encode($users);
            break;
        }
       // @file_put_contents(CACHE_PATH ."users.js",(time() + 120) . $users_json);
    }
    return json_decode($users_json, true);
}

?>
