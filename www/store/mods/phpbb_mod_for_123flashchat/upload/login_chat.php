<?php
/**
 *
 * @author TopCMM daniel@topcmm.com
 * @copyright (c) 2010 TopCMM
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */
define('IN_PHPBB', true);
$phpbb_root_path = isset($phpbb_root_path) ? $phpbb_root_path : ((defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : dirname(__FILE__).'/');
$phpEx = isset($phpEx) ? $phpEx : (substr(strrchr(__FILE__, '.'), 1));
include($phpbb_root_path . 'common.' . $phpEx);
$user->session_begin();
$auth->acl($user->data);
$user->setup();
require_once( '123flashchat_config.php');

// 123 Flash Chat Login Return Value
define('FC_LOGIN_SUCCESS',	0);
define('FC_LOGIN_ERROR_PASSWD', 1);
define('FC_LOGIN_ERROR', 3);
define('FC_LOGIN_ERROR_NOUSERID', 4);
define('FC_LOGIN_SUCCESS_ADMIN', 5);

if (($username = request_var('username', '', true)) && ($password = request_var('password', '', true)))
{
	global $db;
	$sql = 'SELECT  *
			  FROM ' . USERS_TABLE . "
			 WHERE username_clean = '" . $db->sql_escape(utf8_clean_string($username)) . "'";
	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);
	$db->sql_freeresult($result);
	
 	$url = generate_board_url();
 	$prourl = "";
 	if(!empty($row["user_birthday"])){
		$userbirthdayarr = explode('-',$row["user_birthday"]);
		$userbirthday_day = str_replace(' ','0',$userbirthdayarr[0]);
		$userbirthday_month = str_replace(' ','0',$userbirthdayarr[1]);
		$userbirthday_year = $userbirthdayarr[2];
		$prourl .= '|a='.$userbirthday_year.$userbirthday_month.$userbirthday_day;
 	}
 	if(!empty($prourl)){
 		if(!empty($row["user_from"])){
 			$prourl .= '&l='.$row["user_from"];
 		}
 	}else{
		if(!empty($row["user_from"])){
 			$prourl .= '|l='.$row["user_from"];
 		}
 	}
 	if(!empty($prourl)){
 		if(!empty($row["user_email"])){
			$prourl .= '&eml='.$row["user_email"];
 		}
 	}else{
		if(!empty($row["user_email"])){
			$prourl .= '|eml='.$row["user_email"];
 		}
 	}
 	if(!empty($prourl)){
 		if(!empty($row["user_avatar"])){
		 	$prourl .= '&avt='.$url.'/download/file.php?avatar='.$row["user_avatar"];
 		}
 	}else{
		if(!empty($row["user_avatar"])){
		 	$prourl .= '|avt='.$url.'/download/file.php?avatar='.$row["user_avatar"];
 		}
 	}
 	$profile = $prourl;
	if (!empty($row['user_id']))
	{
		if (phpbb_check_hash($password ,$row['user_password']) || $password == $row['user_password'])
		{
			if ($row['user_type'] == USER_FOUNDER)
			{
				//echo FC_LOGIN_SUCCESS_ADMIN;
				echo FC_LOGIN_SUCCESS_ADMIN . $profile;
			}
			else
			{
				//echo FC_LOGIN_SUCCESS;
				echo FC_LOGIN_SUCCESS.$profile;
			}
		}
		else
		{
			echo FC_LOGIN_ERROR_PASSWD;
		}
	}
	else
	{
		echo FC_LOGIN_ERROR_NOUSERID;
	}
}
else
{
	echo FC_LOGIN_ERROR;
}

exit_handler();
