<?php
class acp_aukz
{
	// Функция, вызываемая ядром phpBB 3
	function main($id, $mode)
	{
		global $template, $phpbb_root_path, $phpEx, $user;
		// Параметр $mode - это имя подраздела, который запрашивает пользователь.
		switch ($mode)
		{
			case 'settings':
				$this->settings($id,$mode);
				$this->aukz($id,$mode,0);
				break;
			/*
			case 'service':
				$this->service($id,$mode);
				$this->aukz($id,$mode,1);
				break;
			*/
		}
	}
	function settings($id, $mode)
	{
		global $config, $db, $user, $auth, $template, $phpbb_root_path, $phpbb_admin_path, $phpEx, $table_prefix;

		$submit = (isset($_POST['submit'])) ? true : false;

		$user->add_lang(array('aukz'));

		$this->tpl_name = 'acp_aukz';
		$this->page_title = $user->lang['AUKZ'];

		define('AUKZ_CONFIG_TABLE', $table_prefix.'aukz_config');
		define('AUKZ_MAIN_TABLE', $table_prefix.'aukz_main');

		$template->assign_vars(array(
			'PAGE0'				=> true,
			'PAGE1'				=> false,
			'PAGE2'				=> false,
			'PAGE3'				=> false,
			'L_TITLE'			=> $user->lang['AUKZ'],
			'L_TITLE_EXPLAIN'	=> '<b>Настройки</b> аукциона',
		));
	}
	/*
	function service($id, $mode)
	{
		global $config, $db, $user, $auth, $template, $phpbb_root_path, $phpbb_admin_path, $phpEx, $table_prefix;

		$submit = (isset($_POST['submit'])) ? true : false;

		$user->add_lang(array('aukz'));

		$this->tpl_name = 'acp_aukz';
		$this->page_title = $user->lang['AUKZ'];

		define('AUKZ_CONFIG_TABLE', $table_prefix.'aukz_config');
		define('AUKZ_MAIN_TABLE', $table_prefix.'aukz_main');

		$template->assign_vars(array(
			'PAGE0'				=> false,
			'PAGE1'				=> true,
			'PAGE2'				=> false,
			'PAGE3'				=> false,
			'L_TITLE'			=> $user->lang['AUKZ'],
			'L_TITLE_EXPLAIN'	=> '<b>Обслуживание</b> аукциона',
		));
	}
	*/
	function aukz($id,$mode,$p)
	{
		global $config, $db, $user, $auth, $template, $phpbb_root_path, $phpbb_admin_path, $phpEx, $table_prefix;

		define('AUKZ_CONFIG_TABLE', $table_prefix.'aukz_config');
		define('AUKZ_MAIN_TABLE', $table_prefix.'aukz_main');

		$points_name = $config['money_name'];
		$forum_points_name = $points_name;
		$user_id = $user->data['user_id'];

		if( $p == 0 ) // первая страница (настройки)
		{
			// заносим настройки в базу
			if ( request_var('save_conf_aukz','') != '' )
			{
				$conf_pause = (request_var('a_aukz_pause','') != '') ? 1 : 0;
				$conf_money_name = utf8_normalize_nfc(request_var('a_money_name','',true));
				if ( $conf_money_name == '' ) $conf_money_name = $points_name;
				$conf_users = request_var('a_users','');
				$conf_type_plata = request_var('a_plata',1);
				$conf_plata_nominal = request_var('a_plata1',1);
				$conf_plata_procent = request_var('a_plata2',3);
				$conf_max_srok = request_var('a_srok',240);
				$conf_type_price = request_var('a_shag',0);
				$conf_price_1 = request_var('a_shag1',1);
				$conf_price_1p = request_var('a_shag0',5);
				$conf_price_2 = request_var('a_pay_leaders',0);
				$conf_antis = (request_var('a_antis','') != '') ? 1 : 0;
				$conf_antist = request_var('a_antist',10);
				$conf_history_st = (request_var('a_hist','') != '') ? 1 : 0;
				$conf_history_depth = request_var('a_hist_depth',30);
				$conf_forum_id = request_var('a_forum_id',0);
				$conf_forumd_id = request_var('a_forumd_id',0);
				$conf_adm_id = request_var('a_adm_id',0);
				$conf_double_id = request_var('a_double_id',0);
				$conf_user_go = (request_var('a_user_go','') != '') ? 1 : 0;
				$conf_user_edit = (request_var('a_user_edit','') != '') ? 1 : 0;
				$conf_user_delete = (request_var('a_user_delete','') != '') ? 1 : 0;

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_pause'
					WHERE config_name = 'pause'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_money_name'
					WHERE config_name = 'money_name'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_users'
					WHERE config_name = 'users_can'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_plata_nominal'
					WHERE config_name = 'plata_nominal'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_plata_procent'
					WHERE config_name = 'plata_procent'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_max_srok'
					WHERE config_name = 'max_srok'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_price_1'
					WHERE config_name = 'price_1'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_price_1p'
					WHERE config_name = 'price_1p'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_price_2'
					WHERE config_name = 'price_2'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_antis'
					WHERE config_name = 'antis'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_antist'
					WHERE config_name = 'antist'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_history_st'
					WHERE config_name = 'history_st'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_history_depth'
					WHERE config_name = 'history_depth'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_type_plata'
				WHERE config_name = 'type_plata'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_type_price'
				WHERE config_name = 'type_price'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_forum_id'
				WHERE config_name = 'forum_id'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_forumd_id'
				WHERE config_name = 'forumd_id'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_adm_id'
				WHERE config_name = 'adm_id'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_double_id'
				WHERE config_name = 'double_id'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_user_go'
				WHERE config_name = 'user_go'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_user_edit'
				WHERE config_name = 'user_edit'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				$sql = "UPDATE " . AUKZ_CONFIG_TABLE . "
					SET config_value = '$conf_user_delete'
				WHERE config_name = 'user_delete'";
				if( !$db->sql_query($sql) ){message_die(GENERAL_ERROR, "Failed to update general configuration for $config_name", "", __LINE__, __FILE__, $sql);}

				add_log('admin',$user->lang['AZ_CONF_CHANGED']);

			}

			// обработка утилит
			// удаление истории
			/*
			if ( (request_var('del_hist1','') != '') AND (request_var('del_hist1','') != '') )
			{
				$sql = "UPDATE " . AUKZ_MAIN_TABLE . "
					SET history_st = ''
					WHERE history_st <> ''";
				$db->sql_query($sql);
			}
			*/
			// удаление завершенных лотов
			/*
			if ( (request_var('del_cli1','') != '') AND (request_var('del_cli2','') != '') )
			{
				$sql = "DELETE FROM " . AUKZ_MAIN_TABLE . "
					WHERE closed > 0";
				$db->sql_query($sql);
			}
            */

			// выводим страницу настроек
			if (!isset ($HTTP_POST_VARS['save_conf_admin']))
			{
				$sql="SELECT * FROM " . AUKZ_CONFIG_TABLE;
				if ( !($result = $db->sql_query($sql)) ) message_die(GENERAL_ERROR, $user->lang['AZ_OBLOMS']."114". $user->lang['AZ_OBLOMS_PLUS'], '', __LINE__, __FILE__, $sql);

				while( $row = $db->sql_fetchrow($result) )
				{
					$aukz_conf[$row['config_name']] = $row['config_value'];
				}
				$db-> sql_freeresult($result);

				$conf_version = $aukz_conf['aukz_ver'];
				$conf_money_name = $aukz_conf['money_name'];
				if ( $conf_money_name != '' ) $points_name = $conf_money_name;
				$conf_pause = $aukz_conf['pause'];
				$conf_users = $aukz_conf['users_can'];
				$conf_type_plata = $aukz_conf['type_plata'];
				$conf_plata_nominal = $aukz_conf['plata_nominal'];
				$conf_plata_procent = $aukz_conf['plata_procent'];
				$conf_max_srok = $aukz_conf['max_srok'];
				$conf_type_price = $aukz_conf['type_price'];
				$conf_price_1 = $aukz_conf['price_1'];
				$conf_price_1p = $aukz_conf['price_1p'];
				$conf_price_2 = $aukz_conf['price_2'];
				$conf_antis = $aukz_conf['antis'];
				$conf_antist = $aukz_conf['antist'];
				$conf_history_st = $aukz_conf['history_st'];
				$conf_history_depth = $aukz_conf['history_depth'];
				$conf_forum_id = $aukz_conf['forum_id'];
				$conf_forumd_id = $aukz_conf['forumd_id'];
				$conf_adm_id = $aukz_conf['adm_id'];
				$conf_double_id = $aukz_conf['double_id'];
				$conf_user_go = $aukz_conf['user_go'];
				$conf_user_edit = $aukz_conf['user_edit'];
				$conf_user_delete = $aukz_conf['user_delete'];

				$template->assign_block_vars('page0', array(
					'ACTION0' => $this->u_action, //append_sid( $module_root_path . 'acp/acp_aukz.php?p=0'),
					'L_YES' => $user->lang['pz_yes'],
					'CHK_PAUSE' => ($conf_pause) ? 'checked="checked"' : '',
					'USERS_CAN' => $conf_users,
					'CHK1_PLATA' => ($conf_type_plata==1) ? 'checked="checked"' : '',
					'CHK2_PLATA' => ($conf_type_plata==2) ? 'checked="checked"' : '',
					'CHK1_SHAG' => ($conf_type_price==1) ? 'checked="checked"' : '',
					'CHK0_SHAG' => ($conf_type_price==0) ? 'checked="checked"' : '',
					'PLATA_N' => $conf_plata_nominal,
					'PLATA_P' => $conf_plata_procent,
					'PRICE_2' => $conf_price_2,
					'SHAG_N' => $conf_price_1,
					'SHAG_P' => $conf_price_1p,
					'SROK' => $conf_max_srok,
					'CHK_ANTIS' => ($conf_antis) ? 'checked="checked"' : '',
					'ANTIST' => $conf_antist,
					'CHK_HIST' => ($conf_history_st) ? 'checked="checked"' : '',
					'HIST_DEPTH' => $conf_history_depth,
					'FORUM_ID' => $conf_forum_id,
					'FORUMD_ID' => $conf_forumd_id,
					'ADMIN_ID' => $conf_adm_id,
					'DOUBLE_ID' => $conf_double_id,
					'MONEY_NAME' => $conf_money_name,
					'CHK_USER_GO' => ($conf_user_go) ? 'checked="checked"' : '',
					'CHK_USER_EDIT' => ($conf_user_edit) ? 'checked="checked"' : '',
					'CHK_USER_DELETE' => ($conf_user_delete) ? 'checked="checked"' : '',

					'L_SETT' => $user->lang['AZ_SETT'],
					'L_OFF' => $user->lang['AZ_TURN_OFF'],
					'L_ONLY' => $user->lang['AZ_ONLY_THIS'],
					'L_ONLY2' => $user->lang['AZ_ONLY_IDS'],
					'L_INFULL' => $user->lang['AZ_IN_FULL'],
					'L_PAY' => $user->lang['AZ_COMISS'],
					'L_PAY2' => $user->lang['AZ_COMISS_EXPLAIN'],
					'L_PROC' => $user->lang['AZ_PROC_END'],
					'L_PROCLAST' => $user->lang['AZ_PROC_LAST'],
					'L_LIBO' => $user->lang['AZ_LIBO'],
					'L_MAXSROK' => $user->lang['AZ_MAX_SROK'],
					'L_MAXSROK2' => $user->lang['AZ_MAX_SROK_EXPLAIN'],
					'L_HH' => $user->lang['AZ_HS'],
					'L_DS' => $user->lang['AZ_DS'],
					'L_SHAG' => $user->lang['AZ_MIN_SHAG'],
					'L_SHAG2' => $user->lang['AZ_MIN_SHAG_EXPLAIN'],
					'L_PAY_LEADERS' => $user->lang['AZ_PAY_LEADERS'],
					'L_ANTIS' => $user->lang['AZ_USE_ANTIS'],
					'L_ANTIS2' => $user->lang['AZ_DESC_ANTIS'],
					'L_YES' => $user->lang['AZ_YES'],
					'L_MM' => $user->lang['AZ_MS'],
					'L_REM' => $user->lang['AZ_REM_HIST'],
					'L_REM2' => $user->lang['AZ_ADM_CANV'],
					'L_UTILS' => $user->lang['AZ_UTILS'],
					'L_SAVE' => $user->lang['AZ_SAVE'],
					'L_DEL5' => $user->lang['AZ_DEL_HIST'],
					'L_DEL52' => $user->lang['AZ_DEL_HIST2'],
					'L_DEL_ALL' => $user->lang['AZ_DEL_OLD'],
					'L_HIST_DEPTH' => $user->lang['AZ_HIST_DEPT'],
					'L_HIST_DEPTH2' => $user->lang['AZ_HIST_DEPT_EXPLAIN'],
					'L_FORUM_ID' => $user->lang['AZ_FORUM_ID'],
					'L_FORUM_ID2' => $user->lang['AZ_FORUM_ID_EXPLAIN'],
					'L_FORUMD_ID' => $user->lang['AZ_FORUMD_ID'],
					'L_FORUMD_ID2' => $user->lang['AZ_FORUMD_ID_EXPLAIN'],
					'L_ADMIN_ID' => $user->lang['AZ_ADMIN_ID'],
					'L_ADMIN_ID2' => $user->lang['AZ_ADMIN_ID_EXPLAIN'],
					'L_DOUBLE_ID' => $user->lang['AZ_DOUBLE_ID'],
					'L_DOUBLE_ID2' => $user->lang['AZ_DOUBLE_ID_EXPLAIN'],
					'L_MONEY_NAME' => $user->lang['AZ_MONEY_NAME'],
					'L_MONEY_NAME2' => $user->lang['AZ_MONEY_NAME_EXPLAIN'],
					'L_USER_GO' => $user->lang['AZ_USER_GO'],
					'L_USER_GO2' => $user->lang['AZ_USER_GO_EXPLAIN'],
					'L_USER_EDIT' => $user->lang['AZ_USER_EDIT'],
					'L_USER_EDIT2' => $user->lang['AZ_USER_EDIT_EXPLAIN'],
					'L_USER_DELETE' => $user->lang['AZ_USER_DELETE'],
					'L_USER_DELETE2' => $user->lang['AZ_USER_DELETE_EXPLAIN'],
					'POINTS' => $points_name,
					'FORUM_POINTS' => $forum_points_name,
					'L_CO' => $user->lang['AZ_CO'],
				));
			}
		}

	}
	// Совместимая функция...
	function message_die ( $message_type, $message)
	{
		trigger_error($message);
	}
}
?>