<?php
/**
* @name acp_chat.php
* @package acp cBB Chat
* @version $Id: acp_chat.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

/**
* @package module_install
*/
class acp_chat_info
{
	function module()
	{
		return array(
			'filename'	=> 'acp_chat',
			'title'		=> 'ACP_CAT_CHAT',
			'version'	=> '1.0.1',
			'modes'		=> array(
				'config'	=> array('title' => 'ACP_CHAT_CONFIG', 	'auth' => 'acl_a_chat', 'cat' => array('ACP_CAT_CHAT')),
				'pages'		=> array('title' => 'ACP_CHAT_PAGES', 	'auth' => 'acl_a_chat', 'cat' => array('ACP_CAT_CHAT')),
				'rooms'		=> array('title' => 'ACP_CHAT_ROOMS', 	'auth' => 'acl_a_chat', 'cat' => array('ACP_CAT_CHAT')),
				'texts'		=> array('title' => 'ACP_CHAT_TEXTS', 	'auth' => 'acl_a_chat', 'cat' => array('ACP_CAT_CHAT')),
			),
		);
	}

	function install()
	{
	}

	function uninstall()
	{
	}
}
