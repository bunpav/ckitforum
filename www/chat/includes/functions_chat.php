<?php
/*
* @name functions_chat.php
* @package phpBB3 cBB icChat
* @version $Id: functions_chat.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// @ignore
if(!defined('IN_PHPBB'))
{
	exit;
}

class chat
{
	var $installed			= false;
	var $enabled			= false;
	var $in_custom_page		= false;
	var $disallowed_bbcodes	= array();
	
	var	$path			= 'chat/';
	var $root_path		= '';
	var $time_limit		= 0;
	var $web_path		= '';

	function chat()
	{
		global $config, $phpbb_root_path, $phpEx, $table_prefix;
		
		// "generate_board_url" build incorrect urls in some cases, we force vars to solve this
		$force_server_vars = $config['force_server_vars'];

		if($phpbb_root_path != './')
		{
			$config['force_server_vars'] = true;
		}

		$this->path			= chat_hook::$path;
		$this->root_path	= $phpbb_root_path . $this->path;
		$this->web_path 	= (defined('PHPBB_USE_BOARD_URL_PATH') && PHPBB_USE_BOARD_URL_PATH) ? generate_board_url() . '/' : $phpbb_root_path;
		$this->time_limit	= (!empty($config['chat_timeout']) ? (time() - (intval($config['chat_timeout']) * 60)) : false);
		
		// return config to original value
		$config['force_server_vars'] = $force_server_vars;
		unset($force_server_vars);
		
		if(!empty($config['chat_version']))
		{
			$this->installed 	= true;
			$this->enabled 		= (!empty($config['chat_enabled']) ? true : false);
			
			if($config['chat_disallowed_bbcode'])
			{
				$bitfield = new bitfield($config['chat_disallowed_bbcode']);
				$this->disallowed_bbcodes = $bitfield->get_all_set();

				unset($bitfield);
			}
			
			include($this->root_path . "includes/constants.$phpEx");
		}
	}

	function setup()
	{
		global $user, $template, $config, $auth, $phpbb_root_path, $phpEx;
		
		if($user->data['is_bot'] || !$auth->acl_get('u_chat_view') || defined('ADMIN_START'))
		{
			$this->enabled = false;
		}

		if($this->enabled || defined('ADMIN_START'))
		{
			$template->assign_vars(array(
				'S_CHAT_VERSION'	=> $config['chat_version'],
				'T_CHAT_PATH'		=> $this->web_path . $this->path,
				'U_CHAT'			=> ($this->enabled && $config['chat_page_enabled']) ? append_sid($phpbb_root_path . "chat.$phpEx") : '',
			));
		}
		
		if(!$this->enabled && !isset($_POST['icajx']))
		{
			return false;
		}
		
		$user->add_lang('mods/chat/main');

		if($this->enabled && isset($_POST['icajx']))
		{
			if(!defined('IN_CHAT_AJAX'))
			{
				define('IN_CHAT_AJAX', true);

				if(!class_exists('chat_ajax'))
				{
					include($this->root_path . "includes/functions_ajax.$phpEx");
				}
				
				$chat_ajax = new chat_ajax();
				$chat_ajax->run();
			}

			exit_handler();
		}
	}
	
	function set_custom_page($state = true)
	{
		$this->in_custom_page = $state;
	}
	
	function display(&$hook, $handle)
	{
		global $db, $user, $auth, $config, $template, $phpbb_root_path, $phpEx;

		if($this->enabled && $handle == 'body' && !defined('IN_ERROR_HANDLER') && !defined('IN_CHAT_AJAX'))
		{
			$user_key	= request_var($config['cookie_name'] . '_chat_key', 0, false, true);
			$lastcheck	= request_var($config['cookie_name'] . '_chat_lastcheck', 0, false, true);
			
			// Update session of user when navigate into forum with chat connected
			$time_limit = time() - ((int)$config['chat_refresh'] * 2);

			if($this->enabled && !$this->in_custom_page && $user_key && $lastcheck && $time_limit < $lastcheck)
			{
				$sql = 'UPDATE ' . CHAT_USERS_TABLE . '
					SET user_lastjoin = ' . time() . '
					WHERE user_id = ' . $user->data['user_id'] . '
					AND user_key = ' . $user_key . '
					AND user_online = 1';
				$db->sql_query($sql);
			}

			if(!$this->in_custom_page)
			{
				$current_page	= (($user->page['page_dir']) ? $user->page['page_dir'] . '/' : '') . $user->page['page_name'];
				$page_data 		= $this->obtain_pages($current_page);
				
				$this->enabled = (!empty($page_data) ? true : false);
				
				if(!empty($page_data['page_data']['forum_ids']) && !in_array(request_var('f', 0), $page_data['page_data']['forum_ids']))
				{
					$this->enabled = false;
				}
			}
			
			if($this->enabled)
			{
				$user->add_lang('posting');

				$texts 			= $this->obtain_texts();
				$options_file 	= $this->web_path . 'styles/' . $user->theme['template_path'] . '/template/chat/jchat-options.js';
				$bbcode_status	= ($config['allow_bbcode'] && $config['chat_allow_bbcode']) ? true : false;
				$smilies_status	= ($config['allow_smilies']) ? true : false;
	
				if(($md5_file = @md5_file($options_file)) !== false)
				{
					$options_file .= '?v=' . $config['chat_version'] . '&amp;hsh=' . substr($md5_file, 0, 16);
				}
				else
				{
					$options_file = '';
				}

				$template->assign_vars(array(
					'S_CHAT_ENABLED'		=> true,
					'S_CHAT_RULES'			=> (!empty($texts[CHAT_TEXT_RULE]) ? true : false),

					'S_CHAT_ALLOW_PM'			=> $config['chat_allow_pm'],
					'S_CHAT_AUTOCONNECT'		=> $config['chat_autoconnect'],
					'S_CHAT_CAN_CLEAR'			=> $auth->acl_get('m_chat_delete'),
					'S_CHAT_CAN_POST'			=> $auth->acl_get('u_chat_post'),
					'S_CHAT_CURRENT_PAGE'		=> ($this->in_custom_page) ? 'custom' : $page_data['page_alias'],
					'S_CHAT_DEBUG'				=> (defined('DEBUG') && defined('DEBUG_EXTRA') && $auth->acl_get('a_')) ? true : false,
					'S_CHAT_DIRECTION'			=> $config['chat_direction'],
					'S_CHAT_FLOOD_TIME'			=> ($config['chat_flood_time'] && !$auth->acl_get('u_chat_ignoreflood')) ? $config['chat_flood_time'] * 1000 : 0,
					'S_CHAT_GUEST_ROOM'			=> CHAT_GUEST_ROOM,
					'S_CHAT_HEIGHT'				=> (!$this->in_custom_page && $page_data['chat_height']) ? $page_data['chat_height'] : $config['chat_height'],
					'S_CHAT_MAX_CHARS'			=> $config['chat_max_chars'],
					'S_CHAT_MAX_FONT_SIZE'		=> (int)$config['max_post_font_size'],
					'S_CHAT_MAX_ROWS'			=> $config['chat_max_rows'],
					'S_CHAT_OPTIONS_FILE'		=> $options_file,
					'S_CHAT_PLAYER_PATH'		=> $this->web_path . $this->path . 'sounds/',
					'S_CHAT_POSITION'			=> (($this->in_custom_page) ? '' : $page_data['chat_position']),
					'S_CHAT_REFRESH'			=> $config['chat_refresh'] * 1000,
					
					'S_CHAT_BBCODE_ALLOWED'		=> $bbcode_status,
					'S_CHAT_BBCODE_COLOR'		=> ($bbcode_status && !in_array(6, $this->disallowed_bbcodes)) ? true : false,
					'S_CHAT_BBCODE_INSELECT'	=> ($config['chat_bbcode_format'] == 'select') ? true : false,
					'S_CHAT_SMILIES_ALLOWED'	=> $smilies_status,
					'S_CHAT_SOUND_ENABLED'		=> $config['chat_sound'],

					'S_CHAT_HIDDEN_FIELDS'	=> build_hidden_fields(array(
						'last_update'	=> 0,
					)),

					'U_CHAT_ACTION'		=> append_sid($phpbb_root_path . $user->page['page'], false, false),
					'U_CHAT_ARCHIVE'	=> ($auth->acl_get('u_chat_archive') ? append_sid($phpbb_root_path . "chat.$phpEx?mode=archive") : ''),
					'U_CHAT_PLAYER'		=> $this->web_path . $this->path . 'assets/player.swf?v=' . $config['chat_version'],
				));
				
				// Generate notices and tips
				foreach(array(CHAT_TEXT_NOTICE => 'notice',	CHAT_TEXT_TIP => 'tip') as $type => $id)
				{
					if(!empty($texts[$type]))
					{
						$tpl_id = 'S_' .strtoupper($id);

						foreach($texts[$type] as $text)
						{
							$template->assign_block_vars("chat_$id", array($tpl_id => $text));
						}
					}
				}
				
				// Generate smilies
				if($smilies_status)
				{
					$this->generate_smilies();
				}
				
				// Generate bbcodes
				if($bbcode_status)
				{
					$this->generate_bbcodes();
				}
			}
		}
	}
	
	function generate_bbcodes()
	{
		global $db, $user, $config, $template;

		// Default BBcodes
		$default_bbcodes = array(
			'b'			=> array('bbcode_id'	=> 1,	'help' => 'BBCODE_B_HELP',			'name' => 'bold'),
			'i'			=> array('bbcode_id'	=> 2,	'help' => 'BBCODE_I_HELP',			'name' => 'italic'),
			'u'			=> array('bbcode_id'	=> 7,	'help' => 'BBCODE_U_HELP',			'name' => 'underline'),
			'quote'		=> array('bbcode_id'	=> 0,	'help' => 'BBCODE_Q_HELP',			'name' => 'quote'),
			'code'		=> array('bbcode_id'	=> 8,	'help' => 'BBCODE_C_HELP',			'name' => 'code'),
			'list'		=> array('bbcode_id'	=> 9,	'help' => 'BBCODE_L_HELP',			'name' => 'list'),
			'list=1'	=> array('bbcode_id'	=> 9,	'help' => 'BBCODE_O_HELP',			'name' => 'list-numeric'),
			'*'			=> array('bbcode_id'	=> 9,	'help' => 'BBCODE_LISTITEM_HELP',	'name' => 'list-item'),
			'img'		=> array('bbcode_id'	=> 4,	'help' => 'BBCODE_P_HELP',			'name' => 'img'),
			'url'		=> array('bbcode_id'	=> 3,	'help' => 'BBCODE_W_HELP',			'name' => 'url'),
			'flash'		=> array('bbcode_id'	=> 11,	'help' => 'BBCODE_D_HELP',			'name' => 'flash'),
			'size'		=> array('bbcode_id'	=> 5,	'help' => '',						'name' => 'size'),
		);

		foreach($default_bbcodes as $bbcode_tag => $row)
		{
			if(!in_array($row['bbcode_id'], $this->disallowed_bbcodes))
			{
				$template->assign_block_vars('chat_default_tags', array(
					'S_HELP'	=> ($row['help']) ? $user->lang[$row['help']] : '',
					'S_NAME'	=> $row['name'],
					'S_TAG'		=> $bbcode_tag,
				));
			}
		}
		
		// Custom BBcodes
		$sql = 'SELECT bbcode_id, bbcode_tag, bbcode_helpline
			FROM ' . BBCODES_TABLE . '
			WHERE display_on_posting = 1
			ORDER BY bbcode_tag';
		$result = $db->sql_query($sql, 3600);
		while($row = $db->sql_fetchrow($result))
		{
			if(in_array($row['bbcode_id'], $this->disallowed_bbcodes))
			{
				continue;
			}

			// If the helpline is defined within the language file, we will use the localised version, else just use the database entry...
			if(isset($user->lang[strtoupper($row['bbcode_helpline'])]))
			{
				$row['bbcode_helpline'] = $user->lang[strtoupper($row['bbcode_helpline'])];
			}
			
			$bbcode_name	= trim(str_replace('=', '-', $row['bbcode_tag']), '-');
			$imagename		= 'images/icons/bbcode-' . $bbcode_name . '.png';

			$template->assign_block_vars('chat_custom_tags', array(
				'S_HELP'	=> $row['bbcode_helpline'],
				'S_IMAGE'	=> (file_exists($this->root_path . $imagename) ? $this->web_path . $this->path . $imagename : ''),
				'S_NAME'	=> $bbcode_name,
				'S_TAG'		=> $row['bbcode_tag'],
			));
		}
		$db->sql_freeresult($result);
	}
	
	function generate_smilies()
	{
		global $db, $template, $config;
		
		$smilies = array();

		$sql = 'SELECT *
			FROM ' . SMILIES_TABLE . '
			ORDER BY smiley_order';
		$result = $db->sql_query($sql, 3600);
		while($row = $db->sql_fetchrow($result))
		{
			if(empty($smilies[$row['smiley_url']]))
			{
				$smilies[$row['smiley_url']] = $row;
			}
		}
		$db->sql_freeresult($result);

		foreach($smilies as $row)
		{
			$template->assign_block_vars('chat_smiley', array(
				'S_CODE'	=> $row['code'],
				'A_CODE'	=> addslashes($row['code']),
				'S_IMG'		=> $this->web_path . $config['smilies_path'] . '/' . $row['smiley_url'],
				'S_WIDTH'	=> $row['smiley_width'],
				'S_HEIGHT'	=> $row['smiley_height'],
				'S_DESC'	=> $row['emotion'])
			);
		}
	}
	
	function obtain_pages($current_page = false)
	{
		global $db, $cache;
		
		if(($page_cache = $cache->get('_chat_pages')) === false)
		{
			$page_cache = array();

			$sql = 'SELECT *
				FROM ' . CHAT_PAGES_TABLE;
			$result = $db->sql_query($sql);
			while($row = $db->sql_fetchrow($result))
			{
				$row_id = $row['page_path'] . $row['page_filename'];
				
				$row['page_data'] = ($row['page_data']) ? @unserialize($row['page_data']) : array();
				
				$page_cache[$row_id] = $row;
			}
			$db->sql_freeresult($result);
			
			$cache->put('_chat_pages', $page_cache);
		}
		
		if($current_page)
		{
			return (isset($page_cache[$current_page]) ? $page_cache[$current_page] : array());
		}
		
		return $page_cache;
	}
	
	function obtain_rooms()
	{
		global $db, $cache;
		
		if(($room_cache = $cache->get('_chat_rooms')) === false)
		{
			$room_cache = array();

			$sql = 'SELECT *
				FROM ' . CHAT_ROOMS_TABLE . '
				ORDER BY room_order';
			$result = $db->sql_query($sql);
			while($row = $db->sql_fetchrow($result))
			{
				$row['room_data'] = @unserialize($row['room_data']);
				
				if(!isset($row['room_data']['groups']))
				{
					$row['room_data']['groups'] = array();
				}
				
				if(!isset($row['room_data']['users']))
				{
					$row['room_data']['users'] = array();
				}
				
				$room_cache[$row['room_key']] = $row;
			}
			$db->sql_freeresult($result);
			
			$cache->put('_chat_rooms', $room_cache);
		}

		return $room_cache;
	}
	
	function obtain_texts($output_type = false, $translate = true)
	{
		global $db, $cache, $user, $phpbb_root_path, $phpEx;
		
		if(($text_cache = $cache->get('_chat_texts')) === false)
		{
			$text_cache = array();
			$bbcode_bitfield = '';
			
			$sql = 'SELECT text_type, text_content, bbcode_uid, bbcode_bitfield
				FROM ' . CHAT_TEXTS_TABLE . '
				ORDER BY text_order ASC';
			$result = $db->sql_query($sql);
			while($row = $db->sql_fetchrow($result))
			{
				$text_type = (int)$row['text_type'];
				
				if(!isset($text_cache[$text_type]))
				{
					$text_cache[$text_type] = array();
				}
				
				$text_cache[$text_type][] = $row;
				
				// Define the global bbcode bitfield, will be used to load bbcodes
				$bbcode_bitfield = $bbcode_bitfield | base64_decode($row['bbcode_bitfield']);
			}
			$db->sql_freeresult($result);
			
			// Instantiate BBCode if needed
			if($bbcode_bitfield !== '')
			{
				if(!class_exists('bbcode'))
				{
					include($phpbb_root_path . "includes/bbcode.$phpEx");
				}

				$bbcode = new bbcode(base64_encode($bbcode_bitfield));
			}
				
			foreach($text_cache as $text_type => $rows)
			{
				foreach($rows as $row_id => $row)
				{
					// Second parse bbcode here
					if($row['bbcode_bitfield'])
					{
						$bbcode->bbcode_second_pass($row['text_content'], $row['bbcode_uid'], $row['bbcode_bitfield']);
					}
					else
					{
						$row['text_content'] = htmlspecialchars_decode($row['text_content']);
					}
					
					$row['text_content'] = bbcode_nl2br($row['text_content']);
					
					// Return and save simple html output
					$text_cache[$text_type][$row_id] = $row['text_content'];
				}
			}
			
			$cache->put('_chat_texts', $text_cache);
		}
		
		foreach($text_cache as $text_type => $rows)
		{
			foreach($rows as $row_id => $text)
			{
				if($translate && strpos($text, '{') !== false)
				{
					$text = preg_replace('#\{L_([A-Z0-9_\-]+)\}#e', "isset(\$user->lang['\$1']) ? \$user->lang['\$1'] : '\$1'", $text);
				}
				
				$text = smiley_text($text);
				
				$text_cache[$text_type][$row_id] = $text;
			}
		}
		
		if($output_type)
		{
			return (isset($text_cache[$output_type]) ? $text_cache[$output_type] : array());
		}

		return $text_cache;
	}
}
