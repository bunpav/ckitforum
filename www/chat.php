<?php
/*
* @name chat.php
* @package phpBB3 Chat
* @version $Id: chat.php,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// @ignore
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();

$mode		= request_var('mode', '');
$is_embed 	= request_var('embed', false);
$no_title	= request_var('notitle', false);

$messages_per_page = 50;

if(empty($chat->enabled) && !$is_embed)
{
	redirect(append_sid("{$phpbb_root_path}index.$phpEx"));
}

switch($mode)
{
	case 'archive':
		if(!$auth->acl_get('u_chat_archive'))
		{
			trigger_error('NOT_AUTHORISED', E_USER_WARNING);
		}
		
		$default_sort_days	= 0;
		$default_sort_key	= 't';
		$default_sort_dir	= 'd';

		$sort_days	= request_var('st', $default_sort_days);
		$sort_key	= request_var('sk', $default_sort_key);
		$sort_dir	= request_var('sd', $default_sort_dir);
		
		$action = request_var('action', '');
		$action = (isset($_POST['delete_selected']) ? 'delete_selected' : $action);

		$start 		= request_var('start', 0);
		$u_archive 	= append_sid($phpbb_root_path . "chat.$phpEx", 'mode=archive');
		
		// Obtain rooms
		$room_ary		= $chat->obtain_rooms();
		$current_room	= request_var('room', CHAT_GUEST_ROOM);

		if(!isset($room_ary[$current_room]))
		{
			redirect($u_archive);
		}
		
		switch($action)
		{
			case 'delete_selected':
				if(!$auth->acl_get('m_chat_delete'))
				{
					trigger_error('NOT_AUTHORISED', E_USER_WARNING);
				}

				$message_ids = request_var('row_id_list', array(0));
				
				if(sizeof($message_ids))
				{
					$sql = 'DELETE FROM ' . CHAT_MESSAGES_TABLE . '
						WHERE ' . $db->sql_in_set('message_id', $message_ids);
					$db->sql_query($sql);
				}
			break;
		}
	
		if(!function_exists('get_user_avatar'))
		{
			include($phpbb_root_path. "includes/functions_display.$phpEx");
		}
		
		if(!class_exists('chat_ajax'))
		{
			include($chat->root_path . "includes/functions_ajax.$phpEx");
		}
		
		if(!class_exists('bbcode'))
		{
			include($phpbb_root_path . "includes/bbcode.$phpEx");
		}

		$bbcode 	= new bbcode();
		$chat_ajax 	= new chat_ajax(true);
		
		$limit_days = array(0 => $user->lang['ALL_MESSAGES'], 1 => $user->lang['1_DAY'], 7 => $user->lang['7_DAYS'], 14 => $user->lang['2_WEEKS'], 30 => $user->lang['1_MONTH'], 90 => $user->lang['3_MONTHS'], 180 => $user->lang['6_MONTHS'], 365 => $user->lang['1_YEAR']);

		$sort_by_text	= array('t' => $user->lang['POST_TIME']);
		$sort_by_sql	= array('t' => 'cm.message_time');

		$s_limit_days = $s_sort_key = $s_sort_dir = $u_sort_param = '';
		gen_sort_selects($limit_days, $sort_by_text, $sort_days, $sort_key, $sort_dir, $s_limit_days, $s_sort_key, $s_sort_dir, $u_sort_param, $default_sort_days, $default_sort_key, $default_sort_dir);
		
		$sql_sort_order = 'ORDER BY ' . $sort_by_sql[$sort_key] . ' ' . (($sort_dir == 'd') ? 'DESC' : 'ASC');
		
		$min_post_time = ($sort_days) ? time() - ($sort_days * 86400) : 0;
		
		// Get total messages
		$sql = 'SELECT COUNT(message_id) AS total
			FROM ' . CHAT_MESSAGES_TABLE . '
			WHERE dest_key = ' . $current_room . "
			AND message_time >= $min_post_time";
		$result = $db->sql_query($sql);
		$total_messages = (int)$db->sql_fetchfield('total');
		$db->sql_freeresult($result);
	
		$sql = 'SELECT cm.*, u.username, u.user_colour,
				u.user_avatar, u.user_avatar_type, u.user_avatar_width, u.user_avatar_height
			FROM ' . CHAT_MESSAGES_TABLE . ' cm
				LEFT JOIN ' . USERS_TABLE . ' u ON(u.user_id = cm.poster_id)
			WHERE cm.dest_key = ' . $current_room . "
			AND message_time >= $min_post_time
			$sql_sort_order";
		$result = $db->sql_query_limit($sql, $messages_per_page, $start);
		while($row = $db->sql_fetchrow($result))
		{
			$row_avatar = '';
			$message 	= censor_text($row['message_text']);
			
			// Second parse bbcode here
			if($row['bbcode_bitfield'])
			{
				$bbcode->bbcode_second_pass($message, $row['bbcode_uid'], $row['bbcode_bitfield']);
			}

			$message = bbcode_nl2br($message);
			$message = smiley_text($message);
			
			if($config['chat_show_avatars'])
			{
				if($row['poster_id'] != ANONYMOUS)
				{
					$row_avatar = get_user_avatar($row['user_avatar'], $row['user_avatar_type'], $row['user_avatar_width'], $row['user_avatar_height']);
				}
				
				if(!$row_avatar)
				{
					$row_avatar = '<img src="' . $chat->web_path . $chat->path . 'images/no-avatar.gif" alt="" />';
				}
			}

			$template->assign_block_vars('message', array(
				'S_ID'				=> $row['message_id'],
				'S_AUTHOR_AVATAR'	=> $row_avatar,
				'S_AUTHOR_FULL'		=> ($row['poster_id'] == ANONYMOUS) ? $row['poster_username'] : get_username_string('full', $row['poster_id'], $row['username'], $row['user_colour']),
				'S_TEXT'			=> $message,
				'S_DATE'			=> $user->format_date($row['message_time']),
				
				'CAN_DELETE'		=> $chat_ajax->auth('delete', $row),
				'CAN_EDIT'			=> $chat_ajax->auth('edit', $row),
				'CAN_VIEW_WHOIS'	=> $auth->acl_get('a_'),
			));
		}
		$db->sql_freeresult($result);

		$u_pagination	= append_sid($phpbb_root_path . "chat.$phpEx", 'mode=archive' . (($current_room != CHAT_GUEST_ROOM) ? '&amp;room=' . $current_room : '') . ((strlen($u_sort_param)) ? "&amp;$u_sort_param" : ''));
	
		$template->assign_vars(array(
			'S_CHAT_IN_ARCHIVE'		=> true,
			'S_CHAT_MAX_CHARS'		=> $config['chat_max_chars'],
			
			'S_SELECT_SORT_DIR'		=> str_replace('<select', '<select class="chat-inputbox"', $s_sort_dir),
			'S_SELECT_SORT_KEY'		=> str_replace('<select', '<select class="chat-inputbox"', $s_sort_key),
			'S_SELECT_SORT_DAYS'	=> str_replace('<select', '<select class="chat-inputbox"', $s_limit_days),
			
			'PAGINATION'		=> generate_pagination($u_pagination, $total_messages, $messages_per_page, $start),
			'PAGE_NUMBER'		=> on_page($total_messages, $messages_per_page, $start),
		
			'U_ACTION'			=> $u_archive,
		));
		
		// Obtain room tabs
		foreach($room_ary as $room_key => $row)
		{
			if($room_key != CHAT_GUEST_ROOM &&
				((!in_array($user->data['group_id'], $row['room_data']['groups']) && !isset($row['room_data']['users'][$user->data['user_id']]))
				|| !$row['room_enabled']))
			{
				unset($room_ary[$room_key]);
			}
		}
		
		// Only show the room tabs if exists more than one
		if(sizeof($room_ary) > 1)
		{
			foreach($room_ary as $room_key => $row)
			{
				$template->assign_block_vars('roomtab', array(
					'S_ACTIVE'		=> ($room_key == $current_room) ? true : false,
					'S_TITLE'		=> (isset($user->lang[$row['room_title']]) ? $user->lang[$row['room_title']] : $row['room_title']),
					'U_ACTION'		=> append_sid($phpbb_root_path . "chat.$phpEx", 'mode=archive' . (($room_key != CHAT_GUEST_ROOM) ? '&amp;room=' . $room_key : '')),
				));
			}
		}
		
		$template->assign_block_vars('navlinks', array(
			'FORUM_NAME'	=> $user->lang['CHAT_ARCHIVE'],
			'U_VIEW_FORUM'	=> $u_archive,
		));	
		
		page_header($user->lang['CHAT_ARCHIVE']);

		$template->set_filenames(array(
			'body' => 'chat/archive_body.html'
		));
	break;
	
	default:
		$chat->set_custom_page();
		
		if(!$config['chat_page_enabled'] && !$is_embed)
		{
			redirect(append_sid($phpbb_root_path . "index.$phpEx"));
		}
		
		$template->assign_vars(array(
			'S_CHAT_EMBEDMODE'	=> $is_embed,
			'S_CHAT_NOTITLE'	=> ($is_embed && $no_title) ? true : false,
		));
		
		$template->assign_block_vars('navlinks', array(
			'FORUM_NAME'	=> $user->lang['CHAT'],
			'U_VIEW_FORUM'	=> append_sid("{$phpbb_root_path}chat.$phpEx")
		));	
		
		page_header($user->lang['CHAT']);

		$template->set_filenames(array(
			'body' => 'chat/chat_body.html'
		));
	break;
}

page_footer();

?>