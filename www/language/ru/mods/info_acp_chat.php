<?php
/** 
 * info_acp_chat.php [Russian [Ru]]
 * @package language cBB Chat
 * @version $Id: info_acp_chat.php v1.0.1 10/09/2014 $
 *
 * @copyright (c) 2014 CaniDev
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_CAT_CHAT'			=> 'Чат',
	
	'ACP_CHAT_CONFIG'		=> 'Основные настройки',
	'ACP_CHAT_PAGES'		=> 'Страницы с чатом',
	'ACP_CHAT_ROOMS'		=> 'Управление комнатами',
	'ACP_CHAT_TEXTS'		=> 'Правила и объявления',

	'LOG_CHAT_CONFIG'		=> 'Изменены основные настройки чата',
	'LOG_CHAT_ROOM_REMOVED'		=> 'Комната <em>%s</em> чата и все сообщения данной комнаты удалены',
));
