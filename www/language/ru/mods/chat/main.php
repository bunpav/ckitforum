<?php
/** 
 * main.php [Russian [Ru]]
 * @package language cBB Chat
 * @version $Id: main.php v1.0.1 10/09/2014 $
 *
 * @copyright (c) 2014 CaniDev
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

// Chat language
$lang = array_merge($lang, array(
	'CHAT'				=> 'Чат',
	'CHAT_ARCHIVE'			=> 'Архив чата',
	'CHAT_BAN_DATE'			=> 'Дата окончания бана',
	'CHAT_BAN_DATE_EXPLAIN'		=> 'Введите дату в формате ДД/ММ/ГГ чч:мм:сс.',
	'CHAT_BAN_PERIOD'		=> 'Период бана',
	'CHAT_BAN_USER'			=> 'Забанить',
	'CHAT_BBCODE'			=> 'ББ-коды',
	'CHAT_CANCEL'			=> 'Отмена',
	'CHAT_CLEAR_BBCODE'		=> 'Удалить ББ-коды',
	'CHAT_CLEAR_INPUT'		=> 'Стереть напечатанное',
	'CHAT_COLOR'			=> 'Цвет текста',
	'CHAT_CONNECT'			=> 'Соединить / Разъединить',
	'CHAT_CUSTOM_BBCODES'		=> 'Используемые ББ-коды',
	'CHAT_DELETE_SELECTED'		=> 'Удалить выбранные',
	'CHAT_DELETE_USER'		=> 'Удалить пользователя',
	'CHAT_DISCONNECT_MESSAGE'	=> 'Вы отсоединены от чата.',
	'CHAT_FLOOD_WAIT'		=> 'Сработала защита от флуда. Пожалуйста, подождите…',
	'CHAT_GUEST_ROOM'		=> 'Основная',
	'CHAT_MENTION'			=> 'Обратиться',
	'CHAT_NO_INFORMATION'		=> 'Нет информации.',
	'CHAT_PM_EXCEED'		=> 'Вы не можете больше открывать приватные комнаты из-за ограничения на максимальное количество приватов, установленное администратором форума.',
	'CHAT_PURGE'			=> 'Очистить сообщения',
	'CHAT_PURGE_CONFIRM'		=> 'Вы уверены, что хотите удалить все сообщения из чата?',
	'CHAT_REFRESH'			=> 'Обновить',
	'CHAT_REMAINING_CHARS'		=> 'символов осталось.',
	'CHAT_RULES'			=> 'Правила чата',
	'CHAT_SEND_PM'			=> 'Приватное сообщение',
	'CHAT_SMILIES'			=> 'Смайлики',
	'CHAT_SOUND'			=> 'Включить / Выключить звук',
	'CHAT_SUBMIT'			=> 'Отправить',
	'CHAT_TIP'			=> 'Подсказка',
	'CHAT_UNBAN_USER'		=> 'Разбанить',
	'CHAT_USER_BANNED_PERIOD'	=> 'Вас забанили в чате до: %s',
	'CHAT_USER_BANNED_PERMANENT'	=> 'Вас навсегда забанили в чате.',
	'CHAT_USER_ALREADY_EXISTS'	=> 'Такой пользователь уже присутствует в чате.',
	
	'CHAT_STATUS_AVAILABLE'		=> 'Доступен',
	'CHAT_STATUS_AWAY'		=> 'Отошел',
	'CHAT_STATUS_BUSY'		=> 'Занят',
	'CHAT_STATUS_HIDDEN'		=> 'Скрыт',

	'CUSTOM_DATE'			=> 'Текущая дата',
	'EDIT'				=> 'Редактировать',
	'NEW_TOPIC'			=> 'Новая тема',
	'PERMANENT'			=> 'Постоянная',
	'QUOTE'				=> 'Цитировать',
	
	'HALF_AN_HOUR'			=> 'Полчаса',
	'NO_LIMIT'			=> 'Без ограничений',
	'ONE_DAY'			=> 'Один день',
	'ONE_HOUR'			=> 'Один час',
	'ONE_WEEK'			=> 'Одна неделя',
	'ONE_MONTH'			=> 'Один месяц',
	'ONE_YEAR'			=> 'Один год',
	'TWO_YEARS'			=> 'Два года',
));

// Text language
$lang = array_merge($lang, array());
