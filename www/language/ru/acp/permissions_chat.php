<?php
/** 
 * permissions_chat.php [Russian [Ru]]
 * @package language cBB Chat
 * @version $Id: permissions_chat.php v1.0.1 10/09/2014 $
 *
 * @copyright (c) 2014 CaniDev
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang['permission_cat']['chat'] = 'Chat';

$lang = array_merge($lang, array(
	'acl_a_chat'			=> array('lang'	=> 'Может управлять чатом',				'cat'	=> 'misc'),

	'acl_m_chat_delete'		=> array('lang'	=> 'Может удалять сообщения в чате',			'cat'	=> 'chat'),
	'acl_m_chat_edit'		=> array('lang'	=> 'Может редактировать сообщения в чате',		'cat'	=> 'chat'),

	'acl_u_chat_archive'		=> array('lang'	=> 'Может просматривать архив чата',			'cat'	=> 'chat'),
	'acl_u_chat_delete'		=> array('lang'	=> 'Может удалять свои сообщения в чате',		'cat'	=> 'chat'),
	'acl_u_chat_edit'		=> array('lang'	=> 'Может редактировать свои сообщения в чате',		'cat'	=> 'chat'),
	'acl_u_chat_ignoreflood'	=> array('lang'	=> 'Может игнорировать ограничение на флуд',		'cat'	=> 'chat'),
	'acl_u_chat_post'		=> array('lang'	=> 'Может отправлять сообщения в чате',			'cat'	=> 'chat'),
	'acl_u_chat_sendpm'		=> array('lang'	=> 'Может отправлять приватные сообщения в чате',	'cat'	=> 'chat'),
	'acl_u_chat_view'		=> array('lang'	=> 'Может видеть блок чата',				'cat'	=> 'chat'),
));
