<?php
/** 
* permissions_chat.php [Spanish [Es]]
* @package language cBB Chat
* @version $Id: permissions_chat.php v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang['permission_cat']['chat'] = 'Chat';

$lang = array_merge($lang, array(
	'acl_a_chat'				=> array('lang'	=> 'Puede administrar el chat',				'cat'	=> 'misc'),

	'acl_m_chat_delete'			=> array('lang'	=> 'Puede borrar mensajes',					'cat'	=> 'chat'),
	'acl_m_chat_edit'			=> array('lang'	=> 'Puede editar mensajes',					'cat'	=> 'chat'),

	'acl_u_chat_archive'		=> array('lang'	=> 'Puede ver el archivo',					'cat'	=> 'chat'),
	'acl_u_chat_delete'			=> array('lang'	=> 'Puede borrar sus mensajes',				'cat'	=> 'chat'),
	'acl_u_chat_edit'			=> array('lang'	=> 'Puede editar sus mensajes',				'cat'	=> 'chat'),
	'acl_u_chat_ignoreflood'	=> array('lang'	=> 'Puede ignorar límite de saturación',	'cat'	=> 'chat'),
	'acl_u_chat_post'			=> array('lang'	=> 'Puede publicar mensajes',				'cat'	=> 'chat'),
	'acl_u_chat_sendpm'			=> array('lang'	=> 'Puede enviar mensajes privados',		'cat'	=> 'chat'),
	'acl_u_chat_view'			=> array('lang'	=> 'Puede ver el chat',						'cat'	=> 'chat'),
));
