<?php
/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
    $lang = array();
}
// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
    'ACP_CLEANTALK'                         => 'CleanTalk',
    'CT_ENABLE'                             => 'Enable MOD',
    'CT_SERVER_URL'                       	=> 'CleanTalk server URL',
    'CT_SERVER_URL_EXPLAIN'               	=> 'Example: http://moderate.cleantalk.ru',
    'CT_AUTH_KEY'                       	=> 'Access key',
    'CT_AUTH_KEY_EXPLAIN'                   => 'To get a new key please register at site <a href="http://cleantalk.org/install/phpbb3?step=2" target="_blank">cleantalk.org</a>',
	'ENABLE_STOPLIST_CHECK'                 => 'Enable stoplist check',
	'ENABLE_STOPLIST_CHECK_EXPLAIN' 		=> 'Messages with offensive language will be sent to premoderation.',
	'CT_ALLOW_LINKS'                        => 'Allow links',
	'CT_ALLOW_LINKS_EXPLAIN'                => 'Allow offtop posts with HTML links, emails, phone and ICQ numbers.',
	'CT_SETTINGS'                           => 'Settings',
	'CT_SETTINGS_FORUM'						=> 'CleanTalk Settings',
    'CT_TITLE'                              => 'CleanTalk. Spam protection',
    'CT_TITLE_EXPLAIN'                      => 'Here you can control the basic settings mode "CleanTalk. Spam protection."',
	'CT_RESPONSE_LANGUAGE'                  => 'Language of service messages',
	'CT_RESPONSE_LANGUAGE_EXPLAIN'          => 'This language will be used to notice boards\'s admins and commentators',
	'CT_MODERATE_GUESTS'                    => 'Moderate Guests',
	'CT_MODERATE_GUESTS_EXPLAIN'            => 'Posts and topics from guests will be test for spam. Spam posts will be send to moderation queue.',
	'CT_MODERATE_NEWLY_REGISTERED'          => 'Moderate Newly Registered Users',
	'CT_MODERATE_NEWLY_REGISTERED_EXPLAIN'  => 'Posts and topics from new users will be test for spam. Spam posts will be send to moderation queue.',
	'CT_MODERATE_REGISTERED'                => 'Moderate Registered Users',
	'CT_NEWUSER'                            => 'Test registration',
	'CT_NEWUSER_EXPLAIN'                    => 'Failed the test will be given a refusal to register with a statement of reasons.',
	'CT_SUCCESSFULLY_INSTALLED'             => 'CleanTalk spam protection successfully installed!',
	'CT_ENABLE_SMS' 		=> 'Confirm registration through SMS code',
	'CT_ENABLE_SMS_EXPLAIN' 				=> 'This option valid only for Russia cell phones numbers (prefix +7). Users with phones from other countries will be sent to manual activation by board\'s administrator.',

));

$lang = array_merge($lang, array(
	'CT_PHONE_NUMBER' 		=> 'Phone number',
	'CT_PHONE_NUMBER_TEXT' 		=> 'For example +7 900 123 45 67. To the phone will be sent activation code.',
	'CT_PHONE_NUMBER_NECESSARILY' 		=> 'Enter your phone.',
	'CT_PHONE_NUMBER_ERROR' 		=> 'The phone number does not correspond to the format +7 900 123 45 67.',
	'CT_SMS_CODE_ERROR' 		=> 'Code format is invalid.',
	'CT_SERVER_NOT_AVAILABLE' 		=> 'The server is not available. Now you get a new SMS.',
	'CT_ENTER_CODE' 		=> 'Enter the code',
	'CT_ENTER_CODE_EXPLAIN' 		=> 'SMS with code will be delivered at your phone for a 1-2 minutes',
	'CT_GO_TO_RIGESTER_FORM' 		=> '← Go back',
	'CT_ERROR'					=> 'Got error: %s while connecting to Cleantalk server %s',
	'CT_USER_DENY'				=> 'User name: %s, email: %s.',
	'CT_CONFIRM_DELETE_USER'	=> 'Are you sure delete user <b>%s</b> with all his posts?',
	'CT_RETURN_MEMBERLIST'		=> '<a href="%s">Return to memberslist</a>',
	'CT_CLOSE_INFO'				=> 'Close this notification',
	'CLEANTALK_INFO'			=> '<a href="http://cleantalk.ru/phpbb3" target="_blank">Spam protection for phpBB</a>',
));


$lang = array_merge($lang, array(
	'CT_PRIVACY_POLICY'			=> '<br />In order to protect the forum from spam your Email, Nickname, IP address, and the message can be sent to the servers anti-spam service <a href="http://cleantalk.org" title="CleanTalk" target="_blank">CleanTalk</a>.'
));

?>
