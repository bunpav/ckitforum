<?php
/** 
* info_acp_chat.php [English [En]]
* @package language cBB Chat
* @version $Id: info_acp_chat.php v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

// DO NOT CHANGE
if(!defined('IN_PHPBB'))
{
	exit;
}

if(empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_CAT_CHAT'			=> 'Chat',
	
	'ACP_CHAT_CONFIG'		=> 'Main configuration',
	'ACP_CHAT_PAGES'		=> 'Manage pages',
	'ACP_CHAT_ROOMS'		=> 'Manage rooms',
	'ACP_CHAT_TEXTS'		=> 'Static texts and Rules',

	'LOG_CHAT_CONFIG'			=> '<strong>cBB Chat:</strong> Changed main configuration',
	'LOG_CHAT_ROOM_REMOVED'		=> '<strong>cBB Chat:</strong> Deleted room <em>%s</em> and their messages',
));
