/*
* @name jchat-options.js
* @package cBB Chat
* @style: subSilver2
* @version $Id: jchat-options.js,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

var jchat_style_options = {
	positions: {
		faq: {
			top		: ['#pagecontent', 'before'],
			bottom	: ['#pagecontent', 'after']
		},
		viewforum: {
			top		: ['#pagecontent', 'before'],
			bottom	: ['.tablebg:has(.breadcrumbs):last', 'before']
		},
		viewtopic: {
			top		: ['#pagecontent', 'before'],
			bottom	: ['.tablebg:has(.breadcrumbs):last', 'before']
		},
		undefined: {
			top		: ['.tablebg:has(.breadcrumbs):first', 'after'],
			bottom	: ['.tablebg:has(.breadcrumbs):last', 'before']
		}
	}
};
