/*
* @name jchat-options.js
* @package cBB Chat
* @style: proSilver
* @version $Id: jchat-options.js,v1.0.1 10/09/2014 $
*
* @copyright (c) 2014 CaniDev
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

var jchat_style_options = {
	positions: {
		faq: {
			top		: ['#faqlinks', 'before'],
			bottom	: ['.panel:last', 'after']
		},
		index: {
			top		: ['.forabg:has(.topiclist):first', 'before'],
			bottom	: ['.forabg:has(.topiclist):last', 'after']
		},
		mcp: {
			top		: ['#tabs', 'before'],
			bottom	: ['#page-body', 'append']
		},
		memberlist: {
			top		: ['h2:first', 'after'],
			bottom	: ['#jumpbox', 'before']
		},
		search: {
			top		: ['h2:first', 'after'],
			bottom	: ['#page-body', 'append']
		},
		ucp: {
			top		: ['#tabs', 'before'],
			bottom	: ['#page-body', 'append']
		},
		viewforum: {
			top		: ['.topic-actions:first', 'before'],
			bottom	: ['.topic-actions:last', 'after']
		},
		viewonline: {
			top		: ['.linklist:first', 'before'],
			bottom	: ['#jumpbox', 'before']
		},
		viewtopic: {
			top		: ['.topic-actions:first', 'before'],
			bottom	: ['.topic-actions:last', 'after']
		},
		undefined: {
			top		: ['#page-body', 'prepend'],
			bottom	: ['#page-body', 'append']
		}
	}
};
